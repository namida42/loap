﻿using LoapCore.Misc;
using System;
using System.Collections.Generic;

namespace LoapCore.Entities
{
    public class EntityInstance
    {
        public readonly LevelEntity LevelEntity;
        public readonly MetaEntity MetaEntity;

        public string Identifier => MetaEntity.Identifier;
        public string UniqueIdentifier = "";

        public Point3D Position => LevelEntity.Position;
        public Point3D Offset => LevelEntity.Offset;
        public int Rotations => LevelEntity.Rotations;
        public EntityDirectionality Directionality => MetaEntity.Directionality;
        public int PairingID => LevelEntity.PairingID;

        public AnimationStyle AnimationStyle => MetaEntity.AnimationStyle;
        public string SoundEffect => MetaEntity.SoundEffect;

        public EntityEffect Effect => MetaEntity.Effect;
        public int TriggerHeight => MetaEntity.TriggerHeight;
        public int Frames => MetaEntity.Frames;
        public int Height => MetaEntity.Height;

        public TrapKillStyle KillStyle => MetaEntity.KillStyle;

        public int Iteration;
        public bool Activated;

        public EntityInstance(MetaEntity metaEntity, LevelEntity baseEntity)
        {
            LevelEntity = baseEntity;
            MetaEntity = metaEntity;
        }

        public static EntityInstance[] CreateInstances(List<LevelEntity> entities, Dictionary<string, MetaEntity> metaEntities)
        {
            var result = new List<EntityInstance>();

            foreach (var entity in entities)
            {
                if (metaEntities.ContainsKey(entity.Identifier))
                    result.Add(new EntityInstance(metaEntities[entity.Identifier], entity));
                else
                    throw new InvalidOperationException("The key " + entity.Identifier + " could not be found in the metaentities.");
            }

            return result.ToArray();
        }
    }
}
