﻿using LoapCore.Misc;

namespace LoapCore.Entities
{
    public class LevelEntity
    {
        public string Identifier = "";
        public Point3D Position;
        public Point3D Offset;
        public int Rotations;
        public int PairingID;
    }
}
