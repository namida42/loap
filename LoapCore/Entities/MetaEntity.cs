﻿using LoapCore.Files;
using LoapCore.Misc;

namespace LoapCore.Entities
{
    public class MetaEntity
    {
        #region File Keywords
        private const string KEY_EFFECT = "EFFECT";
        private const string KEY_FRAMES = "FRAMES";
        private const string KEY_EDITOR_FRAME = "EDITOR_FRAME";
        private const string KEY_HEIGHT = "HEIGHT";
        private const string KEY_ANIMATION_STYLE = "ANIMATION";
        private const string KEY_DIRECTIONALITY = "DIRECTIONAL";
        private const string KEY_OVERLAY_SIDE = "OVERLAY_SIDE";
        private const string KEY_OVERLAY_FRAMES = "OVERLAY_FRAMES";
        private const string KEY_SOUND_EFFECT = "SOUND";

        private const string KEY_TRIGGER_HEIGHT = "TRIGGER_HEIGHT";

        private const string KEY_KILL_STYLE = "KILL_STYLE";
        #endregion

        public readonly string Identifier;
        public EntityEffect Effect;
        public int Frames;
        public int EditorFrame;
        public int Height;
        public AnimationStyle AnimationStyle;
        public EntityDirectionality Directionality;
        public Direction OverlaySide;
        public int OverlayFrames;
        public string SoundEffect = "";

        public int TriggerHeight;

        public TrapKillStyle KillStyle;

        public MetaEntity(string identifier)
        {
            Identifier = identifier;
        }

        public void LoadFromDataItem(DataItem src)
        {
            Effect = src.GetChildValueEnum(KEY_EFFECT, EntityEffect.None);
            Frames = src.GetChildValueInt(KEY_FRAMES, 1);
            EditorFrame = src.GetChildValueInt(KEY_EDITOR_FRAME, 0);
            Height = src.GetChildValueInt(KEY_HEIGHT, 1);
            AnimationStyle = src.GetChildValueEnum(KEY_ANIMATION_STYLE, AnimationStyle.Normal);
            Directionality = src.GetChildValueEnum(KEY_DIRECTIONALITY, EntityDirectionality.None);
            OverlaySide = src.GetChildValueEnum(KEY_OVERLAY_SIDE, default(Direction));
            OverlayFrames = src.GetChildValueInt(KEY_OVERLAY_FRAMES, 0);
            SoundEffect = src.GetChildValue(KEY_SOUND_EFFECT);

            TriggerHeight = src.GetChildValueInt(KEY_TRIGGER_HEIGHT, 1);

            KillStyle = src.GetChildValueEnum(KEY_KILL_STYLE, default(TrapKillStyle));
        }
    }
}
