﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LoapCore.Files
{
    public class DataItem
    {
        public readonly List<DataItem> Children = new List<DataItem>();

        private bool _IsSection;
        public bool IsSection
        {
            get => (_IsSection || Children.Count > 0);
            set => _IsSection = value || Children.Count > 0;
        }

        private string _Identifier = "";
        public string Identifier
        {
            get => _Identifier;
            set => _Identifier = (value == null ? "" : value.Trim().ToUpperInvariant());
        }

        private string _Value = "";
        public string Value
        {
            get => _Value;
            set => _Value = value ?? "";
        }

        public int ValueInt
        {
            get => int.TryParse(_Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out int result) ? result : 0;
            set => _Value = value.ToString();
        }

        public float ValueFloat
        {
            get => float.TryParse(_Value, NumberStyles.Float, CultureInfo.InvariantCulture, out float result) ? result : 0;
            set => _Value = value.ToString();
        }

        public T GetValueEnum<T>(T fallback = default(T)) where T : struct, Enum
        {
            var result = Enum.TryParse(typeof(T), Value, true, out var localResult) ? localResult : fallback;
            if (result == null)
                result = fallback;

            return (T)result;
        }

        public void SetValueEnum<T>(T value) where T : struct, Enum
        {
            _Value = value.ToString();
        }

        public DataItem AddChildSection(string key)
        {
            var newChild = new DataItem() { Identifier = key };
            Children.Add(newChild);
            _IsSection = true;
            newChild.IsSection = true;
            return newChild;
        }

        /// <summary>
        /// Used to seperate single-line entries when writing to file. It is NOT needed to seperate sections (this is automatic).
        /// </summary>
        public void AddNullChild()
        {
            Children.Add(new DataItem());
            _IsSection = true;
        }

        public bool GetChildExists(string key)
        {
            return Children.Count(c => c.Identifier == key.ToUpperInvariant()) > 0;
        }

        public string GetChildValue(string key, string fallback = "")
        {
            var child = GetChild(key);
            if (child == null)
                return fallback;
            else
                return child.Value;
        }

        public void SetChildValue(string key, string value)
        {
            var child = GetOrAddChild(key);
            child.Value = value;
        }

        public void AddChildValue(string key, string value)
        {
            var child = new DataItem() { Identifier = key };
            Children.Add(child);
            child.Value = value;
        }

        public int GetChildValueInt(string key, int fallback = 0)
        {
            var child = GetChild(key);
            if (child == null)
                return fallback;
            else
                return int.TryParse(child.Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out int result) ? result : fallback;
        }

        public void SetChildValueInt(string key, int value)
        {
            var child = GetOrAddChild(key);
            child.ValueInt = value;
        }

        public void AddChildValueInt(string key, int value)
        {
            var child = new DataItem() { Identifier = key };
            Children.Add(child);
            child.ValueInt = value;
        }

        public float GetChildValueFloat(string key, float fallback = 0)
        {
            var child = GetChild(key);
            if (child == null)
                return fallback;
            else
                return float.TryParse(child.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out float result) ? result : fallback;
        }

        public void SetChildValueFloat(string key, float value)
        {
            var child = GetOrAddChild(key);
            child.ValueFloat = value;
        }

        public void AddChildValueFloat(string key, float value)
        {
            var child = new DataItem() { Identifier = key };
            Children.Add(child);
            child.ValueFloat = value;
        }

        public T GetChildValueEnum<T>(string key, T fallback = default(T)) where T : struct, Enum
        {
            var child = GetChild(key);
            if (child == null)
                return fallback;
            else
                return child.GetValueEnum(fallback);
        }

        public void SetChildValueEnum<T>(string key, T value) where T : struct, Enum
        {
            var child = GetOrAddChild(key);
            child.SetValueEnum(value);
        }

        public void AddChildValueEnum<T>(string key, T value) where T : struct, Enum
        {
            var child = new DataItem() { Identifier = key };
            Children.Add(child);
            child.SetValueEnum(value);
        }

        public DataItem? GetChild(string key)
        {
            return Children.LastOrDefault(c => c.Identifier == key.ToUpperInvariant());
        }

        public DataItem GetOrAddChild(string key)
        {
            var child = GetChild(key);

            if (child == null)
            {
                child = new DataItem() { Identifier = key };
                Children.Add(child);
                _IsSection = true;
            }

            return child;
        }

        public IEnumerable<DataItem> GetChildren(string key)
        {
            return Children.Where(c => c.Identifier == key.ToUpperInvariant());
        }
    }
}
