﻿using LoapCore.Constants;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LoapCore.Files
{
    public static class DataItemReader
    {
        public static DataItem LoadFromFile(string filename)
        {
            var lines = File.ReadAllLines(filename);
            return LoadFromStrings(lines);
        }

        public static DataItem LoadFromStream(Stream stream)
        {
            List<string> lines = new List<string>();
            using StreamReader reader = new StreamReader(stream);
            while (!reader.EndOfStream)
                lines.Add(reader.ReadLine() ?? "");

            return LoadFromStrings(lines);
        }

        public static DataItem LoadFromStrings(IEnumerable<string> strings)
        {
            string[] stringsArray = strings as string[] ?? strings.ToArray();

            DataItem result = new DataItem();
            int lineIndex = 0;
            InternalLoadFromStrings(result, stringsArray, ref lineIndex);
            return result;
        }

        private static void InternalLoadFromStrings(DataItem dst, string[] src, ref int index)
        {
            while (index < src.Length)
            {
                string line = src[index].TrimStart();
                index++;

                if (string.IsNullOrWhiteSpace(line))
                    continue;

                if (line[0] == DataItemConstants.FILE_COMMENT_CHAR)
                    continue;

                if (line.TrimEnd().ToUpperInvariant() == DataItemConstants.FILE_END_SECTION)
                    break;

                DataItem newChild = new DataItem();
                dst.Children.Add(newChild);

                if (line[0] == DataItemConstants.FILE_BEGIN_SECTION_CHAR)
                {
                    newChild.Identifier = line[1..];
                    newChild.IsSection = true;
                    InternalLoadFromStrings(newChild, src, ref index);
                }
                else
                {
                    int spacePos = line.IndexOf(' ');

                    if (spacePos < 0)
                        newChild.Identifier = line;
                    else
                    {
                        newChild.Identifier = line[0..spacePos];
                        newChild.Value = line[(spacePos + 1)..];
                    }
                }
            }
        }
    }
}
