﻿using LoapCore.Constants;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LoapCore.Files
{
    public class DataItemWriter
    {
        public static void SaveToFile(string filename, DataItem dataItem)
        {
            File.WriteAllLines(filename, SaveToStrings(dataItem).ToArray());
        }

        public static void SaveToStream(Stream stream, DataItem dataItem)
        {
            using StreamWriter writer = new StreamWriter(stream);
            foreach (var line in SaveToStrings(dataItem))
                writer.WriteLine(line);
        }

        public static List<string> SaveToStrings(DataItem dataItem)
        {
            List<string> result = new List<string>();
            SaveToStringsInternal(result, dataItem, 0);
            return result;
        }

        public static void SaveToStringsInternal(List<string> dst, DataItem src, int indentLevel)
        {
            string prefix = new string(' ', indentLevel * DataItemConstants.FILE_INDENT_SPACES);

            var singleLineChildren = src.Children.Where(c => !c.IsSection);
            var sectionChildren = src.Children.Where(c => c.IsSection);

            foreach (var child in singleLineChildren)
            {
                if (child.Value == "")
                    dst.Add(prefix + child.Identifier);
                else
                    dst.Add(prefix + child.Identifier + " " + child.Value);
            }

            while (dst.Count > 0 && dst[^1].Trim() == "")
                dst.RemoveAt(dst.Count - 1);

            if (singleLineChildren.Any() && sectionChildren.Any())
                dst.Add(prefix);

            int sectionCount = sectionChildren.Count(); // To track the last one, so we don't add an unnecessary blank line. Just looks tidier. :)

            foreach (var child in sectionChildren)
            {
                sectionCount--;

                dst.Add(prefix + DataItemConstants.FILE_BEGIN_SECTION_CHAR + child.Identifier);
                SaveToStringsInternal(dst, child, indentLevel + 1);
                dst.Add(prefix + DataItemConstants.FILE_END_SECTION);

                if (sectionCount > 0)
                    dst.Add("");
            }
        }
    }
}
