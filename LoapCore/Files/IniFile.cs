﻿using System.Collections.Generic;
using System.IO;

namespace LoapCore.Files
{
    public static class IniFile
    {
        public static Dictionary<string, string> ReadIniFile(string filename)
        {
            var result = new Dictionary<string, string>();

            var lines = File.ReadAllLines(filename);
            foreach (var line in lines)
            {
                var split = line.Split('=');
                if (split.Length < 2)
                    continue;

                result.Add(split[0], string.Join('=', split[1..]));
            }

            return result;
        }
    }
}
