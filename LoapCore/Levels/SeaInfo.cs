﻿using LoapCore.Constants;
using LoapCore.Misc;

namespace LoapCore.Levels
{
    public class SeaInfo
    {
        public string Texture = "";
        public int MotionX;
        public int MotionY;

        public LevelFloorEffect Effect;

        public float MotionXFloat => MotionX * PhysicsConstants.STEP_SIZE_FLOAT / PhysicsConstants.ITERATIONS_PER_INGAME_SECOND;
        public float MotionYFloat => MotionY * PhysicsConstants.STEP_SIZE_FLOAT / PhysicsConstants.ITERATIONS_PER_INGAME_SECOND;
    }
}
