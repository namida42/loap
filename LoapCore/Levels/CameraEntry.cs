﻿namespace LoapCore.Levels
{
    public struct CameraEntry
    {
        public float X;
        public float Y;
        public float Z;
        public float Rotation;
        public float Pitch;

        public CameraEntry(float x, float y, float z, float rotation, float pitch)
        {
            X = x;
            Y = y;
            Z = z;
            Rotation = rotation;
            Pitch = pitch;
        }

        public static CameraEntry Default => new CameraEntry(-12f, -12f, 8f, 315f, 5.625f);
    }
}
