﻿using LoapCore.Constants;
using LoapCore.Misc;
using System;
using System.Collections.Generic;

namespace LoapCore.Levels
{
    public class LevelInfo
    {
        public string Title = "";

        public int LemmingCount;
        public int SaveRequirement;
        public int TimeLimit;
        public int SpawnRate;

        public string Music = "";

        public readonly Dictionary<Skill, int> Skillset = new Dictionary<Skill, int>();

        public readonly List<CameraEntry> Cameras = new List<CameraEntry>();

        public readonly SeaInfo Sea = new SeaInfo();
        public readonly SkyInfo Sky = new SkyInfo();

        public readonly PreviewCameraInfo PreviewCamera = new PreviewCameraInfo();
        public bool DisableMinimap;

        public readonly Dictionary<Direction, int> BoundaryPoints = new Dictionary<Direction, int>();

        public LevelInfo()
        {
            foreach (Skill skill in Utils.GetEnumValues<Skill>())
                Skillset.Add(skill, 0);

            foreach (Direction dir in Utils.GetEnumValues<Direction>())
                BoundaryPoints.Add(dir, 0);
        }

        public void Sanitize()
        {
            LemmingCount = Math.Max(LemmingCount, 1);
            SaveRequirement = Math.Min(SaveRequirement, LemmingCount);
            TimeLimit = Math.Clamp(TimeLimit, 0, (99 * 60) + 59);

            SpawnRate = Math.Clamp(SpawnRate, 0, PhysicsConstants.MAX_SPAWN_RATE);

            foreach (Skill skill in Utils.GetEnumValues<Skill>())
                Skillset[skill] = Math.Clamp(Skillset[skill], 0, 99);

            for (int i = 0; i < Cameras.Count; i++)
                Cameras[i] = new CameraEntry(
                    Utils.RoundFloat(Cameras[i].X, DataItemConstants.CAMERA_FLOAT_PRECISION),
                    Utils.RoundFloat(Cameras[i].Y, DataItemConstants.CAMERA_FLOAT_PRECISION),
                    Utils.RoundFloat(Math.Max(Cameras[i].Z, PhysicsConstants.CAMERA_MIN_Z), DataItemConstants.CAMERA_FLOAT_PRECISION),
                    Utils.RoundFloat(Utils.ModuloNegativeAdjusted(Cameras[i].Rotation, 360), DataItemConstants.CAMERA_FLOAT_PRECISION),
                    Utils.RoundFloat(Math.Clamp(Cameras[i].Pitch, -45, 45), DataItemConstants.CAMERA_FLOAT_PRECISION)
                    );

            if (Cameras.Count == 0)
                Cameras.Add(CameraEntry.Default);

            BoundaryPoints[Direction.ZMinus] = 0;
        }
    }
}
