﻿using LoapCore.Misc;
using System.Collections.Generic;
using System.Linq;

namespace LoapCore.Levels
{
    public class LandRegion
    {
        public class RegionVertex
        {
            public int X;
            public int Y;
        }

        public LevelFloorEffect Effect;

        public string Texture = "";
        public int Shading;
        public readonly List<RegionVertex> Vertices = new List<RegionVertex>();
        public List<RegionVertex> TriangulatedVertices
        {
            get
            {
                var result = new List<RegionVertex>();
                for (int i = 1; i < Vertices.Count - 1; i++)
                {
                    result.Add(Vertices[0]);
                    result.Add(Vertices[i]);
                    result.Add(Vertices[i + 1]);
                }

                return result;
            }
        }

        public float MidX => Vertices.Sum(v => v.X) / (float)Vertices.Count;
        public float MidY => Vertices.Sum(v => v.Y) / (float)Vertices.Count;

        public void Sanitize()
        {
            if (Vertices.Count >= 3)
            {
                int keyVertexIndex = 0;
                for (int i = 1; i < Vertices.Count; i++)
                    if (Vertices[i].Y < Vertices[keyVertexIndex].Y ||
                        (Vertices[i].Y == Vertices[keyVertexIndex].Y && Vertices[i].X > Vertices[keyVertexIndex].X))
                        keyVertexIndex = i;

                RegionVertex prevVertex = Vertices[Utils.ModuloNegativeAdjusted(keyVertexIndex - 1, Vertices.Count)];
                RegionVertex thisVertex = Vertices[Utils.ModuloNegativeAdjusted(keyVertexIndex, Vertices.Count)];
                RegionVertex nextVertex = Vertices[Utils.ModuloNegativeAdjusted(keyVertexIndex + 1, Vertices.Count)];

                double cross = ((thisVertex.X * nextVertex.Y) + (prevVertex.X * thisVertex.Y) + (prevVertex.Y * nextVertex.X)) -
                    ((prevVertex.Y * thisVertex.X) + (thisVertex.Y * nextVertex.X) + (prevVertex.X * nextVertex.Y));

                if (cross == 0)
                    Vertices.Clear();
                else if (cross < 0)
                    Vertices.Reverse();
            }
        }

        public bool IsPointInside(int x, int y)
        {
            return Utils.IsPointInsidePolygon(x, y, Vertices.Select(v => (float)v.X), Vertices.Select(v => (float)v.Y), false);
        }
    }
}
