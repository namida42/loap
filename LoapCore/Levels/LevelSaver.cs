﻿using LoapCore.Blocks;
using LoapCore.Constants;
using LoapCore.Entities;
using LoapCore.Files;
using LoapCore.Misc;
using System;
using System.Collections.Generic;

namespace LoapCore.Levels
{
    public static class LevelSaver
    {
        private const int METABLOCK_ENCODE_BASE = 62;

        public static DataItem SaveLevelToDataItem(Level level)
        {
            DataItem result = new DataItem();

            level.Sanitize();

            SaveLevelInfo(level, result);
            SaveLands(level.Lands, result);
            SaveBlockData(level, result);
            SaveOverlays(level.Overlays, result);
            SaveEntities(level.Entities, result);

            return result;
        }

        private static void SaveLevelInfo(Level level, DataItem dest)
        {
            dest.SetChildValue(LevelFileKeys.INFO_TITLE, level.LevelInfo.Title);
            dest.AddNullChild();
            dest.SetChildValueInt(LevelFileKeys.INFO_LEMMING_COUNT, level.LevelInfo.LemmingCount);
            dest.SetChildValueInt(LevelFileKeys.INFO_SAVE_REQUIREMENT, level.LevelInfo.SaveRequirement);
            if (level.LevelInfo.TimeLimit > 0)
                dest.SetChildValueInt(LevelFileKeys.INFO_TIME_LIMIT, level.LevelInfo.TimeLimit);
            dest.SetChildValueInt(LevelFileKeys.INFO_SPAWN_RATE, level.LevelInfo.SpawnRate);

            SaveSkillset(level.LevelInfo.Skillset, dest.AddChildSection(LevelFileKeys.INFO_SKILLSET));

            if (level.LevelInfo.Music != "")
            {
                dest.AddNullChild();
                dest.SetChildValue(LevelFileKeys.INFO_MUSIC, level.LevelInfo.Music);
            }

            if (!level.LevelInfo.Sky.FullHeight)
                SaveSea(level.LevelInfo.Sea, dest.AddChildSection(LevelFileKeys.INFO_SEA));
            SaveSky(level.LevelInfo.Sky, dest.AddChildSection(LevelFileKeys.INFO_SKY));

            foreach (var cam in level.LevelInfo.Cameras)
                SaveCamera(cam, dest.AddChildSection(LevelFileKeys.CAMERA));

            SavePreviewCamera(level.LevelInfo.PreviewCamera, dest.AddChildSection(LevelFileKeys.PREVIEW_CAMERA));

            dest.AddNullChild();

            if (level.LevelInfo.DisableMinimap)
            {
                dest.GetOrAddChild(LevelFileKeys.DISABLE_MINIMAP);
                dest.AddNullChild();
            }

            foreach (Direction dir in Utils.GetEnumValues<Direction>())
                if (dir != Direction.ZMinus)
                    dest.SetChildValueInt(LevelFileKeys.BOUNDARY_BASE + dir.ToString(), level.LevelInfo.BoundaryPoints[dir]);
        }

        private static void SaveSkillset(Dictionary<Skill, int> src, DataItem dst)
        {
            foreach (Skill skill in Utils.GetEnumValues<Skill>())
                if (src[skill] > 0)
                    dst.SetChildValueInt(skill.ToString(), src[skill]);
        }

        private static void SavePreviewCamera(PreviewCameraInfo camera, DataItem dest)
        {
            if (camera.Style != PreviewCameraStyle.Pivot)
                dest.SetChildValueEnum(LevelFileKeys.PREVIEW_CAMERA_STYLE, camera.Style);
            dest.SetChildValueFloat(LevelFileKeys.CAMERA_X, camera.X);
            dest.SetChildValueFloat(LevelFileKeys.CAMERA_Y, camera.Y);
            dest.SetChildValueFloat(LevelFileKeys.CAMERA_Z, camera.Z);
            dest.SetChildValueFloat(LevelFileKeys.CAMERA_ROTATION, camera.Rotation);
            dest.SetChildValueFloat(LevelFileKeys.CAMERA_PITCH, camera.Pitch);
            if (camera.Style == PreviewCameraStyle.Pivot)
                dest.SetChildValueFloat(LevelFileKeys.PREVIEW_CAMERA_DISTANCE, camera.Distance);
        }

        private static void SaveSea(SeaInfo sea, DataItem dest)
        {
            dest.SetChildValue(LevelFileKeys.INFO_SEA_TEXTURE1, sea.Texture);
            if (sea.MotionX != 0 || sea.MotionY != 0)
            {
                dest.SetChildValueInt(LevelFileKeys.INFO_SEA_MOVEMENT_X, sea.MotionX);
                dest.SetChildValueInt(LevelFileKeys.INFO_SEA_MOVEMENT_Y, sea.MotionY);
            }
            if (sea.Effect != LevelFloorEffect.Water)
                dest.SetChildValueEnum(LevelFileKeys.INFO_SEA_EFFECT, sea.Effect);
        }

        private static void SaveSky(SkyInfo sky, DataItem dest)
        {
            dest.SetChildValue(LevelFileKeys.INFO_SKY_TEXTURE1, sky.Texture);
            if (sky.FullHeight)
                dest.GetOrAddChild(LevelFileKeys.INFO_SKY_FULL_HEIGHT);
        }

        private static void SaveLands(List<LandRegion> lands, DataItem dest)
        {
            foreach (var land in lands)
            {
                var thisLandSec = dest.AddChildSection(LevelFileKeys.LAND);
                if (land.Effect != LevelFloorEffect.Solid)
                    thisLandSec.AddChildValueEnum(LevelFileKeys.LAND_EFFECT, land.Effect);
                thisLandSec.AddChildValue(LevelFileKeys.LAND_TEXTURE, land.Texture);
                thisLandSec.AddChildValueInt(LevelFileKeys.LAND_SHADING, land.Shading);
                foreach (var vertex in land.Vertices)
                    thisLandSec.AddChildValue(LevelFileKeys.LAND_VERTEX, vertex.X.ToString() + "," + vertex.Y.ToString());
            }
        }

        private static void SaveCamera(CameraEntry camera, DataItem dest)
        {
            dest.SetChildValueFloat(LevelFileKeys.CAMERA_X, camera.X);
            dest.SetChildValueFloat(LevelFileKeys.CAMERA_Y, camera.Y);
            dest.SetChildValueFloat(LevelFileKeys.CAMERA_Z, camera.Z);
            dest.SetChildValueFloat(LevelFileKeys.CAMERA_ROTATION, camera.Rotation);
            dest.SetChildValueFloat(LevelFileKeys.CAMERA_PITCH, camera.Pitch);
        }

        private static int GetRequiredIdentifierLength(int totalMetablockCount)
        {
            int result = 1;

            while (Math.Pow(METABLOCK_ENCODE_BASE, result) < totalMetablockCount)
                result += 1;

            return result;
        }

        private static string MakeBlockIdentifier(int index, int idLength)
        {
            if (index >= 0)
            {
                int[] digits = new int[idLength];
                for (int i = idLength - 1; i >= 0; i--)
                {
                    digits[i] = index / (int)Math.Pow(METABLOCK_ENCODE_BASE, i);
                    index %= (int)Math.Pow(METABLOCK_ENCODE_BASE, i);
                }

                string result = "";

                for (int i = 0; i < idLength; i++)
                {
                    int srcDigit = digits[idLength - 1 - i];

                    char thisChar;

                    if (srcDigit < 10)
                        thisChar = (char)((byte)'0' + srcDigit);
                    else if (srcDigit < 36)
                        thisChar = (char)((byte)'A' + srcDigit - 10);
                    else
                        thisChar = (char)((byte)'a' + srcDigit - 36);

                    result += thisChar;
                }

                return result;
            }
            else
                return new string('.', idLength);
        }

        private static void SaveBlockData(Level level, DataItem dest)
        {
            int[,,] blockIndexMap = new int[level.BlockStructure.Width, level.BlockStructure.Depth, level.BlockStructure.Height];
            List<BlockInfo> metablocks = new List<BlockInfo>();

            BuildSaveableBlockData(level.BlockStructure, blockIndexMap, metablocks);

            int idLength = GetRequiredIdentifierLength(metablocks.Count);

            SaveBlockStructure(blockIndexMap, dest, idLength);
            SaveMetablocks(metablocks, dest, idLength);
        }

        private static void BuildSaveableBlockData(BlockStructure structure, int[,,] indexMap, List<BlockInfo> metablocks)
        {
            for (int z = 0; z < indexMap.GetLength(2); z++)
                for (int y = 0; y < indexMap.GetLength(1); y++)
                    for (int x = 0; x < indexMap.GetLength(0); x++)
                    {
                        var thisBlock = structure[x, y, z];

                        if (thisBlock == null)
                            indexMap[x, y, z] = -1;
                        else
                        {
                            bool foundMatch = false;

                            for (int i = 0; i < metablocks.Count; i++)
                                if (thisBlock.IsIdentical(metablocks[i]))
                                {
                                    indexMap[x, y, z] = i;
                                    foundMatch = true;
                                    break;
                                }

                            if (!foundMatch)
                            {
                                BlockInfo newBlock = new BlockInfo();
                                newBlock.Clone(thisBlock);
                                metablocks.Add(newBlock);
                                indexMap[x, y, z] = metablocks.Count - 1;
                            }
                        }
                    }
        }

        private static void SaveBlockStructure(int[,,] indexMap, DataItem dest, int idLength)
        {
            dest.AddNullChild();
            dest.SetChildValueInt(LevelFileKeys.LAYOUT_WIDTH, indexMap.GetLength(0));
            dest.SetChildValueInt(LevelFileKeys.LAYOUT_DEPTH, indexMap.GetLength(1));
            dest.SetChildValueInt(LevelFileKeys.LAYOUT_HEIGHT, indexMap.GetLength(2));

            DataItem blockItem = dest.AddChildSection(LevelFileKeys.LAYOUT_STRUCTURE);

            for (int z = 0; z < indexMap.GetLength(2); z++)
            {
                DataItem layerItem = blockItem.AddChildSection(LevelFileKeys.LAYOUT_LAYER);

                for (int y = 0; y < indexMap.GetLength(1); y++)
                {
                    string newLine = "";

                    for (int x = 0; x < indexMap.GetLength(0); x++)
                        newLine += MakeBlockIdentifier(indexMap[x, y, z], idLength);

                    layerItem.AddChildValue(LevelFileKeys.LAYOUT_LINE, newLine);
                }
            }
        }

        private static void SaveMetablocks(List<BlockInfo> metablocks, DataItem dest, int idLength)
        {
            for (int i = 0; i < metablocks.Count; i++)
            {
                var metablock = metablocks[i];
                DataItem metablockSection = dest.AddChildSection(LevelFileKeys.METABLOCK);

                metablockSection.SetChildValue(LevelFileKeys.METABLOCK_IDENTIFIER, MakeBlockIdentifier(i, idLength));
                metablockSection.AddNullChild();

                metablockSection.SetChildValueEnum(LevelFileKeys.METABLOCK_SHAPE, metablock.Shape);
                metablockSection.SetChildValueInt(LevelFileKeys.METABLOCK_SEGMENT, metablock.Segment);
                metablockSection.SetChildValueInt(LevelFileKeys.METABLOCK_ROTATIONS, metablock.Rotations);
                metablockSection.AddNullChild();

                if (metablock.Physics != BlockPhysics.Solid) metablockSection.SetChildValueEnum(LevelFileKeys.METABLOCK_PHYSICS, metablock.Physics);
                if (metablock.OneWayDirection != null) metablockSection.SetChildValueEnum(LevelFileKeys.METABLOCK_ONE_WAY, metablock.OneWayDirection.Value);
                if (metablock.Slippery) metablockSection.GetOrAddChild(LevelFileKeys.METABLOCK_SLIPPERY);
                if (metablock.Antisplat) metablockSection.GetOrAddChild(LevelFileKeys.METABLOCK_ANTISPLAT);
                if (metablock.IgnoredByCamera) metablockSection.GetOrAddChild(LevelFileKeys.METABLOCK_IGNORED_BY_CAMERA);
                if (metablock.Visibility != BlockVisibility.Normal) metablockSection.SetChildValueEnum(LevelFileKeys.METABLOCK_VISIBILITY, metablock.Visibility);

                foreach (Direction dir in Utils.GetEnumValues<Direction>())
                {
                    DataItem faceSection = metablockSection.AddChildSection(LevelFileKeys.METABLOCK_FACE_PREFIX + dir.ToString().ToUpperInvariant());

                    if (metablock.Faces[dir].Physics != FacePhysics.None)
                        faceSection.SetChildValueEnum(LevelFileKeys.METABLOCK_FACE_PHYSICS, metablock.Faces[dir].Physics);

                    foreach (var entry in metablock.Faces[dir].TextureOuter)
                    {
                        var texSec = faceSection.AddChildSection(LevelFileKeys.METABLOCK_FACE_OUTSIDE_TEXTURE);
                        texSec.SetChildValue(LevelFileKeys.METABLOCK_FACE_TEXTURE_FILE, entry.File);
                        texSec.SetChildValueInt(LevelFileKeys.METABLOCK_FACE_TEXTURE_INDEX, entry.Index);
                        if (entry.Flip)
                            texSec.GetOrAddChild(LevelFileKeys.METABLOCK_FACE_FLIP);
                        if (Utils.ModuloNegativeAdjusted(entry.Rotations, 4) > 0)
                            texSec.SetChildValueInt(LevelFileKeys.METABLOCK_FACE_ROTATIONS, Utils.ModuloNegativeAdjusted(entry.Rotations, 4));
                        if (entry.Shading != 0)
                            texSec.SetChildValueInt(LevelFileKeys.METABLOCK_FACE_SHADING, entry.Shading);
                    }

                    foreach (var entry in metablock.Faces[dir].TextureInner)
                    {
                        var texSec = faceSection.AddChildSection(LevelFileKeys.METABLOCK_FACE_INSIDE_TEXTURE);
                        texSec.SetChildValue(LevelFileKeys.METABLOCK_FACE_TEXTURE_FILE, entry.File);
                        texSec.SetChildValueInt(LevelFileKeys.METABLOCK_FACE_TEXTURE_INDEX, entry.Index);
                        if (entry.Flip)
                            texSec.GetOrAddChild(LevelFileKeys.METABLOCK_FACE_FLIP);
                        if (Utils.ModuloNegativeAdjusted(entry.Rotations, 4) > 0)
                            texSec.SetChildValueInt(LevelFileKeys.METABLOCK_FACE_ROTATIONS, Utils.ModuloNegativeAdjusted(entry.Rotations, 4));
                        if (entry.Shading != 0)
                            texSec.SetChildValueInt(LevelFileKeys.METABLOCK_FACE_SHADING, entry.Shading);
                    }
                }
            }
        }

        private static void SaveOverlays(List<LevelOverlay> src, DataItem dest)
        {
            foreach (var overlay in src)
            {
                var overlayItem = dest.AddChildSection(LevelFileKeys.OVERLAY);

                overlayItem.SetChildValueInt(LevelFileKeys.OVERLAY_X, overlay.Position.X);
                overlayItem.SetChildValueInt(LevelFileKeys.OVERLAY_Y, overlay.Position.Y);
                overlayItem.SetChildValueInt(LevelFileKeys.OVERLAY_Z, overlay.Position.Z);
                overlayItem.SetChildValueEnum(LevelFileKeys.OVERLAY_SIDE, overlay.Side);

                if (overlay.FrontTexture.File != "")
                {
                    var frontItem = overlayItem.AddChildSection(LevelFileKeys.OVERLAY_OUTER);
                    frontItem.SetChildValue(LevelFileKeys.OVERLAY_TEXTURE, overlay.FrontTexture.File);
                    frontItem.SetChildValueInt(LevelFileKeys.OVERLAY_TEXTURE_INDEX, overlay.FrontTexture.Index);
                    if (overlay.FrontTexture.Rotations != 0)
                        frontItem.SetChildValueInt(LevelFileKeys.OVERLAY_ROTATIONS, overlay.FrontTexture.Rotations);
                    if (overlay.FrontTexture.Flip)
                        frontItem.GetOrAddChild(LevelFileKeys.OVERLAY_FLIP);
                    if (overlay.FrontTexture.Shading != 0)
                        frontItem.SetChildValueInt(LevelFileKeys.OVERLAY_SHADING, overlay.FrontTexture.Shading);
                }

                if (overlay.BackTexture.File != "")
                {
                    var backItem = overlayItem.AddChildSection(LevelFileKeys.OVERLAY_INNER);
                    backItem.SetChildValue(LevelFileKeys.OVERLAY_TEXTURE, overlay.BackTexture.File);
                    backItem.SetChildValueInt(LevelFileKeys.OVERLAY_TEXTURE_INDEX, overlay.BackTexture.Index);
                    if (overlay.BackTexture.Rotations != 0)
                        backItem.SetChildValueInt(LevelFileKeys.OVERLAY_ROTATIONS, overlay.BackTexture.Rotations);
                    if (overlay.BackTexture.Flip)
                        backItem.GetOrAddChild(LevelFileKeys.OVERLAY_FLIP);
                    if (overlay.BackTexture.Shading != 0)
                        backItem.SetChildValueInt(LevelFileKeys.OVERLAY_SHADING, overlay.BackTexture.Shading);
                }
            }
        }

        private static void SaveEntities(List<LevelEntity> entities, DataItem dest)
        {
            foreach (var entity in entities)
            {
                var thisItem = dest.AddChildSection(LevelFileKeys.ENTITY);

                thisItem.SetChildValue(LevelFileKeys.ENTITY_IDENTIFIER, entity.Identifier);
                thisItem.SetChildValueInt(LevelFileKeys.ENTITY_X, entity.Position.X);
                thisItem.SetChildValueInt(LevelFileKeys.ENTITY_Y, entity.Position.Y);
                thisItem.SetChildValueInt(LevelFileKeys.ENTITY_Z, entity.Position.Z);
                if (entity.Offset.X != 0 || entity.Offset.Y != 0 || entity.Offset.Z != 0)
                {
                    thisItem.SetChildValueInt(LevelFileKeys.ENTITY_OFFSET_X, entity.Offset.X);
                    thisItem.SetChildValueInt(LevelFileKeys.ENTITY_OFFSET_Y, entity.Offset.Y);
                    thisItem.SetChildValueInt(LevelFileKeys.ENTITY_OFFSET_Z, entity.Offset.Z);
                }
                if (entity.Rotations != 0)
                    thisItem.SetChildValueInt(LevelFileKeys.ENTITY_ROTATION, entity.Rotations);
                if (entity.PairingID != 0)
                    thisItem.SetChildValueInt(LevelFileKeys.ENTITY_PAIRING_ID, entity.PairingID);
            }
        }
    }
}
