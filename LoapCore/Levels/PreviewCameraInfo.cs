﻿using LoapCore.Misc;

namespace LoapCore.Levels
{
    public class PreviewCameraInfo
    {
        public PreviewCameraStyle Style;
        public float X;
        public float Y;
        public float Z;
        public float Rotation;
        public float Pitch;

        public float Distance; // for PreviewCameraStyle.Pivot's use

    }
}
