﻿using LoapCore.Blocks;
using LoapCore.Misc;

namespace LoapCore.Levels
{
    public class LevelOverlay
    {
        public BlockTextureRef FrontTexture;
        public BlockTextureRef BackTexture;
        public Point3D Position;
        public Direction Side;

        public LevelOverlay()
        {
            FrontTexture.File = "";
            BackTexture.File = "";
        }
    }
}
