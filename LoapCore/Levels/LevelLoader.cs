﻿using LoapCore.Blocks;
using LoapCore.Constants;
using LoapCore.Entities;
using LoapCore.Files;
using LoapCore.Misc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LoapCore.Levels
{
    public static class LevelLoader
    {
        public static Level LoadLevelFromDataItem(DataItem dataItem)
        {
            Level result = new Level();

            LoadLevelInfo(result, dataItem);
            LoadLands(result, dataItem.GetChildren(LevelFileKeys.LAND));
            LoadBlockData(result, dataItem);
            LoadOverlays(result.Overlays, dataItem.GetChildren(LevelFileKeys.OVERLAY));
            LoadEntities(result.Entities, dataItem.GetChildren(LevelFileKeys.ENTITY));

            return result;
        }

        private static void LoadLevelInfo(Level dest, DataItem mainSection)
        {
            dest.LevelInfo.Title = mainSection.GetChildValue(LevelFileKeys.INFO_TITLE);

            dest.LevelInfo.LemmingCount = mainSection.GetChildValueInt(LevelFileKeys.INFO_LEMMING_COUNT, 1);
            dest.LevelInfo.SaveRequirement = mainSection.GetChildValueInt(LevelFileKeys.INFO_SAVE_REQUIREMENT);
            dest.LevelInfo.TimeLimit = mainSection.GetChildValueInt(LevelFileKeys.INFO_TIME_LIMIT);
            dest.LevelInfo.SpawnRate = mainSection.GetChildValueInt(LevelFileKeys.INFO_SPAWN_RATE);

            LoadSkillsetInfo(dest.LevelInfo.Skillset, mainSection.GetOrAddChild(LevelFileKeys.INFO_SKILLSET));

            dest.LevelInfo.Music = mainSection.GetChildValue(LevelFileKeys.INFO_MUSIC);

            LoadSeaInfo(dest.LevelInfo.Sea, mainSection.GetOrAddChild(LevelFileKeys.INFO_SEA));
            LoadSkyInfo(dest.LevelInfo.Sky, mainSection.GetOrAddChild(LevelFileKeys.INFO_SKY));

            LoadCameraInfo(dest.LevelInfo.Cameras, mainSection.GetChildren(LevelFileKeys.CAMERA));

            LoadPreviewCamera(dest.LevelInfo.PreviewCamera, mainSection.GetOrAddChild(LevelFileKeys.PREVIEW_CAMERA));

            dest.LevelInfo.DisableMinimap = mainSection.GetChildExists(LevelFileKeys.DISABLE_MINIMAP);

            foreach (Direction dir in Utils.GetEnumValues<Direction>())
                dest.LevelInfo.BoundaryPoints[dir] = mainSection.GetChildValueInt(LevelFileKeys.BOUNDARY_BASE + dir.ToString());
        }

        private static void LoadSkillsetInfo(Dictionary<Skill, int> dest, DataItem skillsetSection)
        {
            foreach (Skill skill in Utils.GetEnumValues<Skill>())
                dest[skill] = skillsetSection.GetChildValueInt(skill.ToString(), 0);
        }

        private static void LoadSeaInfo(SeaInfo dest, DataItem seaSection)
        {
            dest.Texture = seaSection.GetChildValue(LevelFileKeys.INFO_SEA_TEXTURE1);
            dest.MotionX = seaSection.GetChildValueInt(LevelFileKeys.INFO_SEA_MOVEMENT_X);
            dest.MotionY = seaSection.GetChildValueInt(LevelFileKeys.INFO_SEA_MOVEMENT_X);
            dest.Effect = seaSection.GetChildValueEnum(LevelFileKeys.INFO_SEA_EFFECT, LevelFloorEffect.Water);
        }

        private static void LoadSkyInfo(SkyInfo dest, DataItem skySection)
        {
            dest.Texture = skySection.GetChildValue(LevelFileKeys.INFO_SKY_TEXTURE1);
            dest.FullHeight = skySection.GetChildExists(LevelFileKeys.INFO_SKY_FULL_HEIGHT);
        }

        private static void LoadCameraInfo(List<CameraEntry> dest, IEnumerable<DataItem> sections)
        {
            foreach (var camSection in sections)
                dest.Add(new CameraEntry(
                    camSection.GetChildValueFloat(LevelFileKeys.CAMERA_X),
                    camSection.GetChildValueFloat(LevelFileKeys.CAMERA_Y),
                    camSection.GetChildValueFloat(LevelFileKeys.CAMERA_Z),
                    camSection.GetChildValueFloat(LevelFileKeys.CAMERA_ROTATION),
                    camSection.GetChildValueFloat(LevelFileKeys.CAMERA_PITCH)
                    ));
        }

        private static void LoadPreviewCamera(PreviewCameraInfo dest, DataItem section)
        {
            dest.Style = section.GetChildValueEnum(LevelFileKeys.PREVIEW_CAMERA_STYLE, PreviewCameraStyle.Pivot);
            dest.X = section.GetChildValueFloat(LevelFileKeys.CAMERA_X);
            dest.Y = section.GetChildValueFloat(LevelFileKeys.CAMERA_Y);
            dest.Z = section.GetChildValueFloat(LevelFileKeys.CAMERA_Z);
            dest.Rotation = section.GetChildValueFloat(LevelFileKeys.CAMERA_ROTATION);
            dest.Pitch = section.GetChildValueFloat(LevelFileKeys.CAMERA_PITCH);
            dest.Distance = section.GetChildValueFloat(LevelFileKeys.PREVIEW_CAMERA_DISTANCE);
        }

        private static void LoadLands(Level dest, IEnumerable<DataItem> landSections)
        {
            foreach (var landSec in landSections)
            {
                var newLand = new LandRegion();

                newLand.Effect = landSec.GetChildValueEnum(LevelFileKeys.LAND_EFFECT, LevelFloorEffect.Solid);
                newLand.Texture = landSec.GetChildValue(LevelFileKeys.LAND_TEXTURE);
                newLand.Shading = landSec.GetChildValueInt(LevelFileKeys.LAND_SHADING);
                newLand.Vertices.AddRange(landSec.GetChildren(LevelFileKeys.LAND_VERTEX).Select(c =>
                {
                    var split = c.Value.Split(',');
                    return new LandRegion.RegionVertex() { X = int.Parse(split[0], CultureInfo.InvariantCulture), Y = int.Parse(split[1], CultureInfo.InvariantCulture) };
                }));

                dest.Lands.Add(newLand);
            }
        }

        private static void LoadBlockData(Level dest, DataItem mainSection)
        {
            var metablockDict = new Dictionary<string, BlockInfo>();

            LoadMetablocks(metablockDict, mainSection.GetChildren(LevelFileKeys.METABLOCK));
            LoadBlockStructure(dest, mainSection, metablockDict);
        }

        private static void LoadMetablocks(Dictionary<string, BlockInfo> dest, IEnumerable<DataItem> metablockItems)
        {
            foreach (var item in metablockItems)
            {
                string key = item.GetChildValue(LevelFileKeys.METABLOCK_IDENTIFIER);

                BlockInfo newInfo = new BlockInfo();

                newInfo.Shape = item.GetChildValueEnum(LevelFileKeys.METABLOCK_SHAPE, BlockShape.Solid);
                newInfo.Segment = item.GetChildValueInt(LevelFileKeys.METABLOCK_SEGMENT);
                newInfo.Rotations = item.GetChildValueInt(LevelFileKeys.METABLOCK_ROTATIONS);

                newInfo.Physics = item.GetChildValueEnum(LevelFileKeys.METABLOCK_PHYSICS, BlockPhysics.Solid);
                newInfo.OneWayDirection = item.GetChildExists(LevelFileKeys.METABLOCK_ONE_WAY) ?
                    item.GetChildValueEnum(LevelFileKeys.METABLOCK_ONE_WAY, default(Direction)) :
                    (Direction?)null;
                newInfo.Slippery = item.GetChildExists(LevelFileKeys.METABLOCK_SLIPPERY);
                newInfo.Antisplat = item.GetChildExists(LevelFileKeys.METABLOCK_ANTISPLAT);
                newInfo.IgnoredByCamera = item.GetChildExists(LevelFileKeys.METABLOCK_IGNORED_BY_CAMERA);
                newInfo.Visibility = item.GetChildValueEnum(LevelFileKeys.METABLOCK_VISIBILITY, BlockVisibility.Normal);

                foreach (Direction dir in Utils.GetEnumValues<Direction>())
                {
                    var faceItem = item.GetOrAddChild(LevelFileKeys.METABLOCK_FACE_PREFIX + dir.ToString().ToUpperInvariant());

                    newInfo.Faces[dir].Physics = faceItem.GetChildValueEnum(LevelFileKeys.METABLOCK_FACE_PHYSICS, FacePhysics.None);

                    foreach (var texSec in faceItem.GetChildren(LevelFileKeys.METABLOCK_FACE_OUTSIDE_TEXTURE))
                        newInfo.Faces[dir].TextureOuter.Add(new BlockTextureRef(
                            texSec.GetChildValue(LevelFileKeys.METABLOCK_FACE_TEXTURE_FILE),
                            texSec.GetChildValueInt(LevelFileKeys.METABLOCK_FACE_TEXTURE_INDEX),
                            texSec.GetChildValueInt(LevelFileKeys.METABLOCK_FACE_ROTATIONS),
                            texSec.GetChildExists(LevelFileKeys.METABLOCK_FACE_FLIP),
                            texSec.GetChildValueInt(LevelFileKeys.METABLOCK_FACE_SHADING)
                            ));

                    foreach (var texSec in faceItem.GetChildren(LevelFileKeys.METABLOCK_FACE_INSIDE_TEXTURE))
                        newInfo.Faces[dir].TextureInner.Add(new BlockTextureRef(
                            texSec.GetChildValue(LevelFileKeys.METABLOCK_FACE_TEXTURE_FILE),
                            texSec.GetChildValueInt(LevelFileKeys.METABLOCK_FACE_TEXTURE_INDEX),
                            texSec.GetChildValueInt(LevelFileKeys.METABLOCK_FACE_ROTATIONS),
                            texSec.GetChildExists(LevelFileKeys.METABLOCK_FACE_FLIP),
                            texSec.GetChildValueInt(LevelFileKeys.METABLOCK_FACE_SHADING)
                            ));
                }

                dest.Add(key, newInfo);
            }
        }

        private static void LoadBlockStructure(Level dest, DataItem mainSection, Dictionary<string, BlockInfo> metablocks)
        {
            int identifierLength = 0;
            int width = mainSection.GetChildValueInt(LevelFileKeys.LAYOUT_WIDTH);
            int depth = mainSection.GetChildValueInt(LevelFileKeys.LAYOUT_DEPTH);
            int height = mainSection.GetChildValueInt(LevelFileKeys.LAYOUT_HEIGHT);

            var structureSection = mainSection.GetOrAddChild(LevelFileKeys.LAYOUT_STRUCTURE);

            dest.BlockStructure.SetSize(width, depth, height);

            var layerSections = structureSection.GetChildren(LevelFileKeys.LAYOUT_LAYER).ToArray();
            if (layerSections.Length != height)
                throw new InvalidOperationException("Mismatch between specified height and layer data.");

            for (int z = 0; z < height; z++)
            {
                var lineSections = layerSections[z].GetChildren(LevelFileKeys.LAYOUT_LINE).ToArray();
                if (lineSections.Length != depth)
                    throw new InvalidOperationException("Mismatch between specified depth and line data at z = " + z.ToString());

                for (int y = 0; y < depth; y++)
                {
                    string thisLine = lineSections[y].Value;
                    if (identifierLength == 0)
                        identifierLength = thisLine.Length / width;
                    else if (thisLine.Length / identifierLength != width)
                        throw new InvalidOperationException("Mismatch between specified width and block data at z = " + z.ToString() + ", y = " + y.ToString());

                    for (int x = 0; x < width; x++)
                    {
                        string thisIdentifier = thisLine[(x * identifierLength)..((x + 1) * identifierLength)];
                        if (metablocks.ContainsKey(thisIdentifier))
                        {
                            BlockInfo newBlock = new BlockInfo();
                            newBlock.Clone(metablocks[thisIdentifier]);
                            dest.BlockStructure[x, y, z] = newBlock;
                        }
                    }
                }
            }
        }

        private static void LoadOverlays(List<LevelOverlay> dest, IEnumerable<DataItem> src)
        {
            foreach (var overlayItem in src)
            {
                var newOverlay = new LevelOverlay();
                newOverlay.Position = new Point3D(
                    overlayItem.GetChildValueInt(LevelFileKeys.OVERLAY_X),
                    overlayItem.GetChildValueInt(LevelFileKeys.OVERLAY_Y),
                    overlayItem.GetChildValueInt(LevelFileKeys.OVERLAY_Z)
                    );
                newOverlay.Side = overlayItem.GetChildValueEnum(LevelFileKeys.OVERLAY_SIDE, default(Direction));

                var frontItem = overlayItem.GetOrAddChild(LevelFileKeys.OVERLAY_OUTER);
                newOverlay.FrontTexture.File = frontItem.GetChildValue(LevelFileKeys.OVERLAY_TEXTURE);
                newOverlay.FrontTexture.Index = frontItem.GetChildValueInt(LevelFileKeys.OVERLAY_TEXTURE_INDEX);
                newOverlay.FrontTexture.Rotations = frontItem.GetChildValueInt(LevelFileKeys.OVERLAY_ROTATIONS);
                newOverlay.FrontTexture.Flip = frontItem.GetChildExists(LevelFileKeys.OVERLAY_FLIP);
                newOverlay.FrontTexture.Shading = frontItem.GetChildValueInt(LevelFileKeys.OVERLAY_SHADING);

                var backItem = overlayItem.GetOrAddChild(LevelFileKeys.OVERLAY_INNER);
                newOverlay.BackTexture.File = backItem.GetChildValue(LevelFileKeys.OVERLAY_TEXTURE);
                newOverlay.BackTexture.Index = backItem.GetChildValueInt(LevelFileKeys.OVERLAY_TEXTURE_INDEX);
                newOverlay.BackTexture.Rotations = backItem.GetChildValueInt(LevelFileKeys.OVERLAY_ROTATIONS);
                newOverlay.BackTexture.Flip = backItem.GetChildExists(LevelFileKeys.OVERLAY_FLIP);
                newOverlay.BackTexture.Shading = backItem.GetChildValueInt(LevelFileKeys.OVERLAY_SHADING);

                dest.Add(newOverlay);
            }
        }

        private static void LoadEntities(List<LevelEntity> dest, IEnumerable<DataItem> src)
        {
            foreach (var thisItem in src)
            {
                var entity = new LevelEntity();
                entity.Identifier = thisItem.GetChildValue(LevelFileKeys.ENTITY_IDENTIFIER);
                entity.Position.X = thisItem.GetChildValueInt(LevelFileKeys.ENTITY_X);
                entity.Position.Y = thisItem.GetChildValueInt(LevelFileKeys.ENTITY_Y);
                entity.Position.Z = thisItem.GetChildValueInt(LevelFileKeys.ENTITY_Z);
                entity.Offset.X = thisItem.GetChildValueInt(LevelFileKeys.ENTITY_OFFSET_X);
                entity.Offset.Y = thisItem.GetChildValueInt(LevelFileKeys.ENTITY_OFFSET_Y);
                entity.Offset.Z = thisItem.GetChildValueInt(LevelFileKeys.ENTITY_OFFSET_Z);
                entity.Rotations = thisItem.GetChildValueInt(LevelFileKeys.ENTITY_ROTATION);
                entity.PairingID = thisItem.GetChildValueInt(LevelFileKeys.ENTITY_PAIRING_ID);

                dest.Add(entity);
            }
        }
    }
}
