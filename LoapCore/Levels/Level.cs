﻿using LoapCore.Blocks;
using LoapCore.Entities;
using System.Collections.Generic;

namespace LoapCore.Levels
{
    public class Level
    {
        public readonly LevelInfo LevelInfo = new LevelInfo();
        public readonly List<LandRegion> Lands = new List<LandRegion>();
        public readonly BlockStructure BlockStructure = new BlockStructure();
        public readonly List<LevelOverlay> Overlays = new List<LevelOverlay>();
        public readonly List<LevelEntity> Entities = new List<LevelEntity>();

        public void Sanitize()
        {
            LevelInfo.Sanitize();
            Lands.ForEach(l => l.Sanitize());
            BlockStructure.Sanitize();
        }
    }
}
