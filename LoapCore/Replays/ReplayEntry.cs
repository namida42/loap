﻿using LoapCore.Misc;

namespace LoapCore.Replays
{
    public class ReplayEntry
    {
        public ReplayAction Action;
        public int Iteration;

        // For skill assignments
        public string LemmingIdentifier = "";
        public Skill Skill;
        public TurnerDirection TurnerDirection;

        // For spawn rate change
        public int NewSpawnRate;
    }
}
