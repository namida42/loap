﻿using LoapCore.Files;
using LoapCore.Misc;
using System.Linq;

namespace LoapCore.Replays
{
    public static class ReplayFile
    {
        private const string KEY_LEVEL_TITLE = "LEVEL_TITLE";
        private const string KEY_LEVEL_PATH = "LEVEL_PATH";

        private const string KEY_ENTRY = "ENTRY";
        private const string KEY_ACTION = "ACTION";
        private const string KEY_ITERATION = "ITERATION";

        private const string KEY_ASSIGNMENT_LEMMING_IDENTIFIER = "LEMMING";
        private const string KEY_ASSIGNMENT_SKILL = "SKILL";
        private const string KEY_ASSIGNMENT_DIRECTION = "DIRECTION";

        private const string KEY_SPAWNRATE_NEW_RATE = "NEW_RATE";

        public static void SaveReplayToDataItem(Replay src, DataItem dst)
        {
            if (!string.IsNullOrEmpty(src.LevelTitle))
                dst.SetChildValue(KEY_LEVEL_TITLE, src.LevelTitle);
            if (!string.IsNullOrEmpty(src.LevelPath))
                dst.SetChildValue(KEY_LEVEL_PATH, src.LevelPath);
            if (!string.IsNullOrEmpty(src.LevelTitle) && !string.IsNullOrEmpty(src.LevelPath))
                dst.AddNullChild();

            var sortedEntries = src.Actions
                .OrderBy(a => a.Iteration)
                .ThenBy(a => a.Action);

            foreach (var entry in sortedEntries)
            {
                var entrySec = dst.AddChildSection(KEY_ENTRY);

                entrySec.SetChildValueEnum(KEY_ACTION, entry.Action);
                entrySec.SetChildValueInt(KEY_ITERATION, entry.Iteration);

                switch (entry.Action)
                {
                    case ReplayAction.AssignSkill:
                        entrySec.SetChildValue(KEY_ASSIGNMENT_LEMMING_IDENTIFIER, entry.LemmingIdentifier);
                        entrySec.SetChildValueEnum(KEY_ASSIGNMENT_SKILL, entry.Skill);
                        if (entry.Skill == Skill.Turner)
                            entrySec.SetChildValueEnum(KEY_ASSIGNMENT_DIRECTION, entry.TurnerDirection);
                        break;
                    case ReplayAction.ChangeSpawnRate:
                        entrySec.SetChildValueInt(KEY_SPAWNRATE_NEW_RATE, entry.NewSpawnRate);
                        break;
                    case ReplayAction.Nuke:
                        // Nothing to do for this one.
                        break;
                }
            }
        }

        public static void LoadReplayFromDataItem(DataItem src, Replay dst)
        {
            dst.Clear();

            dst.LevelTitle = src.GetChildValue(KEY_LEVEL_TITLE);
            dst.LevelPath = src.GetChildValue(KEY_LEVEL_PATH);

            foreach (var entrySec in src.GetChildren(KEY_ENTRY))
            {
                var newEntry = new ReplayEntry();

                newEntry.Action = entrySec.GetChildValueEnum(KEY_ACTION, default(ReplayAction));
                newEntry.Iteration = entrySec.GetChildValueInt(KEY_ITERATION);

                switch (newEntry.Action)
                {
                    case ReplayAction.AssignSkill:
                        newEntry.LemmingIdentifier = entrySec.GetChildValue(KEY_ASSIGNMENT_LEMMING_IDENTIFIER);
                        newEntry.Skill = entrySec.GetChildValueEnum(KEY_ASSIGNMENT_SKILL, default(Skill));
                        if (newEntry.Skill == Skill.Turner)
                            newEntry.TurnerDirection = entrySec.GetChildValueEnum(KEY_ASSIGNMENT_DIRECTION, default(TurnerDirection));
                        break;
                    case ReplayAction.ChangeSpawnRate:
                        newEntry.NewSpawnRate = entrySec.GetChildValueInt(KEY_SPAWNRATE_NEW_RATE);
                        break;
                    case ReplayAction.Nuke:
                        // Nothing to do for this one.
                        break;
                }

                dst.Actions.Add(newEntry);
            }
        }
    }
}
