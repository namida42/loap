﻿using LoapCore.Misc;
using System.Collections.Generic;
using System.Linq;

namespace LoapCore.Replays
{
    public class Replay
    {
        public string LevelTitle = "";
        public string LevelPath = "";
        public readonly List<ReplayEntry> Actions = new List<ReplayEntry>();

        public void Clear()
        {
            LevelTitle = "";
            LevelPath = "";
            Actions.Clear();
        }

        /// <summary>
        /// The "fromIteration" parameter is INCLUSIVE.
        /// </summary>
        public void Cut(int fromIteration)
        {
            Actions.RemoveAll(a => a.Iteration >= fromIteration);
        }

        public ReplayEntry? GetAssignmentActionForIteration(int iteration)
        {
            return Actions.FirstOrDefault(a => a.Iteration == iteration && a.Action != ReplayAction.ChangeSpawnRate);
        }

        public ReplayEntry? GetSpawnRateActionForIteration(int iteration)
        {
            return Actions.FirstOrDefault(a => a.Iteration == iteration && a.Action == ReplayAction.ChangeSpawnRate);
        }

        public void AddAssignment(int iteration, string lemmingIdentifier, Skill skill, TurnerDirection direction = TurnerDirection.None)
        {
            var newAction = new ReplayEntry();

            newAction.Action = ReplayAction.AssignSkill;
            newAction.Iteration = iteration;

            newAction.LemmingIdentifier = lemmingIdentifier;
            newAction.Skill = skill;
            newAction.TurnerDirection = direction;

            Actions.RemoveAll(a => a.Iteration == iteration && a.Action != ReplayAction.ChangeSpawnRate);
            Actions.Add(newAction);
        }

        public void AddSpawnRateChange(int iteration, int newSpawnRate)
        {
            var newAction = new ReplayEntry();

            newAction.Action = ReplayAction.ChangeSpawnRate;
            newAction.Iteration = iteration;

            newAction.NewSpawnRate = newSpawnRate;

            Actions.RemoveAll(a => a.Iteration == iteration && a.Action == ReplayAction.ChangeSpawnRate);
            Actions.Add(newAction);
        }

        public void AddNuke(int iteration)
        {
            var newAction = new ReplayEntry();

            newAction.Action = ReplayAction.Nuke;
            newAction.Iteration = iteration;

            Actions.RemoveAll(a => a.Iteration == iteration && a.Action != ReplayAction.ChangeSpawnRate);
            Actions.Add(newAction);
        }
    }
}
