﻿using LoapCore.Blocks;
using LoapCore.Constants;
using LoapCore.Game;
using LoapCore.Misc;
using System;
using System.Collections.Generic;
using System.Linq;

// AS A GENERAL NOTE FOR THE Lemming CLASS
//
// The Transporter-related fields should not be assumed to always be filled in. Only the ones
// actually needed by a given kind of object will be used. For example, TransportIterations is
// not used when the lemming is in a teleporter; it is only used for rope slides and springs.

namespace LoapCore.Lemmings
{
    public class Lemming
    {
        public Lemming(GameEngine engine, string identifier, Point3D position, Direction direction) : this(engine, identifier)
        {
            Position = position;
            Direction = direction;

            if (!IsSolidAtFeet)
            {
                Action = LemmingAction.Faller;
                FallMomentum = FALLER_INITIAL_MOMENTUM;
                FallForward = false;
            }
            else
                Action = LemmingAction.Walker;
        }

        private Lemming(GameEngine engine, string identifier)
        {
            GameEngine = engine;
            Identifier = identifier;
        }

        #region Constants

        public const int SLIDER_SPEED = 3;

        public const int FALLER_INITIAL_MOMENTUM = 1;
        public const int FALLER_MOMENTUM_CAP = 10;
        public const int FALLER_MOMENTUM_INCREASE_FREQUENCY = 2;
        public const int FALLER_FORWARD_DISTANCE_CAP = PhysicsConstants.HALF_BLOCK_STEPS;
        public const int FALLER_FORWARD_SLOWDOWN_POINT = PhysicsConstants.HALF_BLOCK_STEPS * 3 / 4;
        public const int FLOATER_BEGIN_FALL_DISTANCE = PhysicsConstants.BLOCK_STEPS + PhysicsConstants.HALF_BLOCK_STEPS;
        public const int FALLER_STUN_DISTANCE = PhysicsConstants.SEGMENT_STEPS * 12;
        public const int FALLER_SPLAT_DISTANCE = PhysicsConstants.SEGMENT_STEPS * 16;

        public const int BLOCKER_FIELD_BOTTOM = 0;
        public const int BLOCKER_FIELD_HEIGHT = 2;
        public const int BLOCKER_FIELD_CHECK_BOTTOM = -1;
        public const int BLOCKER_FIELD_CHECK_TOP = 2;

        public const int TURNER_FIELD_BOTTOM = 0;
        public const int TURNER_FIELD_HEIGHT = 2;
        public const int TURNER_FIELD_CHECK_BOTTOM = -1;
        public const int TURNER_FIELD_CHECK_TOP = 3;

        public const int BOMBER_DESTRUCTION_TIME = 16;
        public const int BOMBER_DOWNWARDS_SPEED = 4;

        public const int BUILDER_BRICK_COUNT = 6;
        public const int BUILDER_PLACE_FRAME = 8;
        public const int BUILDER_CONTINUE_FRAME = 16;
        public const int SHRUG_DURATION = 30;

        public const int BASHER_SOUND_INTERVAL = 15;
        public const int BASHER_DESTRUCTION_TIME = 60;
        public const int BASHER_DESTRUCTION_BOTTOM = 0; // Inclusive
        public const int BASHER_DESTRUCTION_TOP = 2; // Exclusive

        public const int MINER_SOUND_INTERVAL = 15;
        public const int MINER_DESTRUCTION_TIME = 60;
        public const int MINER_DESTRUCTION_BOTTOM = -1; // Inclusive
        public const int MINER_DESTRUCTION_TOP = 2; // Exclusive

        public const int DIGGER_SOUND_INTERVAL = 15;
        public const int DIGGER_DESTRUCTION_TIME = 60;
        public const int DIGGER_DESTRUCTION_BOTTOM = 0; // Inclusive
        public const int DIGGER_DESTRUCTION_TOP = 3; // Exclusive

        public const int DROWNER_BEGIN_FINAL_FRAMES_ITERATION = 16;
        public const int DROWNER_REMOVE_ITERATION = 20;
        public const int EXITER_REMOVE_ITERATION = 12;
        public const int STUN_RECOVER_ITERATION = 75;
        public const int ZAPPER_POOF_ITERATION = 22;
        public const int ZAPPER_REMOVE_ITERATION = 30;
        public const int DEATH_SPIN_REMOVE_ITERATION = 30;
        public const int WARPING_DURATION = 24;
        public const int ZIPPING_DURATION_PER_BLOCK = 10;
        public const int SPRING_DURATION_PER_BLOCK = 4;
        public const int SPRING_PEAK_HEIGHT = 17 * PhysicsConstants.SEGMENT_STEPS + PhysicsConstants.HALF_SEGMENT_STEPS;
        public const int SPRING_COLLISION_CHECK_POINTS = 4;
        public const int TRAMPOLINE_FORWARD_PER_ITERATION = 4;
        public const int STRONG_TRAMPOLINE_BOOST = PhysicsConstants.SEGMENT_STEPS * 4;
        public const int WEAK_TRAMPOLINE_BOOST = PhysicsConstants.SEGMENT_STEPS * -5;

        #endregion

        #region Fields

        // Linkbacks

        public readonly GameEngine GameEngine;
        private BlockStructure BlockStructure => GameEngine.Blocks;
        private List<EffectField> EffectFields => GameEngine.EffectFields;

        // General
        public readonly string Identifier = "";

        public bool LemmingRemoved;
        public LemmingRemoveReason LemmingRemoveReason;

        public Point3D Position;
        public int X { get => Position.X; set => Position.X = value; }
        public int Y { get => Position.Y; set => Position.Y = value; }
        public int Z { get => Position.Z; set => Position.Z = value; }

        public Direction Direction;
        public bool IsOnYAxis => (Direction == Direction.YMinus || Direction == Direction.YPlus);

        public LemmingAction Action;
        public QueuedSkill QueuedSkill;

        public bool IsClimber;
        public bool IsFloater;

        public int CurrentActionIterations;

        // Walker
        public bool SkipNextMovement;
        public bool SkipNextBlockCheck;
        public bool SkipNextSplitter;
        public bool TurnedLastUpdate;

        // Faller
        public int FallMomentum;
        public bool FallForward;
        public int DistanceFallen;
        public int DistanceMovedForwardAsFaller;

        // Turner
        public TurnerDirection TurnerDirection;

        // Bomber
        public bool ExClimberBomber;
        public bool ExBlockerOrTurnerBomber;

        // Builder
        public int BuilderBricksPlaced;
        public bool BuilderBrickCollision;

        // Transport states
        public Point3D TransporterSource;
        public Point3D TransporterTarget;
        public int BouncePeakZ;
        public int TransportIterations;
        public string TransporterSourceIdentifier = "";
        public string TransporterTargetIdentifier = "";

        #endregion

        #region Cloning

        public Lemming Clone()
        {
            var result = new Lemming(GameEngine, Identifier);

            result.LemmingRemoved = this.LemmingRemoved;
            result.LemmingRemoveReason = this.LemmingRemoveReason;

            result.Position = this.Position;
            result.Direction = this.Direction;

            result.Action = this.Action;
            result.QueuedSkill = this.QueuedSkill;

            result.IsClimber = this.IsClimber;
            result.IsFloater = this.IsFloater;

            result.CurrentActionIterations = this.CurrentActionIterations;

            result.SkipNextMovement = this.SkipNextMovement;
            result.SkipNextBlockCheck = this.SkipNextBlockCheck;
            result.SkipNextSplitter = this.SkipNextSplitter;

            result.FallMomentum = this.FallMomentum;
            result.FallForward = this.FallForward;
            result.DistanceFallen = this.DistanceFallen;
            result.DistanceMovedForwardAsFaller = this.DistanceMovedForwardAsFaller;

            result.TurnerDirection = this.TurnerDirection;

            result.ExClimberBomber = this.ExClimberBomber;
            result.ExBlockerOrTurnerBomber = this.ExBlockerOrTurnerBomber;

            result.BuilderBricksPlaced = this.BuilderBricksPlaced;
            result.BuilderBrickCollision = this.BuilderBrickCollision;

            result.TransporterSource = this.TransporterSource;
            result.TransporterTarget = this.TransporterTarget;
            result.BouncePeakZ = this.BouncePeakZ;
            result.TransportIterations = this.TransportIterations;
            result.TransporterSourceIdentifier = this.TransporterSourceIdentifier;
            result.TransporterTargetIdentifier = this.TransporterTargetIdentifier;

            return result;
        }

        #endregion

        #region Assignability Info

        public bool MayBeAssignedSkill(Skill skill)
        {
            if (skill == Skill.Climber && IsClimber)
                return false;

            if (skill == Skill.Floater && IsFloater)
                return false;

            if (((skill == Skill.Turner) || (skill == Skill.Blocker)) && !BlockerTurnerSplitterCheck(true))
                return false;

            switch (Action)
            {
                case LemmingAction.Walker:
                    switch (QueuedSkill)
                    {
                        case QueuedSkill.None:
                            return true;
                        case QueuedSkill.Blocker:
                            return (skill == Skill.Climber || skill == Skill.Floater || skill == Skill.Bomber);
                        case QueuedSkill.Turner:
                            return (skill == Skill.Climber || skill == Skill.Floater || skill == Skill.Bomber);
                        case QueuedSkill.BuilderBegin:
                        case QueuedSkill.BuilderContinue:
                        case QueuedSkill.Shrugger:
                            return (skill != Skill.Builder) &&
                                ((skill != Skill.Blocker && skill != Skill.Turner) || AdjustedPositionOnBlock > PhysicsConstants.HALF_BLOCK_STEPS || IsOnBlockEdge);
                        case QueuedSkill.BasherBegin:
                        case QueuedSkill.BasherOverflow:
                        case QueuedSkill.BasherContinue:
                            return (skill != Skill.Basher);
                        case QueuedSkill.MinerBegin:
                        case QueuedSkill.MinerContinue:
                            return (skill != Skill.Miner);
                        case QueuedSkill.Digger:
                            return (skill != Skill.Digger);
                        default:
                            throw new NotImplementedException();
                    }
                case LemmingAction.Slider:
                    return (skill == Skill.Climber || skill == Skill.Floater || skill == Skill.Bomber);
                case LemmingAction.Faller:
                    return (skill == Skill.Climber || skill == Skill.Floater || skill == Skill.Bomber);
                case LemmingAction.Stunned:
                    return (skill == Skill.Climber || skill == Skill.Floater);
                case LemmingAction.Blocker:
                    return (skill == Skill.Climber || skill == Skill.Floater || skill == Skill.Bomber);
                case LemmingAction.Turner:
                    return (skill == Skill.Climber || skill == Skill.Floater || skill == Skill.Bomber);
                case LemmingAction.Builder:
                    return (skill != Skill.Builder) &&
                           ((skill != Skill.Blocker && skill != Skill.Turner) || AdjustedPositionOnBlock > PhysicsConstants.HALF_BLOCK_STEPS || IsOnBlockEdge);
                case LemmingAction.Shrugger:
                    return ((skill != Skill.Blocker && skill != Skill.Turner) || AdjustedPositionOnBlock > PhysicsConstants.HALF_BLOCK_STEPS || IsOnBlockEdge);
                case LemmingAction.Basher:
                    return (skill != Skill.Basher);
                case LemmingAction.Miner:
                    return (skill != Skill.Miner);
                case LemmingAction.Digger:
                    return (skill != Skill.Digger);
                case LemmingAction.Climber:
                    return (skill == Skill.Climber || skill == Skill.Floater || skill == Skill.Bomber);
                case LemmingAction.Floater:
                    return (skill == Skill.Climber || skill == Skill.Floater || skill == Skill.Bomber);
                case LemmingAction.Drowner:
                    return false;
                case LemmingAction.Zapper:
                    return false;
                case LemmingAction.DeathSpin:
                    return false;
                case LemmingAction.Bomber:
                    return false;
                case LemmingAction.Exiter:
                    return false;
                case LemmingAction.Warper:
                    return (skill == Skill.Climber || skill == Skill.Floater);
                case LemmingAction.Zipper:
                    return (skill == Skill.Climber || skill == Skill.Floater);
                case LemmingAction.Springer:
                    return (skill == Skill.Climber || skill == Skill.Floater);
                case LemmingAction.Bouncer:
                    return (skill == Skill.Climber || skill == Skill.Floater || skill == Skill.Bomber);
                default:
                    throw new NotImplementedException();
            }
        }

        public bool MayBeNuked
        {
            get
            {
                switch (Action)
                {
                    case LemmingAction.Stunned:
                        return true; // Special case - a Stunned lemming can be nuked but can't be assigned Bomber.
                    default:
                        return MayBeAssignedSkill(Skill.Bomber);
                }
            }
        }

        private bool BlockerTurnerSplitterCheck(bool checkFrontBlock)
        {
            var checkPos = GetAscendableBlockPosition();

            int startX = checkPos.X;
            int startY = checkPos.Y;
            int startZ = checkPos.Z;

            int endX = startX;
            int endY = startY;
            int endZ = startZ + 1;

            if (IsOnBlockEdge && checkFrontBlock)
            {
                endX += ForwardDX;
                endY += ForwardDY;

                if (endX < startX)
                {
                    int temp = startX;
                    startX = endX;
                    endX = temp;
                }

                if (endY < startY)
                {
                    int temp = startY;
                    startY = endY;
                    endY = temp;
                }
            }

            for (int z = startZ; z <= endZ; z++)
                for (int y = startY; y <= endY; y++)
                    for (int x = startX; x <= endX; x++)
                    {
                        var block = BlockStructure[x, y, z];
                        if (block != null && block.Physics == BlockPhysics.Splitter)
                            return false;
                    }

            return true;
        }

        #endregion

        #region General Control

        public void UpdatePreMove()
        {
            switch (Action)
            {
                case LemmingAction.Walker:
                    SetQueuedDirectionalSkillFields();
                    break;
                case LemmingAction.Blocker:
                    SetBlockerField();
                    break;
                case LemmingAction.Turner:
                    SetTurnerField();
                    break;
                case LemmingAction.Basher:
                    ApplyBasher();
                    break;
                case LemmingAction.Miner:
                    ApplyMiner();
                    break;
                case LemmingAction.Digger:
                    ApplyDigger();
                    break;
                case LemmingAction.Bomber:
                    ApplyBomber();
                    break;
                case LemmingAction.Slider:
                case LemmingAction.Faller:
                case LemmingAction.Stunned:
                case LemmingAction.Builder:
                case LemmingAction.Shrugger:
                case LemmingAction.Climber:
                case LemmingAction.Floater:
                case LemmingAction.Drowner:
                case LemmingAction.Zapper:
                case LemmingAction.DeathSpin:
                case LemmingAction.Exiter:
                case LemmingAction.Warper:
                case LemmingAction.Zipper:
                case LemmingAction.Springer:
                case LemmingAction.Bouncer:
                    // No pre-move actions.
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public void UpdateMove()
        {
            switch (Action)
            {
                case LemmingAction.Walker:
                    UpdateWalker();
                    break;
                case LemmingAction.Slider:
                    UpdateSlider();
                    break;
                case LemmingAction.Faller:
                    UpdateFaller();
                    CheckHazards(false);
                    break;
                case LemmingAction.Stunned:
                    FallIfBlockRemoved(false, false);
                    if (Action == LemmingAction.Stunned)
                        WalkAfterFrames(STUN_RECOVER_ITERATION);
                    break;
                case LemmingAction.Blocker:
                    FallIfBlockRemoved(true, false, true);
                    break;
                case LemmingAction.Turner:
                    FallIfBlockRemoved(true, false, true);
                    break;
                case LemmingAction.Builder:
                    UpdateBuilder();
                    CheckHazards(true);
                    CheckExit();
                    break;
                case LemmingAction.Shrugger:
                    WalkAfterFrames(SHRUG_DURATION);
                    break;
                case LemmingAction.Basher:
                    UpdateBasher();
                    CheckHazards(false);
                    CheckExit();
                    break;
                case LemmingAction.Miner:
                    UpdateMiner();
                    CheckHazards(false);
                    CheckExit();
                    break;
                case LemmingAction.Digger:
                    UpdateDigger();
                    CheckHazards(true);
                    break;
                case LemmingAction.Climber:
                    UpdateClimber();
                    CheckExit();
                    break;
                case LemmingAction.Floater:
                    UpdateFloater();
                    CheckHazards(false);
                    break;
                case LemmingAction.Drowner:
                    RemoveAfterFrames(DROWNER_REMOVE_ITERATION, LemmingRemoveReason.Drown);
                    break;
                case LemmingAction.Zapper:
                    RemoveAfterFrames(ZAPPER_REMOVE_ITERATION, LemmingRemoveReason.Electrocute);
                    break;
                case LemmingAction.DeathSpin:
                    RemoveAfterFrames(DEATH_SPIN_REMOVE_ITERATION, LemmingRemoveReason.Trap);
                    break;
                case LemmingAction.Bomber:
                    UpdateBomber();
                    break;
                case LemmingAction.Exiter:
                    RemoveAfterFrames(EXITER_REMOVE_ITERATION, LemmingRemoveReason.Exit);
                    break;
                case LemmingAction.Warper:
                    UpdateWarper();
                    break;
                case LemmingAction.Zipper:
                    UpdateZipper();
                    break;
                case LemmingAction.Springer:
                    UpdateSpringer();
                    break;
                case LemmingAction.Bouncer:
                    UpdateBouncer();
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public void UpdatePostMove()
        {
            switch (Action)
            {
                case LemmingAction.Builder:
                    PlaceBuilderBrick();
                    break;
                case LemmingAction.Walker:
                case LemmingAction.Slider:
                case LemmingAction.Faller:
                case LemmingAction.Stunned:
                case LemmingAction.Blocker:
                case LemmingAction.Turner:
                case LemmingAction.Shrugger:
                case LemmingAction.Basher:
                case LemmingAction.Miner:
                case LemmingAction.Digger:
                case LemmingAction.Climber:
                case LemmingAction.Floater:
                case LemmingAction.Drowner:
                case LemmingAction.Zapper:
                case LemmingAction.DeathSpin:
                case LemmingAction.Bomber:
                case LemmingAction.Exiter:
                case LemmingAction.Warper:
                case LemmingAction.Zipper:
                case LemmingAction.Springer:
                case LemmingAction.Bouncer:
                    // No post-move actions.
                    break;
                default:
                    throw new NotImplementedException();
            }

            CurrentActionIterations++;
        }

        public void Transition(LemmingAction newAction, QueuedSkill queuedAction = QueuedSkill.None)
        {
            CurrentActionIterations = -1;

            switch (newAction)
            {
                case LemmingAction.Walker:
                    SkipNextMovement = false; // Some specific transitions might turn this back on, but we want it off by default.
                    break;
                case LemmingAction.Slider:
                    SkipNextMovement = false;
                    break;
                case LemmingAction.Faller:
                    DistanceFallen = 0;
                    DistanceMovedForwardAsFaller = 0;
                    FallMomentum = FALLER_INITIAL_MOMENTUM;
                    break;
                case LemmingAction.Shrugger:
                    QueueSound(SoundNames.SHRUGGER);
                    break;
                case LemmingAction.Floater:
                    QueueSound(SoundNames.FLOATER);
                    break;
                case LemmingAction.Drowner:
                    QueueSound(SoundNames.DROWN);
                    break;
                case LemmingAction.Zapper:
                    QueueSound(SoundNames.ELECTROCUTE);
                    break;
                case LemmingAction.Bomber:
                    QueueSound(SoundNames.OH_NO);
                    ExClimberBomber = (Action == LemmingAction.Climber);
                    ExBlockerOrTurnerBomber = (Action == LemmingAction.Blocker || Action == LemmingAction.Turner);
                    break;
                case LemmingAction.Exiter:
                    QueueSound(SoundNames.EXIT);
                    break;
                case LemmingAction.Builder:
                    BuilderBrickCollision = false;
                    break;
                case LemmingAction.Bouncer:
                    SetBounceIterations();
                    break;
                case LemmingAction.Stunned:
                    QueueSound(SoundNames.THUD);
                    break;
                case LemmingAction.Blocker:
                case LemmingAction.Turner:
                case LemmingAction.Basher:
                case LemmingAction.Miner:
                case LemmingAction.Digger:
                case LemmingAction.Climber:
                case LemmingAction.DeathSpin:
                case LemmingAction.Warper:
                case LemmingAction.Zipper:
                case LemmingAction.Springer:
                    // No transition-to actions.
                    break;
                default:
                    throw new NotImplementedException();
            }

            Action = newAction;
            QueuedSkill = queuedAction;
        }

        public void QueueSound(string soundName)
        {
            // This has a dedicated function so that positioning etc can be added later if desired.
            GameEngine.LatestIterationSounds.Add(new GameSound(soundName, Position));
        }

        #endregion

        #region Pre-Move Behaviors

        private void SetQueuedDirectionalSkillFields()
        {
            if (QueuedSkill == QueuedSkill.Blocker && AdjustedPositionOnBlock >= PhysicsConstants.THREE_QUARTER_BLOCK_STEPS)
                SetBlockerField();

            if (QueuedSkill == QueuedSkill.Turner && AdjustedPositionOnBlock >= PhysicsConstants.HALF_BLOCK_STEPS)
                SetTurnerField();
        }

        private void SetBlockerField()
        {
            var basePosition = GetBasePlaceBlockerFieldPosition();

            for (int i = BLOCKER_FIELD_BOTTOM; i < BLOCKER_FIELD_HEIGHT; i++)
                EffectFields.Add(EffectField.CreateBlocker(
                    new Point3D(basePosition.X, basePosition.Y, basePosition.Z + i),
                    IsOnYAxis, Identifier
                    ));
        }

        private void SetTurnerField()
        {
            var basePosition = GetBasePlaceTurnerFieldPosition();
            for (int i = TURNER_FIELD_BOTTOM; i < TURNER_FIELD_HEIGHT; i++)
                EffectFields.Add(EffectField.CreateTurner(
                    new Point3D(basePosition.X, basePosition.Y, basePosition.Z + i),
                    Direction, TurnerDirection, Identifier
                    ));
        }

        public void ApplyBomber()
        {
            if (CurrentActionIterations == BOMBER_DESTRUCTION_TIME)
            {
                var target = GetAscendableBlockPosition();
                if (Utils.ModuloNegativeAdjusted(Z, PhysicsConstants.SEGMENT_STEPS) >= PhysicsConstants.HALF_SEGMENT_STEPS)
                    target.Z += 1;
                if (AdjustedPositionOnBlock == 0 && !ExBlockerOrTurnerBomber)
                {
                    target.X += ForwardDX;
                    target.Y += ForwardDY;
                }
                GameEngine.ApplyBomberEffect(target);
                QueueSound(SoundNames.BOMBER_BLAST);

                LemmingRemoved = true;
                LemmingRemoveReason = LemmingRemoveReason.Bomber;
            }
        }

        public void ApplyBasher()
        {
            if (CurrentActionIterations == BASHER_DESTRUCTION_TIME)
            {
                var basePosition = GetBaseWallPosition();
                for (int i = BASHER_DESTRUCTION_BOTTOM; i < BASHER_DESTRUCTION_TOP; i++)
                    GameEngine.RemoveBlock(basePosition.X, basePosition.Y, basePosition.Z + i, true, true);

                SnapVerticalPositionDownwards();
                Transition(LemmingAction.Walker, QueuedSkill.BasherContinue);
            }
            else if (CurrentActionIterations > 0)
            {
                var basePosition = GetBaseWallPosition();
                for (int i = BASHER_DESTRUCTION_BOTTOM; i < BASHER_DESTRUCTION_TOP; i++)
                    GameEngine.DamageBlock(basePosition.X, basePosition.Y, basePosition.Z + i);
            }
        }

        public void ApplyMiner()
        {
            if (CurrentActionIterations == MINER_DESTRUCTION_TIME)
            {
                var basePosition = GetBaseWallPosition();
                for (int i = MINER_DESTRUCTION_BOTTOM; i < MINER_DESTRUCTION_TOP; i++)
                    GameEngine.RemoveBlock(basePosition.X, basePosition.Y, basePosition.Z + i, true, true);

                SnapVerticalPositionDownwards();
                Transition(LemmingAction.Walker, QueuedSkill.MinerContinue);
            }
            else if (CurrentActionIterations > 0)
            {
                var basePosition = GetBaseWallPosition();
                for (int i = MINER_DESTRUCTION_BOTTOM; i < MINER_DESTRUCTION_TOP; i++)
                    GameEngine.DamageBlock(basePosition.X, basePosition.Y, basePosition.Z + i);
            }
        }

        public void ApplyDigger()
        {
            if ((CurrentActionIterations > 0) && (CurrentActionIterations % DIGGER_DESTRUCTION_TIME == 0))
            {
                var basePosition = GetDiggerTargetBlockPosition();

                GameEngine.RemoveBlock(basePosition.X, basePosition.Y, basePosition.Z, true, false);

                var oneAboveBlock = BlockStructure[basePosition.X, basePosition.Y, basePosition.Z + 1];
                if (oneAboveBlock != null && PhysicsConstants.DiggerDestroyAboveShapes.Contains(oneAboveBlock.Shape))
                {
                    GameEngine.RemoveBlock(basePosition.X, basePosition.Y, basePosition.Z + 1, false, false);

                    var twoAboveBlock = BlockStructure[basePosition.X, basePosition.Y, basePosition.Z + 2];
                    if (twoAboveBlock != null &&
                        !PhysicsConstants.IndestructibleBlockTypes.Contains(oneAboveBlock.Physics) &&
                        twoAboveBlock.Shape == oneAboveBlock.Shape &&
                        twoAboveBlock.Rotations == oneAboveBlock.Rotations &&
                        twoAboveBlock.Segment == oneAboveBlock.Segment + 1)
                        GameEngine.RemoveBlock(basePosition.X, basePosition.Y, basePosition.Z + 2, false, false);
                }

                if (IsOnSegmentEdge)
                    Z -= PhysicsConstants.SEGMENT_STEPS;
                else
                    SnapVerticalPositionDownwards();
            }
            else if (CurrentActionIterations > 0)
            {
                var basePosition = GetDiggerTargetBlockPosition();

                GameEngine.DamageBlock(basePosition.X, basePosition.Y, basePosition.Z);

                var oneAboveBlock = BlockStructure[basePosition.X, basePosition.Y, basePosition.Z + 1];
                if (oneAboveBlock != null && PhysicsConstants.DiggerDestroyAboveShapes.Contains(oneAboveBlock.Shape))
                {
                    GameEngine.DamageBlock(basePosition.X, basePosition.Y, basePosition.Z + 1);

                    var twoAboveBlock = BlockStructure[basePosition.X, basePosition.Y, basePosition.Z + 2];
                    if (twoAboveBlock != null &&
                        !PhysicsConstants.IndestructibleBlockTypes.Contains(oneAboveBlock.Physics) &&
                        twoAboveBlock.Shape == oneAboveBlock.Shape &&
                        twoAboveBlock.Rotations == oneAboveBlock.Rotations &&
                        twoAboveBlock.Segment == oneAboveBlock.Segment + 1)
                        GameEngine.DamageBlock(basePosition.X, basePosition.Y, basePosition.Z + 2);
                }
            }
        }

        #endregion

        #region Move (main phase) Behaviors (Walker / Slider)

        private void UpdateWalker()
        {
            if (!SkipNextMovement)
            {
                X += ForwardDX;
                Y += ForwardDY;
            }
            else
                SkipNextMovement = false;

            if (Action == LemmingAction.Walker)
                HandleWalkerSlopeVerticalMovement();

            CheckExit();

            if (Action == LemmingAction.Walker)
                HandleFirstRoundSkillTransitionsAndDequeueing();

            if (Action == LemmingAction.Walker)
                HandleWalkerStepVerticalMovement();

            if (Action == LemmingAction.Walker)
            {
                var oldDir = Direction;
                HandleWalkerDirectionChanges();

                if (Direction != oldDir)
                    return;

                HandleSecondRoundSkillTransitionsAndDequeueing();
            }

            CheckHazards(true);
        }

        private void UpdateSlider()
        {
            for (int i = 0; i < SLIDER_SPEED; i++)
            {
                if (!SkipNextMovement)
                {
                    X += ForwardDX;
                    Y += ForwardDY;
                }
                else
                    SkipNextMovement = false;

                if (Action == LemmingAction.Slider)
                    HandleWalkerSlopeVerticalMovement(); // works fine for sliders too

                CheckExit();

                if (Action == LemmingAction.Slider)
                    if (IsClimber && GetIsClimbableWallInFront())
                        Transition(LemmingAction.Climber);

                if (Action == LemmingAction.Slider)
                    HandleWalkerStepVerticalMovement(); // this too

                if (Action == LemmingAction.Slider)
                    HandleWalkerDirectionChanges(); // and this

                CheckHazards(true);

                if (Action == LemmingAction.Slider)
                    CheckSliderRevertToWalker();

                if (Action != LemmingAction.Slider)
                    break;
            }
        }

        private void CheckSliderRevertToWalker()
        {
            var blockPos = GetStandingOnBlockPosition();
            var block = BlockStructure[blockPos.X, blockPos.Y, blockPos.Z];

            if (block != null && block.Slippery)
                return;

            if (IsOnSegmentEdge)
            {
                var ascendPos = GetAscendableBlockPosition();
                var ascend = BlockStructure[ascendPos.X, ascendPos.Y, ascendPos.Z];

                if (ascend != null && ascend.Slippery)
                    return;
            }

            if (Z <= PhysicsConstants.LAND_HEIGHT)
                if (block == null || PhysicsConstants.NonSolidBlockTypes.Contains(block.Physics))
                    if (GameEngine.GetFloorEffectAt(X, Y) == LevelFloorEffect.Slippery)
                        return;

            Transition(LemmingAction.Walker);
        }

        private void HandleWalkerSlopeVerticalMovement()
        {
            int oldZ = Z;
            var blockPos = GetStandingOnBlockPosition();
            var relPos = GetPositionOnBlock(blockPos);
            if (IsBlockSolidAt(blockPos, new Point3D(relPos.X, relPos.Y, relPos.Z + 1)) && !IsBlockSolidAt(blockPos, new Point3D(relPos.X, relPos.Y, relPos.Z + 2)))
                Z += 1;
            else if (IsBlockSolidAt(blockPos, new Point3D(relPos.X, relPos.Y, relPos.Z + 2)) && !IsBlockSolidAt(blockPos, new Point3D(relPos.X, relPos.Y, relPos.Z + 3)))
                Z += 2;
            else
            {
                var brickCheckPos = GetAscendableBlockPosition();

                if (!CheckForBuilderBrick(brickCheckPos, BuilderBrickCheckDirection))
                {
                    if (!IsBlockSolidAt(blockPos, new Point3D(relPos.X, relPos.Y, relPos.Z)) && IsBlockSolidAt(blockPos, new Point3D(relPos.X, relPos.Y, relPos.Z - 1)))
                        Z -= 1;
                    else if (!IsBlockSolidAt(blockPos, new Point3D(relPos.X, relPos.Y, relPos.Z - 1)) && IsBlockSolidAt(blockPos, new Point3D(relPos.X, relPos.Y, relPos.Z - 2)))
                        Z -= 2;
                }
            }

            if (Z != oldZ)
                if (QueuedSkill == QueuedSkill.BuilderContinue || QueuedSkill == QueuedSkill.BasherContinue || QueuedSkill == QueuedSkill.MinerContinue)
                    QueuedSkill = QueuedSkill.None;
        }

        private void HandleFirstRoundSkillTransitionsAndDequeueing()
        {
            if (((IsOnSegmentEdge && IsOnBlockQuarter) || IsOnBlockEdge) && (QueuedSkill == QueuedSkill.BasherBegin || QueuedSkill == QueuedSkill.BasherOverflow))
                if (CheckIfCanBash())
                {
                    Transition(LemmingAction.Basher);
                    return;
                }

            if (IsAtBlockMiddle)
            {
                switch (QueuedSkill)
                {
                    case QueuedSkill.BasherOverflow:
                        QueuedSkill = QueuedSkill.None;
                        break;
                }
            }

            if (IsOnBlockEdge)
            {
                switch (QueuedSkill)
                {
                    case QueuedSkill.Blocker:
                        if (BlockerTurnerSplitterCheck(false))
                            Transition(LemmingAction.Blocker);
                        else
                            QueuedSkill = QueuedSkill.None;
                        return;
                    case QueuedSkill.Turner:
                        if (BlockerTurnerSplitterCheck(false))
                            Transition(LemmingAction.Turner);
                        else
                        {
                            QueuedSkill = QueuedSkill.None;
                            TurnerDirection = TurnerDirection.None;
                        }
                        return;
                    case QueuedSkill.BasherBegin:
                        QueuedSkill = QueuedSkill.BasherOverflow;
                        break;
                    case QueuedSkill.BasherContinue:
                        if (CheckIfCanBash())
                        {
                            Transition(LemmingAction.Basher);
                            QueuedSkill = QueuedSkill.None;
                            return;
                        }
                        QueuedSkill = QueuedSkill.None;
                        break;
                    case QueuedSkill.MinerBegin:
                    case QueuedSkill.MinerContinue:
                        QueuedSkill = QueuedSkill.None;
                        if (CheckIfCanMine())
                        {
                            Transition(LemmingAction.Miner);
                            return;
                        }
                        break;
                }
            }

            if (IsClimber && GetIsClimbableWallInFront())
            {
                Transition(LemmingAction.Climber);
                return;
            }
        }

        private bool CheckIfCanBash()
        {
            if (IsOnBlockEdge || QueuedSkill == QueuedSkill.BasherBegin || QueuedSkill == QueuedSkill.BasherOverflow)
            {
                var wallPos = GetBaseWallPosition();
                for (int i = BASHER_DESTRUCTION_BOTTOM; i < BASHER_DESTRUCTION_TOP; i++)
                {
                    var thisPos = new Point3D(wallPos.X, wallPos.Y, wallPos.Z + i);

                    if ((IsOnBlockEdge || IsAtBlockMiddle) && GameEngine.BuilderBricks.Any(brick => brick.Position == thisPos))
                        return true;

                    var posOnBlock = GetPositionOnBlock(wallPos);
                    if ((
                            IsBlockSolidAt(thisPos, new Point3D(posOnBlock.X + ForwardDX * 2, posOnBlock.Y + ForwardDY * 2, 1)) && // check position offset to avoid some issues with slopes
                            QueuedSkill == QueuedSkill.BasherBegin
                        ) ||
                        IsBlockSolidAt(thisPos, new Point3D(posOnBlock.X, posOnBlock.Y, PhysicsConstants.SEGMENT_STEPS)))
                        return true;
                }
            }

            return false;
        }

        private bool CheckIfCanMine()
        {
            var wallPos = GetBaseWallPosition();

            for (int i = MINER_DESTRUCTION_BOTTOM; i < MINER_DESTRUCTION_TOP; i++)
            {
                var thisPos = new Point3D(wallPos.X, wallPos.Y, wallPos.Z + i);
                var block = BlockStructure[thisPos.X, thisPos.Y, thisPos.Z];
                if (block != null)
                    if (!PhysicsConstants.NonSolidBlockTypes.Contains(block.Physics))
                        return true;
                if (GameEngine.BuilderBricks.Any(brick => brick.Position == thisPos))
                    return true;
            }

            return false;
        }

        private void HandleWalkerStepVerticalMovement()
        {
            int zModulo = Utils.ModuloNegativeAdjusted(Z, PhysicsConstants.SEGMENT_STEPS);

            if (GetIsStepInFront())
            {
                Z = Z - zModulo + PhysicsConstants.SEGMENT_STEPS;
                if (QueuedSkill != QueuedSkill.Digger && QueuedSkill != QueuedSkill.BasherBegin && QueuedSkill != QueuedSkill.MinerBegin)
                    QueuedSkill = QueuedSkill.None;
            }
            else
            {
                var blockPos = GetAscendableBlockPosition();

                if (CheckForBuilderBrick(blockPos, BuilderBrickCheckDirection) && !GetIsWallInFront())
                {
                    var oldZ = Z;
                    Z = Z - zModulo + PhysicsConstants.SEGMENT_STEPS;

                    var oldQueuedSkill = QueuedSkill;
                    if (QueuedSkill != QueuedSkill.Digger && QueuedSkill != QueuedSkill.BasherBegin && QueuedSkill != QueuedSkill.MinerBegin)
                        if ((QueuedSkill != QueuedSkill.BuilderContinue && QueuedSkill != QueuedSkill.Shrugger) ||
                            AdjustedPositionOnBlock != 1 && AdjustedPositionOnBlock != PhysicsConstants.HALF_BLOCK_STEPS + 1)
                            QueuedSkill = QueuedSkill.None;

                    if (IsSolidAtHead || GetIsWallInFront() || GetIsStepInFront())
                    {
                        Z = oldZ;
                        if (oldQueuedSkill == QueuedSkill.BuilderContinue || oldQueuedSkill == QueuedSkill.Shrugger)
                        {
                            Direction = Utils.InvertDirection(Direction);
                            QueuedSkill = QueuedSkill.None;
                            X += ForwardDX;
                            Y += ForwardDY;
                            return;
                        }
                    }
                    else
                        return;
                }

                if (!IsSolidAtFeet)
                {
                    int oldZ = Z;

                    Z -= (zModulo == 0) ? PhysicsConstants.SEGMENT_STEPS : zModulo;
                    if (QueuedSkill != QueuedSkill.Digger && QueuedSkill != QueuedSkill.BasherBegin && QueuedSkill != QueuedSkill.MinerBegin && QueuedSkill != QueuedSkill.BuilderBegin)
                        if (QueuedSkill != QueuedSkill.MinerContinue || AdjustedPositionOnBlock != 1)
                            QueuedSkill = QueuedSkill.None;

                    if (!IsSolidAtFeet)
                    {
                        Z = oldZ;
                        Transition(LemmingAction.Faller);
                        FallForward = !TurnedLastUpdate;
                    }
                }
            }
        }

        private bool CheckAndApplyDeflector()
        {
            if (IsAtBlockMiddle)
            {
                var aboveWallBlock = GetBaseWallPosition();
                aboveWallBlock.Z++;
                var block = BlockStructure[aboveWallBlock.X, aboveWallBlock.Y, aboveWallBlock.Z];
                if (block != null && block.Shape == BlockShape.Deflector)
                {
                    Direction = CalculatePostDeflectorDirection(block.Rotations);
                    SkipNextMovement = true;
                    SkipNextBlockCheck = true;

                    if (QueuedSkill == QueuedSkill.BuilderContinue || QueuedSkill == QueuedSkill.BasherOverflow ||
                        QueuedSkill == QueuedSkill.BasherContinue || QueuedSkill == QueuedSkill.MinerContinue)
                        QueuedSkill = QueuedSkill.None;

                    return true;
                }
            }

            return false;
        }

        private void HandleWalkerDirectionChanges()
        {
            if (!SkipNextBlockCheck && GetIsWallInFront())
            {
                if (CheckAndApplyDeflector())
                {
                    TurnedLastUpdate = true;
                    return;
                }

                if (QueuedSkill != QueuedSkill.Digger)
                    QueuedSkill = QueuedSkill.None;

                Direction = Utils.InvertDirection(Direction);
                TurnedLastUpdate = false;
                return;
            }
            else
                SkipNextBlockCheck = false;

            if (IsAtBlockMiddle)
            {
                var turnerDir = CheckRelevantTurnerDirection();
                if (turnerDir != TurnerDirection.None)
                {
                    if (QueuedSkill == QueuedSkill.BuilderContinue || QueuedSkill == QueuedSkill.BasherOverflow ||
                        QueuedSkill == QueuedSkill.BasherContinue || QueuedSkill == QueuedSkill.MinerContinue)
                        QueuedSkill = QueuedSkill.None;

                    Direction = Utils.RotateDirection(Direction, turnerDir == TurnerDirection.Left ? 1 : -1);
                    SkipNextMovement = true;
                    TurnedLastUpdate = true;
                    return;
                }

                if (!SkipNextSplitter)
                {
                    var checkPos = GetAscendableBlockPosition();
                    var block = BlockStructure[checkPos.X, checkPos.Y, checkPos.Z];
                    var aboveBlock = BlockStructure[checkPos.X, checkPos.Y, checkPos.Z + 1];
                    if (block != null && block.Physics == BlockPhysics.Splitter)
                    {
                        var turnDir = GameEngine.GetSplitterSwitchState(checkPos, Direction);
                        GameEngine.FlipSplitterState(checkPos, Direction);
                        Direction = Utils.RotateDirection(Direction, turnDir == TurnerDirection.Left ? 1 : -1);

                        SkipNextMovement = true;
                        SkipNextSplitter = true;
                        TurnedLastUpdate = true;
                        return;
                    }
                    else if (aboveBlock != null && aboveBlock.Physics == BlockPhysics.Splitter)
                    {
                        checkPos.Z += 1;
                        var turnDir = GameEngine.GetSplitterSwitchState(checkPos, Direction);
                        GameEngine.FlipSplitterState(checkPos, Direction);
                        Direction = Utils.RotateDirection(Direction, turnDir == TurnerDirection.Left ? 1 : -1);

                        SkipNextMovement = true;
                        SkipNextSplitter = true;
                        TurnedLastUpdate = true;
                        return;
                    }
                }
                else
                    SkipNextSplitter = false;
            }
            else
                SkipNextSplitter = false;

            if (AdjustedPositionOnBlock == PhysicsConstants.THREE_QUARTER_BLOCK_STEPS && CheckForRelevantBlocker())
            {
                Direction = Utils.InvertDirection(Direction);
                SkipNextMovement = true;

                if (QueuedSkill == QueuedSkill.BuilderContinue || QueuedSkill == QueuedSkill.BasherOverflow ||
                    QueuedSkill == QueuedSkill.BasherContinue || QueuedSkill == QueuedSkill.MinerContinue)
                    QueuedSkill = QueuedSkill.None;
            }

            TurnedLastUpdate = false;
        }

        private void HandleSecondRoundSkillTransitionsAndDequeueing()
        {
            switch (QueuedSkill)
            {
                case QueuedSkill.BuilderBegin:
                case QueuedSkill.BuilderContinue:
                    if (QueuedSkill == QueuedSkill.BuilderBegin)
                        BuilderBricksPlaced = 0;
                    if (IsAtBlockMiddle || IsOnBlockEdge)
                        Transition(LemmingAction.Builder);
                    return;
                case QueuedSkill.Shrugger:
                    Transition(LemmingAction.Shrugger);
                    return;
                case QueuedSkill.Digger:
                    if (IsAtBlockMiddle)
                        Transition(LemmingAction.Digger);
                    return;
            }
        }

        #endregion

        #region Move (main phase) Behaviors (non-Walker / general)

        private void CheckHazards(bool alsoCheckNonHazards, bool forceIgnoreSlippery = false)
        {
            var blockPos = GetStandingOnBlockPosition();
            var block = BlockStructure[blockPos.X, blockPos.Y, blockPos.Z];

            var boundaries = GameEngine.Level.LevelInfo.BoundaryPoints;

            if (CanRespondToHazards)
            {
                if (blockPos.X < boundaries[Direction.XMinus] ||
                    blockPos.X > boundaries[Direction.XPlus] ||
                    blockPos.Y < boundaries[Direction.YMinus] ||
                    blockPos.Y > boundaries[Direction.YPlus] ||
                    blockPos.Z > boundaries[Direction.ZPlus])
                {
                    Transition(LemmingAction.Zapper);
                    return;
                }

                if (IsAtBlockMiddle)
                {
                    var fields = GameEngine.EffectFields.Where(field => field.Effect == FieldType.Trap && field.Position == GetAscendableBlockPosition());
                    foreach (var field in fields)
                    {
                        var trapSource = GameEngine.EntityInstances.First(ei => ei.UniqueIdentifier == field.SourceIdentifier);
                        if (!trapSource.Activated)
                        {
                            trapSource.Activated = true;

                            if (trapSource.SoundEffect != "")
                                QueueSound(trapSource.SoundEffect);

                            if (trapSource.Effect == EntityEffect.LinkedTrap)
                            {
                                if (trapSource.Directionality == EntityDirectionality.None)
                                {
                                    foreach (var entity in GameEngine.EntityInstances.Where(e => e.Identifier == trapSource.Identifier && e != trapSource))
                                        entity.Activated = true;
                                }
                                else
                                {
                                    int offsetHigher = 1;
                                    int offsetLower = 1;
                                    bool continueHigher = true;
                                    bool continueLower = true;

                                    bool checkAlongY = Utils.ModuloNegativeAdjusted(trapSource.Rotations, 2) == 0;

                                    while (continueHigher || continueLower)
                                    {
                                        if (continueHigher)
                                        {
                                            var higherEntity = GameEngine.EntityInstances.FirstOrDefault(e =>
                                                e.Identifier == trapSource.Identifier &&
                                                Utils.ModuloNegativeAdjusted(e.Rotations, 2) == Utils.ModuloNegativeAdjusted(trapSource.Rotations, 2) &&
                                                e.Position.X == trapSource.Position.X + (checkAlongY ? 0 : offsetHigher) &&
                                                e.Position.Y == trapSource.Position.Y + (checkAlongY ? offsetHigher : 0) &&
                                                e.Position.Z == trapSource.Position.Z
                                            );

                                            if (higherEntity == null)
                                                continueHigher = false;
                                            else
                                                higherEntity.Activated = true;

                                            offsetHigher++;
                                        }

                                        if (continueLower)
                                        {
                                            var lowerEntity = GameEngine.EntityInstances.FirstOrDefault(e =>
                                                   e.Identifier == trapSource.Identifier &&
                                                   Utils.ModuloNegativeAdjusted(e.Rotations, 2) == Utils.ModuloNegativeAdjusted(trapSource.Rotations, 2) &&
                                                   e.Position.X == trapSource.Position.X - (checkAlongY ? 0 : offsetLower) &&
                                                   e.Position.Y == trapSource.Position.Y - (checkAlongY ? offsetLower : 0) &&
                                                   e.Position.Z == trapSource.Position.Z
                                               );

                                            if (lowerEntity == null)
                                                continueLower = false;
                                            else
                                                lowerEntity.Activated = true;

                                            offsetLower++;
                                        }
                                    }
                                }
                            }

                            switch (trapSource.KillStyle)
                            {
                                case TrapKillStyle.Instant:
                                    LemmingRemoved = true;
                                    LemmingRemoveReason = LemmingRemoveReason.Trap;
                                    return;
                                case TrapKillStyle.Spin:
                                    Transition(LemmingAction.DeathSpin);
                                    return;
                                case TrapKillStyle.Electrocute:
                                    Transition(LemmingAction.Zapper);
                                    return;
                                default:
                                    throw new NotImplementedException();
                            }
                        }
                    }
                }
            }

            if (block != null)
            {
                if (CanRespondToHazards)
                    switch (block.Physics)
                    {
                        case BlockPhysics.Water:
                            Transition(LemmingAction.Drowner);
                            return;
                        case BlockPhysics.Electrocute:
                            Transition(LemmingAction.Zapper);
                            return;
                    }

                if (CanRespondToSlippery && !forceIgnoreSlippery && block.Slippery && IsSolidAtFeet)
                {
                    Transition(LemmingAction.Slider);
                    return;
                }
            }

            if (Z <= PhysicsConstants.LAND_HEIGHT && (block == null || PhysicsConstants.NonSolidBlockTypes.Contains(block.Physics)))
            {
                switch (GameEngine.GetFloorEffectAt(X, Y))
                {
                    case LevelFloorEffect.Water:
                        if (CanRespondToHazards)
                        {
                            Transition(LemmingAction.Drowner);
                            return;
                        }
                        break;
                    case LevelFloorEffect.Electrocute:
                        if (CanRespondToHazards)
                        {
                            Transition(LemmingAction.Zapper);
                            return;
                        }
                        break;
                    case LevelFloorEffect.Slippery:
                        if (CanRespondToSlippery && !forceIgnoreSlippery)
                        {
                            Transition(LemmingAction.Slider);
                            return;
                        }
                        break;
                }
            }

            if (alsoCheckNonHazards)
                CheckNonHazardInteractives();
        }

        private void CheckNonHazardInteractives()
        {
            if (CanRespondToNonHazardInteractives && IsAtBlockMiddle)
            {
                var blockPos = GetAscendableBlockPosition();
                var fields = GameEngine.EffectFields.Where(ef => ef.Position == blockPos);

                foreach (var field in fields)
                {
                    switch (field.Effect)
                    {
                        case FieldType.Blocker:
                        case FieldType.Turner:
                        case FieldType.Trap:
                            // Ignore - handled elsewhere.
                            break;
                        case FieldType.Teleporter:
                            {
                                var entity = GameEngine.EntityInstances.First(e => e.UniqueIdentifier == field.SourceIdentifier);
                                var target = GameEngine.EntityInstances.First(e => (e.Position == field.Destination) && (e.Effect == EntityEffect.Teleporter || e.Effect == EntityEffect.Receiver));

                                if (!entity.Activated && !target.Activated)
                                {
                                    Transition(LemmingAction.Warper);
                                    TransporterSource = entity.Position;
                                    TransporterTarget = field.Destination;
                                    TransporterSourceIdentifier = entity.UniqueIdentifier;
                                    TransporterTargetIdentifier = target.UniqueIdentifier;
                                    entity.Activated = true;
                                    target.Activated = true;

                                    QueueSound(SoundNames.TELEPORTER);

                                    return;
                                }
                            }
                            break;
                        case FieldType.RopeSlide:
                            {
                                if (!GameEngine.Lemmings.Any(lem => lem.Action == LemmingAction.Zipper && lem.TransporterSourceIdentifier == field.SourceIdentifier))
                                {
                                    var zipUnitTarget = Utils.BlocksToUnits(field.Destination, true, false);
                                    int directionOnX = Math.Sign(zipUnitTarget.X - X) * ForwardDX;
                                    int directionOnY = Math.Sign(zipUnitTarget.Y - Y) * ForwardDY;

                                    if ((directionOnX > 0 || directionOnY > 0) && (directionOnX >= 0 && directionOnY >= 0))
                                    {
                                        var entity = GameEngine.EntityInstances.First(e => e.UniqueIdentifier == field.SourceIdentifier);
                                        Transition(LemmingAction.Zipper);
                                        TransporterSource = entity.Position;
                                        TransporterTarget = field.Destination;
                                        TransporterSourceIdentifier = entity.UniqueIdentifier;

                                        var zipUnitSource = Utils.BlocksToUnits(TransporterSource, true, false);

                                        TransportIterations =
                                            (Math.Abs(zipUnitTarget.X - zipUnitSource.X) + Math.Abs(zipUnitTarget.Y - zipUnitSource.Y) + Math.Abs(zipUnitTarget.Z - zipUnitSource.Z))
                                            * ZIPPING_DURATION_PER_BLOCK / PhysicsConstants.BLOCK_STEPS;
                                    }
                                }
                            }
                            break;
                        case FieldType.Spring:
                            {
                                if (!GameEngine.Lemmings.Any(lem => lem.Action == LemmingAction.Springer && lem.TransporterSourceIdentifier == field.SourceIdentifier))
                                {
                                    var entity = GameEngine.EntityInstances.First(e => e.UniqueIdentifier == field.SourceIdentifier);

                                    if (!entity.Activated)
                                    {
                                        Transition(LemmingAction.Springer);
                                        TransporterSource = entity.Position;
                                        TransporterTarget = field.Destination;
                                        TransporterSourceIdentifier = entity.UniqueIdentifier;

                                        var springUnitSource = Utils.BlocksToUnits(entity.Position, true, false);
                                        var springUnitTarget = Utils.BlocksToUnits(field.Destination, true, false);

                                        TransportIterations =
                                                (Math.Abs(springUnitTarget.X - springUnitSource.X) + Math.Abs(springUnitTarget.Y - springUnitSource.Y) + Math.Abs(springUnitTarget.Z - springUnitSource.Z))
                                                * SPRING_DURATION_PER_BLOCK / PhysicsConstants.BLOCK_STEPS;

                                        BouncePeakZ = Math.Max(springUnitSource.Z, springUnitTarget.Z) + SPRING_PEAK_HEIGHT;

                                        QueueSound(SoundNames.SPRING);

                                        entity.Activated = true;
                                    }
                                }
                            }
                            break;
                        case FieldType.Trampoline:
                            {
                                if (Action != LemmingAction.Faller && Action != LemmingAction.Floater && Action != LemmingAction.Bouncer)
                                {
                                    var entity = GameEngine.EntityInstances.First(e => e.UniqueIdentifier == field.SourceIdentifier);

                                    entity.Activated = true;
                                    entity.Iteration = 0;

                                    QueueSound(SoundNames.TRAMPOLINE);

                                    if (entity.Effect == EntityEffect.TrampolineStrong)
                                    {
                                        TransporterSource = entity.Position;
                                        BouncePeakZ = Z + PhysicsConstants.SEGMENT_STEPS * 4;
                                        Transition(LemmingAction.Bouncer);
                                    }
                                }
                            }
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }
            }
        }

        private void SetBounceIterations()
        {
            int height = BouncePeakZ - Z;
            TransportIterations = (int)MathF.Round(height / (float)TRAMPOLINE_FORWARD_PER_ITERATION) + 1;
        }

        private void CheckExit(bool forceAllowExit = false)
        {
            if (!IsOnBlockEdge || (!CanExit && !forceAllowExit))
                return;

            if (Action == LemmingAction.Walker && (QueuedSkill == QueuedSkill.Blocker || QueuedSkill == QueuedSkill.Turner))
                return;

            var wallPos = GetBaseWallPosition();
            wallPos.Z += 1;

            if (CheckExitOnRelevantFace(BlockStructure[wallPos.X, wallPos.Y, wallPos.Z]))
            {
                if (CheckExitOnRelevantFace(BlockStructure[wallPos.X, wallPos.Y, wallPos.Z - 1]))
                {
                    Transition(LemmingAction.Exiter);
                    GameEngine.TriggerExit(wallPos, Utils.InvertDirection(Direction));
                }
                else if (CheckExitOnRelevantFace(BlockStructure[wallPos.X, wallPos.Y, wallPos.Z + 1]))
                {
                    SnapVerticalPositionDownwards();
                    Z += PhysicsConstants.SEGMENT_STEPS;
                    Transition(LemmingAction.Exiter);
                    GameEngine.TriggerExit(wallPos, Utils.InvertDirection(Direction));
                }
            }
        }

        private bool CheckExitOnRelevantFace(BlockInfo? block)
        {
            if (block == null)
                return false;

            var adjustedFace = Utils.RotateDirection(Utils.InvertDirection(Direction), -block.Rotations);
            return PhysicsConstants.ExitFaceTypes.Contains(block.Faces[adjustedFace].Physics);
        }

        private void RemoveAfterFrames(int removeFrameCount, LemmingRemoveReason reason)
        {
            if (CurrentActionIterations == removeFrameCount)
            {
                LemmingRemoved = true;
                LemmingRemoveReason = reason;
            }
        }

        private void WalkAfterFrames(int walkFrameCount)
        {
            if (CurrentActionIterations == walkFrameCount)
                Transition(LemmingAction.Walker, QueuedSkill.None);
        }

        private void FallIfBlockRemoved(bool withMomentum, bool forceTurnAround, bool allowExit = false)
        {
            if (!IsSolidAtFeet)
            {
                if (allowExit)
                {
                    CheckExit(true);
                    if (Action == LemmingAction.Exiter)
                        return;
                }

                Transition(LemmingAction.Faller);
                FallForward = withMomentum;
                if (forceTurnAround || GetIsWallInFront())
                {
                    Direction = Utils.InvertDirection(Direction);
                    X += ForwardDX;
                    Y += ForwardDY;
                }
            }
        }

        private void UpdateFaller()
        {
            if (CurrentActionIterations > 0 && CurrentActionIterations % FALLER_MOMENTUM_INCREASE_FREQUENCY == 0 && FallMomentum < FALLER_MOMENTUM_CAP)
                FallMomentum++;

            if (FallForward &&
                (DistanceMovedForwardAsFaller < FALLER_FORWARD_SLOWDOWN_POINT ||
                (DistanceMovedForwardAsFaller < FALLER_FORWARD_DISTANCE_CAP && CurrentActionIterations % 2 == 0))
                )
            {
                int oldX = X;
                int oldY = Y;
                X += ForwardDX;
                Y += ForwardDY;
                DistanceMovedForwardAsFaller++;

                if (GetIsWallInFront())
                {
                    X = oldX;
                    Y = oldY;
                }
            }

            if (IsSolidAtFeet)
            {
                HandleEndFall();
                return;
            }

            for (int i = 0; i < FallMomentum; i++)
            {
                Z -= 1;
                DistanceFallen++;

                if (DistanceFallen == FALLER_STUN_DISTANCE)
                    QueueSound(SoundNames.FALLER_SCREAM);

                if (IsSolidAtFeet)
                {
                    HandleEndFall();
                    return;
                }

                if (IsFloater && DistanceFallen >= FLOATER_BEGIN_FALL_DISTANCE)
                {
                    Transition(LemmingAction.Floater);
                    return;
                }
            }
        }

        private void HandleEndFall()
        {
            CheckHazards(true, true);

            if (Action == LemmingAction.Faller)
            {
                var trampolineCheckPos = GetAscendableBlockPosition();
                var field = GameEngine.EffectFields.FirstOrDefault(e => e.Position == trampolineCheckPos && e.Effect == FieldType.Trampoline);

                if (field != default)
                {
                    var entity = GameEngine.EntityInstances.First(e => e.UniqueIdentifier == field.SourceIdentifier);

                    entity.Activated = true;
                    entity.Iteration = 0;

                    QueueSound(SoundNames.TRAMPOLINE);

                    BouncePeakZ = Z + DistanceFallen + (entity.Effect == EntityEffect.TrampolineStrong ? STRONG_TRAMPOLINE_BOOST : WEAK_TRAMPOLINE_BOOST);
                    TransporterSource = entity.Position;

                    if (BouncePeakZ > Z)
                        Transition(LemmingAction.Bouncer);
                    else
                        Transition(LemmingAction.Walker);
                }
                else
                {
                    var landingBlockPos = GetStandingOnBlockPosition();
                    var landingBlock = BlockStructure[landingBlockPos.X, landingBlockPos.Y, landingBlockPos.Z];

                    if (landingBlock != null && landingBlock.Antisplat)
                    {
                        if (landingBlock != null && landingBlock.Slippery)
                            Transition(LemmingAction.Slider);
                        else
                            Transition(LemmingAction.Walker);
                    }
                    else if (DistanceFallen >= FALLER_SPLAT_DISTANCE)
                    {
                        LemmingRemoved = true;
                        LemmingRemoveReason = LemmingRemoveReason.Splat;
                        QueueSound(SoundNames.GENERAL_DIE);
                    }
                    else if (DistanceFallen >= FALLER_STUN_DISTANCE)
                        Transition(LemmingAction.Stunned);
                    else
                    {
                        if (landingBlock != null && landingBlock.Slippery)
                            Transition(LemmingAction.Slider);
                        else
                            Transition(LemmingAction.Walker);
                    }
                }
            }
        }

        private void UpdateClimber()
        {
            var frontBlock = GetBaseWallPosition();
            var relativePos = GetPositionOnBlock(frontBlock);

            if (!IsBlockSolidAt(frontBlock, new Point3D(relativePos.X, relativePos.Y, PhysicsConstants.SEGMENT_STEPS)))
            {
                var aboveFrontBlock = new Point3D(frontBlock.X, frontBlock.Y, frontBlock.Z + 1);

                if (IsBlockSolidAt(aboveFrontBlock, new Point3D(relativePos.X, relativePos.Y, 0)))
                {
                    Transition(LemmingAction.Faller);
                    FallForward = true;
                    Direction = Utils.InvertDirection(Direction);
                    X += ForwardDX;
                    Y += ForwardDY;
                }
                else
                    Transition(LemmingAction.Walker);
            }
            else
            {
                if (IsSolidAtHead)
                {
                    Transition(LemmingAction.Faller);
                    FallForward = true;
                    Direction = Utils.InvertDirection(Direction);
                    X += ForwardDX;
                    Y += ForwardDY;
                }
                else
                {
                    Z += 1;
                    if (Utils.ModuloNegativeAdjusted(Z, PhysicsConstants.SEGMENT_STEPS) == PhysicsConstants.HALF_SEGMENT_STEPS)
                        QueueSound(SoundNames.CLIMBER);
                }
            }
        }

        private void UpdateFloater()
        {
            if (IsSolidAtFeet)
            {
                var trampolineCheckPos = GetAscendableBlockPosition();
                var field = GameEngine.EffectFields.FirstOrDefault(e => e.Position == trampolineCheckPos && e.Effect == FieldType.Trampoline);

                bool bounced = false;

                if (field != default)
                {
                    var entity = GameEngine.EntityInstances.First(e => e.UniqueIdentifier == field.SourceIdentifier);

                    entity.Activated = true;
                    entity.Iteration = 0;

                    QueueSound(SoundNames.TRAMPOLINE);

                    BouncePeakZ = Z + (entity.Effect == EntityEffect.TrampolineStrong ? STRONG_TRAMPOLINE_BOOST : WEAK_TRAMPOLINE_BOOST);
                    TransporterSource = entity.Position;

                    if (BouncePeakZ > Z)
                    {
                        Transition(LemmingAction.Bouncer);
                        bounced = true;
                    }
                }

                if (!bounced)
                {
                    var onBlockPos = GetStandingOnBlockPosition();
                    var onBlock = BlockStructure[onBlockPos.X, onBlockPos.Y, onBlockPos.Z];
                    if (onBlock != null && onBlock.Slippery)
                        Transition(LemmingAction.Slider);
                    else
                        Transition(LemmingAction.Walker);
                }
            }
            else
                Z -= 1;
        }

        private void UpdateBomber()
        {
            for (int i = 0; i < BOMBER_DOWNWARDS_SPEED; i++)
                if (IsSolidAtFeet)
                    break;
                else
                {
                    if (ExClimberBomber)
                    {
                        Direction = Utils.InvertDirection(Direction);
                        bool solidReverse = IsSolidAtFeet;
                        Direction = Utils.InvertDirection(Direction);

                        if (solidReverse)
                            break;
                    }

                    Z -= 1;
                }

            RemoveAfterFrames(BOMBER_DESTRUCTION_TIME, LemmingRemoveReason.Bomber);
        }

        private void UpdateBuilder()
        {
            if (CurrentActionIterations == BUILDER_CONTINUE_FRAME)
            {
                if (BuilderBrickCollision)
                    Transition(LemmingAction.Walker, QueuedSkill.None);
                else if (BuilderBricksPlaced == BUILDER_BRICK_COUNT)
                    Transition(LemmingAction.Walker, QueuedSkill.Shrugger);
                else
                    Transition(LemmingAction.Walker, QueuedSkill.BuilderContinue);
            }

            FallIfBlockRemoved(false, false, true);
        }

        private void UpdateBasher()
        {
            if (CurrentActionIterations == 0)
            {
                // Steel check
                var baseFront = GetBaseWallPosition();
                for (int i = BASHER_DESTRUCTION_BOTTOM; i < BASHER_DESTRUCTION_TOP; i++)
                {
                    var block = BlockStructure[baseFront.X, baseFront.Y, baseFront.Z + i];
                    if ((baseFront.Z + i) < 1 ||
                        (block != null &&
                        (
                            PhysicsConstants.IndestructibleBlockTypes.Contains(block.Physics) ||
                            (block.OneWayDirection.HasValue && block.OneWayDirection.Value != Utils.RotateDirection(Direction, -block.Rotations))
                        )))
                    {
                        QueueSound(SoundNames.STEEL);
                        if (GetIsWallInFront())
                        {
                            if (IsClimber && GetIsClimbableWallInFront())
                            {
                                Transition(LemmingAction.Climber);
                            }
                            else
                            {
                                Transition(LemmingAction.Walker, QueuedSkill.None);
                                if (!CheckAndApplyDeflector())
                                    Direction = Utils.InvertDirection(Direction);
                                X += ForwardDX;
                                Y += ForwardDY;
                            }
                        }
                        else
                            Transition(LemmingAction.Walker, QueuedSkill.None);

                        return;
                    }
                }
            }
            else if (CurrentActionIterations % BASHER_SOUND_INTERVAL == 0)
                QueueSound(SoundNames.BASHER);

            FallIfBlockRemoved(true, false);
        }

        private void UpdateMiner()
        {
            if (CurrentActionIterations == 0)
            {
                // Steel check
                var baseFront = GetBaseWallPosition();
                for (int i = MINER_DESTRUCTION_BOTTOM; i < MINER_DESTRUCTION_TOP; i++)
                {
                    var block = BlockStructure[baseFront.X, baseFront.Y, baseFront.Z + i];
                    if ((baseFront.Z + i) < 1 ||
                        (block != null &&
                        (
                            PhysicsConstants.IndestructibleBlockTypes.Contains(block.Physics) ||
                            (block.OneWayDirection.HasValue && block.OneWayDirection.Value != Utils.RotateDirection(Direction, -block.Rotations))
                        )))
                    {
                        QueueSound(SoundNames.STEEL);
                        if (GetIsWallInFront())
                        {
                            if (IsClimber && GetIsClimbableWallInFront())
                            {
                                Transition(LemmingAction.Climber);
                            }
                            else
                            {
                                Transition(LemmingAction.Walker, QueuedSkill.None);
                                if (!CheckAndApplyDeflector())
                                    Direction = Utils.InvertDirection(Direction);
                                X += ForwardDX;
                                Y += ForwardDY;
                            }
                        }
                        else
                            Transition(LemmingAction.Walker, QueuedSkill.None);

                        return;
                    }
                }
            }
            else if (CurrentActionIterations % MINER_SOUND_INTERVAL == 0)
                QueueSound(SoundNames.MINER);

            FallIfBlockRemoved(true, false);
        }

        private void UpdateDigger()
        {
            if (CurrentActionIterations % DIGGER_DESTRUCTION_TIME == 0)
            {
                // Steel check
                var basePos = GetDiggerTargetBlockPosition();
                for (int i = DIGGER_DESTRUCTION_BOTTOM; i < DIGGER_DESTRUCTION_TOP - 1; i++) // The digger does NOT steel-check the highest affected block.
                {
                    var block = BlockStructure[basePos.X, basePos.Y, basePos.Z + i];
                    if (basePos.Z + i < 1 ||
                        (block != null &&
                        (
                            PhysicsConstants.IndestructibleBlockTypes.Contains(block.Physics) ||
                            block.OneWayDirection.HasValue
                        )))
                    {
                        QueueSound(SoundNames.STEEL);
                        Transition(LemmingAction.Walker, QueuedSkill.None);

                        return;
                    }
                }
            }

            if (CurrentActionIterations > 0 &&
                CurrentActionIterations % DIGGER_SOUND_INTERVAL == 0)
                QueueSound(SoundNames.DIGGER);

            FallIfBlockRemoved(false, false);
        }

        private void UpdateWarper()
        {
            if (CurrentActionIterations == WARPING_DURATION / 2)
            {
                Position = Utils.BlocksToUnits(TransporterTarget, true, false);
                GameEngine.EntityInstances.First(e => (e.Position == TransporterTarget) && (e.Effect == EntityEffect.Teleporter || e.Effect == EntityEffect.Receiver)).Activated = true;
            }
            else if (CurrentActionIterations == WARPING_DURATION)
            {
                Transition(LemmingAction.Walker);
                SkipNextMovement = true;
            }
        }

        private void UpdateZipper()
        {
            if (CurrentActionIterations == TransportIterations)
            {
                Position = Utils.BlocksToUnits(TransporterTarget, true, false);
                if (IsSolidAtFeet)
                {
                    Transition(LemmingAction.Walker);
                    SkipNextMovement = true;
                }
                else
                {
                    Transition(LemmingAction.Faller);
                    FallForward = false;
                }
            }
            else
            {
                var srcPos = Utils.BlocksToUnits(TransporterSource, true, false);
                var dstPos = Utils.BlocksToUnits(TransporterTarget, true, false);

                X = srcPos.X + ((dstPos.X - srcPos.X) * CurrentActionIterations / TransportIterations);
                Y = srcPos.Y + ((dstPos.Y - srcPos.Y) * CurrentActionIterations / TransportIterations);
                Z = srcPos.Z + ((dstPos.Z - srcPos.Z) * CurrentActionIterations / TransportIterations);
            }
        }

        private void UpdateSpringer()
        {
            if (CurrentActionIterations == TransportIterations)
            {
                Position = Utils.BlocksToUnits(TransporterTarget, true, false);
                if (IsSolidAtFeet)
                {
                    Transition(LemmingAction.Walker);
                    SkipNextMovement = true;
                }
                else
                {
                    Transition(LemmingAction.Faller);
                    FallForward = false;
                }
            }
            else
            {
                var srcPos = Utils.BlocksToUnits(TransporterSource, true, false);
                var dstPos = Utils.BlocksToUnits(TransporterTarget, true, false);

                X = srcPos.X + ((dstPos.X - srcPos.X) * CurrentActionIterations / TransportIterations);
                Y = srcPos.Y + ((dstPos.Y - srcPos.Y) * CurrentActionIterations / TransportIterations);

                float halfTransportIterations = TransportIterations / 2f;
                bool atOrPastHalf = (CurrentActionIterations >= halfTransportIterations);
                int endpointZ = atOrPastHalf ? dstPos.Z : srcPos.Z;
                float localIterations = Math.Clamp(CurrentActionIterations - halfTransportIterations, -halfTransportIterations, halfTransportIterations);
                float heightRatio = MathF.Pow(localIterations / halfTransportIterations, 2);

                Z = BouncePeakZ + (int)Math.Round((endpointZ - BouncePeakZ) * heightRatio);

                var wallCheckPos = GetBaseWallPosition();
                var posOnWall = GetPositionOnBlock(wallCheckPos);

                posOnWall.Z += PhysicsConstants.HALF_SEGMENT_STEPS;

                int i = 0;
                while (i < SPRING_COLLISION_CHECK_POINTS)
                {
                    posOnWall.Z += PhysicsConstants.HALF_SEGMENT_STEPS / 2;
                    if (posOnWall.Z > PhysicsConstants.SEGMENT_STEPS)
                    {
                        posOnWall.Z -= PhysicsConstants.SEGMENT_STEPS;
                        wallCheckPos.Z += 1;
                    }

                    if (IsBlockSolidAt(wallCheckPos, posOnWall))
                    {
                        LemmingRemoved = true;
                        LemmingRemoveReason = LemmingRemoveReason.Splat;
                        QueueSound(SoundNames.GENERAL_DIE);
                        return;
                    }

                    if (posOnWall.Z == PhysicsConstants.SEGMENT_STEPS)
                    {
                        posOnWall.Z = 0;
                        wallCheckPos.Z += 1;
                    }
                    else
                        i++; // We want to redo the same check, but for the other block, if we tested exactly on the boundary.
                }
            }
        }

        private void UpdateBouncer()
        {
            float halfTransportIterations = TransportIterations / 2f;
            var srcPos = Utils.BlocksToUnits(TransporterSource, true, false);

            for (int i = 0; i < TRAMPOLINE_FORWARD_PER_ITERATION; i++)
            {
                X += ForwardDX;
                Y += ForwardDY;

                float localIterations = Math.Max(CurrentActionIterations - halfTransportIterations, -halfTransportIterations);
                float heightRatio = MathF.Pow(localIterations / halfTransportIterations, 2);

                Z = BouncePeakZ + (int)Math.Round((srcPos.Z - BouncePeakZ) * heightRatio);

                CheckHazards(true);

                if (Action == LemmingAction.Bouncer)
                    CheckExit();

                if (Action == LemmingAction.Bouncer)
                {
                    if (GetIsWallInFront())
                    {
                        if (CheckAndApplyDeflector())
                            break;

                        Direction = Utils.InvertDirection(Direction);
                        X += ForwardDX;
                        Y += ForwardDY;
                        FallForward = true;
                        Transition(LemmingAction.Faller);
                        break;
                    }

                    if (!SkipNextBlockCheck && IsSolidAtHead)
                    {
                        Transition(LemmingAction.Faller);
                        break;
                    }

                    SkipNextBlockCheck = false;

                    if (IsSolidAtFeet && CurrentActionIterations > 0)
                    {
                        var trampolineCheckPos = GetAscendableBlockPosition();
                        var field = GameEngine.EffectFields.FirstOrDefault(e => e.Position == trampolineCheckPos && e.Effect == FieldType.Trampoline);

                        bool newBounce = false;

                        if (field != default)
                        {
                            var entity = GameEngine.EntityInstances.First(e => e.UniqueIdentifier == field.SourceIdentifier);

                            entity.Activated = true;
                            entity.Iteration = 0;

                            QueueSound(SoundNames.TRAMPOLINE);

                            BouncePeakZ = BouncePeakZ + (entity.Effect == EntityEffect.TrampolineStrong ? STRONG_TRAMPOLINE_BOOST : WEAK_TRAMPOLINE_BOOST);
                            TransporterSource = entity.Position;

                            if (BouncePeakZ > Z)
                            {
                                Transition(LemmingAction.Bouncer); // Resets CurrentActionIterations and recalculates TransportIterations
                                newBounce = true;
                            }
                        }

                        if (!newBounce)
                        {
                            var blockPos = GetStandingOnBlockPosition();
                            var posOnBlock = GetPositionOnBlock(blockPos);
                            var block = BlockStructure[blockPos.X, blockPos.Y, blockPos.Z];

                            Z -= posOnBlock.Z;
                            while (IsBlockSolidAt(blockPos, new Point3D(posOnBlock.X, posOnBlock.Y, posOnBlock.Z + 1)))
                                posOnBlock.Z++;
                            Z += posOnBlock.Z;

                            if (block != null && block.Antisplat)
                            {
                                if (block.Slippery)
                                    Transition(LemmingAction.Slider);
                                else
                                    Transition(LemmingAction.Walker);
                            }
                            else if (BouncePeakZ - Z >= FALLER_SPLAT_DISTANCE)
                            {
                                LemmingRemoved = true;
                                LemmingRemoveReason = LemmingRemoveReason.Splat;
                                QueueSound(SoundNames.GENERAL_DIE);
                            }
                            else if (BouncePeakZ - Z >= FALLER_STUN_DISTANCE)
                                Transition(LemmingAction.Stunned);
                            else
                            {
                                if (block != null && block.Slippery)
                                    Transition(LemmingAction.Slider);
                                else
                                    Transition(LemmingAction.Walker);
                            }
                        }

                        break;
                    }
                }

                if (Action != LemmingAction.Bouncer)
                    break;
            }
        }

        #endregion

        #region Post-Move Behaviors

        private void PlaceBuilderBrick()
        {
            if (CurrentActionIterations == BUILDER_PLACE_FRAME)
            {
                var brickPos = GetBaseWallPosition();
                var brickDir = IsOnBlockEdge ? Utils.InvertDirection(Direction) : Direction;
                if (GameEngine.BuilderBricks.Contains(new Point3DAndDirection(brickPos, brickDir)))
                    BuilderBrickCollision = true;
                else
                    GameEngine.BuilderBricks.Add(new Point3DAndDirection(brickPos, brickDir));

                QueueSound(SoundNames.BUILDER_PLACE);
                BuilderBricksPlaced++;
            }
        }

        #endregion

        #region Utility Functions

        /// <summary>
        /// Used to move a lemming downwards after an action that removed (or maybe removed) blocks below him.
        /// </summary>
        private void SnapVerticalPositionDownwards()
        {
            int excessZ = Utils.ModuloNegativeAdjusted(Z, PhysicsConstants.SEGMENT_STEPS);

            if (excessZ == 0)
                return;

            var ascendPos = GetAscendableBlockPosition();
            var ascendBlock = BlockStructure[ascendPos.X, ascendPos.Y, ascendPos.Z];
            var relativePos = GetPositionOnBlock(ascendPos);
            if (ascendBlock != null)
            {
                for (int i = excessZ; i >= 0; i--)
                    if (IsBlockSolidAt(ascendPos, new Point3D(relativePos.X, relativePos.Y, i)))
                    {
                        Z = Z - (Utils.ModuloNegativeAdjusted(Z, PhysicsConstants.SEGMENT_STEPS)) + i;
                        return;
                    }
            }

            if (ascendPos.Z <= 1)
            {
                Z = PhysicsConstants.SEGMENT_STEPS;
                return;
            }

            if (IsBlockSolidAt(
                new Point3D(ascendPos.X, ascendPos.Y, ascendPos.Z - 1),
                new Point3D(relativePos.X, relativePos.Y, PhysicsConstants.SEGMENT_STEPS)
                ))
            {
                Z -= excessZ;
                return;
            }
        }

        public int ForwardDX => Direction switch
        {
            Direction.XPlus => 1,
            Direction.XMinus => -1,
            _ => 0
        };

        public int ForwardDY => Direction switch
        {
            Direction.YPlus => 1,
            Direction.YMinus => -1,
            _ => 0
        };

        public bool IsOnBlockEdge => Utils.ModuloNegativeAdjusted(X, PhysicsConstants.BLOCK_STEPS) == 0 ||
                                     Utils.ModuloNegativeAdjusted(Y, PhysicsConstants.BLOCK_STEPS) == 0;
        public bool IsAtBlockMiddle => Utils.ModuloNegativeAdjusted(X, PhysicsConstants.BLOCK_STEPS) == PhysicsConstants.HALF_BLOCK_STEPS &&
                                       Utils.ModuloNegativeAdjusted(Y, PhysicsConstants.BLOCK_STEPS) == PhysicsConstants.HALF_BLOCK_STEPS;
        public bool IsOnBlockQuarter => Utils.ModuloNegativeAdjusted(AdjustedPositionOnBlock, PhysicsConstants.QUARTER_BLOCK_STEPS) == 0;
        public bool IsOnSegmentEdge => Utils.ModuloNegativeAdjusted(Z, PhysicsConstants.SEGMENT_STEPS) == 0;

        public bool IsSolidAtFeet
        {
            get
            {
                if (Z <= PhysicsConstants.LAND_HEIGHT &&
                    (GameEngine.Level.LevelInfo.Sea.Effect == LevelFloorEffect.Solid || GameEngine.Level.LevelInfo.Sea.Effect == LevelFloorEffect.Slippery))
                    return true;

                var floorPos = GetStandingOnBlockPosition();
                var relativePos = GetPositionOnBlock(floorPos);

                if (IsBlockSolidAt(floorPos, relativePos))
                    return true;

                if (IsOnSegmentEdge)
                {
                    if (CheckForBuilderBrick(floorPos, BuilderBrickCheckDirection))
                        return true;
                }

                if (floorPos.Z < 1 || (floorPos.Z == 1 && relativePos.Z == 0))
                {
                    var landEffect = GameEngine.GetFloorEffectAt(X, Y);
                    if (landEffect == LevelFloorEffect.Solid || landEffect == LevelFloorEffect.Slippery)
                        return true;
                }

                return false;
            }
        }

        public Direction BuilderBrickCheckDirection => (AdjustedPositionOnBlock > PhysicsConstants.HALF_BLOCK_STEPS || AdjustedPositionOnBlock == 0) ?
            Direction : Utils.InvertDirection(Direction);

        public bool IsSolidAtHead
        {
            get
            {
                var ceilingPos = GetAscendableBlockPosition();
                var relativePos = GetPositionOnBlock(ceilingPos);
                ceilingPos.Z += 2;
                relativePos.Z -= 1;

                relativePos.X = Math.Clamp(relativePos.X - ForwardDX, 0, PhysicsConstants.BLOCK_STEPS);
                relativePos.Y = Math.Clamp(relativePos.Y - ForwardDY, 0, PhysicsConstants.BLOCK_STEPS);

                if (relativePos.Z < 0)
                {
                    ceilingPos.Z -= 1;
                    relativePos.Z += PhysicsConstants.SEGMENT_STEPS;
                }

                if (IsBlockSolidAt(ceilingPos, relativePos))
                    return true;

                // Land won't happen, and builder bricks are intentionally ignored.

                return false;
            }
        }

        /// <summary>
        /// Always returns 0 when the lemming is on a block edge. Adjusts for the lemming's direction.
        /// </summary>
        public int AdjustedPositionOnBlock
        {
            get
            {
                int moduloPos = Utils.ModuloNegativeAdjusted((IsOnYAxis ? Y : X), PhysicsConstants.BLOCK_STEPS);

                if (moduloPos == 0)
                    return 0;

                switch (Direction)
                {
                    case Direction.XMinus:
                    case Direction.YMinus:
                        return PhysicsConstants.BLOCK_STEPS - moduloPos;
                    case Direction.XPlus:
                    case Direction.YPlus:
                        return moduloPos;
                    case Direction.ZMinus:
                    case Direction.ZPlus:
                    default:
                        throw new InvalidOperationException();
                }
            }
        }

        public bool GetIsStepInFront()
        {
            var wallPos = GetBaseWallPosition();
            var oneAboveWall = new Point3D(wallPos.X, wallPos.Y, wallPos.Z + 1);
            var relativePos = GetPositionOnBlock(wallPos);

            if (IsBlockSolidAt(wallPos, new Point3D(relativePos.X, relativePos.Y, PhysicsConstants.SEGMENT_STEPS)) ||
                IsBlockSolidAt(oneAboveWall, new Point3D(relativePos.X, relativePos.Y, 0)))
            {
                var twoAboveWall = new Point3D(wallPos.X, wallPos.Y, wallPos.Z + 2);
                if (!IsBlockSolidAt(oneAboveWall, new Point3D(relativePos.X, relativePos.Y, PhysicsConstants.SEGMENT_STEPS)) &&
                    !IsBlockSolidAt(twoAboveWall, new Point3D(relativePos.X, relativePos.Y, 0)))
                    return true;
            }

            return false;
        }

        public bool GetIsWallInFront()
        {
            var wallPos = GetBaseWallPosition();
            var oneAboveWall = new Point3D(wallPos.X, wallPos.Y, wallPos.Z + 1);
            var relativePos = GetPositionOnBlock(wallPos);

            if (IsBlockSolidAt(wallPos, new Point3D(relativePos.X, relativePos.Y, PhysicsConstants.SEGMENT_STEPS)) ||
                IsBlockSolidAt(oneAboveWall, new Point3D(relativePos.X, relativePos.Y, 0)))
            {
                var twoAboveWall = new Point3D(wallPos.X, wallPos.Y, wallPos.Z + 2);
                if (IsBlockSolidAt(oneAboveWall, new Point3D(relativePos.X, relativePos.Y, PhysicsConstants.SEGMENT_STEPS)) ||
                    IsBlockSolidAt(twoAboveWall, new Point3D(relativePos.X, relativePos.Y, 0)))
                    return true;
            }

            return false;
        }

        public bool GetIsClimbableWallInFront()
        {
            if (!GetIsWallInFront())
                return false;

            var wallPos = GetBaseWallPosition();
            var relativePos = GetPositionOnBlock(wallPos);
            if (!IsBlockSolidAt(wallPos, new Point3D(relativePos.X, relativePos.Y, PhysicsConstants.HALF_SEGMENT_STEPS)))
                return false;

            return true;
        }

        public Point3D GetPositionOnBlock(Point3D blockPosition)
        {
            return new Point3D(
                X - (blockPosition.X * PhysicsConstants.BLOCK_STEPS),
                Y - (blockPosition.Y * PhysicsConstants.BLOCK_STEPS),
                Z - (blockPosition.Z * PhysicsConstants.SEGMENT_STEPS)
                );
        }

        /// <summary>
        /// This explicitly does NOT account for builder bricks!
        /// </summary>
        public bool IsBlockSolidAt(Point3D blockPosition, Point3D positionOnBlock)
        {
            var blockInfo = BlockStructure[blockPosition.X, blockPosition.Y, blockPosition.Z];

            if (blockInfo == null)
                return false;
            else
                return blockInfo.IsSolidAt(positionOnBlock.X, positionOnBlock.Y, positionOnBlock.Z, false);
        }

        /// <summary>
        /// Gets the block coordinate of the block the lemming is standing on. If they are on an X / Y boundary, it
        /// returns the block behind them. If they are on a Z boundary, it returns the upper block if it's solid where
        /// the lemming is standing, otherwise the lower block.
        /// </summary>
        public Point3D GetStandingOnBlockPosition()
        {
            var result = Utils.UnitsToBlocks(Position);

            if (IsOnBlockEdge)
                switch (Direction)
                {
                    case Direction.XPlus:
                        result.X--;
                        break;
                    case Direction.YPlus:
                        result.Y--;
                        break;
                }

            if (IsOnSegmentEdge)
            {
                var relativePos = GetPositionOnBlock(result);
                if (!IsBlockSolidAt(result, relativePos))
                    result.Z--;
            }

            return result;
        }

        /// <summary>
        /// Gets the block coordinate of the block the lemming is standing on. If they are on an X / Y boundary, it
        /// returns the block behind them. If they are on a Z boundary, it returns the lower block.
        /// </summary>
        public Point3D GetDiggerTargetBlockPosition()
        {
            var result = Utils.UnitsToBlocks(Position);

            if (IsOnBlockEdge)
                switch (Direction)
                {
                    case Direction.XPlus:
                        result.X--;
                        break;
                    case Direction.YPlus:
                        result.Y--;
                        break;
                }

            if (IsOnSegmentEdge)
                result.Z--;

            return result;
        }

        /// <summary>
        /// Gets the block coordinate of the block the lemming may walk up as a ramp. If they are on an X / Y boundary,
        /// it returns the block behind them. If they are on a Z boundary, it returns the upper block.
        /// </summary>
        public Point3D GetAscendableBlockPosition()
        {
            var result = Utils.UnitsToBlocks(Position);

            if (IsOnBlockEdge)
                switch (Direction)
                {
                    case Direction.XPlus:
                        result.X--;
                        break;
                    case Direction.YPlus:
                        result.Y--;
                        break;
                }

            return result;
        }

        /// <summary>
        /// Gets the block coordinate of the lowest block that may act as part of a wall to a lemming. If they are
        /// on an X / Y boundary, it returns the block in front of them. If they are on a Z boundary, it returns the
        /// upper block.
        /// </summary>
        public Point3D GetBaseWallPosition()
        {
            var result = Utils.UnitsToBlocks(Position);

            if (IsOnBlockEdge)
                switch (Direction)
                {
                    case Direction.XMinus:
                        result.X--;
                        break;
                    case Direction.YMinus:
                        result.Y--;
                        break;
                }

            return result;
        }

        /// <summary>
        /// Gets the block coordinate of the lowest space where the lemming would place a blocker field.
        /// </summary>
        public Point3D GetBasePlaceBlockerFieldPosition()
        {
            var result = Utils.UnitsToBlocks(Position);

            if (!IsOnBlockEdge)
                switch (Direction)
                {
                    case Direction.XPlus:
                        result.X++;
                        break;
                    case Direction.YPlus:
                        result.Y++;
                        break;
                }

            return result;
        }

        /// <summary>
        /// Gets the block coordinate of the lowest space where the lemming would place a turner field.
        /// </summary>
        public Point3D GetBasePlaceTurnerFieldPosition()
        {
            var result = Utils.UnitsToBlocks(Position);

            if (IsOnBlockEdge)
                switch (Direction)
                {
                    case Direction.XPlus:
                        result.X--;
                        break;
                    case Direction.YPlus:
                        result.Y--;
                        break;
                }

            return result;
        }

        public bool CanExit
        {
            get
            {
                switch (Action)
                {
                    case LemmingAction.Walker:
                        return true;
                    case LemmingAction.Slider:
                        return true;
                    case LemmingAction.Faller:
                        return true;
                    case LemmingAction.Stunned:
                        return false;
                    case LemmingAction.Blocker:
                        return false;
                    case LemmingAction.Turner:
                        return false;
                    case LemmingAction.Builder:
                        return true;
                    case LemmingAction.Shrugger:
                        return true;
                    case LemmingAction.Basher:
                        return true;
                    case LemmingAction.Miner:
                        return true;
                    case LemmingAction.Digger:
                        return true; // Although it shouldn't ever actually come up...
                    case LemmingAction.Climber:
                        return true;
                    case LemmingAction.Floater:
                        return true;
                    case LemmingAction.Drowner:
                        return false;
                    case LemmingAction.Zapper:
                        return false;
                    case LemmingAction.DeathSpin:
                        return false;
                    case LemmingAction.Bomber:
                        return false;
                    case LemmingAction.Exiter:
                        return false; // he's already exiting, so he can't start exiting *again*
                    case LemmingAction.Warper:
                        return false;
                    case LemmingAction.Zipper:
                        return false;
                    case LemmingAction.Springer:
                        return false;
                    case LemmingAction.Bouncer:
                        return true;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        public bool CanRespondToHazards
        {
            get
            {
                switch (Action)
                {
                    case LemmingAction.Walker:
                        return true;
                    case LemmingAction.Slider:
                        return true;
                    case LemmingAction.Faller:
                        return true;
                    case LemmingAction.Stunned:
                        return true;
                    case LemmingAction.Blocker:
                        return false;
                    case LemmingAction.Turner:
                        return false;
                    case LemmingAction.Builder:
                        return true;
                    case LemmingAction.Shrugger:
                        return true;
                    case LemmingAction.Basher:
                        return true;
                    case LemmingAction.Miner:
                        return true;
                    case LemmingAction.Digger:
                        return true;
                    case LemmingAction.Climber:
                        return true;
                    case LemmingAction.Floater:
                        return true;
                    case LemmingAction.Drowner:
                        return false;
                    case LemmingAction.Zapper:
                        return false;
                    case LemmingAction.DeathSpin:
                        return false;
                    case LemmingAction.Bomber:
                        return false;
                    case LemmingAction.Exiter:
                        return false;
                    case LemmingAction.Warper:
                        return false;
                    case LemmingAction.Zipper:
                        return false;
                    case LemmingAction.Springer:
                        return false;
                    case LemmingAction.Bouncer:
                        return true;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        public bool CanRespondToNonHazardInteractives
        {
            get
            {
                switch (Action)
                {
                    case LemmingAction.Stunned:
                        return false;
                    default:
                        return CanRespondToHazards;
                }
            }
        }

        public bool CanRespondToSlippery
        {
            get
            {
                switch (Action)
                {
                    case LemmingAction.Climber:
                        return false;
                    case LemmingAction.Slider:
                        return false;
                    case LemmingAction.Stunned:
                        return false;
                    default:
                        return CanRespondToHazards;
                }
            }
        }

        private Direction CalculatePostDeflectorDirection(int deflectorRotations)
        {
            var originalDirection = Direction;
            int dirRots = Utils.DirectionToRotation(originalDirection);
            deflectorRotations = Utils.ModuloNegativeAdjusted(deflectorRotations - dirRots, 4);

            switch (deflectorRotations)
            {
                case 0:
                    return Utils.RotateDirection(originalDirection, 1);
                case 1:
                    return Utils.RotateDirection(originalDirection, -1);
                default:
                    return originalDirection;
            }
        }

        private TurnerDirection CheckRelevantTurnerDirection()
        {
            var basePos = GetAscendableBlockPosition();
            for (int i = TURNER_FIELD_CHECK_BOTTOM; i < TURNER_FIELD_CHECK_TOP; i++)
            {
                var field = EffectFields.FirstOrDefault(field =>
                    field.SourceIdentifier != Identifier &&
                    field.Effect == FieldType.Turner &&
                    field.Position == new Point3D(basePos.X, basePos.Y, basePos.Z + i) &&
                    field.Side == Direction
                    );

                if (field != default)
                    return field.Direction;
            }

            return TurnerDirection.None;
        }

        private bool CheckForRelevantBlocker()
        {
            var basePos = GetAscendableBlockPosition();
            switch (Direction)
            {
                case Direction.XPlus:
                    basePos.X++;
                    break;
                case Direction.YPlus:
                    basePos.Y++;
                    break;
            }

            for (int i = BLOCKER_FIELD_CHECK_BOTTOM; i < BLOCKER_FIELD_CHECK_TOP; i++)
            {
                if (EffectFields.Any(field =>
                    field.SourceIdentifier != Identifier &&
                    field.Effect == FieldType.Blocker &&
                    field.Position == new Point3D(basePos.X, basePos.Y, basePos.Z + i) &&
                    field.Side == (IsOnYAxis ? Direction.YMinus : Direction.XMinus)
                    ))
                    return true;
            }
            return false;
        }

        private bool CheckForBuilderBrick(Point3D blockPosition, Direction side)
        {
            return GameEngine.BuilderBricks.Any(brick =>
                brick.Position == blockPosition &&
                brick.Direction == side
                );
        }

        #endregion
    }
}
