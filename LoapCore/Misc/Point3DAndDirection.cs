﻿using System;

namespace LoapCore.Misc
{
    /// <summary>
    /// This should generally only be used when the combination is needed as a key (eg. for a Dictionary<,>)
    /// </summary>
    public struct Point3DAndDirection : IEquatable<Point3DAndDirection>
    {
        public Point3D Position;
        public Direction Direction;

        public Point3DAndDirection(Point3D position, Direction direction)
        {
            Position = position;
            Direction = direction;
        }

        public override bool Equals(object? obj)
        {
            return obj is Point3DAndDirection direction && Equals(direction);
        }

        public bool Equals(Point3DAndDirection other)
        {
            return Position.Equals(other.Position) &&
                   Direction == other.Direction;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Position, Direction);
        }

        public static bool operator ==(Point3DAndDirection left, Point3DAndDirection right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Point3DAndDirection left, Point3DAndDirection right)
        {
            return !(left == right);
        }
    }
}
