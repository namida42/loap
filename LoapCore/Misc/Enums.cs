﻿namespace LoapCore.Misc
{
    public enum Direction { XMinus, XPlus, YMinus, YPlus, ZMinus, ZPlus }
    public enum TurnerDirection { None, Left, Right }

    public enum BlockShape
    {
        Solid, SolidNarrow,
        Deflector,
        Ramp45Above, Ramp45Below, Ramp22Above, Ramp22Below,
        Corner90Above, Corner90Below, Corner45Above, Corner45Below, CornerInsideAbove, CornerInsideBelow,
        LargePyramidAbove, LargePyramidBelow, SmallPyramidAbove, SmallPyramidBelow
    }

    public enum BlockVisibility { Normal, Transparent, Invisible, AntiRender }
    public enum BlockPhysics { Solid, Steel, NonSolid, Entrance, Splitter, Water, Electrocute, Antisplat }
    public enum FacePhysics { None, Exit, AnimateExit }

    public enum EntityEffect { None, Trap, LinkedTrap, Teleporter, Receiver, RopeSlideStart, RopeSlideEnd, Spring, SpringPad, TrampolineStrong, TrampolineWeak }
    public enum FieldType { Blocker, Turner, Trap, Teleporter, RopeSlide, Spring, Trampoline }
    public enum TrapKillStyle { Instant, Spin, Electrocute }
    public enum EntityDirectionality { None, ByAxis, ByDirection }

    public enum LevelFloorEffect { Water, Electrocute, Solid, Slippery }

    public enum LemmingAction
    {
        Walker, Slider,
        Faller, Stunned,
        Blocker, Turner,
        Builder, Shrugger,
        Basher, Miner, Digger,
        Climber, Floater,
        Drowner, Zapper, DeathSpin,
        Bomber, Exiter,
        Warper, Zipper, Springer, Bouncer
    }
    public enum LemmingRemoveReason { Unassigned, Exit, Bomber, Splat, Drown, Electrocute, Trap }

    public enum PreviewCameraStyle { Pivot, Rotate, Fixed }
    public enum AnimationStyle { Normal, Reverse, Stop }

    public enum Skill { Blocker, Turner, Bomber, Builder, Basher, Miner, Digger, Climber, Floater }
    public enum QueuedSkill { None, Blocker, Turner, BuilderBegin, BuilderContinue, Shrugger, BasherBegin, BasherOverflow, BasherContinue, MinerBegin, MinerContinue, Digger }

    public enum BehaviorResult { Continue, TerminatePhase }

    public enum ReplayAction { AssignSkill, Nuke, ChangeSpawnRate }
}
