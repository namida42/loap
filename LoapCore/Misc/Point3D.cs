﻿#region License of original file
// This file is derived from Point.cs from MonoGame.
// Licence below is for Point.cs.

/*
MIT License
Copyright © 2006 The Mono.Xna Team
All rights reserved.
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#endregion License
using System;

namespace LoapCore.Misc
{
    public struct Point3D : IEquatable<Point3D>
    {
        #region Private Fields

        private static Point3D zeroPoint = new Point3D();

        #endregion Private Fields


        #region Public Fields

        public int X;
        public int Y;
        public int Z;

        #endregion Public Fields


        #region Properties

        public static Point3D Zero
        {
            get { return zeroPoint; }
        }

        #endregion Properties


        #region Constructors

        public Point3D(int x, int y, int z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        #endregion Constructors


        #region Public methods

        public static bool operator ==(Point3D a, Point3D b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Point3D a, Point3D b)
        {
            return !a.Equals(b);
        }

        public bool Equals(Point3D other)
        {
            return ((X == other.X) && (Y == other.Y) && (Z == other.Z));
        }

        public override bool Equals(object? obj)
        {
            return (obj is Point3D other) && Equals(other);
        }

        public override int GetHashCode()
        {
            return Tuple.Create(X, Y, Z).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{{X:{0} Y:{1} Z:{2}}}", X, Y, Z);
        }

        #endregion
    }
}