﻿using LoapCore.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoapCore.Misc
{
    public static class Utils
    {
        #region Maths Functions

        public static int DivideNegativeAdjusted(int dividend, int divisor)
        {
            int result = dividend / divisor;
            if (dividend < 0)
                result -= 1;

            return result;
        }

        public static int ModuloNegativeAdjusted(int dividend, int divisor)
        {
            int result = dividend % divisor;
            if (result < 0)
                result += divisor;

            return result;
        }

        public static float ModuloNegativeAdjusted(float dividend, float divisor)
        {
            float result = dividend % divisor;
            if (result < 0)
                result += divisor;

            return result;
        }

        public static float RoundFloat(float input, float unit)
        {
            return (float)(Math.Round(input / (double)unit) * unit);
        }

        public static int MinPositive(int a, int b)
        {
            if (a < 0 && b < 0)
                return -1;
            else if (a < 0)
                return b;
            else if (b < 0)
                return a;
            else
                return Math.Min(a, b);
        }

        #endregion

        #region Data Processing

        public static int GetMultiByteValueFromBytes(byte[] src, int firstIndex, int bytesToRead, bool bigEndian = false)
        {
            int step = bigEndian ? 1 : -1;
            int pos = bigEndian ? firstIndex : firstIndex + bytesToRead - 1;
            int result = 0;

            for (int i = 0; i < bytesToRead; i++)
            {
                result = (result << 8) + src[pos];
                pos += step;
            }

            return result;
        }

        public static string GetStringFromBytes(byte[] src, int firstIndex, int maxLength)
        {
            StringBuilder result = new StringBuilder(maxLength);

            for (int i = firstIndex; i < firstIndex + maxLength; i++)
                if (src[i] == 0x00)
                    break;
                else
                    result.Append((char)src[i]);

            return result.ToString();
        }

        public static T[] GetEnumValues<T>() where T : struct, Enum
        {
            return (T[])Enum.GetValues(typeof(T));
        }

        #endregion

        #region Direction / Angle Manipulation

        public static void ApplyRotations(ref int x, ref int y, int width, int height, int rotations)
        {
            int origX = x;
            int origY = y;

            switch (Utils.ModuloNegativeAdjusted(rotations, 4))
            {
                case 1:
                    x = height - origY;
                    y = origX;
                    break;
                case 2:
                    x = width - origX;
                    y = height - origY;
                    break;
                case 3:
                    x = origY;
                    y = width - origX;
                    break;
            }
        }

        public static float DirectionToDegrees(Direction value)
        {
            return value switch
            {
                Direction.XMinus => 90,
                Direction.XPlus => 270,
                Direction.YMinus => 180,
                Direction.YPlus => 0,
                _ => throw new ArgumentException("DirectionToDegrees must be given an X or Y direction", nameof(value)),
            };
        }

        public static int DirectionToRotation(Direction value)
        {
            return value switch
            {
                Direction.XMinus => 1,
                Direction.XPlus => 3,
                Direction.YMinus => 2,
                Direction.YPlus => 0,
                _ => throw new ArgumentException("DirectionToRotation must be given an X or Y direction", nameof(value)),
            };
        }

        public static Direction InvertDirection(Direction value)
        {
            return value switch
            {
                Direction.XMinus => Direction.XPlus,
                Direction.XPlus => Direction.XMinus,
                Direction.YMinus => Direction.YPlus,
                Direction.YPlus => Direction.YMinus,
                Direction.ZMinus => Direction.ZPlus,
                Direction.ZPlus => Direction.ZMinus,
                _ => throw new ArgumentException("Invalid direction value", nameof(value)),
            };
        }

        public static Direction RotateDirection(Direction value, int rotations)
        {
            return RotationToDirection(DirectionToRotation(value) + rotations);
        }

        public static Direction RotationToDirection(int value)
        {
            return Utils.ModuloNegativeAdjusted(value, 4) switch
            {
                1 => Direction.XMinus,
                2 => Direction.YMinus,
                3 => Direction.XPlus,
                _ => Direction.YPlus,
            };
        }

        public static void RotateAroundOrigin(ref int x, ref int y, int rotations)
        {
            int origX = x;
            int origY = y;

            switch (Utils.ModuloNegativeAdjusted(rotations, 4))
            {
                case 1:
                    x = -origY;
                    y = origX;
                    break;
                case 2:
                    x = -origX;
                    y = -origY;
                    break;
                case 3:
                    x = origY;
                    y = -origX;
                    break;
            }
        }

        public static void RotateWithinZeroToOne(ref float x, ref float y, int rotations)
        {
            float origX = x;
            float origY = y;

            switch (Utils.ModuloNegativeAdjusted(rotations, 4))
            {
                case 1:
                    x = 1 - origY;
                    y = origX;
                    break;
                case 2:
                    x = 1 - origX;
                    y = 1 - origY;
                    break;
                case 3:
                    x = origY;
                    y = 1 - origX;
                    break;
            }
        }

        #endregion

        #region Gameplay Functions

        public static Point3D BlocksToUnits(Point3D blockCoordinate, bool returnMiddleXY = false, bool returnMiddleZ = false)
        {
            var result = new Point3D(
                    blockCoordinate.X * PhysicsConstants.BLOCK_STEPS,
                    blockCoordinate.Y * PhysicsConstants.BLOCK_STEPS,
                    blockCoordinate.Z * PhysicsConstants.SEGMENT_STEPS
                    );

            if (returnMiddleXY)
            {
                result.X += PhysicsConstants.HALF_BLOCK_STEPS;
                result.Y += PhysicsConstants.HALF_BLOCK_STEPS;
            }

            if (returnMiddleZ)
                result.Z += PhysicsConstants.HALF_SEGMENT_STEPS;

            return result;
        }

        public static bool IsPointInsidePolygon(float x, float y, IEnumerable<float> xVertices, IEnumerable<float> yVertices, bool isTriangulated)
        {
            int verts = Math.Min(xVertices.Count(), yVertices.Count());

            var xV = xVertices.Take(verts).ToArray();
            var yV = yVertices.Take(verts).ToArray();

            float midX = xV.Sum() / verts;
            float midY = yV.Sum() / verts;

            if (verts < 3)
                return false;

            if (isTriangulated && (verts > 3))
            {
                for (int i = 0; i < verts; i += 3)
                    if (IsPointInsidePolygon(x, y, xV[i..(i + 3)], yV[i..(i + 3)], false))
                        return true;

                return false;
            }
            else
            {
                for (int i = 0; i < verts; i++)
                {
                    float thisX = xV[i];
                    float thisY = yV[i];
                    float nextX = xV[(i + 1) % verts];
                    float nextY = yV[(i + 1) % verts];

                    if (thisX == nextX)
                    {
                        if ((x < thisX && midX > thisX) ||
                            (x > thisX && midX < thisX))
                            return false;
                    }
                    else if (thisY == nextY)
                    {
                        if ((y < thisY && midY > thisY) ||
                            (y > thisY && midY < thisY))
                            return false;
                    }
                    else
                    {
                        float slope = (nextY - thisY) / (float)(nextX - thisX);

                        float yOnLineForMidX = ((midX - thisX) * slope) + thisY;
                        float yOnLineForTestX = ((x - thisX) * slope) + thisY;

                        if ((y < yOnLineForTestX && midY > yOnLineForMidX) ||
                            (y > yOnLineForTestX && midY < yOnLineForMidX))
                            return false;
                    }
                }

                return true;
            }
        }

        public static int SpawnRateToSpawnInterval(int rate)
        {
            return 4 + ((PhysicsConstants.MAX_SPAWN_RATE - rate) * 20);
        }

        /// <summary>
        /// In the case of being given a point on the boundary of two blocks, it returns the one with the higher coordinate.
        /// </summary>
        public static Point3D UnitsToBlocks(Point3D unitCoordinate)
        {
            return new Point3D(
                DivideNegativeAdjusted(unitCoordinate.X, PhysicsConstants.BLOCK_STEPS),
                DivideNegativeAdjusted(unitCoordinate.Y, PhysicsConstants.BLOCK_STEPS),
                DivideNegativeAdjusted(unitCoordinate.Z, PhysicsConstants.SEGMENT_STEPS)
                );
        }

        #endregion

        #region Animation

        public static int GetAnimationCycleLength(int rawFrameCount, AnimationStyle animationStyle)
        {
            return animationStyle switch
            {
                AnimationStyle.Normal => rawFrameCount,
                AnimationStyle.Reverse => (rawFrameCount * 2) - 2,
                AnimationStyle.Stop => rawFrameCount,
                _ => throw new NotImplementedException()
            };
        }

        public static int CalculateAnimationFrameIndex(int totalFrameUpdates, int rawFrameCount, AnimationStyle animationStyle)
        {
            int internalFrameCount = GetAnimationCycleLength(rawFrameCount, animationStyle);

            switch (animationStyle)
            {
                case AnimationStyle.Normal:
                    return Utils.ModuloNegativeAdjusted(totalFrameUpdates, internalFrameCount);
                case AnimationStyle.Reverse:
                    int internalFrame = Utils.ModuloNegativeAdjusted(totalFrameUpdates, internalFrameCount);
                    if (internalFrame < rawFrameCount)
                        return internalFrame;
                    else
                        return internalFrameCount - internalFrame;
                case AnimationStyle.Stop:
                    return Math.Clamp(totalFrameUpdates, 0, internalFrameCount - 1);
                default:
                    throw new NotImplementedException();
            }
        }

        #endregion
    }
}
