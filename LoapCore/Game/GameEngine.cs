﻿using LoapCore.Blocks;
using LoapCore.Constants;
using LoapCore.Entities;
using LoapCore.Lemmings;
using LoapCore.Levels;
using LoapCore.Misc;
using LoapCore.Replays;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoapCore.Game
{
    public class GameEngine
    {
        public struct SpawnRecord
        {
            public Point3D BlockCoordinates;
            public Point3D SpawnPoint;
            public Direction SpawnDirection;

            public SpawnRecord(Point3D blockCoordinates, Point3D spawnPoint, Direction spawnDirection)
            {
                BlockCoordinates = blockCoordinates;
                SpawnPoint = spawnPoint;
                SpawnDirection = spawnDirection;
            }
        }

        public readonly GameState GameState = new GameState();
        public readonly Replay Replay;

        public int Iteration => GameState.Iteration;

        public readonly Level Level;
        public Dictionary<Skill, int> Skillset => GameState.Skillset;
        public BlockStructure Blocks => GameState.Blocks;
        public List<Lemming> Lemmings => GameState.Lemmings;

        public List<Point3DAndDirection> BuilderBricks => GameState.BuilderBricks;
        public List<EffectField> EffectFields => GameState.EffectFields;

        public readonly List<SpawnRecord> SpawnPoints = new List<SpawnRecord>();

        public int LemmingsToSpawn => NukeActive ? 0 : GameState.LemmingsToSpawn;
        public int LemmingsAlive => LemmingsToSpawn + Lemmings.Count;
        public int LemmingsRescued => GameState.LemmingsRescued;
        public int LemmingsNeededToRescue => Level.LevelInfo.SaveRequirement - GameState.LemmingsRescued;
        public int MetRequirementIteration => GameState.LastRequiredLemmingSaveIteration;
        public int LastIterationWithLemmingsAlive => GameState.LastIterationWithLemmingsAlive;

        public int SpawnRate
        {
            get
            {
                var action = Replay.GetSpawnRateActionForIteration(GameState.Iteration);
                if (action == null)
                    return GameState.SpawnRate;
                else
                    return action.NewSpawnRate;
            }
        }

        private bool _CheatMode;
        public bool CheatMode // Cannot be turned off once active, a whole new GameEngine must be created.
        {
            get => _CheatMode;
            set
            {
                if (value)
                    _CheatMode = true;
            }
        }

        public readonly List<Lemming> RemovedOnLatestIterationLemmings = new List<Lemming>();
        public readonly List<Point3D> RemovedOnLatestIterationBlocks = new List<Point3D>();
        public readonly List<Point3D> DamagedOnLatestIterationBlocks = new List<Point3D>();
        public readonly List<GameSound> LatestIterationSounds = new List<GameSound>();

        public List<EntityInstance> EntityInstances => GameState.Entities;

        public bool NukeActive
        {
            get
            {
                var action = Replay.GetAssignmentActionForIteration(GameState.Iteration);
                if (action == null || action.Action != ReplayAction.Nuke)
                    return GameState.NukeActive;
                else
                    return true;
            }
        }

        public GameEngine(Level level, Dictionary<string, MetaEntity> metaEntities, Replay? replay)
        {
            Level = level;
            GameState.Entities.AddRange(EntityInstance.CreateInstances(level.Entities, metaEntities));

            if (replay == null)
                Replay = new Replay();
            else
                Replay = replay;

            Initialize();
        }

        private void Initialize()
        {
            GameState.Blocks.Clone(Level.BlockStructure);
            GameState.Blocks.Sanitize();

            CopySkillsetToGameState();
            PrepareSpawnPointAndTiming();
            InitializeEntities();
        }

        public void CopySkillsetToGameState()
        {
            foreach (Skill skill in Utils.GetEnumValues<Skill>())
                GameState.Skillset[skill] = Level.LevelInfo.Skillset[skill];
        }

        public void InitializeEntities()
        {
            foreach (var entity in GameState.Entities)
            {
                if (entity.Effect == EntityEffect.None)
                {
                    entity.Activated = true;
                    continue;
                }

                string uniqueIdentifierBase = entity.Position.X.ToString() + "," + entity.Position.Y.ToString() + "," + entity.Position.Z.ToString();
                int uniqueIdentifierSuffix = 0;
                while (GameState.Entities.Any(e => e.UniqueIdentifier == uniqueIdentifierBase + uniqueIdentifierSuffix.ToString("D3")))
                    uniqueIdentifierSuffix++;

                entity.UniqueIdentifier = uniqueIdentifierBase + uniqueIdentifierSuffix.ToString("D3");

                Point3D destination = new Point3D();
                if (entity.Effect == EntityEffect.Teleporter)
                {
                    var other = GameState.Entities.FirstOrDefault(e =>
                        e != entity &&
                        (e.Effect == EntityEffect.Teleporter || e.Effect == EntityEffect.Receiver) &&
                        e.PairingID == entity.PairingID
                        );

                    if (other == null)
                        continue;

                    destination = other.Position;
                }
                else if (entity.Effect == EntityEffect.RopeSlideStart)
                {
                    var other = GameState.Entities.FirstOrDefault(e =>
                        (e.Effect == EntityEffect.RopeSlideEnd) &&
                        e.PairingID == entity.PairingID
                        );

                    if (other == null)
                        continue;

                    destination = other.Position;
                }
                else if (entity.Effect == EntityEffect.Spring)
                {
                    var other = GameState.Entities.FirstOrDefault(e =>
                        (e.Effect == EntityEffect.SpringPad) &&
                        e.PairingID == entity.PairingID
                        );

                    if (other == null)
                        continue;

                    destination = other.Position;
                }

                for (int i = 0; i < entity.TriggerHeight; i++)
                {
                    Point3D fieldPos = new Point3D(entity.Position.X, entity.Position.Y, entity.Position.Z + i);

                    switch (entity.Effect)
                    {
                        case EntityEffect.Trap:
                        case EntityEffect.LinkedTrap:
                            {
                                var newEffect = new EffectField();
                                newEffect.Effect = FieldType.Trap;
                                newEffect.SourceIdentifier = entity.UniqueIdentifier;
                                newEffect.Position = fieldPos;

                                GameState.EffectFields.Add(newEffect);
                            }
                            break;
                        case EntityEffect.Teleporter:
                            {
                                var newEffect = new EffectField();
                                newEffect.Effect = FieldType.Teleporter;
                                newEffect.SourceIdentifier = entity.UniqueIdentifier;
                                newEffect.Position = fieldPos;
                                newEffect.Destination = destination;

                                var onBlock = GameState.Blocks[fieldPos.X, fieldPos.Y, fieldPos.Z - 1];
                                if (onBlock != null)
                                {
                                    onBlock.OneWayDirection = null;
                                    if (!PhysicsConstants.IndestructibleBlockTypes.Contains(onBlock.Physics))
                                        onBlock.Physics = BlockPhysics.Steel;
                                }

                                GameState.EffectFields.Add(newEffect);
                            }
                            break;
                        case EntityEffect.RopeSlideStart:
                            {
                                var newEffect = new EffectField();
                                newEffect.Effect = FieldType.RopeSlide;
                                newEffect.SourceIdentifier = entity.UniqueIdentifier;
                                newEffect.Position = fieldPos;
                                newEffect.Destination = destination;

                                GameState.EffectFields.Add(newEffect);
                            }
                            break;
                        case EntityEffect.Spring:
                            {
                                var newEffect = new EffectField();
                                newEffect.Effect = FieldType.Spring;
                                newEffect.SourceIdentifier = entity.UniqueIdentifier;
                                newEffect.Position = fieldPos;
                                newEffect.Destination = destination;

                                GameState.EffectFields.Add(newEffect);
                            }
                            break;
                        case EntityEffect.TrampolineStrong:
                        case EntityEffect.TrampolineWeak:
                            {
                                var newEffect = new EffectField();
                                newEffect.Effect = FieldType.Trampoline;
                                newEffect.SourceIdentifier = entity.UniqueIdentifier;
                                newEffect.Position = fieldPos;

                                var onBlock = GameState.Blocks[fieldPos.X, fieldPos.Y, fieldPos.Z - 1];
                                if (onBlock != null)
                                {
                                    onBlock.OneWayDirection = null;
                                    if (!PhysicsConstants.IndestructibleBlockTypes.Contains(onBlock.Physics))
                                        onBlock.Physics = BlockPhysics.Steel;
                                }

                                GameState.EffectFields.Add(newEffect);
                            }
                            break;
                        case EntityEffect.Receiver:
                        case EntityEffect.SpringPad:
                            {
                                var onBlock = GameState.Blocks[fieldPos.X, fieldPos.Y, fieldPos.Z - 1];
                                if (onBlock != null)
                                {
                                    onBlock.OneWayDirection = null;
                                    if (!PhysicsConstants.IndestructibleBlockTypes.Contains(onBlock.Physics))
                                        onBlock.Physics = BlockPhysics.Steel;
                                }
                            }
                            break;
                        case EntityEffect.RopeSlideEnd:
                            // Do nothing;
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }
            }
        }

        private void PrepareSpawnPointAndTiming()
        {
            for (int z = 0; z < Blocks.Height; z++)
                for (int y = 0; y < Blocks.Depth; y++)
                    for (int x = 0; x < Blocks.Width; x++)
                    {
                        var thisBlock = Blocks[x, y, z];
                        if (thisBlock != null && thisBlock.Physics == BlockPhysics.Entrance)
                            SpawnPoints.Add(new SpawnRecord(
                                new Point3D(x, y, z),
                                MakeSpawnPointFromBlockInfo(new Point3D(x, y, z), thisBlock.Rotations),
                                Utils.RotationToDirection(thisBlock.Rotations)
                                ));
                    }

            GameState.LemmingsToSpawn = Level.LevelInfo.LemmingCount;
            GameState.SpawnRate = Level.LevelInfo.SpawnRate;
            GameState.LemmingSpawnTimer = PhysicsConstants.FIRST_SPAWN_DELAY;
        }

        private static Point3D MakeSpawnPointFromBlockInfo(Point3D blockPos, int rotations)
        {
            Point3D basePos = new Point3D(blockPos.X * PhysicsConstants.BLOCK_STEPS + PhysicsConstants.HALF_BLOCK_STEPS,
                                          blockPos.Y * PhysicsConstants.BLOCK_STEPS + PhysicsConstants.HALF_BLOCK_STEPS,
                                          (blockPos.Z - 2) * PhysicsConstants.SEGMENT_STEPS);

            Point3D offset = new Point3D(0, -1, 0);
            Utils.RotateAroundOrigin(ref offset.X, ref offset.Y, rotations);

            return new Point3D(basePos.X + offset.X, basePos.Y + offset.Y, basePos.Z);
        }

        public void Update()
        {
            RemovedOnLatestIterationLemmings.Clear();
            RemovedOnLatestIterationBlocks.Clear();
            DamagedOnLatestIterationBlocks.Clear();
            LatestIterationSounds.Clear();

            if (GameState.Iteration == 1)
            {
                LatestIterationSounds.Add(new GameSound(SoundNames.LETS_GO, null));
                LatestIterationSounds.Add(new GameSound(SoundNames.ENTRANCE, null));
            }

            if (Level.LevelInfo.TimeLimit > 0 && GameState.Iteration == Level.LevelInfo.TimeLimit * PhysicsConstants.ITERATIONS_PER_INGAME_SECOND && !CheatMode)
                HandleActivateNuke();

            HandleReplayActions();
            HandleSpawnLemmings();
            UpdateLemmings();
            UpdateTriggeredExits();
            UpdateEntities();

            if (LemmingsAlive > 0)
                GameState.LastIterationWithLemmingsAlive = GameState.Iteration;

            if (_CheatMode)
            {
                var skillTypes = (Skill[])Enum.GetValues(typeof(Skill));
                foreach (var skilltype in skillTypes)
                    GameState.Skillset[skilltype] = 99;
            }

            GameState.Iteration++;
        }

        private void UpdateEntities()
        {
            foreach (var entity in GameState.Entities)
                UpdateEntity(entity);
        }

        private void UpdateEntity(EntityInstance entity)
        {
            if (entity.Activated)
            {
                entity.Iteration++;
                if (entity.Effect != EntityEffect.None)
                {
                    if (entity.Iteration / 2 == Utils.GetAnimationCycleLength(entity.Frames, entity.AnimationStyle))
                    {
                        entity.Iteration = 0;

                        if (entity.Effect == EntityEffect.Teleporter || entity.Effect == EntityEffect.Receiver)
                            entity.Activated = GameState.Lemmings.Any(lem =>
                                lem.Action == LemmingAction.Warper &&
                                (lem.TransporterSourceIdentifier == entity.UniqueIdentifier || lem.TransporterTargetIdentifier == entity.UniqueIdentifier)
                                );
                        else
                            entity.Activated = false;
                    }
                }
            }
        }

        private void HandleReplayActions()
        {
            HandleReplayAssignmentAction();
            HandleReplaySpawnRateAction();
        }

        private void HandleReplayAssignmentAction()
        {
            var assignment = Replay.GetAssignmentActionForIteration(GameState.Iteration);
            if (assignment != null)
            {
                if (assignment.Action == ReplayAction.AssignSkill)
                    if (GameState.Skillset[assignment.Skill] > 0)
                    {
                        var targetLemming = GameState.Lemmings.FirstOrDefault(lem => lem.Identifier == assignment.LemmingIdentifier);
                        if (targetLemming != null)
                            if (targetLemming.MayBeAssignedSkill(assignment.Skill))
                            {
                                HandleSkillAssignment(assignment.Skill, targetLemming, assignment.TurnerDirection);
                                if (assignment.Skill != Skill.Bomber) // so that we don't double-up with "oh no"
                                    LatestIterationSounds.Add(new GameSound(SoundNames.OKAY, targetLemming.Position));
                            }
                    }

                if (assignment.Action == ReplayAction.Nuke && !GameState.NukeActive)
                    HandleActivateNuke();
            }
        }

        private void HandleReplaySpawnRateAction()
        {
            var spawnRateChange = Replay.GetSpawnRateActionForIteration(GameState.Iteration);
            if (spawnRateChange != null)
            {
                if (spawnRateChange.NewSpawnRate >= Level.LevelInfo.SpawnRate &&
                    spawnRateChange.NewSpawnRate <= PhysicsConstants.MAX_SPAWN_RATE &&
                    spawnRateChange.NewSpawnRate != GameState.SpawnRate)
                    GameState.SpawnRate = spawnRateChange.NewSpawnRate;
            }
        }

        private void UpdateLemmings()
        {
            if (NukeActive)
            {
                var nukeLem = GameState.Lemmings.FirstOrDefault(lem => lem.MayBeNuked);
                if (nukeLem != null)
                    nukeLem.Transition(LemmingAction.Bomber);
            }

            GameState.EffectFields.RemoveAll(field => field.Transient);

            foreach (var lem in GameState.Lemmings.Where(lem => !lem.LemmingRemoved))
                lem.UpdatePreMove();

            foreach (var lem in GameState.Lemmings.Where(lem => !lem.LemmingRemoved))
                lem.UpdateMove();

            foreach (var lem in GameState.Lemmings.Where(lem => !lem.LemmingRemoved))
                lem.UpdatePostMove();

            RemovedOnLatestIterationLemmings.AddRange(Lemmings.Where(lem => lem.LemmingRemoved));

            GameState.LemmingsRescued += RemovedOnLatestIterationLemmings.Count(lem => lem.LemmingRemoveReason == LemmingRemoveReason.Exit);
            if (GameState.LastRequiredLemmingSaveIteration < 0 && GameState.LemmingsRescued >= Level.LevelInfo.SaveRequirement)
            {
                GameState.LastRequiredLemmingSaveIteration = GameState.Iteration;
                LatestIterationSounds.Add(new GameSound(SoundNames.WIN, null));
            }

            Lemmings.RemoveAll(lem => lem.LemmingRemoved);
        }

        private void HandleSpawnLemmings()
        {
            if (GameState.LemmingSpawnTimer > 0 && !NukeActive)
            {
                GameState.LemmingSpawnTimer--;
                if (GameState.LemmingSpawnTimer == 0)
                {
                    foreach (var spawnPoint in SpawnPoints)
                    {
                        SpawnLemming(spawnPoint.SpawnPoint, spawnPoint.SpawnDirection);

                        GameState.LemmingsToSpawn--;

                        if (GameState.LemmingsToSpawn == 0)
                            break;
                    }

                    if (GameState.LemmingsToSpawn > 0)
                        GameState.LemmingSpawnTimer = Utils.SpawnRateToSpawnInterval(GameState.SpawnRate);
                }
            }
        }

        private void SpawnLemming(Point3D position, Direction direction)
        {
            Lemming newLem = new Lemming(this, "S" + (Level.LevelInfo.LemmingCount - LemmingsToSpawn).ToString(), position, direction);
            Lemmings.Add(newLem);
        }

        public LevelFloorEffect GetFloorEffectAt(int x, int y)
        {
            foreach (var land in Level.Lands.AsEnumerable().Reverse())
                if (land.IsPointInside(x, y))
                    return land.Effect;

            return Level.LevelInfo.Sea.Effect;
        }

        private Point3D[] GetMatchingVerticalBlockPositions(Point3D startPos, Func<BlockInfo, bool> condition)
        {
            List<Point3D> result = new List<Point3D>();
            int offset = 0;
            bool doAbove = true;
            bool doBelow = true;
            while (doAbove || doBelow)
            {
                if (doAbove && (offset > 0))
                {
                    var aboveBlock = Blocks[startPos.X, startPos.Y, startPos.Z + offset];
                    if (aboveBlock != null && condition.Invoke(aboveBlock))
                        result.Add(new Point3D(startPos.X, startPos.Y, startPos.Z + offset));
                    else
                        doAbove = false;
                }

                if (doBelow)
                {
                    var belowBlock = Blocks[startPos.X, startPos.Y, startPos.Z - offset];
                    if (belowBlock != null && condition.Invoke(belowBlock))
                        result.Add(new Point3D(startPos.X, startPos.Y, startPos.Z - offset));
                    else
                        doBelow = false;
                }

                offset++;
            }

            return result.ToArray();
        }

        public int GetExitFrameIndex(Point3D pos, Direction face)
        {
            var key = new Point3DAndDirection(pos, face);
            if (GameState.ExitFrames.ContainsKey(key))
            {
                var block = GameState.Blocks[pos.X, pos.Y, pos.Z]!;
                var frameCount = block.Faces[face].TextureOuter.Count;
                if (frameCount < 2)
                    return 0;

                var frame = GameState.ExitFrames[key];
                if (frame < 0)
                    return Math.Min(-frame, frameCount - 1);
                else
                    return Math.Min(frame, frameCount - 1);
            }
            else
                return 0;
        }

        private void UpdateTriggeredExits()
        {
            var keys = GameState.ExitFrames.Keys.ToArray(); // We want it disconnected from the dictionary, so we can modify the dict.
            foreach (var key in keys)
            {
                var block = GameState.Blocks[key.Position.X, key.Position.Y, key.Position.Z];
                if (block == null)
                    continue;

                var face = block.Faces[key.Direction];

                int curVal = GameState.ExitFrames[key];

                if (curVal == -1)
                    GameState.ExitFrames.Remove(key);
                else if (curVal < Math.Max(Lemming.EXITER_REMOVE_ITERATION, face.TextureOuter.Count))
                    GameState.ExitFrames[key] = curVal + 1;
                else
                {
                    curVal = -face.TextureOuter.Count + 1;
                    if (curVal == 0)
                        GameState.ExitFrames.Remove(key);
                    else
                        GameState.ExitFrames[key] = curVal;
                }

            }
        }

        public void TriggerExit(Point3D pos, Direction face)
        {
            foreach (var adjustPos in GetMatchingVerticalBlockPositions(pos, b => b.Faces[Utils.RotateDirection(face, -b.Rotations)].Physics == FacePhysics.AnimateExit))
            {
                var block = GameState.Blocks[adjustPos.X, adjustPos.Y, adjustPos.Z]!;
                var localFace = Utils.RotateDirection(face, -block.Rotations);
                var frameCount = block.Faces[localFace].TextureOuter.Count;
                if (frameCount > 1)
                {
                    var key = new Point3DAndDirection(adjustPos, localFace);
                    if (GameState.ExitFrames.ContainsKey(key))
                    {
                        int frame = GameState.ExitFrames[key];

                        if (frame >= frameCount)
                            frame = (frameCount * 2) - 2 - frame;

                        GameState.ExitFrames[key] = frame;
                    }
                    else
                        GameState.ExitFrames.Add(key, 0);
                }
            }
        }

        public TurnerDirection GetSplitterSwitchState(Point3D pos, Direction direction)
        {
            var key = new Point3DAndDirection(pos, direction);
            if (GameState.SplitterSwitches.ContainsKey(key))
                return GameState.SplitterSwitches[key];
            else
            {
                var inverseKey = new Point3DAndDirection(pos, Utils.InvertDirection(direction));
                if (GameState.SplitterSwitches.ContainsKey(inverseKey))
                    return GameState.SplitterSwitches[inverseKey] == TurnerDirection.Left ? TurnerDirection.Right : TurnerDirection.Left;
                else
                    return TurnerDirection.Left;
            }
        }

        public void FlipSplitterState(Point3D pos, Direction direction)
        {
            foreach (var adjustPos in GetMatchingVerticalBlockPositions(pos, b => b.Physics == BlockPhysics.Splitter))
            {
                var key = new Point3DAndDirection(adjustPos, direction);
                var newDir = GetSplitterSwitchState(pos, direction) == TurnerDirection.Left ? TurnerDirection.Right : TurnerDirection.Left;
                if (GameState.SplitterSwitches.ContainsKey(key))
                    GameState.SplitterSwitches[key] = newDir;
                else
                    GameState.SplitterSwitches.Add(key, newDir);
            }
        }

        public void RemoveBlock(int blockX, int blockY, int blockZ, bool includeBuilderBricks, bool includeOneWayBlocks)
        {
            if (includeBuilderBricks)
                GameState.BuilderBricks.RemoveAll(brick => brick.Position == new Point3D(blockX, blockY, blockZ));

            if (blockZ > 0) // can't destroy blocks on ground level
            {
                var existingBlock = GameState.Blocks[blockX, blockY, blockZ];
                if (
                    existingBlock != null &&
                    !PhysicsConstants.IndestructibleBlockTypes.Contains(existingBlock.Physics) &&
                    !PhysicsConstants.NonSolidBlockTypes.Contains(existingBlock.Physics) &&
                    (includeOneWayBlocks || (existingBlock.OneWayDirection == null))
                   )
                {
                    GameState.Blocks[blockX, blockY, blockZ] = null;
                    RemovedOnLatestIterationBlocks.Add(new Point3D(blockX, blockY, blockZ));
                }
            }
        }

        public void DamageBlock(int blockX, int blockY, int blockZ)
        {
            if (blockZ > 0)
            {
                var existingBlock = GameState.Blocks[blockX, blockY, blockZ];
                if (existingBlock != null)
                    if (!PhysicsConstants.IndestructibleBlockTypes.Contains(existingBlock.Physics) &&
                        !PhysicsConstants.NonSolidBlockTypes.Contains(existingBlock.Physics))
                        if (!DamagedOnLatestIterationBlocks.Contains(new Point3D(blockX, blockY, blockZ)))
                            DamagedOnLatestIterationBlocks.Add(new Point3D(blockX, blockY, blockZ));
            }
        }

        public void ApplyBomberEffect(Point3D blockPos)
        {
            for (int offset = -1; offset < 3; offset++)
            {
                RemoveBlock(blockPos.X, blockPos.Y, blockPos.Z + offset, true, false);
                if (offset >= 0 && offset <= 1)
                {
                    RemoveBlock(blockPos.X + 1, blockPos.Y, blockPos.Z + offset, true, false);
                    RemoveBlock(blockPos.X - 1, blockPos.Y, blockPos.Z + offset, true, false);
                    RemoveBlock(blockPos.X, blockPos.Y + 1, blockPos.Z + offset, true, false);
                    RemoveBlock(blockPos.X, blockPos.Y - 1, blockPos.Z + offset, true, false);
                }
            }
        }

        public void HandleSkillAssignment(Skill skill, Lemming lemming, TurnerDirection turnerDirection = TurnerDirection.None)
        {
            if (GameState.Skillset[skill] > 0 && lemming.MayBeAssignedSkill(skill))
            {
                switch (skill)
                {
                    case Skill.Blocker:
                        HandleBlockerAssignment(lemming);
                        break;
                    case Skill.Turner:
                        HandleTurnerAssignment(lemming, turnerDirection);
                        break;
                    case Skill.Bomber:
                        HandleBomberAssignment(lemming);
                        break;
                    case Skill.Builder:
                        HandleBuilderAssignment(lemming);
                        break;
                    case Skill.Basher:
                        HandleBasherAssignment(lemming);
                        break;
                    case Skill.Miner:
                        HandleMinerAssignment(lemming);
                        break;
                    case Skill.Digger:
                        HandleDiggerAssignment(lemming);
                        break;
                    case Skill.Climber:
                        HandleClimberAssignment(lemming);
                        break;
                    case Skill.Floater:
                        HandleFloaterAssignment(lemming);
                        break;
                    default:
                        throw new ArgumentException("Invalid skill", nameof(skill));
                }
            }
        }

        private bool HandleBlockerAssignment(Lemming lemming)
        {
            if (lemming.Action != LemmingAction.Walker)
            {
                lemming.Transition(LemmingAction.Walker);
                lemming.SkipNextMovement = true;
            }

            lemming.QueuedSkill = QueuedSkill.Blocker;
            GameState.Skillset[Skill.Blocker]--;
            return true;
        }

        private bool HandleTurnerAssignment(Lemming lemming, TurnerDirection direction)
        {
            if (lemming.Action != LemmingAction.Walker)
            {
                lemming.Transition(LemmingAction.Walker);
                lemming.SkipNextMovement = true;
            }

            lemming.QueuedSkill = QueuedSkill.Turner;
            lemming.TurnerDirection = direction;
            GameState.Skillset[Skill.Turner]--;
            return true;
        }

        private bool HandleBomberAssignment(Lemming lemming)
        {
            lemming.Transition(LemmingAction.Bomber);
            GameState.Skillset[Skill.Bomber]--;
            return true;
        }

        private bool HandleBuilderAssignment(Lemming lemming)
        {
            if (lemming.Action != LemmingAction.Walker)
                lemming.Transition(LemmingAction.Walker);

            lemming.QueuedSkill = QueuedSkill.BuilderBegin;
            GameState.Skillset[Skill.Builder]--;
            return true;
        }

        private bool HandleBasherAssignment(Lemming lemming)
        {
            if (lemming.Action != LemmingAction.Walker)
            {
                bool wasDigger = lemming.Action == LemmingAction.Digger;
                lemming.Transition(LemmingAction.Walker);
                lemming.SkipNextMovement = !wasDigger;
            }

            lemming.QueuedSkill = QueuedSkill.BasherBegin;
            GameState.Skillset[Skill.Basher]--;
            return true;
        }

        private bool HandleMinerAssignment(Lemming lemming)
        {
            if (lemming.Action != LemmingAction.Walker)
            {
                lemming.Transition(LemmingAction.Walker);
                lemming.SkipNextMovement = true;
            }

            lemming.QueuedSkill = QueuedSkill.MinerBegin;
            GameState.Skillset[Skill.Miner]--;
            return true;
        }

        private bool HandleDiggerAssignment(Lemming lemming)
        {
            if (lemming.Action != LemmingAction.Walker)
            {
                lemming.Transition(LemmingAction.Walker);
                lemming.SkipNextMovement = true;
            }

            lemming.QueuedSkill = QueuedSkill.Digger;
            GameState.Skillset[Skill.Digger]--;
            return true;
        }

        private bool HandleClimberAssignment(Lemming lemming)
        {
            lemming.IsClimber = true;
            GameState.Skillset[Skill.Climber]--;
            return true;
        }

        private bool HandleFloaterAssignment(Lemming lemming)
        {
            lemming.IsFloater = true;
            GameState.Skillset[Skill.Floater]--;
            return true;
        }

        private void HandleActivateNuke()
        {
            GameState.NukeActive = true;
        }

        public void RecordAssignment(Skill skill, Lemming lemming, bool cancelLaterActions, TurnerDirection direction = TurnerDirection.None)
        {
            if (cancelLaterActions)
                Replay.Actions.RemoveAll(a => a.Iteration > GameState.Iteration);

            if (GameState.Skillset[skill] > 0 && lemming.MayBeAssignedSkill(skill))
                Replay.AddAssignment(Iteration, lemming.Identifier, skill, direction);
        }

        public void RecordNuke(bool cancelLaterActions)
        {
            if (cancelLaterActions)
                Replay.Actions.RemoveAll(a => a.Iteration > GameState.Iteration);

            Replay.AddNuke(GameState.Iteration);
        }

        public void RecordSpawnRateChange(int newRate, bool cancelLaterActions)
        {
            if (cancelLaterActions)
                Replay.Actions.RemoveAll(a => a.Iteration > GameState.Iteration);

            Replay.AddSpawnRateChange(GameState.Iteration, Math.Clamp(newRate, Level.LevelInfo.SpawnRate, PhysicsConstants.MAX_SPAWN_RATE));
        }
    }
}
