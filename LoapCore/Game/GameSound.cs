﻿using LoapCore.Misc;
using System;
using System.Collections.Generic;

namespace LoapCore.Game
{
    public struct GameSound : IEquatable<GameSound>
    {
        public string SoundEffect;
        public Point3D? Position;

        public GameSound(string soundEffect, Point3D? position)
        {
            SoundEffect = soundEffect;
            Position = position;
        }

        public override bool Equals(object? obj)
        {
            return obj is GameSound sound && Equals(sound);
        }

        public bool Equals(GameSound other)
        {
            return SoundEffect == other.SoundEffect &&
                   EqualityComparer<Point3D?>.Default.Equals(Position, other.Position);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(SoundEffect, Position);
        }

        public static bool operator ==(GameSound left, GameSound right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(GameSound left, GameSound right)
        {
            return !(left == right);
        }
    }
}
