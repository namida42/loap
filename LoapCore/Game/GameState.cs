﻿using LoapCore.Blocks;
using LoapCore.Entities;
using LoapCore.Lemmings;
using LoapCore.Misc;
using System.Collections.Generic;

namespace LoapCore.Game
{
    public class GameState
    {
        public int Iteration;

        public int LemmingsToSpawn;
        public int LemmingSpawnTimer;
        public int SpawnRate;

        public int LemmingsRescued;
        public int LastRequiredLemmingSaveIteration = -1;
        public int LastIterationWithLemmingsAlive = -1;

        public readonly Dictionary<Skill, int> Skillset = new Dictionary<Skill, int>();
        public bool NukeActive;

        public readonly BlockStructure Blocks = new BlockStructure();
        public readonly List<Lemming> Lemmings = new List<Lemming>();

        public readonly List<Point3DAndDirection> BuilderBricks = new List<Point3DAndDirection>();
        public readonly List<EffectField> EffectFields = new List<EffectField>();

        /// <summary>
        /// The Direction component of the key is the direction the *lemming* is facing.
        /// </summary>
        public readonly Dictionary<Point3DAndDirection, TurnerDirection> SplitterSwitches = new Dictionary<Point3DAndDirection, TurnerDirection>();
        public readonly Dictionary<Point3DAndDirection, int> ExitFrames = new Dictionary<Point3DAndDirection, int>();

        public readonly List<EntityInstance> Entities = new List<EntityInstance>();

        public GameState()
        {
            foreach (Skill skill in Utils.GetEnumValues<Skill>())
                Skillset.Add(skill, 0);
        }

        public void CloneFrom(GameState other)
        {
            Iteration = other.Iteration;

            LemmingsToSpawn = other.LemmingsToSpawn;
            LemmingSpawnTimer = other.LemmingSpawnTimer;
            SpawnRate = other.SpawnRate;

            LemmingsRescued = other.LemmingsRescued;
            LastRequiredLemmingSaveIteration = other.LastRequiredLemmingSaveIteration;
            LastIterationWithLemmingsAlive = other.LastIterationWithLemmingsAlive;

            foreach (var skill in Utils.GetEnumValues<Skill>())
                Skillset[skill] = other.Skillset[skill];
            NukeActive = other.NukeActive;

            Blocks.Clone(other.Blocks);

            Lemmings.Clear();
            foreach (var lem in other.Lemmings)
                Lemmings.Add(lem.Clone());

            BuilderBricks.Clear();
            BuilderBricks.AddRange(other.BuilderBricks);

            EffectFields.Clear();
            EffectFields.AddRange(other.EffectFields);

            SplitterSwitches.Clear();
            foreach (var pair in other.SplitterSwitches)
                SplitterSwitches.Add(pair.Key, pair.Value);

            ExitFrames.Clear();
            foreach (var pair in other.ExitFrames)
                ExitFrames.Add(pair.Key, pair.Value);
        }

        public GameState Clone()
        {
            var result = new GameState();
            result.CloneFrom(this);
            return result;
        }
    }
}
