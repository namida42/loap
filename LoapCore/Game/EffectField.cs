﻿using LoapCore.Misc;
using System;

namespace LoapCore.Game
{
    public struct EffectField : IEquatable<EffectField>
    {
        public bool Transient;
        public FieldType Effect;

        public Point3D Position;
        public Direction Side;
        public TurnerDirection Direction;
        public Point3D Destination;

        public string SourceIdentifier;

        public static EffectField CreateBlocker(Point3D position, bool yAxis, string sourceLemmingIdentifier)
        {
            return new EffectField(
                true,
                FieldType.Blocker,
                position,
                yAxis ? Misc.Direction.YMinus : Misc.Direction.XMinus,
                TurnerDirection.None,
                sourceLemmingIdentifier
                );
        }

        public static EffectField CreateTurner(Point3D position, Direction sideOfBlock, TurnerDirection turnDirection, string sourceLemmingIdentifier)
        {
            return new EffectField(
                true,
                FieldType.Turner,
                position,
                sideOfBlock,
                turnDirection,
                sourceLemmingIdentifier
                );
        }

        private EffectField(bool transient, FieldType effect, Point3D position, Direction side, TurnerDirection direction, string sourceIdentifier)
        {
            Transient = transient;
            Effect = effect;
            Position = position;
            Side = side;
            Direction = direction;
            SourceIdentifier = sourceIdentifier;
            Destination = new Point3D();
        }

        public override bool Equals(object? obj)
        {
            return obj is EffectField field && Equals(field);
        }

        public bool Equals(EffectField other)
        {
            return Transient == other.Transient &&
                   Effect == other.Effect &&
                   Position.Equals(other.Position) &&
                   Side == other.Side &&
                   Direction == other.Direction &&
                   SourceIdentifier == other.SourceIdentifier;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Transient, Effect, Position, Side, Direction, SourceIdentifier);
        }

        public static bool operator ==(EffectField left, EffectField right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(EffectField left, EffectField right)
        {
            return !(left == right);
        }
    }
}
