﻿using System.Collections.Generic;

namespace LoapCore.L3DImport
{
    public class L3DLevelInfo
    {
        public string Title = "";
        public string Comment = "";

        public int LemmingCount;
        public int SaveRequirement;
        public int ReleaseRate;
        public int TimeLimit;

        public int LowerBoundaryX;
        public int UpperBoundaryX;
        public int LowerBoundaryY;
        public int UpperBoundaryY;
        public int UpperBoundaryZ;

        public readonly int[] SkillCounts = new int[9];

        public int ThemeIndex;
        public int TextureSetIndex;
        public int SkyIndex;
        public int LandSetIndex;
        public int SeaIndex;
        public int StaticObjectSetIndex;
        public int AnimatedObjectIndex;
        public int InteractiveObjectIndex;
        public int SignSetIndex;
        public int WallSetIndex;
        public int MusicIndex;

        public int WaterSpeedX;
        public int WaterSpeedY;

        public bool SolidLevelBottom;
        public bool SlipperyLevelBottom;
        public bool SlipperyLands;
        public bool NoDrowning;

        public bool SolidColorSky;
        public bool FullHeightSkyTexture;
        public bool SolidColorSea;
        public bool AnimatedSea;
        public bool LargeSeaTexture;
        public bool InvisibleLand;
        public bool LargeLandTexture;
        public bool ShrinkLandTexture;

        public bool Fun8WallEffect;

        public bool DisableMinimapClickDrag;
        public bool ForceHideMinimap;
        public bool FixedPreviewCamera;

        public int PreviewCameraPivotX;
        public int PreviewCameraPivotY;
        public int PreviewCameraPivotZ;

        public readonly List<List<L3DLandVertex>> Lands = new List<List<L3DLandVertex>>();
        public readonly L3DCameraPosition[] Cameras = new L3DCameraPosition[4];
    }

    public struct L3DLandVertex
    {
        public int X;
        public int Y;
        public int GraphicIndex;
        public int Shading;
    }

    public struct L3DCameraPosition
    {
        public int RawX;
        public int RawY;
        public int RawZ;
        public int Rotation;
    }
}
