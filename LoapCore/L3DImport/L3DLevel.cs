﻿namespace LoapCore.L3DImport
{
    public class L3DLevel
    {
        public readonly L3DLevelInfo LevelInfo;
        public readonly L3DBlockInstance[,,] BlockInstances;
        public readonly int[,,] ObjectInstances;
        public readonly L3DMetablock[] Metablocks;

        public L3DLevel(L3DLevelInfo levelInfo, L3DBlockInstance[,,] blockInstances, int[,,] objectInstances, L3DMetablock[] metablocks)
        {
            LevelInfo = levelInfo;
            BlockInstances = blockInstances;
            ObjectInstances = objectInstances;
            Metablocks = metablocks;
        }
    }
}
