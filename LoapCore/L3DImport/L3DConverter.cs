﻿using LoapCore.Blocks;
using LoapCore.Constants;
using LoapCore.Entities;
using LoapCore.Levels;
using LoapCore.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LoapCore.L3DImport
{
    public static class L3DConverter
    {
        public static Level LoadL3DLevel(string levelFilename, string metablockFilename)
        {
            L3DLevel l3dLevel = L3DLoader.LoadL3DLevelFromFiles(levelFilename, metablockFilename);
            return ConvertLevel(l3dLevel);
        }

        public static Level LoadL3DLevel(Stream levelStream, Stream metablockStream)
        {
            L3DLevel l3dLevel = L3DLoader.LoadL3DLevelFromStreams(levelStream, metablockStream);
            return ConvertLevel(l3dLevel);
        }

        public static Level ConvertLevel(L3DLevel src)
        {
            Level result = new Level();

            ConvertMetainfo(src, result);
            ConvertLand(src, result);
            ConvertBlockStructure(src, result);
            ConvertObjects(src, result);
            ValidatePairedObjects(src, result);

            result.Sanitize();

            return result;
        }

        public static int ReleaseRateToSpawnRate(int releaseRate)
        {
            return Math.Clamp((int)Math.Round((releaseRate + 1) / 10f, MidpointRounding.AwayFromZero) - 1, 0, PhysicsConstants.MAX_SPAWN_RATE);
        }

        public static string GetMusicName(int themeIndex, int musicIndex)
        {
            var musicIdentifier = new L3DTranslation.IntegerPair(themeIndex, musicIndex);
            if (L3DTranslation.MusicMaps.ContainsKey(musicIdentifier))
                return L3DTranslation.MusicMaps[musicIdentifier];
            else
                return "";
        }

        private static void ConvertMetainfo(L3DLevel src, Level dst)
        {
            dst.LevelInfo.Title = src.LevelInfo.Title;

            dst.LevelInfo.LemmingCount = src.LevelInfo.LemmingCount;
            dst.LevelInfo.SaveRequirement = src.LevelInfo.SaveRequirement;
            dst.LevelInfo.TimeLimit = src.LevelInfo.TimeLimit;
            dst.LevelInfo.SpawnRate = ReleaseRateToSpawnRate(src.LevelInfo.ReleaseRate);

            dst.LevelInfo.Skillset[Skill.Blocker] = src.LevelInfo.SkillCounts[0];
            dst.LevelInfo.Skillset[Skill.Turner] = src.LevelInfo.SkillCounts[1];
            dst.LevelInfo.Skillset[Skill.Bomber] = src.LevelInfo.SkillCounts[2];
            dst.LevelInfo.Skillset[Skill.Builder] = src.LevelInfo.SkillCounts[3];
            dst.LevelInfo.Skillset[Skill.Basher] = src.LevelInfo.SkillCounts[4];
            dst.LevelInfo.Skillset[Skill.Miner] = src.LevelInfo.SkillCounts[5];
            dst.LevelInfo.Skillset[Skill.Digger] = src.LevelInfo.SkillCounts[6];
            dst.LevelInfo.Skillset[Skill.Climber] = src.LevelInfo.SkillCounts[7];
            dst.LevelInfo.Skillset[Skill.Floater] = src.LevelInfo.SkillCounts[8];

            dst.LevelInfo.Music = GetMusicName(src.LevelInfo.ThemeIndex, src.LevelInfo.MusicIndex);

            if (L3DTranslation.SeaMaps.ContainsKey(src.LevelInfo.SeaIndex) && !src.LevelInfo.FullHeightSkyTexture)
            {
                dst.LevelInfo.Sea.Texture = L3DTranslation.SeaMaps[src.LevelInfo.SeaIndex];
                if (dst.LevelInfo.Sea.Texture == "official/grass" && src.LevelInfo.AnimatedSea && !src.LevelInfo.LargeSeaTexture)
                    dst.LevelInfo.Sea.Texture = "namida/corruption"; // Special case for LP3D Tricky 40 "Data Corruption"

                dst.LevelInfo.Sea.MotionX = -src.LevelInfo.WaterSpeedX * 15;
                dst.LevelInfo.Sea.MotionY = src.LevelInfo.WaterSpeedY * 15;
            }

            dst.LevelInfo.Sea.Effect = src.LevelInfo.SolidLevelBottom ? (src.LevelInfo.SlipperyLevelBottom ? LevelFloorEffect.Slippery : LevelFloorEffect.Solid) :
                src.LevelInfo.NoDrowning ? LevelFloorEffect.Electrocute :
                LevelFloorEffect.Water;

            if (L3DTranslation.SkyMaps.ContainsKey(src.LevelInfo.SkyIndex))
                dst.LevelInfo.Sky.Texture = L3DTranslation.SkyMaps[src.LevelInfo.SkyIndex];
            dst.LevelInfo.Sky.FullHeight = src.LevelInfo.FullHeightSkyTexture;

            ConvertCameras(src.LevelInfo.Cameras, dst.LevelInfo.Cameras);
            ConvertPreviewCamera(src.LevelInfo, dst.LevelInfo.PreviewCamera, dst.LevelInfo.Cameras[0]);

            dst.LevelInfo.DisableMinimap = src.LevelInfo.ForceHideMinimap;

            dst.LevelInfo.BoundaryPoints[Direction.XMinus] = src.LevelInfo.LowerBoundaryX;
            dst.LevelInfo.BoundaryPoints[Direction.XPlus] = src.LevelInfo.UpperBoundaryX - 1;
            dst.LevelInfo.BoundaryPoints[Direction.YMinus] = 32 - src.LevelInfo.UpperBoundaryY;
            dst.LevelInfo.BoundaryPoints[Direction.YPlus] = 32 - src.LevelInfo.LowerBoundaryY - 1;
            dst.LevelInfo.BoundaryPoints[Direction.ZPlus] = (src.LevelInfo.UpperBoundaryZ * 4) - 3;

            if (dst.LevelInfo.BoundaryPoints[Direction.XMinus] >= dst.LevelInfo.BoundaryPoints[Direction.XPlus])
            {
                dst.LevelInfo.BoundaryPoints[Direction.XMinus] = 0;
                dst.LevelInfo.BoundaryPoints[Direction.XPlus] = 30;
            }

            if (dst.LevelInfo.BoundaryPoints[Direction.YMinus] >= dst.LevelInfo.BoundaryPoints[Direction.YPlus])
            {
                dst.LevelInfo.BoundaryPoints[Direction.YMinus] = 0;
                dst.LevelInfo.BoundaryPoints[Direction.YPlus] = 30;
            }

            if (dst.LevelInfo.BoundaryPoints[Direction.ZPlus] < 0)
                dst.LevelInfo.BoundaryPoints[Direction.ZPlus] = 61;
        }

        private static void ConvertPreviewCamera(L3DLevelInfo src, PreviewCameraInfo dest, CameraEntry firstCam)
        {
            if (src.FixedPreviewCamera)
            {
                dest.Style = PreviewCameraStyle.Fixed;
                dest.X = firstCam.X;
                dest.Y = firstCam.Y;
                dest.Z = firstCam.Z;
                dest.Rotation = firstCam.Rotation;
                dest.Pitch = firstCam.Pitch;
            }
            else
            {
                dest.Style = PreviewCameraStyle.Pivot;

                dest.X = (src.PreviewCameraPivotX == 0) ? 16 : src.PreviewCameraPivotX;
                dest.Y = (src.PreviewCameraPivotY == 0) ? 16 : 32 - src.PreviewCameraPivotY;
                dest.Z = (src.PreviewCameraPivotZ == 0) ? 10 : src.PreviewCameraPivotZ + 2;

                dest.Rotation = 0f;
                dest.Pitch = -11.75f;
                dest.Distance = 32f;
            }
        }

        private static void ConvertCameras(L3DCameraPosition[] srcList, List<CameraEntry> destList)
        {
            foreach (var src in srcList)
            {
                var dest = new CameraEntry();

                dest.X = ConvertCameraCoordinate(src.RawX, 16);
                dest.Y = ConvertCameraCoordinate(-src.RawY, 16);
                dest.Z = ConvertCameraCoordinate(src.RawZ, 7.25f);

                dest.Rotation = src.Rotation switch
                {
                    1 => 0,
                    2 => 270,
                    3 => 180,
                    _ => 90
                };

                dest.Pitch = 0;

                destList.Add(dest);
            }
        }

        private static float ConvertCameraCoordinate(int input, float zeroPoint)
        {
            return (Utils.ModuloNegativeAdjusted((input + 32768), 65536) - 32768) / 256f + zeroPoint;
        }

        private static void ConvertLand(L3DLevel src, Level dst)
        {
            foreach (var land in src.LevelInfo.Lands)
                if (land.Count >= 3)
                {
                    LandRegion newLand = new LandRegion();

                    newLand.Effect = (src.LevelInfo.SlipperyLands ? LevelFloorEffect.Slippery : LevelFloorEffect.Solid);

                    if (src.LevelInfo.InvisibleLand)
                        newLand.Texture = "";
                    else if (L3DTranslation.LandMaps.ContainsKey(new L3DTranslation.IntegerPair(src.LevelInfo.LandSetIndex, land[0].GraphicIndex)))
                        newLand.Texture = L3DTranslation.LandMaps[new L3DTranslation.IntegerPair(src.LevelInfo.LandSetIndex, land[0].GraphicIndex)];
                    else if (L3DTranslation.LandMaps.ContainsKey(new L3DTranslation.IntegerPair(src.LevelInfo.LandSetIndex, 0)))
                        newLand.Texture = L3DTranslation.LandMaps[new L3DTranslation.IntegerPair(src.LevelInfo.LandSetIndex, 0)];
                    else
                        newLand.Texture = "";

                    newLand.Shading = land[0].Shading;

                    newLand.Vertices.AddRange(land.Select(l => new LandRegion.RegionVertex()
                    {
                        X = l.X * PhysicsConstants.BLOCK_STEPS,
                        Y = (32 - l.Y) * PhysicsConstants.BLOCK_STEPS
                    }));

                    dst.Lands.Add(newLand);
                }
        }

        private static List<BlockInfo?> ConvertMetablocks(L3DMetablock[] src, int textureSet, bool useElectrocution)
        {
            List<BlockInfo?> result = new List<BlockInfo?>();

            for (int i = 0; i < src.Length; i++)
            {
                var srcBlock = src[i];

                if (srcBlock.Faces[0].RawTextureIndex == 255 && srcBlock.Faces[1].RawTextureIndex == 255 && srcBlock.Faces[2].RawTextureIndex == 255 &&
                    srcBlock.Faces[3].RawTextureIndex == 255 && srcBlock.Faces[4].RawTextureIndex == 255 && srcBlock.Faces[5].RawTextureIndex == 255 &&
                    srcBlock.NonSolidForCamera && srcBlock.NonSolidForLemmings && srcBlock.DoubleSidedRender)
                {
                    result.Add(null);
                    continue;
                }

                var dstBlock = new BlockInfo();

                dstBlock.Physics = i switch
                {
                    0 => BlockPhysics.Entrance,
                    2 => BlockPhysics.Splitter,
                    _ => srcBlock.NonSolidForLemmings ? BlockPhysics.NonSolid : BlockPhysics.Solid
                };

                dstBlock.Visibility =
                    (i == 3) ? BlockVisibility.AntiRender :
                    (i == 4) ? BlockVisibility.Invisible :
                    srcBlock.Faces.Values.Any(f => IsFaceTransparent(f)) ? BlockVisibility.Transparent :
                    BlockVisibility.Normal;

                dstBlock.OneWayDirection = i switch
                {
                    5 => Direction.YMinus,
                    6 => Direction.XMinus,
                    7 => Direction.YPlus,
                    8 => Direction.XPlus,
                    _ => null
                };

                if (dstBlock.Physics == BlockPhysics.Solid && srcBlock.Steel)
                    dstBlock.Physics = BlockPhysics.Steel;

                dstBlock.Slippery = srcBlock.SlipperyTopFace;
                dstBlock.Antisplat = srcBlock.AntiSplatTopFace;
                dstBlock.IgnoredByCamera = srcBlock.NonSolidForCamera;

                ConvertFace(srcBlock.Faces[0], dstBlock.Faces[Direction.YMinus], i, srcBlock, textureSet);
                ConvertFace(srcBlock.Faces[1], dstBlock.Faces[Direction.YPlus], i, srcBlock, textureSet);
                ConvertFace(srcBlock.Faces[2], dstBlock.Faces[Direction.XPlus], i, srcBlock, textureSet);
                ConvertFace(srcBlock.Faces[3], dstBlock.Faces[Direction.XMinus], i, srcBlock, textureSet);
                ConvertFace(srcBlock.Faces[4], dstBlock.Faces[Direction.ZPlus], i, srcBlock, textureSet);
                ConvertFace(srcBlock.Faces[5], dstBlock.Faces[Direction.ZMinus], i, srcBlock, textureSet);

                if (srcBlock.LiquidTopFace)
                    dstBlock.Physics = useElectrocution ? BlockPhysics.Electrocute : BlockPhysics.Water;

                result.Add(dstBlock);
            }

            return result;
        }

        private static bool IsFaceTransparent(L3DMetablockFace face)
        {
            return face.HasTransparency || face.DoubleSidedTexturePairModifier;
        }

        private static void ConvertFace(L3DMetablockFace src, FaceInfo dst, int blockIndex, L3DMetablock srcBlock, int textureSet)
        {
            if (src.Dummied)
                return;

            bool isExit = (blockIndex == 1 && dst.Side == Direction.YMinus);

            int baseFrame = L3DImportUtils.GetMetablockFaceTextureIndex(src);
            int frameCount = (isExit && baseFrame == 8) ? 4 : L3DImportUtils.GetMetablockFaceAnimationFrames(src);

            for (int i = 0; i < frameCount; i++)
            {
                var srcKey = new L3DTranslation.IntegerPair(textureSet, baseFrame + i);

                if (L3DTranslation.TextureSetMaps.ContainsKey(srcKey))
                {
                    dst.TextureOuter.Add(new BlockTextureRef(
                        L3DTranslation.TextureSetMaps[srcKey].File,
                        L3DTranslation.TextureSetMaps[srcKey].Index,
                        0, false,
                        src.Shading
                        ));
                }
            }

            if (dst.TextureOuter.Count > 0)
            {
                if (dst.TextureOuter.All(t => t == dst.TextureOuter.First()))
                    dst.TextureOuter.RemoveRange(1, dst.TextureOuter.Count - 1);
                else if (src.FiveFrameReverseAnimation || src.FourFrameReverseAnimation)
                    for (int i = frameCount - 2; i > 0; i--)
                        dst.TextureOuter.Add(dst.TextureOuter[i]);
            }

            if (srcBlock.DoubleSidedRender || src.DoubleSidedTexturePairModifier)
            {
                int insideFrame;
                if (src.DoubleSidedTexturePairModifier)
                    insideFrame = (baseFrame % 2 == 0) ? baseFrame - 1 : baseFrame + 1;
                else
                    insideFrame = baseFrame;

                var insideFrameKey = new L3DTranslation.IntegerPair(textureSet, insideFrame);
                if (L3DTranslation.TextureSetMaps.ContainsKey(insideFrameKey))
                {
                    dst.TextureInner.Add(new BlockTextureRef(
                        L3DTranslation.TextureSetMaps[insideFrameKey].File,
                        L3DTranslation.TextureSetMaps[insideFrameKey].Index,
                        0, false,
                        src.Shading
                        ));
                }
            }

            if (isExit)
                dst.Physics = dst.TextureOuter.Distinct().Count() > 1 ? FacePhysics.AnimateExit : FacePhysics.Exit;
        }

        private static void ConvertBlockStructure(L3DLevel src, Level dst)
        {
            dst.BlockStructure.SetSize(32, 32, 61);

            List<BlockInfo?> samples = ConvertMetablocks(src.Metablocks, src.LevelInfo.TextureSetIndex, src.LevelInfo.NoDrowning);

            for (int srcZ = 0; srcZ < 16; srcZ++)
            {
                int dstBaseZ = srcZ * 4 - 3;

                for (int y = 0; y < 32; y++)
                    for (int x = 0; x < 32; x++)
                    {
                        var srcBlock = src.BlockInstances[x, y, srcZ];

                        if (srcBlock.SegmentMask == 0)
                            continue;

                        if (srcBlock.BlockShapeIndex == 0x01 && (srcBlock.SegmentMask & 0x0C) != 0)
                            continue;

                        if (srcBlock.BlockShapeIndex == 0x02 && (srcBlock.SegmentMask & 0x03) != 0)
                            continue;

                        var sample = samples[srcBlock.BlockID];

                        if (sample == null)
                            continue;

                        var shape = ConvertBlockShapeIndex(srcBlock.BlockShapeIndex);

                        if (shape == BlockShape.Ramp22Above)
                        {
                            int highestSeg = -1;
                            int lowestSeg = -1;
                            for (int testSeg = 0; testSeg < 4; testSeg++)
                            {
                                if ((srcBlock.SegmentMask & (1 << testSeg)) != 0)
                                {
                                    highestSeg = testSeg;
                                    if (lowestSeg < 0)
                                        lowestSeg = testSeg;
                                }
                            }

                            if (highestSeg >= 0)
                            {
                                var newBlock = DeriveBlock(sample, BlockShape.Ramp22Above, highestSeg, srcBlock.Rotation, srcBlock.BlockID, 1);
                                dst.BlockStructure[x, y, dstBaseZ + highestSeg] = newBlock;
                            }

                            if (highestSeg - lowestSeg >= 1)
                            {
                                var newBlock = DeriveBlock(sample, BlockShape.Ramp22Above, highestSeg - 1, srcBlock.Rotation, srcBlock.BlockID, 0);
                                dst.BlockStructure[x, y, dstBaseZ + highestSeg - 1] = newBlock;
                            }

                            for (int i = highestSeg - 2; i >= lowestSeg; i--)
                            {
                                var newBlock = DeriveBlock(sample, BlockShape.Solid, i, srcBlock.Rotation, srcBlock.BlockID);
                                dst.BlockStructure[x, y, dstBaseZ + i] = newBlock;
                            }
                        }
                        else if (shape == BlockShape.Ramp22Below)
                        {
                            int highestSeg = -1;
                            int lowestSeg = -1;
                            for (int testSeg = 0; testSeg < 4; testSeg++)
                            {
                                if ((srcBlock.SegmentMask & (1 << testSeg)) != 0)
                                {
                                    highestSeg = testSeg;
                                    if (lowestSeg < 0)
                                        lowestSeg = testSeg;
                                }
                            }

                            if (lowestSeg >= 0)
                            {
                                var newBlock = DeriveBlock(sample, BlockShape.Ramp22Below, lowestSeg, srcBlock.Rotation, srcBlock.BlockID, 0);
                                dst.BlockStructure[x, y, dstBaseZ + lowestSeg] = newBlock;
                            }

                            if (highestSeg - lowestSeg >= 1)
                            {
                                var newBlock = DeriveBlock(sample, BlockShape.Ramp22Below, lowestSeg + 1, srcBlock.Rotation, srcBlock.BlockID, 1);
                                dst.BlockStructure[x, y, dstBaseZ + lowestSeg + 1] = newBlock;
                            }

                            for (int i = lowestSeg + 2; i <= highestSeg; i++)
                            {
                                var newBlock = DeriveBlock(sample, BlockShape.Solid, i, srcBlock.Rotation, srcBlock.BlockID);
                                dst.BlockStructure[x, y, dstBaseZ + i] = newBlock;
                            }
                        }
                        else
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                if ((srcBlock.SegmentMask & (1 << i)) == 0)
                                    continue;

                                var newBlock = DeriveBlock(sample, shape, i, srcBlock.Rotation, srcBlock.BlockID);
                                dst.BlockStructure[x, y, dstBaseZ + i] = newBlock;
                            }
                        }

                        if (sample.Visibility == BlockVisibility.Invisible)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                var block = dst.BlockStructure[x, y, dstBaseZ + i];
                                if (block != null)
                                    if (block.Shape == BlockShape.LargePyramidAbove)
                                    {
                                        if (i == 0)
                                            dst.BlockStructure[x, y, dstBaseZ + i] = null;
                                        else
                                        {
                                            block.Shape = BlockShape.SolidNarrow;
                                            block.Segment = 0;
                                        }
                                    }
                            }
                        }

                        if (sample.Visibility == BlockVisibility.Transparent)
                        {
                            int lowestSeg = 4;
                            int highestSeg = -1;
                            for (int i = 0; i < 4; i++)
                                if ((srcBlock.SegmentMask & (1 << i)) != 0)
                                {
                                    lowestSeg = Math.Min(i, lowestSeg);
                                    highestSeg = Math.Max(i, highestSeg);
                                }

                            for (int i = lowestSeg; i <= highestSeg; i++)
                            {
                                var thisBlock = dst.BlockStructure[x, y, dstBaseZ + i];
                                if (thisBlock == null)
                                    continue; // this should NEVER happen but it keeps compiler warnings happy

                                if (i > lowestSeg)
                                {
                                    if ((thisBlock.Shape != BlockShape.Ramp22Above || thisBlock.Segment != 0) &&
                                        (thisBlock.Shape != BlockShape.Ramp22Below || thisBlock.Segment != 1))
                                    {
                                        thisBlock.Faces[Direction.ZMinus].TextureOuter.Clear();
                                        thisBlock.Faces[Direction.ZMinus].TextureInner.Clear();
                                    }
                                }
                                if (i < highestSeg)
                                {
                                    if ((thisBlock.Shape != BlockShape.Ramp22Above || thisBlock.Segment != 0) &&
                                        (thisBlock.Shape != BlockShape.Ramp22Below || thisBlock.Segment != 1))
                                    {
                                        thisBlock.Faces[Direction.ZPlus].TextureOuter.Clear();
                                        thisBlock.Faces[Direction.ZPlus].TextureInner.Clear();
                                    }
                                }
                            }
                        }
                    }
            }
        }

        private static BlockInfo DeriveBlock(BlockInfo sample, BlockShape shape, int srcSegment, int srcRotations, int blockIndex, int internalSegment = -1)
        {
            var newBlock = sample.MakeClone();
            newBlock.Shape = shape;
            newBlock.Segment = (internalSegment >= 0) ? internalSegment :
                               (shape == BlockShape.Solid || shape == BlockShape.Deflector) ? 0 :
                               (shape == BlockShape.SmallPyramidBelow) ? srcSegment - 2 :
                               srcSegment;
            newBlock.Rotations = AdaptBlockRotations(srcRotations, blockIndex == 0, shape, out int faceRotations);
            AdjustFaceTextures(newBlock, faceRotations, srcSegment);
            if (srcSegment != 2 && newBlock.Physics == BlockPhysics.Entrance)
                newBlock.Physics = BlockPhysics.Steel;
            if (srcSegment == 3 && newBlock.Physics == BlockPhysics.Splitter)
                newBlock.Physics = BlockPhysics.Steel;
            if (sample.OneWayDirection.HasValue)
                newBlock.OneWayDirection = Utils.RotateDirection(sample.OneWayDirection.Value, srcRotations - newBlock.Rotations);

            return newBlock;
        }

        public static BlockShape ConvertBlockShapeIndex(int index)
        {
            return index switch
            {
                0 => BlockShape.Solid,
                1 => BlockShape.SmallPyramidAbove,
                2 => BlockShape.SmallPyramidBelow,
                3 => BlockShape.LargePyramidAbove,
                4 => BlockShape.LargePyramidBelow,
                5 => BlockShape.Ramp45Above,
                6 => BlockShape.Ramp45Below,
                7 => BlockShape.Deflector,
                8 => BlockShape.Ramp22Above,
                9 => BlockShape.Ramp22Below,
                10 => BlockShape.Corner90Above,
                11 => BlockShape.Corner90Below,
                12 => BlockShape.Corner45Above,
                13 => BlockShape.Corner45Below,
                _ => BlockShape.Solid
            };
        }

        public static void AdjustFaceTextures(BlockInfo block, int faceRotations, int segmentHeight)
        {
            foreach (var dir in PhysicsConstants.NonVerticalDirections)
            {
                for (int i = 0; i < block.Faces[dir].TextureOuter.Count; i++)
                {
                    var oldRef = block.Faces[dir].TextureOuter[i];
                    block.Faces[dir].TextureOuter[i] = new BlockTextureRef(oldRef.File, oldRef.Index + segmentHeight, oldRef.Rotations, oldRef.Flip, oldRef.Shading);
                }

                for (int i = 0; i < block.Faces[dir].TextureInner.Count; i++)
                {
                    var oldRef = block.Faces[dir].TextureInner[i];
                    block.Faces[dir].TextureInner[i] = new BlockTextureRef(oldRef.File, oldRef.Index + segmentHeight, oldRef.Rotations, oldRef.Flip, oldRef.Shading);
                }
            }

            for (int i = 0; i < faceRotations; i++)
            {
                var tempFace = new FaceInfo(Direction.YPlus);
                tempFace.Clone(block.Faces[Direction.YPlus]);
                block.Faces[Direction.YPlus].Clone(block.Faces[Direction.XPlus]);
                block.Faces[Direction.XPlus].Clone(block.Faces[Direction.YMinus]);
                block.Faces[Direction.YMinus].Clone(block.Faces[Direction.XMinus]);
                block.Faces[Direction.XMinus].Clone(tempFace);
            }

            if (block.Rotations < 2)
                foreach (var dir in PhysicsConstants.VerticalDirections)
                {
                    for (int i = 0; i < block.Faces[dir].TextureOuter.Count; i++)
                    {
                        var oldRef = block.Faces[dir].TextureOuter[i];
                        block.Faces[dir].TextureOuter[i] = new BlockTextureRef(oldRef.File, oldRef.Index, oldRef.Rotations + 2, oldRef.Flip, oldRef.Shading);
                    }

                    for (int i = 0; i < block.Faces[dir].TextureInner.Count; i++)
                    {
                        var oldRef = block.Faces[dir].TextureInner[i];
                        block.Faces[dir].TextureInner[i] = new BlockTextureRef(oldRef.File, oldRef.Index, oldRef.Rotations + 2, oldRef.Flip, oldRef.Shading);
                    }
                }
        }

        public static int AdaptBlockRotations(int srcRotations, bool isEntrance, BlockShape shape, out int faceRotations)
        {
            int result = 6 - srcRotations;

            if (srcRotations % 2 == 0)
                result += 2;
            if (isEntrance && (result % 2 == 1))
                result += 2;

            faceRotations = 0;

            if (shape == BlockShape.Corner45Above || shape == BlockShape.Corner45Below || shape == BlockShape.Deflector)
            {
                result += 1;
                faceRotations += 3;
            }

            faceRotations %= 4;

            return result % 4;
        }

        /// <summary>
        /// Used for object positioning. Would be more accurately "check if block exists".
        /// </summary>
        private static bool CheckForSolid(BlockStructure structure, Point3D position)
        {
            var block = structure[position.X, position.Y, Math.Max(position.Z, 0)];
            return block != null;
        }

        private enum ObjectShiftBehavior { None, NoEffectObjectShift, TrapShift, TransporterShift }

        /// <summary>
        /// When shiftBehavior is TransporterShift, may return int.MinValue to indicate "delete the object".
        /// </summary>
        private static int CalculateObjectShift(Point3D basePos, BlockStructure blockStructure, ObjectShiftBehavior shiftBehavior)
        {
            switch (shiftBehavior)
            {
                case ObjectShiftBehavior.None:
                    return 0;
                case ObjectShiftBehavior.NoEffectObjectShift:
                    {
                        if (CheckForSolid(blockStructure, new Point3D(basePos.X, basePos.Y, basePos.Z + 3)))
                            return 0;

                        for (int i = 2; i >= 0; i--)
                            if (CheckForSolid(blockStructure, new Point3D(basePos.X, basePos.Y, basePos.Z + i)))
                                return i + 1;

                        return 0;
                    }
                case ObjectShiftBehavior.TrapShift:
                    {
                        for (int i = 3; i >= 0; i--)
                            if (CheckForSolid(blockStructure, new Point3D(basePos.X, basePos.Y, basePos.Z + i)))
                                return i + 1;

                        return 0;
                    }
                case ObjectShiftBehavior.TransporterShift:
                    {
                        for (int i = 3; i >= 0; i--)
                            if (CheckForSolid(blockStructure, new Point3D(basePos.X, basePos.Y, basePos.Z + i)))
                                return i + 1;

                        return int.MinValue;
                    }
                default:
                    throw new ArgumentException("Invalid shift behavior", nameof(shiftBehavior));
            }
        }

        public static void ConvertObjects(L3DLevel src, Level dst)
        {
            for (int z = 0; z < 16; z++)
            {
                int dstBaseZ = (z * 4) - 3;

                for (int y = 0; y < 32; y++)
                    for (int x = 0; x < 32; x++)
                    {
                        var objIndex = src.ObjectInstances[x, y, z];
                        if (objIndex == 0)
                            continue;
                        else if (objIndex <= 0x1F)
                        {
                            // Static objects
                            if (objIndex > 20)
                                objIndex = 1;
                            var key = new L3DTranslation.IntegerPair(src.LevelInfo.StaticObjectSetIndex, objIndex - 1);
                            if (L3DTranslation.StaticObjectMaps.ContainsKey(key))
                            {
                                var newEntity = new LevelEntity();
                                newEntity.Identifier = L3DTranslation.StaticObjectMaps[key];
                                newEntity.Position = new Point3D(x, y, dstBaseZ);
                                newEntity.Position.Z += CalculateObjectShift(newEntity.Position, dst.BlockStructure, ObjectShiftBehavior.NoEffectObjectShift);

                                dst.Entities.Add(newEntity);
                            }
                        }
                        else if (objIndex <= 0x3F)
                        {
                            // Signs
                            int outerIndex = (objIndex & 0xFC) switch
                            {
                                0x20 => 8,
                                0x24 => 9,
                                0x28 => 10,
                                0x2C => 11,
                                0x30 => 0,
                                0x34 => 1,
                                0x38 => 4,
                                0x3C => 5,
                                _ => 8
                            };

                            int innerIndex = (objIndex & 0xFC) switch
                            {
                                0x20 => 12,
                                0x24 => 13,
                                0x28 => 14,
                                0x2C => 15,
                                0x30 => 2,
                                0x34 => 3,
                                0x38 => 6,
                                0x3C => 7,
                                _ => 12
                            };

                            if (L3DTranslation.SignMaps.ContainsKey(new L3DTranslation.IntegerPair(src.LevelInfo.SignSetIndex, outerIndex)) &&
                                L3DTranslation.SignMaps.ContainsKey(new L3DTranslation.IntegerPair(src.LevelInfo.SignSetIndex, innerIndex)))
                            {
                                var newOverlay = new LevelOverlay();

                                var frontMapping = L3DTranslation.SignMaps[new L3DTranslation.IntegerPair(src.LevelInfo.SignSetIndex, outerIndex)];
                                newOverlay.FrontTexture.File = frontMapping.File;
                                newOverlay.FrontTexture.Index = frontMapping.Index;

                                var backMapping = L3DTranslation.SignMaps[new L3DTranslation.IntegerPair(src.LevelInfo.SignSetIndex, innerIndex)];
                                newOverlay.BackTexture.File = backMapping.File;
                                newOverlay.BackTexture.Index = backMapping.Index;

                                newOverlay.Position = new Point3D(x, y, dstBaseZ);

                                newOverlay.Side = (objIndex & 0x03) switch
                                {
                                    0x00 => Direction.XPlus,
                                    0x01 => Direction.YPlus,
                                    0x02 => Direction.XMinus,
                                    0x03 => Direction.YMinus,
                                    _ => Direction.YPlus
                                };

                                dst.Overlays.Add(newOverlay);

                                for (int i = 1; i < (outerIndex >= 8 ? 4 : 2); i++)
                                {
                                    var overlaySlab = new LevelOverlay();
                                    overlaySlab.Position = new Point3D(newOverlay.Position.X, newOverlay.Position.Y, newOverlay.Position.Z + i);
                                    overlaySlab.Side = newOverlay.Side;
                                    overlaySlab.FrontTexture = newOverlay.FrontTexture;
                                    overlaySlab.BackTexture = newOverlay.BackTexture;
                                    overlaySlab.FrontTexture.Index += i;
                                    overlaySlab.BackTexture.Index += i;

                                    dst.Overlays.Add(overlaySlab);
                                }
                            }
                        }
                        else if (objIndex >= 0x50 && objIndex <= 0x5F)
                        {
                            // Animated objects
                            var key = src.LevelInfo.AnimatedObjectIndex;
                            if (L3DTranslation.AnimatedObjectMaps.ContainsKey(key))
                            {
                                var newEntity = new LevelEntity();
                                newEntity.Identifier = L3DTranslation.AnimatedObjectMaps[key];
                                newEntity.Position = new Point3D(x, y, dstBaseZ);
                                newEntity.Position.Z += CalculateObjectShift(newEntity.Position, dst.BlockStructure, ObjectShiftBehavior.NoEffectObjectShift);

                                if (objIndex > 0x51)
                                    switch (objIndex % 4)
                                    {
                                        case 0:
                                            newEntity.Offset.X = -16;
                                            newEntity.Offset.Y = 16;
                                            break;
                                        case 1:
                                            newEntity.Offset.X = -16;
                                            newEntity.Offset.Y = -16;
                                            break;
                                        case 2:
                                            newEntity.Offset.X = 16;
                                            newEntity.Offset.Y = -16;
                                            break;
                                        case 3:
                                            newEntity.Offset.X = 16;
                                            newEntity.Offset.Y = 16;
                                            break;
                                    }

                                dst.Entities.Add(newEntity);
                            }
                        }
                        else if (objIndex <= 0x67)
                        {
                            // Traps
                            switch (src.LevelInfo.InteractiveObjectIndex)
                            {
                                case 0x00:
                                    {
                                        // Flamethrower
                                        var newEntity = new LevelEntity();
                                        newEntity.Identifier = "official_interactive/trap_flamethrower";
                                        newEntity.Position = new Point3D(x, y, dstBaseZ);
                                        newEntity.Rotations = -(objIndex & 0x03);

                                        dst.Entities.Add(newEntity);

                                        for (int i = 0; i < 4; i++)
                                        {
                                            var overlay = new LevelOverlay();
                                            overlay.Position = new Point3D(x, y, dstBaseZ + i);
                                            overlay.Side = Utils.RotateDirection(Direction.YPlus, newEntity.Rotations);
                                            overlay.BackTexture.File = "official_general/overlay02";
                                            overlay.BackTexture.Index = 12 + i;
                                            dst.Overlays.Add(overlay);
                                        }
                                    }
                                    break;
                                case 0x01:
                                    {
                                        // Squasher
                                        var newEntity = new LevelEntity();
                                        newEntity.Identifier = "official_interactive/trap_squasher";
                                        newEntity.Position = new Point3D(x, y, dstBaseZ);
                                        newEntity.Position.Z += CalculateObjectShift(newEntity.Position, dst.BlockStructure, ObjectShiftBehavior.TrapShift);

                                        dst.Entities.Add(newEntity);
                                    }
                                    break;
                                case 0x02:
                                    {
                                        // Bear trap
                                        var newEntity = new LevelEntity();
                                        newEntity.Identifier = "official_interactive/trap_beartrap";
                                        newEntity.Position = new Point3D(x, y, dstBaseZ);
                                        newEntity.Position.Z += CalculateObjectShift(newEntity.Position, dst.BlockStructure, ObjectShiftBehavior.TrapShift);

                                        dst.Entities.Add(newEntity);
                                    }
                                    break;
                                case 0x03:
                                    {
                                        // Trampoline

                                        int offset = CalculateObjectShift(new Point3D(x, y, dstBaseZ), dst.BlockStructure, ObjectShiftBehavior.TransporterShift);

                                        if (offset >= 0)
                                        {
                                            var newEntity = new LevelEntity();
                                            newEntity.Identifier = ((objIndex & 0x04) == 0) ?
                                                "official_interactive/trampoline_strong" :
                                                "official_interactive/trampoline_weak";
                                            newEntity.Position = new Point3D(x, y, dstBaseZ + offset);

                                            dst.Entities.Add(newEntity);
                                        }
                                    }
                                    break;
                                case 0x04:
                                    {
                                        // Spring

                                        int offset = CalculateObjectShift(new Point3D(x, y, dstBaseZ), dst.BlockStructure, ObjectShiftBehavior.TransporterShift);

                                        if (offset >= 0)
                                        {
                                            var newEntity = new LevelEntity();
                                            newEntity.Identifier = ((objIndex & 0x01) == 0) ?
                                                "official_interactive/spring" :
                                                "official_interactive/spring_pad";
                                            newEntity.Position = new Point3D(x, y, dstBaseZ + offset);
                                            newEntity.PairingID = (objIndex - 0x60) / 2 + 1;

                                            dst.Entities.Add(newEntity);
                                        }
                                    }
                                    break;
                                case 0x05:
                                    {
                                        // Teleporter

                                        int offset = CalculateObjectShift(new Point3D(x, y, dstBaseZ), dst.BlockStructure, ObjectShiftBehavior.TransporterShift);

                                        if (offset >= 0)
                                        {
                                            var newEntity = new LevelEntity();
                                            newEntity.Identifier = "official_interactive/teleporter";
                                            newEntity.Position = new Point3D(x, y, dstBaseZ + offset);
                                            newEntity.PairingID = (objIndex & 0x0F) + 1;

                                            dst.Entities.Add(newEntity);
                                        }
                                    }
                                    break;
                                case 0x06:
                                    {
                                        // Alien trap
                                        var newEntity = new LevelEntity();
                                        newEntity.Identifier = "official_interactive/trap_alien";
                                        newEntity.Position = new Point3D(x, y, dstBaseZ);
                                        newEntity.Position.Z += CalculateObjectShift(newEntity.Position, dst.BlockStructure, ObjectShiftBehavior.TrapShift);

                                        dst.Entities.Add(newEntity);
                                    }
                                    break;
                                case 0x07:
                                    {
                                        // Laser trap
                                        var newEntity = new LevelEntity();
                                        newEntity.Identifier = "official_interactive/trap_laser";
                                        newEntity.Position = new Point3D(x, y, dstBaseZ);
                                        newEntity.Rotations = (objIndex & 0x01);

                                        dst.Entities.Add(newEntity);
                                    }
                                    break;
                                case 0x08:
                                    {
                                        // Rope slide

                                        int offset = CalculateObjectShift(new Point3D(x, y, dstBaseZ), dst.BlockStructure, ObjectShiftBehavior.TransporterShift);

                                        if (offset < 0 && ((objIndex & 0x01) == 1))
                                            offset = 4; // I don't know why this is correct, but Death Slide is the example case.

                                        if (offset >= 0)
                                        {
                                            var newEntity = new LevelEntity();
                                            newEntity.Identifier = (objIndex & 0x01) == 0 ? "official_interactive/rope_slide_start" : "official_interactive/rope_slide_end";
                                            newEntity.Position = new Point3D(x, y, dstBaseZ + offset);
                                            newEntity.PairingID = (objIndex - 0x60) / 2 + 1;

                                            dst.Entities.Add(newEntity);
                                        }
                                    }
                                    break;
                                default:
                                    // Do nothing.
                                    break;
                            }
                        }
                        else if (objIndex >= 0x70 && objIndex <= 0xCF)
                        {
                            // Walls
                            if (src.LevelInfo.Fun8WallEffect)
                            {
                                var newOverlay = new LevelOverlay();

                                newOverlay.FrontTexture.File = "*screen";
                                newOverlay.FrontTexture.Index = (objIndex & 0x0F) * 4;
                                newOverlay.BackTexture = newOverlay.FrontTexture;

                                newOverlay.Position = new Point3D(x, y, dstBaseZ);

                                newOverlay.Side = (objIndex & 0xF0) switch
                                {
                                    0x70 => Direction.XPlus,
                                    0x80 => Direction.YPlus,
                                    0x90 => Direction.XMinus,
                                    0xA0 => Direction.YMinus,
                                    0xB0 => Direction.ZMinus,
                                    0xC0 => Direction.ZPlus,
                                    _ => Direction.YPlus
                                };

                                if (newOverlay.Side == Direction.ZMinus || newOverlay.Side == Direction.ZPlus)
                                {
                                    newOverlay.FrontTexture.Rotations = 1;
                                    newOverlay.BackTexture.Rotations = 3;
                                }

                                dst.Overlays.Add(newOverlay);

                                if (newOverlay.Side == Direction.ZPlus)
                                    newOverlay.Position.Z += 3;
                                else if (newOverlay.Side != Direction.ZMinus)
                                {
                                    for (int i = 1; i < 4; i++)
                                    {
                                        var overlaySlab = new LevelOverlay();
                                        overlaySlab.Position = new Point3D(newOverlay.Position.X, newOverlay.Position.Y, newOverlay.Position.Z + i);
                                        overlaySlab.Side = newOverlay.Side;
                                        overlaySlab.FrontTexture = newOverlay.FrontTexture;
                                        overlaySlab.BackTexture = newOverlay.BackTexture;
                                        overlaySlab.FrontTexture.Index += i;
                                        overlaySlab.BackTexture.Index += i;

                                        dst.Overlays.Add(overlaySlab);
                                    }
                                }
                            }
                            else if (L3DTranslation.WallMaps.ContainsKey(new L3DTranslation.IntegerPair(src.LevelInfo.WallSetIndex, objIndex & 0x0F)))
                            {
                                var newOverlay = new LevelOverlay();

                                var mapping = L3DTranslation.WallMaps[new L3DTranslation.IntegerPair(src.LevelInfo.WallSetIndex, objIndex & 0x0F)];
                                newOverlay.FrontTexture.File = mapping.File;
                                newOverlay.FrontTexture.Index = mapping.Index;
                                newOverlay.BackTexture = newOverlay.FrontTexture;

                                newOverlay.Position = new Point3D(x, y, dstBaseZ);

                                newOverlay.Side = (objIndex & 0xF0) switch
                                {
                                    0x70 => Direction.XPlus,
                                    0x80 => Direction.YPlus,
                                    0x90 => Direction.XMinus,
                                    0xA0 => Direction.YMinus,
                                    0xB0 => Direction.ZMinus,
                                    0xC0 => Direction.ZPlus,
                                    _ => Direction.YPlus
                                };

                                if (newOverlay.Side == Direction.ZMinus || newOverlay.Side == Direction.ZPlus)
                                {
                                    newOverlay.FrontTexture.Rotations = 1;
                                    newOverlay.BackTexture.Rotations = 3;
                                }

                                dst.Overlays.Add(newOverlay);

                                if (newOverlay.Side == Direction.ZPlus)
                                    newOverlay.Position.Z += 3;
                                else if (newOverlay.Side != Direction.ZMinus)
                                {
                                    for (int i = 1; i < 4; i++)
                                    {
                                        var overlaySlab = new LevelOverlay();
                                        overlaySlab.Position = new Point3D(newOverlay.Position.X, newOverlay.Position.Y, newOverlay.Position.Z + i);
                                        overlaySlab.Side = newOverlay.Side;
                                        overlaySlab.FrontTexture = newOverlay.FrontTexture;
                                        overlaySlab.BackTexture = newOverlay.BackTexture;
                                        overlaySlab.FrontTexture.Index += i;
                                        overlaySlab.BackTexture.Index += i;

                                        dst.Overlays.Add(overlaySlab);
                                    }
                                }
                            }
                        }
                    }
            }
        }

        private static void ValidatePairedObjects(L3DLevel src, Level dst)
        {
            switch (src.LevelInfo.InteractiveObjectIndex)
            {
                // 4 spring 5 tele 8 rope
                case 4:
                case 8:
                    {
                        for (int i = 1; i <= 4; i++)
                        {
                            string senderID = "official_interactive/" + ((src.LevelInfo.InteractiveObjectIndex == 4) ?
                                "spring" : "rope_slide_start");
                            string receiverID = "official_interactive/" + ((src.LevelInfo.InteractiveObjectIndex == 4) ?
                                "spring_pad" : "rope_slide_end");

                            if (dst.Entities.Count(e => e.Identifier == senderID && e.PairingID == i) == 0 ||
                                dst.Entities.Count(e => e.Identifier == receiverID && e.PairingID == i) == 0)
                                dst.Entities.RemoveAll(e => e.PairingID == i);
                            else
                            {
                                if (dst.Entities.Count(e => e.Identifier == senderID && e.PairingID == i) >= 1)
                                {
                                    var keepEntity = dst.Entities.Where(e => e.Identifier == senderID && e.PairingID == i)
                                        .OrderBy(e => GetL3DObjectPriority(e))
                                        .Last();

                                    dst.Entities.RemoveAll(e => e.Identifier == senderID && e.PairingID == i);
                                    dst.Entities.Add(keepEntity);
                                }

                                if (dst.Entities.Count(e => e.Identifier == receiverID && e.PairingID == i) >= 1)
                                {
                                    var keepEntity = dst.Entities.Where(e => e.Identifier == receiverID && e.PairingID == i)
                                        .OrderBy(e => GetL3DObjectPriority(e))
                                        .Last();

                                    dst.Entities.RemoveAll(e => e.Identifier == receiverID && e.PairingID == i);
                                    dst.Entities.Add(keepEntity);
                                }
                            }
                        }
                    }
                    break;
                case 5:
                    {
                        for (int i = 1; i <= 8; i++)
                        {
                            if (dst.Entities.Count(e => e.PairingID == i) < 2)
                                dst.Entities.RemoveAll(e => e.PairingID == i);
                            else if (dst.Entities.Count(e => e.PairingID == i) >= 2)
                            {
                                var keepEntities = dst.Entities.Where(e => e.PairingID == i)
                                    .OrderBy(e => GetL3DObjectPriority(e))
                                    .ToArray();

                                dst.Entities.RemoveAll(e => e.PairingID == i);
                                dst.Entities.AddRange(keepEntities);
                            }
                        }
                    }
                    break;
            }
        }

        private static int GetL3DObjectPriority(LevelEntity entity)
        {
            return (31 - entity.Position.Y) +
                (((entity.Position.Z + 3) / 4) * 32) +
                (entity.Position.X * 32 * 16);
        }
    }
}
