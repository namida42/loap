﻿using LoapCore.Files;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace LoapCore.L3DImport
{
    public static class L3DTranslation
    {
        public struct IntegerPair
        {
            public IntegerPair(int value1, int value2)
            {
                Value1 = value1;
                Value2 = value2;
            }

            public int Value1;
            public int Value2;
        }

        public struct TextureRef
        {
            public TextureRef(string file, int index)
            {
                File = file;
                Index = index;
            }

            public string File;
            public int Index;
        }

        public static readonly Dictionary<int, string> ThemeMaps = new Dictionary<int, string>();
        public static readonly Dictionary<IntegerPair, string> MusicMaps = new Dictionary<IntegerPair, string>();
        public static readonly Dictionary<IntegerPair, TextureRef> TextureSetMaps = new Dictionary<IntegerPair, TextureRef>();
        public static readonly Dictionary<IntegerPair, string> LandMaps = new Dictionary<IntegerPair, string>();
        public static readonly Dictionary<int, string> SeaMaps = new Dictionary<int, string>();
        public static readonly Dictionary<int, string> SkyMaps = new Dictionary<int, string>();
        public static readonly Dictionary<IntegerPair, string> StaticObjectMaps = new Dictionary<IntegerPair, string>();
        public static readonly Dictionary<int, string> AnimatedObjectMaps = new Dictionary<int, string>();
        public static readonly Dictionary<IntegerPair, TextureRef> SignMaps = new Dictionary<IntegerPair, TextureRef>();
        public static readonly Dictionary<IntegerPair, TextureRef> WallMaps = new Dictionary<IntegerPair, TextureRef>();

        static L3DTranslation()
        {
            ResetToDefaultTranslationData();
        }

        public static void LoadTranslationData(DataItem translationFile)
        {
            LoadThemeMappings(translationFile);
            LoadMusicMappings(translationFile);
            LoadTextureMappings(translationFile);
            LoadLandMappings(translationFile);
            LoadSeaMappings(translationFile);
            LoadSkyMappings(translationFile);
            LoadStaticObjectMappings(translationFile);
            LoadAnimatedObjectMappings(translationFile);
            LoadSignMappings(translationFile);
            LoadWallMappings(translationFile);
        }

        public static void ResetToDefaultTranslationData()
        {
            ThemeMaps.Clear();
            MusicMaps.Clear();
            TextureSetMaps.Clear();
            LandMaps.Clear();
            SeaMaps.Clear();
            SkyMaps.Clear();
            StaticObjectMaps.Clear();
            AnimatedObjectMaps.Clear();
            SignMaps.Clear();
            WallMaps.Clear();

            using var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("LoapCore.Resources.translation.lpmi");

            if (stream == null)
                throw new FileNotFoundException("Embedded L3D translation data is missing");

            var translationFileData = DataItemReader.LoadFromStream(stream);

            LoadTranslationData(translationFileData);
        }

        private static void LoadThemeMappings(DataItem source)
        {
            foreach (var child in source.GetChildren("THEME"))
            {
                KeyValuePair<int, string> newPair = new KeyValuePair<int, string>(
                    child.GetChildValueInt("SOURCE_INDEX"),
                    child.GetChildValue("TRANSLATION")
                    );

                if (ThemeMaps.ContainsKey(newPair.Key))
                    ThemeMaps[newPair.Key] = newPair.Value;
                else
                    ThemeMaps.Add(newPair.Key, newPair.Value);
            }
        }

        private static void LoadMusicMappings(DataItem source)
        {
            foreach (var child in source.GetChildren("MUSIC"))
            {
                KeyValuePair<IntegerPair, string> newPair = new KeyValuePair<IntegerPair, string>(
                    new IntegerPair(child.GetChildValueInt("SOURCE_THEME_INDEX"), child.GetChildValueInt("SOURCE_MUSIC_INDEX")),
                    child.GetChildValue("TRANSLATION")
                    );

                if (MusicMaps.ContainsKey(newPair.Key))
                    MusicMaps[newPair.Key] = newPair.Value;
                else
                    MusicMaps.Add(newPair.Key, newPair.Value);
            }
        }

        private static void LoadAnimatedObjectMappings(DataItem source)
        {
            foreach (var child in source.GetChildren("ANIMOBJ"))
            {
                KeyValuePair<int, string> newPair = new KeyValuePair<int, string>(
                    child.GetChildValueInt("SOURCE_INDEX"),
                    child.GetChildValue("TRANSLATION")
                    );

                if (AnimatedObjectMaps.ContainsKey(newPair.Key))
                    AnimatedObjectMaps[newPair.Key] = newPair.Value;
                else
                    AnimatedObjectMaps.Add(newPair.Key, newPair.Value);
            }
        }

        private static void LoadStaticObjectMappings(DataItem source)
        {
            foreach (var child in source.GetChildren("OBJ"))
            {
                KeyValuePair<IntegerPair, string> newPair = new KeyValuePair<IntegerPair, string>(
                    new IntegerPair(child.GetChildValueInt("SOURCE_OBJECT_SET_INDEX"), child.GetChildValueInt("SOURCE_OBJECT_INDEX")),
                    child.GetChildValue("TRANSLATION")
                    );

                if (StaticObjectMaps.ContainsKey(newPair.Key))
                    StaticObjectMaps[newPair.Key] = newPair.Value;
                else
                    StaticObjectMaps.Add(newPair.Key, newPair.Value);
            }
        }

        private static void LoadSkyMappings(DataItem source)
        {
            foreach (var child in source.GetChildren("SKY"))
            {
                KeyValuePair<int, string> newPair = new KeyValuePair<int, string>(
                    child.GetChildValueInt("SOURCE_INDEX"),
                    child.GetChildValue("TRANSLATION")
                    );

                if (SkyMaps.ContainsKey(newPair.Key))
                    SkyMaps[newPair.Key] = newPair.Value;
                else
                    SkyMaps.Add(newPair.Key, newPair.Value);
            }
        }

        private static void LoadSeaMappings(DataItem source)
        {
            foreach (var child in source.GetChildren("SEA"))
            {
                KeyValuePair<int, string> newPair = new KeyValuePair<int, string>(
                    child.GetChildValueInt("SOURCE_INDEX"),
                    child.GetChildValue("TRANSLATION")
                    );

                if (SeaMaps.ContainsKey(newPair.Key))
                    SeaMaps[newPair.Key] = newPair.Value;
                else
                    SeaMaps.Add(newPair.Key, newPair.Value);
            }
        }

        private static void LoadLandMappings(DataItem source)
        {
            foreach (var child in source.GetChildren("LAND"))
            {
                KeyValuePair<IntegerPair, string> newPair = new KeyValuePair<IntegerPair, string>(
                    new IntegerPair(child.GetChildValueInt("SOURCE_LAND_SET_INDEX"), child.GetChildValueInt("SOURCE_LAND_INDEX")),
                    child.GetChildValue("TRANSLATION")
                    );

                if (LandMaps.ContainsKey(newPair.Key))
                    LandMaps[newPair.Key] = newPair.Value;
                else
                    LandMaps.Add(newPair.Key, newPair.Value);
            }
        }

        private static void LoadTextureMappings(DataItem source)
        {
            foreach (var child in source.GetChildren("TEXTURE"))
            {
                KeyValuePair<IntegerPair, TextureRef> newPair = new KeyValuePair<IntegerPair, TextureRef>(
                    new IntegerPair(child.GetChildValueInt("SOURCE_TEXTURE_SET_INDEX"), child.GetChildValueInt("SOURCE_TEXTURE_INDEX")),
                    new TextureRef(child.GetChildValue("TRANSLATION_TEXTURE_SET"), child.GetChildValueInt("TRANSLATION_TEXTURE_INDEX"))
                    );

                if (TextureSetMaps.ContainsKey(newPair.Key))
                    TextureSetMaps[newPair.Key] = newPair.Value;
                else
                    TextureSetMaps.Add(newPair.Key, newPair.Value);
            }
        }

        private static void LoadSignMappings(DataItem source)
        {
            foreach (var child in source.GetChildren("SIGN"))
            {
                KeyValuePair<IntegerPair, TextureRef> newPair = new KeyValuePair<IntegerPair, TextureRef>(
                    new IntegerPair(child.GetChildValueInt("SOURCE_SIGN_SET_INDEX"), child.GetChildValueInt("SOURCE_SIGN_INDEX")),
                    new TextureRef(child.GetChildValue("TRANSLATION_TEXTURE_SET"), child.GetChildValueInt("TRANSLATION_TEXTURE_INDEX"))
                    );

                if (SignMaps.ContainsKey(newPair.Key))
                    SignMaps[newPair.Key] = newPair.Value;
                else
                    SignMaps.Add(newPair.Key, newPair.Value);
            }
        }

        private static void LoadWallMappings(DataItem source)
        {
            foreach (var child in source.GetChildren("WALL"))
            {
                KeyValuePair<IntegerPair, TextureRef> newPair = new KeyValuePair<IntegerPair, TextureRef>(
                    new IntegerPair(child.GetChildValueInt("SOURCE_WALL_SET_INDEX"), child.GetChildValueInt("SOURCE_WALL_INDEX")),
                    new TextureRef(child.GetChildValue("TRANSLATION_TEXTURE_SET"), child.GetChildValueInt("TRANSLATION_TEXTURE_INDEX"))
                    );

                if (WallMaps.ContainsKey(newPair.Key))
                    WallMaps[newPair.Key] = newPair.Value;
                else
                    WallMaps.Add(newPair.Key, newPair.Value);
            }
        }
    }
}
