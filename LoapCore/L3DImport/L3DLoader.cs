﻿using DeRNC;
using LoapCore.Misc;
using System;
using System.Collections.Generic;
using System.IO;

namespace LoapCore.L3DImport
{
    public static class L3DLoader
    {
        // As the L3D file format is very much a set in stone thing that will
        // never change, we will not be using consts for the most part here.
        // Please do not continue reading if you are afraid of magic numbers.

        public static L3DLevel LoadL3DLevelFromFiles(string levelFilename, string metablockFilename)
        {
            using FileStream lF = new FileStream(levelFilename, FileMode.Open);
            using FileStream mF = new FileStream(metablockFilename, FileMode.Open);
            return LoadL3DLevelFromStreams(lF, mF);
        }

        public static L3DLevel LoadL3DLevelFromStreams(Stream levelStream, Stream metablockStream)
        {
            Stream internalLevelStream = new MemoryStream();
            long oldPos = levelStream.Position;
            var rncStatus = Rnc.ReadRnc(levelStream, internalLevelStream);
            if (rncStatus == RncStatus.FileIsNotRnc)
            {
                levelStream.Position = oldPos;
                internalLevelStream = levelStream;
            }
            else
                internalLevelStream.Position = 0;

            byte[] header = new byte[512];
            byte[] blocks = new byte[32768];
            byte[] objects = new byte[32768];
            byte[] metablocks = new byte[1408];

            internalLevelStream.Read(header);
            internalLevelStream.Read(blocks);
            internalLevelStream.Read(objects);
            metablockStream.Read(metablocks);

            var levelInfo = ReadLevelInfo(header);
            var levelBlocks = ReadBlocks(blocks);
            var levelObjects = ReadObjects(objects);
            var levelMetablocks = ReadMetablocks(metablocks);

            return new L3DLevel(levelInfo, levelBlocks, levelObjects, levelMetablocks);
        }

        public static L3DLevelInfo ReadLevelInfo(byte[] header)
        {
            if (header.Length != 512)
                throw new ArgumentException("Header should be exactly 512 bytes", nameof(header));

            L3DLevelInfo info = new L3DLevelInfo();

            info.TimeLimit = header[0x0000] + (header[0x0001] * 60);

            for (int i = 0; i < 9; i++)
                info.SkillCounts[header[0x000A + (i * 2)]] = header[0x000B + (i * 2)];

            for (int landIndex = 0; landIndex < 8; landIndex++)
            {
                int landBasePos = 0x0022 + (landIndex * 24);
                List<L3DLandVertex> landVertices = new List<L3DLandVertex>();

                for (int vertexIndex = 0; vertexIndex < 8; vertexIndex++)
                {
                    int vertBasePos = landBasePos + (vertexIndex * 3);
                    L3DLandVertex newVertex = new L3DLandVertex();
                    newVertex.Y = header[vertBasePos + 0x00];
                    newVertex.X = header[vertBasePos + 0x01];

                    if (newVertex.Y == 0)
                        break;

                    byte newVertexShadingTexture = header[vertBasePos + 0x02];
                    newVertex.GraphicIndex = newVertexShadingTexture & 0x07;
                    newVertex.Shading = (newVertexShadingTexture >> 5) & 0x07;

                    landVertices.Add(newVertex);
                }

                if (landVertices.Count > 0)
                    info.Lands.Add(landVertices);
            }

            info.ReleaseRate = Utils.GetMultiByteValueFromBytes(header, 0x00E2, 2);
            info.LemmingCount = Utils.GetMultiByteValueFromBytes(header, 0x00E4, 2);
            info.SaveRequirement = Utils.GetMultiByteValueFromBytes(header, 0x00E6, 2);

            info.TextureSetIndex = header[0x00E8];
            info.LandSetIndex = header[0x00E9];

            info.Title = Utils.GetStringFromBytes(header, 0x00F0, 32);

            info.StaticObjectSetIndex = header[0x0110];
            info.SignSetIndex = header[0x0111];
            info.SeaIndex = header[0x0112];
            info.AnimatedObjectIndex = header[0x0113];
            info.InteractiveObjectIndex = header[0x0114];
            info.SkyIndex = header[0x0115];
            info.WallSetIndex = header[0x0117];

            info.WaterSpeedY = header[0x0118];
            info.WaterSpeedX = header[0x0119];

            int flags11B = Utils.GetMultiByteValueFromBytes(header, 0x011B, 2);
            info.SolidColorSea = (flags11B & 0x0001) != 0;
            info.SolidColorSky = (flags11B & 0x0002) != 0;
            info.InvisibleLand = (flags11B & 0x0004) != 0;
            info.DisableMinimapClickDrag = (flags11B & 0x0008) != 0;
            info.AnimatedSea = (flags11B & 0x0010) == 0; // not a mistake, this one is meant to be ==
            info.LargeSeaTexture = (flags11B & 0x0020) != 0;
            info.FixedPreviewCamera = (flags11B & 0x0040) != 0;
            info.SolidLevelBottom = (flags11B & 0x0080) != 0;
            info.LargeLandTexture = (flags11B & 0x0100) != 0;
            info.FullHeightSkyTexture = (flags11B & 0x0200) != 0;
            info.Fun8WallEffect = (flags11B & 0x0400) != 0;
            info.ShrinkLandTexture = (flags11B & 0x0800) != 0;
            info.ForceHideMinimap = (flags11B & 0x1000) != 0;
            info.NoDrowning = (flags11B & 0x8000) != 0;

            info.Comment = Utils.GetStringFromBytes(header, 0x011D, 32);

            info.ThemeIndex = header[0x0143];
            info.MusicIndex = header[0x0151];

            info.LowerBoundaryY = header[0x0153];
            info.LowerBoundaryX = header[0x0154];
            info.UpperBoundaryY = header[0x0155];
            info.UpperBoundaryX = header[0x0156];

            int flags157 = header[0x0157];
            info.SlipperyLevelBottom = (flags157 & 0x01) != 0;
            info.SlipperyLands = (flags157 & 0x02) != 0;

            info.PreviewCameraPivotZ = header[0x0159];
            info.PreviewCameraPivotY = header[0x015A];
            info.PreviewCameraPivotX = header[0x015B];

            info.UpperBoundaryZ = header[0x015C];

            for (int i = 0; i < 4; i++)
            {
                int cameraBasePos = 0x015D + (i * 8);
                L3DCameraPosition newCamera = new L3DCameraPosition();
                newCamera.RawZ = Utils.GetMultiByteValueFromBytes(header, cameraBasePos + 0x00, 2);
                newCamera.RawY = Utils.GetMultiByteValueFromBytes(header, cameraBasePos + 0x02, 2);
                newCamera.RawX = Utils.GetMultiByteValueFromBytes(header, cameraBasePos + 0x04, 2);
                newCamera.Rotation = header[cameraBasePos + 0x06];
                info.Cameras[i] = newCamera;
            }

            return info;
        }

        public static L3DBlockInstance[,,] ReadBlocks(byte[] blockData)
        {
            if (blockData.Length != 32768)
                throw new ArgumentException("Block data should be exactly 32768 bytes", nameof(blockData));

            L3DBlockInstance[,,] result = new L3DBlockInstance[32, 32, 16];
            int dataPos = 0;

            for (int x = 0; x < 32; x++)
                for (int z = 0; z < 16; z++)
                    for (int y = 0; y < 32; y++)
                    {
                        L3DBlockInstance newBlock = new L3DBlockInstance();
                        int thisBlockRaw = Utils.GetMultiByteValueFromBytes(blockData, dataPos, 2);
                        dataPos += 2;

                        newBlock.BlockID = (thisBlockRaw >> 10) & 0x3F;
                        newBlock.BlockShapeIndex = (thisBlockRaw >> 6) & 0x0F;
                        newBlock.Rotation = (thisBlockRaw >> 4) & 0x03;
                        newBlock.SegmentMask = thisBlockRaw & 0x0F;

                        result[x, 31 - y, z] = newBlock;
                    }

            return result;
        }

        public static int[,,] ReadObjects(byte[] objectData)
        {
            if (objectData.Length != 32768)
                throw new ArgumentException("Object data should be exactly 32768 bytes", nameof(objectData));

            int[,,] result = new int[32, 32, 16];
            int dataPos = 0;

            for (int x = 0; x < 32; x++)
                for (int z = 0; z < 16; z++)
                    for (int y = 0; y < 32; y++)
                    {
                        result[x, 31 - y, z] = objectData[dataPos];
                        dataPos += 2;
                    }

            return result;
        }

        public static L3DMetablock[] ReadMetablocks(byte[] metablockData)
        {
            if (metablockData.Length != 1408)
                throw new ArgumentException("Metablock data should be exactly 1408 bytes", nameof(metablockData));

            L3DMetablock[] result = new L3DMetablock[64];

            for (int i = 0; i < 64; i++)
            {
                int metablockPos = 0x0016 * i;
                L3DMetablock newBlock = new L3DMetablock();

                byte flags = metablockData[metablockPos + 0x02];
                newBlock.DoubleSidedRender = (flags & 0x01) != 0;
                newBlock.Steel = (flags & 0x02) != 0;
                newBlock.LiquidTopFace = (flags & 0x04) != 0;
                newBlock.NonSolidForLemmings = (flags & 0x10) != 0;
                newBlock.AntiSplatTopFace = (flags & 0x20) != 0;
                newBlock.SlipperyTopFace = (flags & 0x40) != 0;
                newBlock.NonSolidForCamera = (flags & 0x80) != 0;

                for (int f = 0; f < 6; f++)
                {
                    int facePos = metablockPos + 0x04 + (f * 0x03);
                    newBlock.Faces[f].RawTextureIndex = metablockData[facePos + 0x00];

                    byte faceFlags = metablockData[facePos + 0x01];
                    newBlock.Faces[f].DoubleSidedTexturePairModifier = (faceFlags & 0x08) != 0;
                    newBlock.Faces[f].FiveFrameReverseAnimation = (faceFlags & 0x10) != 0;
                    newBlock.Faces[f].FourFrameReverseAnimation = (faceFlags & 0x20) != 0;
                    newBlock.Faces[f].SpecialAnimation = (faceFlags & 0x40) != 0;
                    newBlock.Faces[f].HasTransparency = (faceFlags & 0x80) != 0;
                    newBlock.Faces[f].Dummied = (faceFlags & 0xFF) == 0xFF;

                    newBlock.Faces[f].Shading = metablockData[facePos + 0x02];
                }

                result[i] = newBlock;
            }

            return result;
        }
    }
}
