﻿namespace LoapCore.L3DImport
{
    public static class L3DImportUtils
    {
        public static int GetMetablockFaceTextureIndex(L3DMetablockFace face)
        {

            if (face.DoubleSidedTexturePairModifier)
            {
                if (face.RawTextureIndex >= 8 && face.RawTextureIndex <= 19)
                    return 35 + face.RawTextureIndex; // 43 - 8 + RawTextureIndex
                else
                    return -1;
            }

            if (face.SpecialAnimation)
            {
                return face.RawTextureIndex switch
                {
                    0 => 12,
                    1 => 16,
                    2 => 0,
                    3 => 28,
                    4 => 32,
                    5 => 36,
                    6 => 0,
                    19 => 0,
                    20 => 0,
                    _ => -1,
                };
            }

            return face.RawTextureIndex;

        }

        public static int GetMetablockFaceAnimationFrames(L3DMetablockFace face)
        {
            if (face.SpecialAnimation)
            {
                if (face.RawTextureIndex == 2)
                    return 8;
                else if (face.RawTextureIndex < 7)
                    return 4;
                else
                    return 1;
            }

            if (face.FourFrameReverseAnimation)
                return 4;

            if (face.FiveFrameReverseAnimation)
                return 5;

            return 1;
        }
    }
}
