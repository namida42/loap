﻿using System.Collections.Generic;

namespace LoapCore.L3DImport
{
    public class L3DMetablock
    {
        public L3DMetablock()
        {
            for (int dir = 0; dir < 6; dir++)
                Faces.Add(dir, new L3DMetablockFace());
        }

        public readonly Dictionary<int, L3DMetablockFace> Faces = new Dictionary<int, L3DMetablockFace>();

        public bool Steel;
        public bool NonSolidForLemmings;
        public bool AntiSplatTopFace;
        public bool LiquidTopFace;
        public bool SlipperyTopFace;

        public bool DoubleSidedRender;
        public bool NonSolidForCamera;
    }

    public class L3DMetablockFace
    {
        public int RawTextureIndex;
        public int Shading;

        public bool DoubleSidedTexturePairModifier;
        public bool FiveFrameReverseAnimation;
        public bool FourFrameReverseAnimation;
        public bool SpecialAnimation;
        public bool HasTransparency;

        public bool Dummied;
    }
}
