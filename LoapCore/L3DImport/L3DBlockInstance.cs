﻿namespace LoapCore.L3DImport
{
    public struct L3DBlockInstance
    {
        public int BlockID;
        public int BlockShapeIndex;
        public int Rotation;
        public int SegmentMask;
    }
}
