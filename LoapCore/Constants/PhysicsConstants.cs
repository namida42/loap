﻿using LoapCore.Misc;

namespace LoapCore.Constants
{
    public static class PhysicsConstants
    {
        public const int ITERATIONS_PER_INGAME_SECOND = 30;

        public const int BLOCK_SEGMENTS = 4; // The number of vertical sections in one block
        public const int BLOCK_STEPS = 64; // The value actually used in game for the number of steps from one side of a block to another. Should be divisible by BLOCK_SEGMENTS
        public const int HALF_BLOCK_STEPS = BLOCK_STEPS / 2; // For quicker reference
        public const int QUARTER_BLOCK_STEPS = HALF_BLOCK_STEPS / 2; // Ditto
        public const int THREE_QUARTER_BLOCK_STEPS = QUARTER_BLOCK_STEPS * 3; // And again
        public const int SEGMENT_STEPS = BLOCK_STEPS / BLOCK_SEGMENTS; // The height of one vertical section of a block
        public const int HALF_SEGMENT_STEPS = SEGMENT_STEPS / 2; // For quicker reference
        public const float SEGMENT_SIZE_FLOAT = 1f / BLOCK_SEGMENTS; // The size of a segment for texture coordinates
        public const float STEP_SIZE_FLOAT = 1f / BLOCK_STEPS; // The size of a step

        public const int LAND_HEIGHT = SEGMENT_STEPS;

        public const int SOLIDITY_CHECK_POINTS = 9;
        public const int SOLIDITY_CHECK_MID_POINT = SOLIDITY_CHECK_POINTS / 2;
        public const int SOLIDITY_CHECK_DISTANCE = BLOCK_STEPS / (SOLIDITY_CHECK_POINTS - 1);
        public const int SOLIDITY_LOW = 0;
        public const int SOLIDITY_MID = SEGMENT_STEPS / 2;
        public const int SOLIDITY_HIGH = SEGMENT_STEPS;
        public const int SOLIDITY_NONE = -1;

        public const int MAX_SPAWN_RATE = 9;
        public const int FIRST_SPAWN_DELAY = 60;

        public const int STUN_DURATION = 75;
        public const int STUN_FALL_DISTANCE = BLOCK_STEPS * 3;
        public const int FATAL_FALL_DISTANCE = BLOCK_STEPS * 4;

        public const int BUILDER_BRICK_LIMIT = 6;
        public const int SHRUG_DURATION = 30;

        public const float CAMERA_PITCH_CAP = 45f; // Not REALLY physics but don't want to create a dedicated file just for it...
        public const float CAMERA_MIN_Z = (SEGMENT_SIZE_FLOAT * 2) + STEP_SIZE_FLOAT;

        public static readonly Direction[] NonVerticalDirections = new[] { Direction.YPlus, Direction.XMinus, Direction.YMinus, Direction.XPlus };
        public static readonly Direction[] VerticalDirections = new[] { Direction.ZMinus, Direction.ZPlus };
        public static readonly BlockPhysics[] NonSolidBlockTypes = new[] { BlockPhysics.NonSolid, BlockPhysics.Splitter };
        public static readonly BlockPhysics[] IndestructibleBlockTypes = new[] { BlockPhysics.Steel, BlockPhysics.Entrance, BlockPhysics.Water, BlockPhysics.Electrocute };
        public static readonly FacePhysics[] ExitFaceTypes = new[] { FacePhysics.Exit, FacePhysics.AnimateExit };
        public static readonly BlockShape[] DiggerDestroyAboveShapes = new[] { BlockShape.Ramp45Above, BlockShape.Ramp22Above, BlockShape.Corner90Above, BlockShape.Corner45Above, BlockShape.CornerInsideAbove };

        /*public enum BlockShape
    {
        Solid, SolidNarrow,
        Deflector,
        Ramp45Above, Ramp45Below, Ramp22Above, Ramp22Below,
        Corner90Above, Corner90Below, Corner45Above, Corner45Below, CornerInsideAbove, CornerInsideBelow,
        LargePyramidAbove, LargePyramidBelow, SmallPyramidAbove, SmallPyramidBelow
    }*/
    }
}
