﻿namespace LoapCore.Constants
{
    public static class DataItemConstants
    {
        public const char FILE_COMMENT_CHAR = '#';
        public const char FILE_BEGIN_SECTION_CHAR = '$';
        public const string FILE_END_SECTION = "$END";
        public const int FILE_INDENT_SPACES = 2;

        public const float CAMERA_FLOAT_PRECISION = 1 / 512f;
    }
}
