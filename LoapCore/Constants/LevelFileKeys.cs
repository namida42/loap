﻿namespace LoapCore.Constants
{
    public static class LevelFileKeys
    {
        public const string INFO_TITLE = "TITLE";

        public const string INFO_LEMMING_COUNT = "LEMMINGS";
        public const string INFO_SAVE_REQUIREMENT = "REQUIREMENT";
        public const string INFO_TIME_LIMIT = "TIME_LIMIT";
        public const string INFO_SPAWN_RATE = "SPAWN_RATE";

        public const string INFO_SKILLSET = "SKILLSET";

        public const string INFO_MUSIC = "MUSIC";

        public const string INFO_SEA = "SEA";
        public const string INFO_SEA_TEXTURE1 = "TEXTURE";
        public const string INFO_SEA_MOVEMENT_X = "MOVEMENT_X";
        public const string INFO_SEA_MOVEMENT_Y = "MOVEMENT_Y";
        public const string INFO_SEA_EFFECT = "EFFECT";

        public const string INFO_SKY = "SKY";
        public const string INFO_SKY_TEXTURE1 = "TEXTURE";
        public const string INFO_SKY_FULL_HEIGHT = "FULL_HEIGHT";

        public const string CAMERA = "CAMERA";
        public const string CAMERA_X = "X";
        public const string CAMERA_Y = "Y";
        public const string CAMERA_Z = "Z";
        public const string CAMERA_ROTATION = "ROTATION";
        public const string CAMERA_PITCH = "PITCH";

        public const string PREVIEW_CAMERA = "PREVIEW_CAMERA";
        public const string PREVIEW_CAMERA_STYLE = "STYLE";
        public const string PREVIEW_CAMERA_DISTANCE = "DISTANCE";

        public const string DISABLE_MINIMAP = "DISABLE_MINIMAP";

        public const string BOUNDARY_BASE = "BOUNDARY_";

        public const string LAND = "LAND";

        public const string LAND_TEXTURE = "TEXTURE";
        public const string LAND_SHADING = "SHADING";
        public const string LAND_VERTEX = "VERTEX";
        public const string LAND_EFFECT = "EFFECT";


        public const string LAYOUT_WIDTH = "WIDTH";
        public const string LAYOUT_DEPTH = "DEPTH";
        public const string LAYOUT_HEIGHT = "HEIGHT";

        public const string LAYOUT_STRUCTURE = "BLOCK_STRUCTURE";
        public const string LAYOUT_LAYER = "LAYER";
        public const string LAYOUT_LINE = "LINE";


        public const string OVERLAY = "OVERLAY";
        public const string OVERLAY_X = "X";
        public const string OVERLAY_Y = "Y";
        public const string OVERLAY_Z = "Z";
        public const string OVERLAY_SIDE = "SIDE";
        public const string OVERLAY_OUTER = "OUTER";
        public const string OVERLAY_INNER = "INNER";
        public const string OVERLAY_TEXTURE = "TEXTURE";
        public const string OVERLAY_TEXTURE_INDEX = "TEXTURE_INDEX";
        public const string OVERLAY_ROTATIONS = "ROTATION";
        public const string OVERLAY_FLIP = "FLIP_HORIZONTAL";
        public const string OVERLAY_SHADING = "SHADING";


        public const string ENTITY = "ENTITY";
        public const string ENTITY_IDENTIFIER = "IDENTIFIER";
        public const string ENTITY_X = "X";
        public const string ENTITY_Y = "Y";
        public const string ENTITY_Z = "Z";
        public const string ENTITY_OFFSET_X = "OFFSET_X";
        public const string ENTITY_OFFSET_Y = "OFFSET_Y";
        public const string ENTITY_OFFSET_Z = "OFFSET_Z";
        public const string ENTITY_ROTATION = "ROTATION";
        public const string ENTITY_PAIRING_ID = "PAIRING";


        public const string METABLOCK = "METABLOCK";

        public const string METABLOCK_IDENTIFIER = "IDENTIFIER";
        public const string METABLOCK_SHAPE = "SHAPE";
        public const string METABLOCK_SEGMENT = "SEGMENT";
        public const string METABLOCK_ROTATIONS = "ROTATION";
        public const string METABLOCK_PHYSICS = "PHYSICS";
        public const string METABLOCK_ONE_WAY = "ONE_WAY";
        public const string METABLOCK_SLIPPERY = "SLIPPERY";
        public const string METABLOCK_ANTISPLAT = "ANTISPLAT";
        public const string METABLOCK_IGNORED_BY_CAMERA = "CAMERA_IGNORE";
        public const string METABLOCK_VISIBILITY = "VISIBILITY";

        public const string METABLOCK_FACE_PREFIX = "FACE_";
        public const string METABLOCK_FACE_PHYSICS = "PHYSICS";
        public const string METABLOCK_FACE_OUTSIDE_TEXTURE = "TEXTURE_OUTER";
        public const string METABLOCK_FACE_INSIDE_TEXTURE = "TEXTURE_INNER";
        public const string METABLOCK_FACE_TEXTURE_FILE = "TEXTURE";
        public const string METABLOCK_FACE_TEXTURE_INDEX = "TEXTURE_INDEX";
        public const string METABLOCK_FACE_ROTATIONS = "ROTATION";
        public const string METABLOCK_FACE_FLIP = "FLIP_HORIZONTAL";
        public const string METABLOCK_FACE_SHADING = "SHADING";
    }
}
