﻿namespace LoapCore.Constants
{
    public static class SoundNames
    {
        public const string LETS_GO = "lets_go";
        public const string OH_NO = "ohno";
        public const string OKAY = "ok";
        public const string UH_OH = "uh_oh";

        public const string CLIMBER = "climber";
        public const string FLOATER = "floater";
        public const string FALLER_SCREAM = "faller";
        public const string BOMBER_BLAST = "explode";
        public const string BUILDER_PLACE = "place_brick";
        public const string SHRUGGER = "shrugger";
        public const string BASHER = "basher";
        public const string MINER = "miner";
        public const string DIGGER = "digger";

        public const string ENTRANCE = "entrance_open";
        public const string EXIT = "yippee";
        public const string DROWN = "drown";
        public const string ELECTROCUTE = "electrocute";
        public const string GENERAL_DIE = "die";
        public const string STEEL = "steel";
        public const string THUD = "thud";

        public const string SPRING = "spring";
        public const string TELEPORTER = "teleporter";
        public const string TRAMPOLINE = "trampoline";

        public const string DING = "ding";

        public const string NUKE_ACTIVATE = "armageddon";
        public const string WIN = "win";

        public const string SKILL_BASHER = "skill_basher";
        public const string SKILL_BLOCKER = "skill_blocker";
        public const string SKILL_BOMBER = "skill_bomber";
        public const string SKILL_BUILDER = "skill_builder";
        public const string SKILL_CLIMBER = "skill_climber";
        public const string SKILL_DIGGER = "skill_digger";
        public const string SKILL_FLOATER = "skill_floater";
        public const string SKILL_MINER = "skill_miner";
        public const string SKILL_TURNER = "skill_turner";
    }
}
