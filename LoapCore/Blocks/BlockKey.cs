﻿using LoapCore.Misc;
using System;

namespace LoapCore.Blocks
{
    public struct BlockSolidityKey : IEquatable<BlockSolidityKey>
    {
        public BlockShape Shape;
        public int Segment;
        public bool IsBottom;

        public BlockSolidityKey(BlockShape shape, int segment, bool isBottom)
        {
            Shape = shape;
            Segment = segment;
            IsBottom = isBottom;
        }

        public override bool Equals(object? obj)
        {
            return obj is BlockSolidityKey key && Equals(key);
        }

        public bool Equals(BlockSolidityKey other)
        {
            return Shape == other.Shape &&
                   Segment == other.Segment &&
                   IsBottom == other.IsBottom;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Shape, Segment, IsBottom);
        }

        public static bool operator ==(BlockSolidityKey left, BlockSolidityKey right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(BlockSolidityKey left, BlockSolidityKey right)
        {
            return !(left == right);
        }
    }
}
