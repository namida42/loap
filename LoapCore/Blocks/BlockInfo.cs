﻿using LoapCore.Constants;
using LoapCore.Misc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoapCore.Blocks
{
    public class BlockInfo
    {
        public BlockShape Shape;
        public int Segment;
        public int Rotations;

        public BlockPhysics Physics;
        public Direction? OneWayDirection;
        public bool Slippery;
        public bool Antisplat;
        public bool IgnoredByCamera;

        public BlockVisibility Visibility = BlockVisibility.Normal;

        public readonly Dictionary<Direction, FaceInfo> Faces = new Dictionary<Direction, FaceInfo>();

        public BlockInfo()
        {
            foreach (Direction dir in Utils.GetEnumValues<Direction>())
                Faces.Add(dir, new FaceInfo(dir));
        }

        public void Clone(BlockInfo source)
        {
            this.Shape = source.Shape;
            this.Segment = source.Segment;
            this.Rotations = source.Rotations;

            this.Physics = source.Physics;
            this.OneWayDirection = source.OneWayDirection;
            this.Slippery = source.Slippery;
            this.Antisplat = source.Antisplat;
            this.IgnoredByCamera = source.IgnoredByCamera;

            this.Visibility = source.Visibility;

            foreach (Direction dir in Utils.GetEnumValues<Direction>())
                Faces[dir].Clone(source.Faces[dir]);
        }

        public BlockInfo MakeClone()
        {
            BlockInfo result = new BlockInfo();
            result.Clone(this);
            return result;
        }

        public bool IsIdentical(BlockInfo other)
        {
            if (this.Shape != other.Shape || this.Segment != other.Segment || this.Rotations != other.Rotations)
                return false;

            if (this.Physics != other.Physics || this.OneWayDirection != other.OneWayDirection || this.Slippery != other.Slippery || this.Antisplat != other.Antisplat || this.IgnoredByCamera != other.IgnoredByCamera)
                return false;

            if (this.Visibility != other.Visibility)
                return false;

            foreach (Direction dir in Utils.GetEnumValues<Direction>())
                if (!this.Faces[dir].IsIdentical(other.Faces[dir]))
                    return false;

            return true;
        }

        public bool IsSolidAt(Point3D position, bool forCamera)
        {
            return IsSolidAt(position.X, position.Y, position.Z, forCamera);
        }

        private enum SolidityRequirement { BothAxes, EitherAxis }

        public bool IsSolidAt(int x, int y, int z, bool forCamera)
        {
            if (
                   (x < 0) || (x > PhysicsConstants.BLOCK_STEPS) ||
                   (y < 0) || (y > PhysicsConstants.BLOCK_STEPS) ||
                   (z < 0) || (z > PhysicsConstants.SEGMENT_STEPS)
                )
                return false;

            if (PhysicsConstants.NonSolidBlockTypes.Contains(Physics) && !forCamera)
                return false;

            if (IgnoredByCamera && forCamera)
                return false;

            Utils.ApplyRotations(ref x, ref y, PhysicsConstants.BLOCK_STEPS, PhysicsConstants.BLOCK_STEPS, -Rotations);

            // Deal with some special cases.
            switch (Shape)
            {
                // Solid, SolidNarrow, Deflector, Corner45Above, Corner45Below - special handling
                case BlockShape.Solid:
                    return (x >= 0) && (x <= PhysicsConstants.BLOCK_STEPS) &&
                        (y >= 0) && (y <= PhysicsConstants.BLOCK_STEPS) &&
                        (z >= 0) && (z <= PhysicsConstants.SEGMENT_STEPS);
                case BlockShape.SolidNarrow:
                    return (x >= PhysicsConstants.QUARTER_BLOCK_STEPS) && (x <= PhysicsConstants.THREE_QUARTER_BLOCK_STEPS) &&
                        (y >= PhysicsConstants.QUARTER_BLOCK_STEPS) && (y <= PhysicsConstants.THREE_QUARTER_BLOCK_STEPS) &&
                        (z >= 0) && (z <= PhysicsConstants.SEGMENT_STEPS);
                case BlockShape.Deflector:
                    return (x >= (PhysicsConstants.BLOCK_STEPS - y)) && (z >= 0) && (z <= PhysicsConstants.SEGMENT_STEPS);
                case BlockShape.Corner45Above:
                    return (x + y) >= (PhysicsConstants.BLOCK_STEPS + (Segment * PhysicsConstants.QUARTER_BLOCK_STEPS) + z);
                case BlockShape.Corner45Below:
                    return (x + y) >= (PhysicsConstants.BLOCK_STEPS + ((3 - Segment) * PhysicsConstants.QUARTER_BLOCK_STEPS) - z);

                // Pyramid shapes - modify X / Y is the easiest way to emulate their symmetry :P
                case BlockShape.SmallPyramidAbove:
                case BlockShape.SmallPyramidBelow:
                case BlockShape.LargePyramidAbove:
                case BlockShape.LargePyramidBelow:
                    if (x > PhysicsConstants.HALF_BLOCK_STEPS)
                        x = PhysicsConstants.BLOCK_STEPS - x;
                    if (y > PhysicsConstants.HALF_BLOCK_STEPS)
                        y = PhysicsConstants.BLOCK_STEPS - y;
                    break;
            }

            int xLowPos = -1;
            int xHighPos = -1;
            int yLowPos = -1;
            int yHighPos = -1;
            SolidityRequirement axisRequirement = SolidityRequirement.BothAxes;

            switch (Shape)
            {
                case BlockShape.Ramp45Above:
                    yLowPos = Segment * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yHighPos = yLowPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    break;
                case BlockShape.Ramp45Below:
                    yHighPos = (3 - Segment) * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yLowPos = yHighPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    break;
                case BlockShape.Ramp22Above:
                    yLowPos = Segment * PhysicsConstants.HALF_BLOCK_STEPS;
                    yHighPos = yLowPos + PhysicsConstants.HALF_BLOCK_STEPS;
                    break;
                case BlockShape.Ramp22Below:
                    yHighPos = (1 - Segment) * PhysicsConstants.HALF_BLOCK_STEPS;
                    yLowPos = yHighPos + PhysicsConstants.HALF_BLOCK_STEPS;
                    break;
                case BlockShape.Corner90Above:
                    xLowPos = Segment * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    xHighPos = xLowPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yLowPos = Segment * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yHighPos = yLowPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    break;
                case BlockShape.Corner90Below:
                    xHighPos = (3 - Segment) * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    xLowPos = xHighPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yHighPos = (3 - Segment) * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yLowPos = yHighPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    break;
                case BlockShape.CornerInsideAbove:
                    xLowPos = Segment * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    xHighPos = xLowPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yLowPos = Segment * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yHighPos = yLowPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    axisRequirement = SolidityRequirement.EitherAxis;
                    break;
                case BlockShape.CornerInsideBelow:
                    xHighPos = (3 - Segment) * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    xLowPos = xHighPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yHighPos = (3 - Segment) * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yLowPos = yHighPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    axisRequirement = SolidityRequirement.EitherAxis;
                    break;
                case BlockShape.LargePyramidAbove:
                    xLowPos = (Segment * PhysicsConstants.QUARTER_BLOCK_STEPS) / 2;
                    xHighPos = xLowPos + (PhysicsConstants.QUARTER_BLOCK_STEPS / 2);
                    yLowPos = (Segment * PhysicsConstants.QUARTER_BLOCK_STEPS / 2);
                    yHighPos = yLowPos + (PhysicsConstants.QUARTER_BLOCK_STEPS / 2);
                    break;
                case BlockShape.LargePyramidBelow:
                    xHighPos = ((3 - Segment) * PhysicsConstants.QUARTER_BLOCK_STEPS) / 2;
                    xLowPos = xHighPos + (PhysicsConstants.QUARTER_BLOCK_STEPS / 2);
                    yHighPos = ((3 - Segment) * PhysicsConstants.QUARTER_BLOCK_STEPS) / 2;
                    yLowPos = yHighPos + (PhysicsConstants.QUARTER_BLOCK_STEPS / 2);
                    break;
                case BlockShape.SmallPyramidAbove:
                    xLowPos = Segment * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    xHighPos = xLowPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yLowPos = Segment * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yHighPos = yLowPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    break;
                case BlockShape.SmallPyramidBelow:
                    xHighPos = (1 - Segment) * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    xLowPos = xHighPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yHighPos = (1 - Segment) * PhysicsConstants.QUARTER_BLOCK_STEPS;
                    yLowPos = yHighPos + PhysicsConstants.QUARTER_BLOCK_STEPS;
                    break;
                case BlockShape.Solid:
                case BlockShape.SolidNarrow:
                case BlockShape.Deflector:
                case BlockShape.Corner45Above:
                case BlockShape.Corner45Below:
                    // These five should never get here!
                    throw new NotImplementedException();
                default:
                    throw new NotImplementedException();
            }

            bool solidOnX = IsSolidOnAxis(xLowPos, xHighPos, x, z);
            bool solidOnY = IsSolidOnAxis(yLowPos, yHighPos, y, z);

            switch (axisRequirement)
            {
                case SolidityRequirement.BothAxes:
                    return (solidOnX && solidOnY);
                case SolidityRequirement.EitherAxis:
                    return (solidOnX || solidOnY);
                default:
                    throw new NotImplementedException();
            }
        }

        private bool IsSolidOnAxis(int lowPos, int highPos, int axisCoord, int z)
        {
            if (lowPos < highPos)
            {
                if (axisCoord < lowPos)
                    return false;
                else if (axisCoord >= highPos)
                    return true;
                else
                {
                    return z <= ((axisCoord - lowPos) * PhysicsConstants.SEGMENT_STEPS / (highPos - lowPos));
                }
            }
            else if (lowPos > highPos)
            {
                if (axisCoord < highPos)
                    return false;
                else if (axisCoord >= lowPos)
                    return true;
                else
                {
                    return (PhysicsConstants.SEGMENT_STEPS - z) <= ((axisCoord - highPos) * PhysicsConstants.SEGMENT_STEPS / (lowPos - highPos));
                }
            }
            else
                return true;
        }
    }
}
