﻿using System;

namespace LoapCore.Blocks
{
    public struct BlockTextureRef : IEquatable<BlockTextureRef>
    {
        public string File;
        public int Index;
        public int Rotations;
        public bool Flip;
        public int Shading;

        public BlockTextureRef(string file, int index, int rotations, bool flip, int shading)
        {
            File = file ?? "";
            Index = index;
            Rotations = rotations;
            Flip = flip;
            Shading = shading;
        }

        public override bool Equals(object? obj)
        {
            return obj is BlockTextureRef @ref && Equals(@ref);
        }

        public bool Equals(BlockTextureRef other)
        {
            return File == other.File &&
                   Index == other.Index &&
                   Rotations == other.Rotations &&
                   Flip == other.Flip &&
                   Shading == other.Shading;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(File, Index, Rotations, Flip, Shading);
        }

        public static bool operator ==(BlockTextureRef left, BlockTextureRef right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(BlockTextureRef left, BlockTextureRef right)
        {
            return !(left == right);
        }
    }
}
