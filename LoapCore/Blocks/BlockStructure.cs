﻿using LoapCore.Constants;
using LoapCore.Misc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoapCore.Blocks
{
    public class BlockStructure
    {
        public int Width { get; private set; }
        public int Depth { get; private set; }
        public int Height { get; private set; }

        private int[] _Blocks = new int[0];
        private readonly List<BlockInfo> _BlockInfos = new List<BlockInfo>();

        public void Clone(BlockStructure source)
        {
            SetSize(source.Width, source.Depth, source.Height);

            for (int i = 0; i < Width * Depth * Height; i++)
                _Blocks[i] = source._Blocks[i];

            _BlockInfos.Clear();
            for (int i = 0; i < source._BlockInfos.Count; i++)
                _BlockInfos.Add(source._BlockInfos[i].MakeClone());
        }

        public void SetSize(int width, int depth, int height)
        {
            Width = width;
            Depth = depth;
            Height = height;
            _Blocks = new int[width * depth * height];
            Clear();
        }

        public void Clear()
        {
            _BlockInfos.Clear();
            for (int i = 0; i < Width * Height * Depth; i++)
                _Blocks[i] = -1;
        }

        private int GetBlockIndex(int x, int y, int z)
        {
            return x + (y * Width) + (z * Width * Depth);
        }

        public BlockInfo? this[int x, int y, int z]
        {
            get
            {
                if (x < 0 || y < 0 || z < 0 || x >= Width || y >= Depth || z >= Height)
                    return null;
                else
                {
                    int blockInfoIndex = _Blocks[GetBlockIndex(x, y, z)];
                    if (blockInfoIndex < 0)
                        return null;
                    else
                        return _BlockInfos[blockInfoIndex];
                }
            }

            set
            {
                if (x >= 0 && y >= 0 && z >= 0 && x < Width && y < Depth && z < Height)
                {
                    if (value == null)
                        _Blocks[GetBlockIndex(x, y, z)] = -1;
                    else
                    {
                        _Blocks[GetBlockIndex(x, y, z)] = _BlockInfos.Count();
                        _BlockInfos.Add(value);
                    }
                }
            }
        }

        public BlockInfo? GetBlockAt(int x, int y, int z)
        {
            int blockX = Utils.DivideNegativeAdjusted(x, PhysicsConstants.BLOCK_STEPS);
            int blockY = Utils.DivideNegativeAdjusted(y, PhysicsConstants.BLOCK_STEPS);
            int blockZ = Utils.DivideNegativeAdjusted(z, PhysicsConstants.SEGMENT_STEPS);

            return this[blockX, blockY, blockZ];
        }

        public Dictionary<Point3D, BlockInfo> ToDictionary()
        {
            var result = new Dictionary<Point3D, BlockInfo>();

            for (int z = 0; z < Height; z++)
                for (int y = 0; y < Depth; y++)
                    for (int x = 0; x < Width; x++)
                    {
                        int blockIndex = _Blocks[GetBlockIndex(x, y, z)];

                        if (blockIndex >= 0)
                            result.Add(new Point3D(x, y, z), _BlockInfos[blockIndex]);
                    }

            return result;
        }

        public bool IsSolidAt(int x, int y, int z, bool forCamera)
        {
            int blockX = Utils.DivideNegativeAdjusted(x, PhysicsConstants.BLOCK_STEPS);
            int blockY = Utils.DivideNegativeAdjusted(y, PhysicsConstants.BLOCK_STEPS);
            int blockZ = Utils.DivideNegativeAdjusted(z, PhysicsConstants.SEGMENT_STEPS);

            bool checkPrevX = (Utils.ModuloNegativeAdjusted(x, PhysicsConstants.BLOCK_STEPS) == 0);
            bool checkPrevY = (Utils.ModuloNegativeAdjusted(y, PhysicsConstants.BLOCK_STEPS) == 0);
            bool checkPrevZ = (Utils.ModuloNegativeAdjusted(z, PhysicsConstants.SEGMENT_STEPS) == 0);

            for (int cZ = (checkPrevZ ? blockZ - 1 : blockZ); cZ <= blockZ; cZ++)
                for (int cY = (checkPrevY ? blockY - 1 : blockY); cY <= blockY; cY++)
                    for (int cX = (checkPrevX ? blockX - 1 : blockX); cX <= blockX; cX++)
                    {
                        int relativeX = x - (cX * PhysicsConstants.BLOCK_STEPS);
                        int relativeY = y - (cY * PhysicsConstants.BLOCK_STEPS);
                        int relativeZ = z - (cZ * PhysicsConstants.SEGMENT_STEPS);

                        BlockInfo? targetBlock = this[cX, cY, cZ];

                        if (targetBlock == null)
                            continue;

                        if (targetBlock.IsSolidAt(relativeX, relativeY, relativeZ, forCamera))
                            return true;
                    }

            return false;
        }

        public void Sanitize()
        {
            List<BlockInfo?> tempList = new List<BlockInfo?>(Width * Depth * Height);
            for (int i = 0; i < Width * Depth * Height; i++)
                if (_Blocks[i] < 0)
                    tempList.Add(null);
                else
                    tempList.Add(_BlockInfos[_Blocks[i]]);

            _BlockInfos.Clear();
            for (int i = 0; i < Width * Depth * Height; i++)
            {
                var thisBlockInfo = tempList[i];

                if (thisBlockInfo == null)
                    _Blocks[i] = -1;
                else
                {
                    _Blocks[i] = _BlockInfos.Count();
                    _BlockInfos.Add(thisBlockInfo);
                }
            }

            foreach (var block in _BlockInfos)
            {
                if (!PhysicsConstants.IndestructibleBlockTypes.Contains(block.Physics))
                    if (block.Faces.Values.Any(f => (f.Physics == FacePhysics.Exit || f.Physics == FacePhysics.AnimateExit)))
                        block.Physics = BlockPhysics.Steel;

                foreach (var face in block.Faces.Values)
                {
                    if (block.Visibility == BlockVisibility.Invisible || block.Visibility == BlockVisibility.AntiRender)
                    {
                        face.TextureInner.Clear();
                        face.TextureOuter.Clear();
                    }
                    else if (block.Visibility != BlockVisibility.Transparent)
                        face.TextureInner.Clear();

                    if (PhysicsConstants.ExitFaceTypes.Contains(face.Physics))
                        if (face.Side == Direction.ZMinus || face.Side == Direction.ZPlus)
                            face.Physics = FacePhysics.None;
                }
            }
        }
    }
}
