﻿using LoapCore.Misc;
using System.Collections.Generic;

namespace LoapCore.Blocks
{
    public class FaceInfo
    {
        public FaceInfo(Direction side)
        {
            Side = side;
        }

        public readonly Direction Side;
        public FacePhysics Physics;
        public readonly List<BlockTextureRef> TextureOuter = new List<BlockTextureRef>();
        public readonly List<BlockTextureRef> TextureInner = new List<BlockTextureRef>();

        public void Clone(FaceInfo source)
        {
            this.Physics = source.Physics;
            TextureOuter.Clear();
            TextureOuter.AddRange(source.TextureOuter);
            TextureInner.Clear();
            TextureInner.AddRange(source.TextureInner);
        }

        public bool IsIdentical(FaceInfo other)
        {
            if (this.Physics != other.Physics)
                return false;

            if (this.TextureOuter.Count != other.TextureOuter.Count)
                return false;

            if (this.TextureInner.Count != other.TextureInner.Count)
                return false;

            for (int i = 0; i < TextureOuter.Count; i++)
                if (this.TextureOuter[i] != other.TextureOuter[i])
                    return false;

            for (int i = 0; i < TextureInner.Count; i++)
                if (this.TextureInner[i] != other.TextureInner[i])
                    return false;

            return true;
        }
    }
}
