﻿using Loap.Constants;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Loap.Views
{
    abstract class BaseView
    {
        private const int FADE_SKIPS = 3;

        protected int InternalCanvasWidth = RenderConstants.DEFAULT_INTERNAL_CANVAS_WIDTH;
        protected int InternalCanvasHeight = RenderConstants.DEFAULT_INTERNAL_CANVAS_HEIGHT;

        protected readonly GraphicsDevice GraphicsDevice;
        public readonly RenderTarget2D Canvas;
        public float ViewRunTime { get; private set; }

        protected SpriteBatch SpriteBatch;

        public Func<BaseView>? NewViewFunction { get; private set; }

        public float IdealFrameDelay;

        public RenderTarget2D? ScreenTexture;

        public float FadeTime = UIConstants.FADE_TIME;
        public bool Stabilized = false;

        public BaseView(GraphicsDevice graphics)
        {
            IdealFrameDelay = UIConstants.IDEAL_FRAMERATE_TIME;

            GraphicsDevice = graphics;
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            if (GraphicsDevice.Viewport.Width > 0 && GraphicsDevice.Viewport.Height > 0)
            {
                InternalCanvasWidth = GraphicsDevice.Viewport.Width;
                InternalCanvasHeight = GraphicsDevice.Viewport.Height;
            }

            Canvas = CreateRenderTarget();
        }

        protected RenderTarget2D CreateRenderTarget()
        {
            return CreateRenderTarget(InternalCanvasWidth, InternalCanvasHeight);
        }

        protected RenderTarget2D CreateRenderTarget(int width, int height, bool preserveContents = false)
        {
            return new RenderTarget2D(GraphicsDevice,
                                      width,
                                      height,
                                      false,
                                      SurfaceFormat.Color,
                                      DepthFormat.Depth24,
                                      0,
                                      preserveContents ? RenderTargetUsage.PreserveContents : RenderTargetUsage.DiscardContents);
        }

        public void Update(float elapsedTime)
        {
            if (FadeTime > 0 && Stabilized)
            {
                FadeTime = Math.Max(FadeTime - elapsedTime, 0);

                if (FadeTime <= 0)
                    FadeTime = 0;
            }

            if (FadeTime > 0 || NewViewFunction == null)
            {
                ViewRunTime += elapsedTime;
                DoUpdate(elapsedTime);
            }
        }

        public void ForceOneUpdate(float elapsedTime)
        {
            DoUpdate(elapsedTime);
        }

        protected abstract void DoUpdate(float elapsedTime);

        public void Render()
        {
            DoRender();
        }

        protected abstract void DoRender();

        protected void SetNewView(Func<BaseView> newViewFunction)
        {
            if (NewViewFunction == null)
            {
                NewViewFunction = newViewFunction;
                FadeTime = UIConstants.FADE_TIME - FadeTime;
            }
        }
    }
}
