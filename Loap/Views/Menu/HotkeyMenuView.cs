﻿using Loap.Audio;
using Loap.Constants;
using Loap.Misc;
using Loap.UI;
using LoapCore.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Loap.Views.Menu
{
    class HotkeyMenuView : BaseMenuView
    {
        private static readonly Color PAGE_HEADER_COLOR = Color.Yellow;

        public HotkeyMenuView(GraphicsDevice graphics) : base(graphics)
        {
        }

        private const int SCROLL_X = 24;
        private const int KEY_X = 64;
        private const int FUNCTION_X = 360;
        private const int DELETE_HOTKEY_X = RenderConstants.DESIGN_WIDTH - 32;
        private const int NEW_HOTKEY_SECOND_COLUMN_X = 608;

        private int _ChildOffset = 0;
        private bool _AtMaxChildOffset;

        private bool _SortByFunction = true;

        private bool _NewKeyMode;
        private HotkeyEntry _NewKey = new HotkeyEntry();

        private List<Keys> _KeyPresses = new List<Keys>();

        private const string NO_MODIFIER = "(none)";
        private const string UNDEFINIED_MODIFIER = "???";

        private static string MakeSingleKeyString(Keys key)
        {
            string result = key.ToString();

            switch (key)
            {
                case Keys.CapsLock:
                    result = "Caps";
                    break;
                case Keys.Escape:
                    result = "Esc";
                    break;
                case Keys.PageUp:
                    result = "PgUp";
                    break;
                case Keys.PageDown:
                    result = "PgDown";
                    break;
                case Keys.PrintScreen:
                    result = "PrtScr";
                    break;
                case Keys.Delete:
                    result = "Del";
                    break;
                case Keys.D0:
                    result = "0";
                    break;
                case Keys.D1:
                    result = "1";
                    break;
                case Keys.D2:
                    result = "2";
                    break;
                case Keys.D3:
                    result = "3";
                    break;
                case Keys.D4:
                    result = "4";
                    break;
                case Keys.D5:
                    result = "5";
                    break;
                case Keys.D6:
                    result = "6";
                    break;
                case Keys.D7:
                    result = "7";
                    break;
                case Keys.D8:
                    result = "8";
                    break;
                case Keys.D9:
                    result = "9";
                    break;
                case Keys.LeftWindows:
                    result = "LWin";
                    break;
                case Keys.RightWindows:
                    result = "RWin";
                    break;
                case Keys.NumPad0:
                    result = "Pad0";
                    break;
                case Keys.NumPad1:
                    result = "Pad1";
                    break;
                case Keys.NumPad2:
                    result = "Pad2";
                    break;
                case Keys.NumPad3:
                    result = "Pad3";
                    break;
                case Keys.NumPad4:
                    result = "Pad4";
                    break;
                case Keys.NumPad5:
                    result = "Pad5";
                    break;
                case Keys.NumPad6:
                    result = "Pad6";
                    break;
                case Keys.NumPad7:
                    result = "Pad7";
                    break;
                case Keys.NumPad8:
                    result = "Pad8";
                    break;
                case Keys.NumPad9:
                    result = "Pad9";
                    break;
                case Keys.Multiply:
                    result = "Mult";
                    break;
                case Keys.Subtract:
                    result = "Minus";
                    break;
                case Keys.LeftControl:
                    result = "LCtrl";
                    break;
                case Keys.RightControl:
                    result = "RCtrl";
                    break;
                case Keys.VolumeMute:
                    result = "Mute";
                    break;
                case Keys.MediaNextTrack:
                    result = "NextTrack";
                    break;
                case Keys.MediaPreviousTrack:
                    result = "PrevTrack";
                    break;
                case Keys.MediaPlayPause:
                    result = "Play";
                    break;
                case Keys.LaunchMail:
                    result = "Mail";
                    break;
                case Keys.LaunchApplication1:
                    result = "LaunchApp1";
                    break;
                case Keys.LaunchApplication2:
                    result = "LaunchApp2";
                    break;
                case Keys.OemOpenBrackets:
                    result = "LBracket";
                    break;
                case Keys.OemCloseBrackets:
                    result = "RBracket";
                    break;
            }

            result = result.Replace("Left", "L");
            result = result.Replace("Right", "R");
            result = result.Replace("Oem", "");
            result = result.Replace("Browser", "");

            return result;
        }

        private static string MakeKeyString(HotkeyEntry hotkey, bool truncate)
        {
            string result = "";

            if (hotkey.KeyModifier < 0)
                result = MakeSingleKeyString(hotkey.Key);
            else if (hotkey.KeyModifier == 0)
                result = NO_MODIFIER + "+" + MakeSingleKeyString(hotkey.Key);
            else
            {
                for (int i = 0; i < 31; i++)
                    if ((hotkey.KeyModifier & (1 << i)) != 0)
                    {
                        var modifierKey = Hotkeys.Entries.Where(hk => hk.Function == HotkeyFunction.Modifier && hk.Parameter == i).FirstOrDefault();
                        result = result +
                            (result == "" ? "" : "+") +
                            (modifierKey == null ? UNDEFINIED_MODIFIER : MakeSingleKeyString(modifierKey.Key));
                    }

                result = result + "+" + MakeSingleKeyString(hotkey.Key);
            }

            string cutResult = result;

            if (truncate)
                while (Font.CalculateBounds(cutResult, new Point(0, 0)).Width > FUNCTION_X - KEY_X - 16)
                    cutResult = result.Substring(0, cutResult.Length - 4) + "...";
            
            return cutResult;
        }

        private static string MakeFunctionNameString(HotkeyFunction keyFunction)
        {
            string result = "";

            if (keyFunction == HotkeyFunction.ShowFPS)
                result = "Show FPS"; // Special case as otherwise this would come out as "Show F P S"
            else if (keyFunction == HotkeyFunction.AdjustCameraSpeed)
                result = "Camera Speed"; // Special case as the "adjust" gets cut out to conserve space
            else
                foreach (var c in keyFunction.ToString())
                    result = result + (result != "" && char.IsUpper(c) ? " " : "") + c;

            return result;
        }

        private static string MakeFunctionString(HotkeyEntry hotkey)
        {
            string result = "";

            if (hotkey.Function == HotkeyFunction.ShowFPS)
                result = "Show FPS"; // Special case as otherwise this would come out as "Show F P S"
            else
                foreach (var c in hotkey.Function.ToString())
                    result = result + (result != "" && char.IsUpper(c) ? " " : "") + c;

            if (HotkeyEntry.SaveParameterFunctions.Contains(hotkey.Function))
            {
                switch (hotkey.Function)
                {
                    case HotkeyFunction.Modifier:
                        result = result + " " + hotkey.Parameter.ToString();
                        break;
                    case HotkeyFunction.FrameSkip:
                    case HotkeyFunction.ChangeSpawnRate:
                        result = result + ": " + (hotkey.Parameter >= 0 ? "+" : "") + hotkey.Parameter.ToString();
                        break;
                    case HotkeyFunction.SelectSkill:
                        result = result + ": " + ((Skill)hotkey.Parameter).ToString();
                        break;
                    case HotkeyFunction.ChangeCamera:
                    case HotkeyFunction.CloneCamera:
                        result = result + ": " + hotkey.Parameter.ToString();
                        break;
                    case HotkeyFunction.MoveCamera:
                        switch (hotkey.Parameter)
                        {
                            case Hotkeys.CAMERA_AXIS_X:
                                result = result + " Right";
                                break;
                            case -Hotkeys.CAMERA_AXIS_X:
                                result = result + " Left";
                                break;
                            case Hotkeys.CAMERA_AXIS_Y:
                                result = result + " Forward";
                                break;
                            case -Hotkeys.CAMERA_AXIS_Y:
                                result = result + " Backward";
                                break;
                            case Hotkeys.CAMERA_AXIS_Z:
                                result = result + " Up";
                                break;
                            case -Hotkeys.CAMERA_AXIS_Z:
                                result = result + " Down";
                                break;
                            case Hotkeys.CAMERA_ROTATION:
                                result = "Rotate Camera Right";
                                break;
                            case -Hotkeys.CAMERA_ROTATION:
                                result = "Rotate Camera Left";
                                break;
                            case Hotkeys.CAMERA_PITCH:
                                result = "Pitch Camera Up";
                                break;
                            case -Hotkeys.CAMERA_PITCH:
                                result = "Pitch Camera Down";
                                break;
                            default:
                                result = result + " (Invalid Axis)";
                                break;
                        }
                        break;
                    case HotkeyFunction.AdjustCameraSpeed:
                        result = "Camera Speed " + hotkey.Parameter.ToString() + "/" + UIConstants.CAMERA_SPEED_FACTOR.ToString();
                        break;
                    default:
                        throw new NotImplementedException(hotkey.Function.ToString() + " requires handling of parameter");
                }
            }

            return result;
        }

        private void ReturnToOptionsMenu()
        {
            Hotkeys.Sanitize(); // Just in case.
            SetNewView(() => new OptionsView(GraphicsDevice, false));
        }

        private void ChangeSortMode()
        {
            _SortByFunction = !_SortByFunction;
            SoundManager.PlaySound("DING");
            _ChildOffset = 0;
            RegenerateElements();
        }

        private void ResetToDefaultKeys()
        {
            Hotkeys.SetDefault();
            SoundManager.PlaySound("DING");
            _ChildOffset = 0;
            RegenerateElements();
        }

        private void EnterNewKeyMode()
        {
            _NewKeyMode = true;
            _ChildOffset = 0;
            RegenerateElements();
        }

        private void ConfirmNewKey()
        {
            if (_NewKey.Key != Keys.None)
                Hotkeys.Entries.Add(_NewKey);
            _NewKey = new HotkeyEntry();
            ExitNewKeyMode();
        }

        private void ExitNewKeyMode()
        {
            _NewKeyMode = false;
            _ChildOffset = 0;
            RegenerateElements();
        }

        private void ActivateSetKeyMode()
        {
            _KeyPresses.Clear();
            SuspendInput = true;
            _ExitSetKeyProtectionFlag = true;
            RegenerateElements();
        }

        private void ExitSetKeyMode(bool confirmKey)
        {
            if (confirmKey && (_KeyPresses.Count > 0))
            {
                if (_NewKey.Function == HotkeyFunction.Modifier)
                    _NewKey.KeyModifier = -1;
                else if (_KeyPresses.Count > 1)
                {
                    _NewKey.KeyModifier = 0;
                    for (int i = 0; i < _KeyPresses.Count - 1; i++)
                    {
                        var modKey = Hotkeys.Entries.FirstOrDefault(hk => hk.Function == HotkeyFunction.Modifier && hk.Key == _KeyPresses[i]);
                        if (modKey != null)
                            _NewKey.KeyModifier = _NewKey.KeyModifier | (1 << modKey.Parameter);
                    }
                }
                else if (_NewKey.KeyModifier != 0)
                    _NewKey.KeyModifier = -1;

                _NewKey.Key = _KeyPresses[_KeyPresses.Count - 1];
            }
            SuspendInput = false;

            RegenerateElements();
        }

        private float _ScrollLockTimer;
        private const float SCROLL_DELAY = 0.025f;

        protected override void GenerateElements()
        {
            AddElement("Loap Hotkeys", new Point(24, 24), PAGE_HEADER_COLOR, -1, -1, null);

            if (_NewKeyMode)
                GenerateElementsNewKey();
            else
                GenerateElementsKeyList();
        }

        private void GenerateElementsNewKey()
        {
            int y = 24;
            y += Font.CharacterHeight * 2;

            if (SuspendInput)
                AddElement("Key: Press the desired key now", new Point(48, y), Color.Red, -1, -1, null);
            else
                AddElement("Key: " + MakeKeyString(_NewKey, false), new Point(48, y), Color.White, -1, -1, null);
            y += Font.CharacterHeight;
            AddElement("Effect: " + MakeFunctionString(_NewKey), new Point(48, y), Color.White, -1, -1, null);
            y += Font.CharacterHeight * 2;

            int selectableStartY = y;
            int elementCount = ((RenderConstants.DESIGN_HEIGHT - selectableStartY - (Font.CharacterHeight * 5 / 4)) / Font.CharacterHeight) - 1;

            IEnumerable<HotkeyFunction> allFunctions = Enum.GetValues(typeof(HotkeyFunction)).Cast<HotkeyFunction>();

            _ChildOffset = Math.Max(Math.Min(_ChildOffset, allFunctions.Count() - elementCount), 0);

            var functions = allFunctions.Skip(_ChildOffset).Take(elementCount);

            foreach (var keyFunction in functions)
            {
                AddElement(MakeFunctionNameString(keyFunction), new Point(KEY_X, y),
                    keyFunction == _NewKey.Function ? Color.Green : Color.White,
                    keyFunction == _NewKey.Function ? Color.Green : Color.LightGreen,
                    -1, -1,
                    delegate ()
                    {
                        SoundManager.PlaySound("DING");
                        _NewKey.Function = keyFunction;
                        if (keyFunction == HotkeyFunction.MoveCamera || keyFunction == HotkeyFunction.ChangeCamera || keyFunction == HotkeyFunction.CloneCamera ||
                        keyFunction == HotkeyFunction.ChangeSpawnRate || keyFunction == HotkeyFunction.FrameSkip)
                            _NewKey.Parameter = 1;
                        else if (keyFunction == HotkeyFunction.AdjustCameraSpeed)
                            _NewKey.Parameter = UIConstants.CAMERA_SPEED_FACTOR;
                        else
                            _NewKey.Parameter = 0;

                        if (keyFunction == HotkeyFunction.Modifier)
                            _NewKey.KeyModifier = -1;

                        RegenerateElements();
                    });

                y += Font.CharacterHeight;
            }

            if (_ChildOffset > 0 || !_AtMaxChildOffset)
            {
                if (_ChildOffset > 0)
                    AddElement("{", new Point(SCROLL_X, selectableStartY), UIConstants.LEVEL_COMPLETED_COLOR, Color.White, -1, -1,
                        delegate ()
                        {
                            if (_ScrollLockTimer == 0)
                            {
                                _ChildOffset--;
                                _ScrollLockTimer = SCROLL_DELAY;
                                RegenerateElements();
                            }
                        }, true
                        );
                else
                    AddElement("{", new Point(SCROLL_X, selectableStartY), UIConstants.LEVEL_SELECT_DISABLED_COLOR, UIConstants.LEVEL_SELECT_DISABLED_COLOR, -1, -1, null);

                if (!_AtMaxChildOffset)
                    AddElement("}", new Point(SCROLL_X, y - Font.CharacterHeight), UIConstants.LEVEL_COMPLETED_COLOR, Color.White, -1, -1,
                        delegate ()
                        {
                            if (_ScrollLockTimer == 0)
                            {
                                _ChildOffset++;
                                _ScrollLockTimer = SCROLL_DELAY;
                                RegenerateElements();
                            }
                        }, true
                        );
                else
                    AddElement("}", new Point(SCROLL_X, y - Font.CharacterHeight), UIConstants.LEVEL_SELECT_DISABLED_COLOR, UIConstants.LEVEL_SELECT_DISABLED_COLOR, -1, -1, null);
            }

            y = selectableStartY;

            AddElement("Set Key", new Point(NEW_HOTKEY_SECOND_COLUMN_X, y), Color.Yellow, Color.White, -1, -1, ActivateSetKeyMode);
            y += Font.CharacterHeight;
            AddElement("Ignore Modifiers", new Point(NEW_HOTKEY_SECOND_COLUMN_X, y),
                _NewKey.KeyModifier < 0 ? Color.Green : Color.Yellow,
                Color.White, -1, -1,
                delegate ()
                {
                    SoundManager.PlaySound("DING");
                    _NewKey.KeyModifier = -1;
                    RegenerateElements();
                });
            y += Font.CharacterHeight;
            AddElement("No Modifiers", new Point(NEW_HOTKEY_SECOND_COLUMN_X, y),
                _NewKey.KeyModifier == 0 ? Color.Green : Color.Yellow,
                Color.White, -1, -1,
                delegate ()
                {
                    SoundManager.PlaySound("DING");
                    _NewKey.KeyModifier = 0;
                    RegenerateElements();
                });
            y += Font.CharacterHeight * 2;

            if (HotkeyEntry.SaveParameterFunctions.Contains(_NewKey.Function))
            {
                switch (_NewKey.Function)
                {
                    case HotkeyFunction.Modifier:
                        AddElement("Modifier Index:", new Point(NEW_HOTKEY_SECOND_COLUMN_X, y), Color.White, -1, -1, null);
                        y += Font.CharacterHeight;

                        AddElement("-", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32, y), Color.Yellow, Color.White, -1, -1,
                            delegate()
                            {
                                if (_NewKey.Parameter > 0)
                                {
                                    SoundManager.PlaySound("DING");
                                    _NewKey.Parameter -= 1;
                                    RegenerateElements();
                                }
                            });

                        AddElement(_NewKey.Parameter.ToString(), new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 64, y), Color.White, 0, -1, null);

                        AddElement("+", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 64 + 64, y), Color.Yellow, Color.White, 1, -1,
                            delegate ()
                            {
                                if (_NewKey.Parameter < 31)
                                {
                                    SoundManager.PlaySound("DING");
                                    _NewKey.Parameter += 1;
                                    RegenerateElements();
                                }
                            });
                        break;
                    case HotkeyFunction.FrameSkip:
                        AddElement("Skip Duration:", new Point(NEW_HOTKEY_SECOND_COLUMN_X, y), Color.White, -1, -1, null);
                        y += Font.CharacterHeight;

                        AddElement("-", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32, y), Color.Yellow, Color.White, -1, -1,
                            delegate ()
                            {
                                if (UserInput.IsMouseButtonPressed(MouseButton.Left) || _RapidFireProtectionTimer == 0)
                                {
                                    _NewKey.Parameter -= 1;
                                    if (_NewKey.Parameter == 0)
                                        _NewKey.Parameter = -1;
                                    RegenerateElements();
                                }
                            }, true);

                        AddElement((_NewKey.Parameter >= 0 ? "+" : "") + _NewKey.Parameter.ToString(), new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 96, y), Color.White, 0, -1, null);

                        AddElement("+", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 96 + 96, y), Color.Yellow, Color.White, 1, -1,
                            delegate ()
                            {
                                if (UserInput.IsMouseButtonPressed(MouseButton.Left) || _RapidFireProtectionTimer == 0)
                                {
                                    _NewKey.Parameter += 1;
                                    if (_NewKey.Parameter == 0)
                                        _NewKey.Parameter = 1;
                                    RegenerateElements();
                                }
                            }, true);
                        break;
                    case HotkeyFunction.ChangeSpawnRate:
                        AddElement("Spawn Rate Change:", new Point(NEW_HOTKEY_SECOND_COLUMN_X, y), Color.White, -1, -1, null);
                        y += Font.CharacterHeight;

                        AddElement("-", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32, y), Color.Yellow, Color.White, -1, -1,
                            delegate ()
                            {
                                if (_NewKey.Parameter > -9)
                                {
                                    SoundManager.PlaySound("DING");
                                    _NewKey.Parameter -= 1;
                                    if (_NewKey.Parameter == 0)
                                        _NewKey.Parameter = -1;
                                    RegenerateElements();
                                }
                            });

                        AddElement((_NewKey.Parameter >= 0 ? "+" : "") + _NewKey.Parameter.ToString(), new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 64, y), Color.White, 0, -1, null);

                        AddElement("+", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 64 + 64, y), Color.Yellow, Color.White, 1, -1,
                            delegate ()
                            {
                                if (_NewKey.Parameter < 9)
                                {
                                    SoundManager.PlaySound("DING");
                                    _NewKey.Parameter += 1;
                                    if (_NewKey.Parameter == 0)
                                        _NewKey.Parameter = 1;
                                    RegenerateElements();
                                }
                            });
                        break;
                    case HotkeyFunction.SelectSkill:
                        {
                            int optionsStartY = y;

                            for (int i = 0; i < 9; i++)
                            {
                                int localI = i;
                                AddElement(((Skill)i).ToString(), new Point(NEW_HOTKEY_SECOND_COLUMN_X + (i > 4 ? 184 : 0), y),
                                    _NewKey.Parameter == i ? Color.Green : Color.Yellow,
                                    _NewKey.Parameter == i ? Color.Green : Color.LightGreen,
                                    -1, -1,
                                    delegate ()
                                    {
                                        SoundManager.PlaySound("DING");
                                        _NewKey.Parameter = localI;
                                        RegenerateElements();
                                    });
                                y += Font.CharacterHeight;

                                if (i == 4)
                                    y = optionsStartY;
                            }
                        }
                        break;
                    case HotkeyFunction.ChangeCamera:
                        AddElement("Switch To Camera:", new Point(NEW_HOTKEY_SECOND_COLUMN_X, y), Color.White, -1, -1, null);
                        y += Font.CharacterHeight;

                        AddElement("-", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32, y), Color.Yellow, Color.White, -1, -1,
                            delegate ()
                            {
                                if (_NewKey.Parameter > 1)
                                {
                                    SoundManager.PlaySound("DING");
                                    _NewKey.Parameter -= 1;
                                    RegenerateElements();
                                }
                            });

                        AddElement(_NewKey.Parameter.ToString(), new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 64, y), Color.White, 0, -1, null);

                        AddElement("+", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 64 + 64, y), Color.Yellow, Color.White, 1, -1,
                            delegate ()
                            {
                                if (_NewKey.Parameter < 9)
                                {
                                    SoundManager.PlaySound("DING");
                                    _NewKey.Parameter += 1;
                                    RegenerateElements();
                                }
                            });
                        break;
                    case HotkeyFunction.CloneCamera:
                        AddElement("Clone To Camera:", new Point(NEW_HOTKEY_SECOND_COLUMN_X, y), Color.White, -1, -1, null);
                        y += Font.CharacterHeight;

                        AddElement("-", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32, y), Color.Yellow, Color.White, -1, -1,
                            delegate ()
                            {
                                if (_NewKey.Parameter > 1)
                                {
                                    SoundManager.PlaySound("DING");
                                    _NewKey.Parameter -= 1;
                                    RegenerateElements();
                                }
                            });

                        AddElement(_NewKey.Parameter.ToString(), new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 64, y), Color.White, 0, -1, null);

                        AddElement("+", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 64 + 64, y), Color.Yellow, Color.White, 1, -1,
                            delegate ()
                            {
                                if (_NewKey.Parameter < 9)
                                {
                                    SoundManager.PlaySound("DING");
                                    _NewKey.Parameter += 1;
                                    RegenerateElements();
                                }
                            });
                        break;
                    case HotkeyFunction.MoveCamera:
                        {
                            int xOffset = 0;

                            void AddMoveCameraElement(string label, int param)
                            {
                                AddElement(label, new Point(NEW_HOTKEY_SECOND_COLUMN_X + xOffset, y),
                                    _NewKey.Parameter == param ? Color.Green : Color.Yellow,
                                    _NewKey.Parameter == param ? Color.Green : Color.LightGreen,
                                    -1, -1,
                                    delegate ()
                                    {
                                        SoundManager.PlaySound("DING");
                                        _NewKey.Parameter = param;
                                        RegenerateElements();
                                    });

                                y += Font.CharacterHeight;
                            }

                            int optionsStartY = y;
                            AddMoveCameraElement("Right", Hotkeys.CAMERA_AXIS_X);
                            AddMoveCameraElement("Left", -Hotkeys.CAMERA_AXIS_X);
                            AddMoveCameraElement("Up", Hotkeys.CAMERA_AXIS_Z);
                            AddMoveCameraElement("Down", -Hotkeys.CAMERA_AXIS_Z);
                            AddMoveCameraElement("Forward", Hotkeys.CAMERA_AXIS_Y);
                            AddMoveCameraElement("Backward", -Hotkeys.CAMERA_AXIS_Y);

                            y = optionsStartY;
                            xOffset = 128;
                            AddMoveCameraElement("(Rotate)", Hotkeys.CAMERA_ROTATION);
                            AddMoveCameraElement("(Rotate)", -Hotkeys.CAMERA_ROTATION);
                            AddMoveCameraElement("(Pitch)", Hotkeys.CAMERA_PITCH);
                            AddMoveCameraElement("(Pitch)", -Hotkeys.CAMERA_PITCH);
                        }
                        break;
                    case HotkeyFunction.AdjustCameraSpeed:
                        AddElement("Speed Adjustment:", new Point(NEW_HOTKEY_SECOND_COLUMN_X, y), Color.White, -1, -1, null);
                        y += Font.CharacterHeight;

                        AddElement("-", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32, y), Color.Yellow, Color.White, -1, -1,
                            delegate ()
                            {
                                if (UserInput.IsMouseButtonPressed(MouseButton.Left) || _RapidFireProtectionTimer == 0)
                                {
                                    if (_NewKey.Parameter > 0)
                                        _NewKey.Parameter -= 1;
                                    RegenerateElements();
                                }
                            }, true);

                        AddElement(_NewKey.Parameter.ToString() + "/" + UIConstants.CAMERA_SPEED_FACTOR.ToString(), new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 144, y), Color.White, 0, -1, null);

                        AddElement("+", new Point(NEW_HOTKEY_SECOND_COLUMN_X + 32 + 144 + 144, y), Color.Yellow, Color.White, 1, -1,
                            delegate ()
                            {
                                if (UserInput.IsMouseButtonPressed(MouseButton.Left) || _RapidFireProtectionTimer == 0)
                                {
                                    _NewKey.Parameter += 1;
                                    RegenerateElements();
                                }
                            }, true);
                        break;
                    default:
                        throw new NotImplementedException(_NewKey.Function.ToString() + " requires handling of parameter");
                }
            }

            y = selectableStartY + (Font.CharacterHeight * (elementCount + 1));

            AddElement("Confirm", new Point(RenderConstants.DESIGN_WIDTH * 5 / 16, y), Color.Yellow, Color.White, 0, -1, ConfirmNewKey);
            AddElement("Cancel", new Point(RenderConstants.DESIGN_WIDTH * 11 / 16, y), Color.Yellow, Color.White, 0, -1, ExitNewKeyMode);
        }

        private void GenerateElementsKeyList()
        {            
            int y = 24;
            y += Font.CharacterHeight * 2;

            int selectableStartY = y;
            int elementCount = ((RenderConstants.DESIGN_HEIGHT - selectableStartY - (Font.CharacterHeight * 5 / 4)) / Font.CharacterHeight) - 1;

            _ChildOffset = Math.Max(Math.Min(_ChildOffset, Hotkeys.Entries.Count - elementCount), 0);

            IEnumerable<HotkeyEntry> hotkeys = Hotkeys.Entries;

            if (_SortByFunction)
                hotkeys = hotkeys
                    .OrderBy(hk => hk.Function)
                    .ThenBy(hk => Math.Abs(hk.Parameter))
                    .ThenBy(hk => -hk.Parameter)
                    .ThenBy(hk => hk.Key)
                    .ThenBy(hk => hk.KeyModifier);
            else
                hotkeys = hotkeys
                    .OrderBy(hk => hk.Key)
                    .ThenBy(hk => hk.KeyModifier)
                    .ThenBy(hk => hk.Function)
                    .ThenBy(hk => Math.Abs(hk.Parameter))
                    .ThenBy(hk => -hk.Parameter);

            hotkeys = hotkeys.Skip(_ChildOffset).Take(elementCount);

            _AtMaxChildOffset = _ChildOffset >= Hotkeys.Entries.Count - elementCount;

            foreach (var hotkey in hotkeys)
            {
                AddElement(MakeKeyString(hotkey, true), new Point(KEY_X, y), Color.White, -1, -1, null);
                AddElement(MakeFunctionString(hotkey), new Point(FUNCTION_X, y), Color.White, -1, -1, null);
                AddElement("X", new Point(DELETE_HOTKEY_X, y), Color.Red, Color.White, 1, -1,
                    delegate ()
                    {
                        SoundManager.PlaySound("DING");
                        Hotkeys.Entries.Remove(hotkey);
                        RegenerateElements();
                    }
                    );

                y += Font.CharacterHeight;
            }

            if (_ChildOffset > 0 || !_AtMaxChildOffset)
            {
                if (_ChildOffset > 0)
                    AddElement("{", new Point(SCROLL_X, selectableStartY), UIConstants.LEVEL_COMPLETED_COLOR, Color.White, -1, -1,
                        delegate ()
                        {
                            if (_ScrollLockTimer == 0)
                            {
                                _ChildOffset--;
                                _ScrollLockTimer = SCROLL_DELAY;
                                RegenerateElements();
                            }
                        }, true
                        );
                else
                    AddElement("{", new Point(SCROLL_X, selectableStartY), UIConstants.LEVEL_SELECT_DISABLED_COLOR, UIConstants.LEVEL_SELECT_DISABLED_COLOR, -1, -1, null);

                if (!_AtMaxChildOffset)
                    AddElement("}", new Point(SCROLL_X, y - Font.CharacterHeight), UIConstants.LEVEL_COMPLETED_COLOR, Color.White, -1, -1,
                        delegate ()
                        {
                            if (_ScrollLockTimer == 0)
                            {
                                _ChildOffset++;
                                _ScrollLockTimer = SCROLL_DELAY;
                                RegenerateElements();
                            }
                        }, true
                        );
                else
                    AddElement("}", new Point(SCROLL_X, y - Font.CharacterHeight), UIConstants.LEVEL_SELECT_DISABLED_COLOR, UIConstants.LEVEL_SELECT_DISABLED_COLOR, -1, -1, null);
            }

            y = selectableStartY + (Font.CharacterHeight * (elementCount + 1));

            AddElement("New", new Point(24, y), Color.Yellow, Color.White, -1, -1, EnterNewKeyMode);
            AddElement("Set to Default", new Point(RenderConstants.DESIGN_WIDTH * 5 / 16, y), Color.Yellow, Color.White, 0, -1, ResetToDefaultKeys);
            AddElement("Sort by " + (_SortByFunction ? "Key" : "Function"), new Point(RenderConstants.DESIGN_WIDTH * 11 / 16, y), Color.Yellow, Color.White, 0, -1, ChangeSortMode);
            AddElement("Exit", new Point(RenderConstants.DESIGN_WIDTH - 24, y), Color.Yellow, Color.White, 1, -1, ReturnToOptionsMenu);
        }

        private float _RapidFireProtectionTimer;
        private bool _ExitSetKeyProtectionFlag;

        protected override void DoUpdate(float elapsedTime)
        {
            base.DoUpdate(elapsedTime);

            if (SuspendInput)
            {
                if ((UserInput.IsMouseButtonPressed(MouseButton.Left) || UserInput.IsMouseButtonPressed(MouseButton.Right))
                    && !_ExitSetKeyProtectionFlag)
                    ExitSetKeyMode(false);
                else
                {
                    _ExitSetKeyProtectionFlag = false;

                    var keys = Enum.GetValues(typeof(Keys)).Cast<Keys>();
                    var modifierKeys = Hotkeys.Entries.Where(hk => hk.Function == HotkeyFunction.Modifier).Select(hk => hk.Key);
                    var state = Keyboard.GetState();

                    foreach (var key in state.GetPressedKeys())
                    {
                        if (!_KeyPresses.Contains(key))
                        {
                            _KeyPresses.Add(key);
                            if (!modifierKeys.Contains(key) || _NewKey.Function == HotkeyFunction.Modifier)
                                ExitSetKeyMode(true);

                            break;
                        }    
                    }

                    if (SuspendInput) // ie: we did not break out of set key mode during the previous loop
                    {
                        foreach (var key in _KeyPresses)
                            if (state.IsKeyUp(key))
                            {
                                ExitSetKeyMode(true);
                                break;
                            }
                    }
                }
            }
            else
            {
                if (UserInput.IsMouseButtonPressed(MouseButton.Left))
                    _RapidFireProtectionTimer = 0.15f;
                else if (_RapidFireProtectionTimer > 0)
                    _RapidFireProtectionTimer = Math.Max(_RapidFireProtectionTimer - elapsedTime, 0);

                _ScrollLockTimer = Math.Max(_ScrollLockTimer - elapsedTime, 0);
                if (_ScrollLockTimer == 0)
                {
                    if (UserInput.MouseScrollDelta < 0 && !_AtMaxChildOffset)
                    {
                        _ScrollLockTimer = SCROLL_DELAY;
                        _ChildOffset += 1;
                        RegenerateElements();
                    }

                    if (UserInput.MouseScrollDelta > 0 && _ChildOffset > 0)
                    {
                        _ScrollLockTimer = SCROLL_DELAY;
                        _ChildOffset -= 1;
                        RegenerateElements();
                    }
                }
            }
        }

    }
}
