﻿using Loap.Audio;
using Loap.Constants;
using Loap.Misc;
using Loap.Packs;
using Loap.Progress;
using Loap.UI;
using Loap.Views.Game;
using LoapCore.Constants;
using LoapCore.Files;
using LoapCore.Game;
using LoapCore.Misc;
using LoapCore.Replays;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NativeFileDialogSharp;
using System;
using System.Collections.Generic;
using System.IO;

namespace Loap.Views.Menu
{
    class PostLevelView : BaseMenuView
    {
        private struct PostviewLemming
        {
            public Rectangle DestRect;
            public bool IsSaved;
            public float AppearTime;
            public Color Color;
        }

        private const int SAVED_OFFSET_FROM_MIDDLE = 160;
        private const int TIME_OFFSET_FROM_MIDDLE = 160;

        private const int SAVED_Y = 160;
        private const int REQUIRED_Y = 212;
        private const int TIME_Y = 282;

        private const float LEMMING_APPEAR_INTERVAL = 0.1f;

        private const int LEMMING_ROWS = 13;
        private const int BASE_LEMMINGS_PER_ROW = 6;
        private const int LEMMING_OFFSET_FROM_CENTER = 214;
        private const int LEMMING_HORIZONTAL_DISTANCE = 36;
        private const int LEMMING_VERTICAL_DISTANCE = 36;
        private const int LEMMING_LOWEST_Y = RenderConstants.DESIGN_HEIGHT - 72;
        private const float LEMMING_FADE_IN_TIME = 0.25f;

        private readonly GameEngine _GameEngine;
        private readonly Texture2D _LemmingSprites;
        private Rectangle _LemmingFrameRect;
        private int _LemmingSpriteFrames;
        private List<PostviewLemming> _Lemmings = new List<PostviewLemming>();

        private bool _DoneAutoReplaySave;

        public PostLevelView(GraphicsDevice graphics, GameEngine gameEngine) : base(graphics)
        {
            _GameEngine = gameEngine;
            _LemmingSprites = Texture2D.FromFile(GraphicsDevice, PathConstants.PostviewLemmingsFilePath);
            GenerateLemmings();
        }

        private int GetMaxLemmingCount(int scale)
        {
            int basePerRow = BASE_LEMMINGS_PER_ROW * scale;
            int baseCountRows = ((LEMMING_ROWS * scale) + 1) / 2;
            int altCountRows = (LEMMING_ROWS * scale) - baseCountRows;

            return (basePerRow * baseCountRows) + ((basePerRow + 1) * altCountRows);
        }

        private void GenerateLemmings()
        {
            _LemmingFrameRect = new Rectangle(0, 0, _LemmingSprites.Width / 2, _LemmingSprites.Width / 2); // Using "Width" for both here is intentional.
            _LemmingSpriteFrames = _LemmingSprites.Height / _LemmingFrameRect.Height;

            int savedCount = _GameEngine.LemmingsRescued;
            int diedCount = _GameEngine.Level.LevelInfo.LemmingCount - savedCount;
            int savedPlaceholderCount = _GameEngine.Level.LevelInfo.SaveRequirement;

            int savedScale = 6;
            int diedScale = 6;

            for (int i = 1; i < 6; i++)
            {
                if (GetMaxLemmingCount(i) >= Math.Max(savedCount, savedPlaceholderCount) && i < savedScale)
                    savedScale = i;
                if (GetMaxLemmingCount(i) >= diedCount && i < diedScale)
                    diedScale = i;
            }

            int divisor = 1;
            while (GetMaxLemmingCount(savedScale) < (Math.Max(savedCount, savedPlaceholderCount) / divisor) || GetMaxLemmingCount(diedScale) < diedCount / divisor)
                divisor++;

            savedCount /= divisor;
            diedCount /= divisor;

            AddLemmings(savedCount, savedPlaceholderCount, true, new Point(RenderConstants.DESIGN_WIDTH / 2 - LEMMING_OFFSET_FROM_CENTER, LEMMING_LOWEST_Y), -LEMMING_HORIZONTAL_DISTANCE, savedScale, 0f);
            AddLemmings(diedCount, 0, false, new Point(RenderConstants.DESIGN_WIDTH / 2 + LEMMING_OFFSET_FROM_CENTER, LEMMING_LOWEST_Y), LEMMING_HORIZONTAL_DISTANCE, diedScale, 0f);
            _Lemmings.Reverse();
        }

        private void AddLemmings(int lemmingCount, int placeholderCount, bool isSaved, Point basePoint, int lemmingOffset, int scale, float appearDelay)
        {
            int thisRowLemmings = 0;
            int thisRowIndex = 0;
            int lemmingsPerStandardRow = BASE_LEMMINGS_PER_ROW * scale;

            float floatScale = scale;

            for (int lemIndex = 0; lemIndex < Math.Max(lemmingCount, placeholderCount); lemIndex++)
            {
                var newLemming = new PostviewLemming();

                newLemming.IsSaved = isSaved;
                if (lemIndex >= lemmingCount)
                    newLemming.Color = UIConstants.POST_LEMMING_MISSING_COLOR;
                else
                    newLemming.Color = Color.White;

                FloatRectangle tempDestRect = new FloatRectangle();
                tempDestRect.Width = _LemmingFrameRect.Width / floatScale;
                tempDestRect.Height = _LemmingFrameRect.Height / floatScale;
                tempDestRect.X = basePoint.X +
                    (thisRowLemmings * lemmingOffset / floatScale) -
                    ((thisRowIndex % 2 == 1) ? (lemmingOffset / floatScale / 2) : 0) -
                    (tempDestRect.Width / 2);
                tempDestRect.Y = basePoint.Y -
                    (thisRowIndex * LEMMING_VERTICAL_DISTANCE / floatScale) -
                    (tempDestRect.Height);

                newLemming.DestRect = new Rectangle(
                    (int)Math.Round(tempDestRect.X),
                    (int)Math.Round(tempDestRect.Y),
                    (int)Math.Round((tempDestRect.X + tempDestRect.Width) - Math.Round(tempDestRect.X)),
                    (int)Math.Round((tempDestRect.Y + tempDestRect.Height) - Math.Round(tempDestRect.Y))
                    );

                newLemming.AppearTime = appearDelay + (LEMMING_APPEAR_INTERVAL * lemIndex);

                _Lemmings.Add(newLemming);

                thisRowLemmings++;
                if (
                    ((thisRowIndex % 2 == 0) && (thisRowLemmings == lemmingsPerStandardRow)) ||
                    ((thisRowIndex % 2 == 1) && (thisRowLemmings == lemmingsPerStandardRow + 1))
                   )
                {
                    thisRowIndex++;
                    thisRowLemmings = 0;
                }
            }
        }

        protected override void GenerateElements()
        {
            AddElement(_GameEngine.CheatMode ? GameTexts.POSTVIEW_HEADER_CHEAT_MODE : GameTexts.POSTVIEW_HEADER,
                new Point(RenderConstants.DESIGN_WIDTH / 2, 32), Color.White, 0, -1, null);

            AddElement(GameTexts.POSTVIEW_YOU_SAVED, new Point(RenderConstants.DESIGN_WIDTH / 2 - SAVED_OFFSET_FROM_MIDDLE, SAVED_Y), Color.White, -1, -1, null);
            AddElement(GameTexts.POSTVIEW_YOU_NEEDED, new Point(RenderConstants.DESIGN_WIDTH / 2 - SAVED_OFFSET_FROM_MIDDLE, REQUIRED_Y), Color.White, -1, -1, null);

            AddElement(_GameEngine.LemmingsRescued.ToString(), new Point(RenderConstants.DESIGN_WIDTH / 2 + SAVED_OFFSET_FROM_MIDDLE, SAVED_Y), Color.White, 1, -1, null);
            AddElement(_GameEngine.Level.LevelInfo.SaveRequirement.ToString(), new Point(RenderConstants.DESIGN_WIDTH / 2 + SAVED_OFFSET_FROM_MIDDLE, REQUIRED_Y), Color.White, 1, -1, null);

            if (_GameEngine.LemmingsNeededToRescue <= 0)
            {
                AddElement(GameTexts.POSTVIEW_YOUR_TIME, new Point(RenderConstants.DESIGN_WIDTH / 2 - TIME_OFFSET_FROM_MIDDLE, TIME_Y), Color.White, -1, -1, null);

                var successIteration = Math.Max(_GameEngine.MetRequirementIteration, 0);
                string timeText = (successIteration / (60 * PhysicsConstants.ITERATIONS_PER_INGAME_SECOND)).ToString() + ":" +
                    ((successIteration / PhysicsConstants.ITERATIONS_PER_INGAME_SECOND) % 60).ToString("D2") + "." +
                    ((successIteration % PhysicsConstants.ITERATIONS_PER_INGAME_SECOND) * 100 / PhysicsConstants.ITERATIONS_PER_INGAME_SECOND).ToString("D2");

                AddElement(timeText, new Point(RenderConstants.DESIGN_WIDTH / 2 + TIME_OFFSET_FROM_MIDDLE, TIME_Y), Color.White, 1, -1, null);

                if (LoapProgress.CurrentLevel == null || _GameEngine.CheatMode)
                    AddElement(GameTexts.LEFT_MOUSE_RETRY, new Point(12, RenderConstants.DESIGN_HEIGHT - 12), Color.White, -1, 1, null);
                else
                    AddElement(GameTexts.LEFT_MOUSE_NEXT, new Point(12, RenderConstants.DESIGN_HEIGHT - 12), Color.White, -1, 1, null);
            }
            else
                AddElement(GameTexts.LEFT_MOUSE_RETRY, new Point(12, RenderConstants.DESIGN_HEIGHT - 12), Color.White, -1, 1, null);

            AddElement(GameTexts.RIGHT_MOUSE_EXIT, new Point(RenderConstants.DESIGN_WIDTH - 12, RenderConstants.DESIGN_HEIGHT - 12), Color.White, 1, 1, null);
        }

        protected override void DoUpdate(float elapsedTime)
        {
            base.DoUpdate(elapsedTime);

            if (!_DoneAutoReplaySave)
            {
                if (LoapSettings.AutosaveReplays && (_GameEngine.LemmingsNeededToRescue <= 0) && !_GameEngine.CheatMode)
                {
                    _GameEngine.Replay.LevelTitle = _GameEngine.Level.LevelInfo.Title;
                    if (LoapProgress.CurrentLevel == null)
                        _GameEngine.Replay.LevelPath = "";
                    else
                        _GameEngine.Replay.LevelPath = LoapProgress.CurrentLevel.FilePath;

                    string saveName = _GameEngine.Level.LevelInfo.Title;
                    foreach (var c in Path.GetInvalidFileNameChars())
                        saveName = saveName.Replace(c, '_');
                    saveName = saveName.Replace(' ', '_');

                    saveName += "__" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

                    DataItem replayItem = new DataItem();
                    ReplayFile.SaveReplayToDataItem(_GameEngine.Replay, replayItem);
                    string savePath;
                    if (LoapProgress.CurrentLevel == null)
                        savePath = Path.Combine(
                            PathConstants.ReplayPath,
                            PathConstants.AUTO_REPLAY_FOLDER
                            );
                    else
                        savePath = Path.Combine(
                            PathConstants.ReplayPath,
                            LoapProgress.CurrentLevel.Parent.FilePath,
                            PathConstants.AUTO_REPLAY_FOLDER
                            );
                    Directory.CreateDirectory(savePath);
                    DataItemWriter.SaveToFile(Path.Combine(savePath, saveName + PathConstants.EXT_REPLAY), replayItem);
                }

                _DoneAutoReplaySave = true;
            }

            if (UserInput.IsMouseButtonPressed(MouseButton.Left))
            {
                if (_GameEngine.LemmingsNeededToRescue <= 0 && LoapProgress.CurrentLevel != null && !_GameEngine.CheatMode)
                {
                    LoapProgress.CurrentLevel = ((LoapPackCachePackEntry)LoapProgress.CurrentLevel.Parent).FindNextLevel(LoapProgress.CurrentLevel);
                    SetNewView(() => new PreLevelView(GraphicsDevice, LoapProgress.CurrentLevel.FullPath));
                }
                else
                    SetNewView(() => new PreLevelView(GraphicsDevice, _GameEngine.Level, _GameEngine.Replay));
            }

            if (UserInput.IsMouseButtonPressed(MouseButton.Middle))
                SetNewView(() => new PreLevelView(GraphicsDevice, _GameEngine.Level, _GameEngine.Replay));

            if (UserInput.IsMouseButtonPressed(MouseButton.Right))
                SetNewView(() => new LevelSelectView(GraphicsDevice));

            if (UserInput.IsKeyPressed(HotkeyFunction.SaveReplay))
                SaveReplay();
        }

        protected override void MenuCustomRenderToDesign()
        {
            foreach (var lem in _Lemmings)
            {
                if (lem.AppearTime > ViewRunTime)
                    continue;

                int lemmingFrame = ((int)Math.Round(ViewRunTime * 15)) % _LemmingSpriteFrames;
                Rectangle srcRect = new Rectangle(
                    lem.IsSaved ? 0 : _LemmingFrameRect.Width,
                    _LemmingFrameRect.Height * lemmingFrame,
                    _LemmingFrameRect.Width,
                    _LemmingFrameRect.Height
                    );

                SpriteBatch.Draw(_LemmingSprites, lem.DestRect, srcRect, GetLemmingFadeinColor(ViewRunTime - lem.AppearTime, lem.Color));
            }
        }

        private Color GetLemmingFadeinColor(float timeSinceAppear, Color baseColor)
        {
            if (timeSinceAppear <= 0)
                return Color.Transparent;
            else if (timeSinceAppear >= LEMMING_FADE_IN_TIME)
                return baseColor;
            else
            {
                float colorScale = timeSinceAppear / LEMMING_FADE_IN_TIME;
                return new Color(colorScale * baseColor.R / 255.0f, colorScale * baseColor.G / 255.0f, colorScale * baseColor.B / 255.0f, colorScale);
            }
        }

        private void SaveReplay()
        {
            _GameEngine.Replay.LevelTitle = _GameEngine.Level.LevelInfo.Title;

            string replayPath;
            if (LoapProgress.CurrentLevel == null)
            {
                replayPath = Path.Combine(
                    PathConstants.ReplayPath
                    );
                _GameEngine.Replay.LevelPath = "";
            }
            else
            {
                replayPath = Path.Combine(
                    PathConstants.ReplayPath,
                    LoapProgress.CurrentLevel.Parent.FilePath
                    );
                _GameEngine.Replay.LevelPath = LoapProgress.CurrentLevel.FilePath;
            }

            Directory.CreateDirectory(replayPath);

            var saveResult = Dialog.FileSave(PathConstants.EXT_REPLAY.Replace(".", ""), replayPath);
            if (saveResult.IsOk)
            {
                string saveName = saveResult.Path;
                if (Path.GetExtension(saveName) == "")
                    saveName = saveName + PathConstants.EXT_REPLAY;

                DataItem replayItem = new DataItem();
                ReplayFile.SaveReplayToDataItem(_GameEngine.Replay, replayItem);
                DataItemWriter.SaveToFile(saveName, replayItem);
                SoundManager.PlaySound(SoundNames.OKAY);
            }
        }
    }
}
