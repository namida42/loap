﻿using Loap.Audio;
using Loap.Constants;
using Loap.Misc;
using Loap.Packs;
using Loap.Progress;
using Loap.UI;
using Loap.Views.Game;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loap.Views.Menu
{
    class LevelSelectView : BaseMenuView
    {
        private bool _PlayedSound;

        private LoapPackCachePackEntry _CurrentPack;

        public LevelSelectView(GraphicsDevice graphics, string? initialPath = null) : base(graphics)
        {
            if (initialPath != null)
            {
                var newBasePack = LoapPackCache.GetPackByDirectory(initialPath);
                if (newBasePack != null)
                    _CurrentPack = newBasePack;
            }

            if (_CurrentPack == null)
            {
                if (LoapProgress.CurrentLevel == null)
                    _CurrentPack = LoapPackCache.BasePack;
                else if (LoapProgress.CurrentLevel.Parent is LoapPackCachePackEntry packEntry)
                    _CurrentPack = packEntry;
                else
                    _CurrentPack = LoapPackCache.BasePack;
            }
        }

        private const int HEADINGS_X = 24;
        private const int SCROLL_X = 24;
        private const int SELECTABLE_X = 64;

        private int _ChildOffset = 0;
        private bool _AtMaxChildOffset;

        protected override void GenerateElements()
        {
            LoapPackCache.Refresh(true);

            _AtMaxChildOffset = true;

            int y = 24;

            _CurrentPack.GetDataFromFile(true);

            if (_CurrentPack == LoapPackCache.BasePack)
                AddElement("Select level pack", new Point(HEADINGS_X, y), Color.White, -1, -1, null);
            else
            {
                string localPath = _CurrentPack.FilePath;
                var pathSplit = localPath.Split(Path.DirectorySeparatorChar);
                List<string> elements = new List<string>(pathSplit.Length);

                foreach (var pathElement in pathSplit)
                {
                    string visibleFolderName = pathElement;

                    var folderSplit = visibleFolderName.Split(' ');
                    if (int.TryParse(folderSplit[0], out _))
                        visibleFolderName = string.Join(' ', folderSplit[1..]);

                    elements.Add(visibleFolderName);
                }

                AddElement(string.Join(" - ", elements), new Point(HEADINGS_X, y), Color.White, -1, -1, null);
            }

            y += Font.CharacterHeight * 2;

            int selectableStartY = y;
            int elementCount = ((RenderConstants.DESIGN_HEIGHT - selectableStartY - (Font.CharacterHeight * 5 / 4)) / Font.CharacterHeight) - 1;

            List<LoapPackCacheBaseEntry?> entries = new List<LoapPackCacheBaseEntry?>();

            if (_ChildOffset == 0)
                entries.Add(null);

            _AtMaxChildOffset = false;

            int localChildOffset = Math.Max(_ChildOffset - 1, 0);
            if (localChildOffset < _CurrentPack.Children.Count)
            {
                while (entries.Count < elementCount)
                {
                    entries.Add(_CurrentPack.Children[localChildOffset]);
                    localChildOffset++;

                    if (localChildOffset == _CurrentPack.Children.Count)
                    {
                        _AtMaxChildOffset = true;
                        break;
                    }
                }
            }
            else
                _AtMaxChildOffset = true;

            var levelChildren = _CurrentPack.Children.Where(c => c is LoapPackCacheLevelEntry).Cast<LoapPackCacheLevelEntry>().ToList();

            foreach (var entry in entries)
            {
                if (entry == null)
                {
                    if (_CurrentPack == LoapPackCache.BasePack)
                    {
                        AddElement("<< Return to Title Screen", new Point(SELECTABLE_X, y), UIConstants.LEVEL_NOT_ATTEMPTED_COLOR, Color.White, -1, -1,
                            delegate ()
                            {
                                SetNewView(() => new TitleView(GraphicsDevice));
                            }
                            );
                    }
                    else
                    {
                        var pathElements = _CurrentPack.FilePath.Split(Path.DirectorySeparatorChar);
                        string parentName = pathElements.Length <= 1 ?
                            "Select level pack" : pathElements[^2];

                        AddElement("<< " + parentName, new Point(SELECTABLE_X, y), UIConstants.PACK_COLOR, Color.White, -1, -1,
                            delegate ()
                            {
                                _ChildOffset = 0;
                                _CurrentPack = _CurrentPack.Parent as LoapPackCachePackEntry ?? LoapPackCache.BasePack;
                                RegenerateElements();
                            }
                            );
                    }
                }
                else if (entry is LoapPackCachePackEntry packChild)
                {
                    string localFolderName = packChild.FilePath;
                    string visibleFolderName = Path.GetFileName(localFolderName);

                    var folderSplit = visibleFolderName.Split(' ');
                    if (int.TryParse(folderSplit[0], out _))
                        visibleFolderName = string.Join(' ', folderSplit[1..]);

                    AddElement(visibleFolderName + " >>", new Point(SELECTABLE_X, y), UIConstants.PACK_COLOR, Color.White, -1, -1,
                        delegate ()
                        {
                            _ChildOffset = 0;
                            _CurrentPack = packChild;
                            RegenerateElements();
                        }
                        );
                }
                else if (entry is LoapPackCacheLevelEntry levelChild)
                {
                    string localTitle = levelChild.Title;
                    if (localTitle.Length > 60)
                        localTitle = localTitle[0..59] + "...";

                    var progress = LoapProgress.GetLevelProgress(levelChild);
                    var titleColor = progress.Completion switch
                    {
                        LevelCompletion.Attempted => UIConstants.LEVEL_ATTEMPTED_COLOR,
                        LevelCompletion.Completed => UIConstants.LEVEL_COMPLETED_COLOR,
                        _ => UIConstants.LEVEL_NOT_ATTEMPTED_COLOR
                    };

                    string prefix;
                    if (_CurrentPack != LoapPackCache.BasePack)
                    {
                        int digits = (levelChildren.Count + 1).ToString().Length;
                        prefix = (levelChildren.IndexOf(levelChild) + 1).ToString("D" + digits.ToString());
                        prefix += new string(' ', Math.Max(digits - prefix.Length, 0)) + "   ";
                    }
                    else
                        prefix = "";

                    AddElement(prefix + localTitle, new Point(SELECTABLE_X, y), titleColor, Color.White, -1, -1,
                        delegate ()
                        {
                            LoapProgress.CurrentLevel = levelChild;
                            SetNewView(() => new PreLevelView(GraphicsDevice, levelChild.FullPath));
                        }
                        );
                }

                y += Font.CharacterHeight;
            }

            if (_ChildOffset > 0 || !_AtMaxChildOffset)
            {
                if (_ChildOffset > 0)
                    AddElement("{", new Point(SCROLL_X, selectableStartY), UIConstants.LEVEL_COMPLETED_COLOR, Color.White, -1, -1,
                        delegate ()
                        {
                            if (_ScrollLockTimer == 0)
                            {
                                _ChildOffset--;
                                _ScrollLockTimer = SCROLL_DELAY;
                                RegenerateElements();
                            }
                        }, true
                        );
                else
                    AddElement("{", new Point(SCROLL_X, selectableStartY), UIConstants.LEVEL_SELECT_DISABLED_COLOR, UIConstants.LEVEL_SELECT_DISABLED_COLOR, -1, -1, null);

                if (!_AtMaxChildOffset)
                    AddElement("}", new Point(SCROLL_X, y - Font.CharacterHeight), UIConstants.LEVEL_COMPLETED_COLOR, Color.White, -1, -1,
                        delegate ()
                        {
                            if (_ScrollLockTimer == 0)
                            {
                                _ChildOffset++;
                                _ScrollLockTimer = SCROLL_DELAY;
                                RegenerateElements();
                            }
                        }, true
                        );
                else
                    AddElement("}", new Point(SCROLL_X, y - Font.CharacterHeight), UIConstants.LEVEL_SELECT_DISABLED_COLOR, UIConstants.LEVEL_SELECT_DISABLED_COLOR, -1, -1, null);
            }

            y = selectableStartY + (Font.CharacterHeight * elementCount);
            AddElement("Mass Replay Check", new Point(RenderConstants.DESIGN_WIDTH - 24, y), Color.Yellow, Color.White, 1, -1, MassReplayCheck);
        }

        private float _ScrollLockTimer;
        private const float SCROLL_DELAY = 0.025f;

        protected override void DoUpdate(float elapsedTime)
        {
            base.DoUpdate(elapsedTime);

            if (UserInput.IsKeyPressed(HotkeyFunction.LoadReplay))
                MassReplayCheck();

            if (!_PlayedSound)
            {
                _PlayedSound = true;
                MusicManager.PlayMusic(Path.Combine(PathConstants.MusicPath, "official/title" + PathConstants.EXT_AUDIO));
            }

            _ScrollLockTimer = Math.Max(_ScrollLockTimer - elapsedTime, 0);
            if (_ScrollLockTimer == 0)
            {
                if (UserInput.MouseScrollDelta < 0 && !_AtMaxChildOffset)
                {
                    _ScrollLockTimer = SCROLL_DELAY;
                    _ChildOffset += 1;
                    RegenerateElements();
                }

                if (UserInput.MouseScrollDelta > 0 && _ChildOffset > 0)
                {
                    _ScrollLockTimer = SCROLL_DELAY;
                    _ChildOffset -= 1;
                    RegenerateElements();
                }
            }
        }

        private void MassReplayCheck()
        {
            SetNewView(() => new ReplayCheckView(GraphicsDevice, _CurrentPack.FilePath));
        }
    }
}
