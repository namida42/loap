﻿using Loap.Audio;
using Loap.Constants;
using Loap.Progress;
using Loap.UI;
using LoapCore.Constants;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;
using System.Linq;

namespace Loap.Views.Menu
{
    class TitleView : BaseMenuView
    {
        private const int PANEL_SIZE = 192;

        private Texture2D _TitleGraphic;
        private Texture2D _PanelGraphics;

        private string _VersionText;

        public TitleView(GraphicsDevice graphics) : base(graphics)
        {
            _VersionText = GetVersionText("Loap") + " / " + GetVersionText("LoapCore");

            _TitleGraphic = Texture2D.FromFile(GraphicsDevice, PathConstants.TitleFilePath);
            _PanelGraphics = Texture2D.FromFile(GraphicsDevice, PathConstants.PanelsFilePath);

            MusicManager.PlayMusic(Path.Combine(PathConstants.MusicPath, "official/title" + PathConstants.EXT_AUDIO));
        }

        private string GetVersionText(string assembly)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var version = assemblies.First(a => a.GetName().Name == assembly).GetName().Version;
            return assembly + " V" + (version == null ? "?.?.?.?" : version.ToString());
        }

        protected override void GenerateElements()
        {
            int horzCenter = RenderConstants.DESIGN_WIDTH / 2;
            int vertUnit = RenderConstants.DESIGN_HEIGHT / 20;

            int panelDistance = PANEL_SIZE * 7 / 6;

            AddElement(_TitleGraphic, _TitleGraphic.Bounds, new Point(horzCenter, vertUnit * 6), null);

            AddElement(_PanelGraphics, new Rectangle(0, 0, PANEL_SIZE, PANEL_SIZE), new Point(horzCenter - panelDistance, vertUnit * 15), Play);
            AddElement(_PanelGraphics, new Rectangle(0, PANEL_SIZE, PANEL_SIZE, PANEL_SIZE), new Point(horzCenter, vertUnit * 15), Options);
            AddElement(_PanelGraphics, new Rectangle(0, PANEL_SIZE * 2, PANEL_SIZE, PANEL_SIZE), new Point(horzCenter + panelDistance, vertUnit * 15), Exit);
        }

        private void Play()
        {
            SoundManager.PlaySound(SoundNames.LETS_GO);
            LoapProgress.CurrentLevel = null;
            SetNewView(() => new LevelSelectView(GraphicsDevice));
        }

        private void Options()
        {
            SetNewView(() => new OptionsView(GraphicsDevice));
        }

        private void Exit()
        {
            SetNewView(() => null!);
        }

        protected override void MenuCustomRenderToDesign()
        {
            Font.WriteText(SpriteBatch, _VersionText, new Point(RenderConstants.DESIGN_WIDTH / 2, RenderConstants.DESIGN_HEIGHT - 8 - Font.CharacterHeight - 1), Color.White, 0, 1);
            Font.WriteText(SpriteBatch, GameTexts.CREDIT_TEXT, new Point(RenderConstants.DESIGN_WIDTH / 2, RenderConstants.DESIGN_HEIGHT - 8), Color.White, 0, 1);
        }
    }
}
