﻿using Loap.Audio;
using Loap.Constants;
using Loap.Misc;
using Loap.Packs;
using Loap.Progress;
using Loap.UI;
using LoapCore.Constants;
using LoapCore.Files;
using LoapCore.Game;
using LoapCore.Levels;
using LoapCore.Misc;
using LoapCore.Replays;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NativeFileDialogSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loap.Views.Menu
{
    class ReplayCheckView : BaseMenuView
    {
        private const int TIMEOUT_ITERATIONS = 30 * 60 * 5;
        private const int FRAMES_PER_SECOND_TARGET = 60; // Applied very dumbly

        private readonly Texture2D _OptionGlyph;
        private readonly Rectangle _OptionBoxOn;
        private readonly Rectangle _OptionBoxOff;

        private const int PATH_DISPLAY_CAP = 32;

        private readonly string CheckBasePath;
        private readonly string DisplayBasePath;
        private string ReplayBasePath;

        public bool IncludeLevelSubdirs = true;
        public bool IncludeReplaySubdirs = true;
        public bool BruteForceUnmatchedLevels = true;
        public bool UpdateRecords = false;

        private const int FEEDBACK_LINE_CAP = 13;

        private bool _RunningCheck;
        private bool _CheckComplete;
        private List<LoapPackCacheLevelEntry>? _LevelEntries;
        private List<string>? _ReplayEntries;
        private int _ReplayIndex;
        private int _LevelIndex = -1;
        private LoapPackCacheLevelEntry? _LevelEntry;
        private LevelProgress? _LevelProgress;
        private int _LastSignificantIteration;

        private List<string> _ReplayCheckFeedback = new List<string>();
        private List<string> _ReplayCheckTempFeedback = new List<string>();
        private Dictionary<string, ReplayResult> _ReplayCheckResults = new Dictionary<string, ReplayResult>();
        private Dictionary<string, Level> _LevelCache = new Dictionary<string, Level>();

        private GameEngine? _Engine;

        public ReplayCheckView(GraphicsDevice graphics, string path) : base(graphics)
        {
            _OptionGlyph = Texture2D.FromFile(GraphicsDevice, PathConstants.OptionBoxFilePath);
            _OptionBoxOn = new Rectangle(0, 0, _OptionGlyph.Width, _OptionGlyph.Height / 2);
            _OptionBoxOff = new Rectangle(0, _OptionGlyph.Height / 2, _OptionGlyph.Width, _OptionGlyph.Height / 2);

            CheckBasePath = path;
            DisplayBasePath = (path.Length <= PATH_DISPLAY_CAP) ? path : "..." + path[^(PATH_DISPLAY_CAP - 3)..];
            ReplayBasePath = Path.Combine(PathConstants.ReplayPath, path);

            if (!Directory.Exists(ReplayBasePath))
                ReplayBasePath = PathConstants.ReplayPath;

            if (!Directory.Exists(ReplayBasePath)) // still
                Directory.CreateDirectory(ReplayBasePath);
        }

        protected override void DoUpdate(float elapsedTime)
        {
            base.DoUpdate(elapsedTime);

            if (_RunningCheck)
            {
                if (_LevelEntries == null)
                    BuildLevelEntries();
                else if (_ReplayEntries == null)
                    BuildReplayEntries();
                else if (!_CheckComplete)
                    UpdateReplayCheck();
            }
        }

        private void UpdateReplayCheck()
        {
            if (_Engine == null)
                SetUpNextReplayCheck();
            else
                ContinueReplayExecution();
        }

        private void SaveResultsToTextFile()
        {
            using var stream = new FileStream(Path.Combine(PathConstants.AppPath, Path.GetFileName(CheckBasePath) + " Replay Check Results.txt"), FileMode.Create);
            using var writer = new StreamWriter(stream);

            writer.WriteLine("Mass replay check results for");
            writer.WriteLine("  " + CheckBasePath);
            writer.WriteLine();

            foreach (var resultType in new[] { ReplayResult.Failed, ReplayResult.Undetermined, ReplayResult.NoMatch, ReplayResult.Passed })
            {
                var resultsOfType = _ReplayCheckResults.Where(p => p.Value == resultType);

                writer.WriteLine("=== " + resultType.ToString().ToUpperInvariant() + " ===");
                foreach (var pair in resultsOfType)
                    writer.WriteLine("  " + pair.Key);

                if (resultsOfType.Count() == 0)
                    writer.WriteLine("  (None)");

                writer.WriteLine();
            }

            _ReplayCheckFeedback.Add("");
            _ReplayCheckFeedback.Add("Results written to text file:");
            _ReplayCheckFeedback.Add("  " + Path.GetFileName(CheckBasePath) + " Replay Check Results.txt");
        }

        private void SetUpNextReplayCheck()
        {
            if (_ReplayIndex >= _ReplayEntries!.Count)
            {
                _ReplayCheckTempFeedback.Clear();
                SaveResultsToTextFile();
                _CheckComplete = true;
                return;
            }

            var thisReplayFilename = _ReplayEntries[_ReplayIndex];
            var testReplay = new Replay();
            ReplayFile.LoadReplayFromDataItem(DataItemReader.LoadFromFile(thisReplayFilename), testReplay);

            LoapPackCacheLevelEntry? matchLevel = null;

            if (_LevelIndex < 0)
            {
                _ReplayCheckFeedback.Add(Path.GetFileName(thisReplayFilename));
                RegenerateElements();

                matchLevel = _LevelEntries!.FirstOrDefault(lvl => lvl.FilePath == testReplay.LevelPath);
                if (matchLevel == null)
                    matchLevel = _LevelEntries!.FirstOrDefault(lvl => lvl.Title == testReplay.LevelTitle);

                if (matchLevel == null)
                {
                    if (BruteForceUnmatchedLevels)
                        _ReplayCheckFeedback.Add("  Cannot match level; attempting brute-force");
                    _LevelIndex = 0;
                    return;
                }
            }
            else
            {
                if (_LevelIndex >= _LevelEntries!.Count || !BruteForceUnmatchedLevels)
                {
                    _ReplayCheckFeedback.Add("  Matching level not found");
                    _ReplayCheckResults.Add(thisReplayFilename, ReplayResult.NoMatch);
                    _LevelIndex = -1;
                    _ReplayIndex++;
                    RegenerateElements();
                    return;
                }
                else
                {
                    matchLevel = _LevelEntries[_LevelIndex];
                    _LevelIndex++;
                }
            }

            _ReplayCheckTempFeedback.Clear();
            _ReplayCheckTempFeedback.Add("  Level: " + matchLevel.Title);
            _ReplayCheckTempFeedback.Add("  Running for 0 iterations / 00:00");

            Level level;
            if (_LevelCache.ContainsKey(matchLevel.FilePath))
                level = _LevelCache[matchLevel.FilePath];
            else
            {
                level = LevelFileLoader.LoadLevel(matchLevel.FullPath);
                _LevelCache.Add(matchLevel.FilePath, level);
            }
            var metaEntities = MetaEntityLoader.LoadLevelMetaEntities(level);
            _Engine = new GameEngine(level, metaEntities, testReplay);
            _LevelEntry = matchLevel;
            _LevelProgress = LoapProgress.GetLevelProgress(matchLevel);
            _LastSignificantIteration = testReplay.Actions.Select(a => a.Iteration).Max();

            if (UpdateRecords)
                if (_LevelProgress.Completion < LevelCompletion.Attempted)
                    _LevelProgress.Completion = LevelCompletion.Attempted;
        }

        private void ContinueReplayExecution()
        {
            bool completedExecution = false;

            DateTime target = DateTime.Now.AddMilliseconds(1000.0 / FRAMES_PER_SECOND_TARGET * 4 / 5);

            while (DateTime.Now < target)
            {
                int oldRescued = _Engine!.LemmingsRescued;
                _Engine.Update();

                if (UpdateRecords && _Engine.LemmingsRescued > oldRescued)
                {
                    _LastSignificantIteration = Math.Max(_LastSignificantIteration, _Engine.Iteration);

                    if (UpdateRecords)
                    {
                        _LevelProgress!.SaveRecord = Math.Max(_LevelProgress.SaveRecord, _Engine.LemmingsRescued);

                        if (_Engine.LemmingsRescued >= _Engine.Level.LevelInfo.SaveRequirement && oldRescued < _Engine.Level.LevelInfo.SaveRequirement)
                        {
                            _LevelProgress.Completion = LevelCompletion.Completed;

                            _LevelProgress.TimeRecord = Utils.MinPositive(_LevelProgress.TimeRecord, _Engine.Iteration);

                            int totalSkills = 0;
                            int skillTypes = 0;

                            foreach (var skill in Utils.GetEnumValues<Skill>())
                            {
                                int skillUsed = _Engine.Level.LevelInfo.Skillset[skill] - _Engine.Skillset[skill];

                                _LevelProgress.SkillRecord[skill] = Utils.MinPositive(_LevelProgress.SkillRecord[skill], skillUsed);
                                totalSkills += skillUsed;
                                if (skillUsed > 0)
                                    skillTypes += 1;
                            }

                            _LevelProgress.TotalSkillsRecord = Utils.MinPositive(_LevelProgress.TotalSkillsRecord, totalSkills);
                            _LevelProgress.SkillTypesRecord = Utils.MinPositive(_LevelProgress.SkillTypesRecord, skillTypes);
                        }
                    }
                }

                if (_Engine.LemmingsAlive < _Engine.LemmingsNeededToRescue && !UpdateRecords)
                {
                    completedExecution = true;
                    break;
                }

                if (_Engine.Iteration - _LastSignificantIteration > TIMEOUT_ITERATIONS)
                {
                    completedExecution = true;
                    break;
                }
            }

            _ReplayCheckTempFeedback.Clear();
            _ReplayCheckTempFeedback.Add("  Level: " + _Engine!.Level.LevelInfo.Title);
            _ReplayCheckTempFeedback.Add("  Running for " + _Engine.Iteration.ToString() + " iterations / " +
                (_Engine.Iteration / (60 * PhysicsConstants.ITERATIONS_PER_INGAME_SECOND)).ToString("D2") +
                ":" +
                ((_Engine.Iteration / PhysicsConstants.ITERATIONS_PER_INGAME_SECOND) % 60).ToString("D2"));

            if (completedExecution)
            {
                if (_LevelIndex < 0 || _Engine!.LemmingsNeededToRescue <= 0)
                {
                    if (UpdateRecords)
                        LoapProgress.SetLevelProgress(_LevelEntry!, _LevelProgress!);

                    var thisReplayFilename = _ReplayEntries![_ReplayIndex];

                    ReplayResult result;
                    if (_Engine!.LemmingsNeededToRescue <= 0)
                        result = ReplayResult.Passed;
                    else if (_Engine.LemmingsAlive >= _Engine.LemmingsNeededToRescue)
                        result = ReplayResult.Undetermined;
                    else
                        result = ReplayResult.Failed;

                    _ReplayCheckFeedback.Add(result switch
                    {
                        ReplayResult.Passed => "  Replay solved the level",
                        ReplayResult.Failed => "  Replay failed to solve the level",
                        _ => "  Replay result indeterminate"
                    }
                        );
                    _ReplayCheckResults.Add(thisReplayFilename, result);
                    _LevelIndex = -1;
                    _ReplayIndex++;
                }

                _Engine = null;
            }
        }

        private void BuildLevelEntries()
        {
            var basePack = LoapPackCache.GetPackByDirectory(CheckBasePath);
            _LevelEntries = new List<LoapPackCacheLevelEntry>();

            if (basePack != null)
                BuildLevelEntriesRecursive(basePack);
        }

        private void BuildLevelEntriesRecursive(LoapPackCachePackEntry pack)
        {
            if (IncludeLevelSubdirs)
                foreach (var child in pack.Children.Where(c => c is LoapPackCachePackEntry))
                    BuildLevelEntriesRecursive((LoapPackCachePackEntry)child);

            pack.GetDataFromFile(true);
            _LevelEntries!.AddRange(pack.Children.Where(c => c is LoapPackCacheLevelEntry).Cast<LoapPackCacheLevelEntry>());
        }

        private void BuildReplayEntries()
        {
            _ReplayEntries = new List<string>();
            BuildReplayEntriesRecursive(ReplayBasePath);
        }

        private void BuildReplayEntriesRecursive(string path)
        {
            if (IncludeReplaySubdirs)
            {
                var subdirs = Directory.GetDirectories(path);
                foreach (var subdir in subdirs)
                    BuildReplayEntriesRecursive(Path.Combine(path, subdir));
            }

            var files = Directory.GetFiles(path);
            _ReplayEntries!.AddRange(files.Where(fn => Path.GetExtension(fn) == PathConstants.EXT_REPLAY));
        }

        protected override void MenuCustomRenderToDesign()
        {
            if (_RunningCheck)
            {
                int y = 96;
                var jointList = _ReplayCheckFeedback.Concat(_ReplayCheckTempFeedback);
                foreach (var line in jointList.TakeLast(Math.Min(jointList.Count(), FEEDBACK_LINE_CAP)))
                {
                    Font.WriteText(SpriteBatch, line, new Point(48, y), Color.White, -1, -1);
                    y += 40;
                }
            }
        }

        private void GenerateSettingsElements()
        {
            AddElement("Sources", new Point(60, 96), Color.White, -1, -1, null);
            AddElement("Levels: " + DisplayBasePath, new Point(96, 136), Color.White, -1, -1, null);
            AddElement("Replay: " + (ReplayBasePath.Length > PATH_DISPLAY_CAP ? "..." + ReplayBasePath[^(PATH_DISPLAY_CAP - 3)..] : ReplayBasePath),
                new Point(96, 176), Color.Yellow, Color.White, -1, -1, SelectReplayPath);

            AddElement("Include level subdirectories", new Point(108, 236), Color.White, -1, -1, null);
            AddElement("Include replay subdirectories", new Point(108, 276), Color.White, -1, -1, null);
            AddElement("Brute force unidentified levels", new Point(108, 316), Color.White, -1, -1, null);
            AddElement("Update records from replays", new Point(108, 356), Color.White, -1, -1, null);

            AddElement(_OptionGlyph, IncludeLevelSubdirs ? _OptionBoxOn : _OptionBoxOff, new Point(72, 257), () =>
                {
                    HandleOptionChange(() => { IncludeLevelSubdirs = !IncludeLevelSubdirs; });
                }
                );
            AddElement(_OptionGlyph, IncludeReplaySubdirs ? _OptionBoxOn : _OptionBoxOff, new Point(72, 297), () =>
                {
                    HandleOptionChange(() => { IncludeReplaySubdirs = !IncludeReplaySubdirs; });
                }
                );
            AddElement(_OptionGlyph, BruteForceUnmatchedLevels ? _OptionBoxOn : _OptionBoxOff, new Point(72, 337), () =>
                {
                    HandleOptionChange(() => { BruteForceUnmatchedLevels = !BruteForceUnmatchedLevels; });
                }
                );
            AddElement(_OptionGlyph, UpdateRecords ? _OptionBoxOn : _OptionBoxOff, new Point(72, 377), () =>
                {
                    HandleOptionChange(() => { UpdateRecords = !UpdateRecords; });
                }
                );

            AddElement("Begin check", new Point(60, 437), Color.Green, Color.White, -1, -1, BeginCheck);
        }

        protected override void GenerateElements()
        {
            AddElement("Mass Replay Check", new Point(24, 24), Color.White, -1, -1, null);

            if (!_RunningCheck)
                GenerateSettingsElements();

            AddElement("<< Back to Level Select", new Point(24, RenderConstants.DESIGN_HEIGHT - 24), Color.Red, Color.White, -1, 1, ReturnToLevelSelect);
        }

        private void BeginCheck()
        {
            _RunningCheck = true;
            SoundManager.PlaySound("OK");
            RegenerateElements();
        }

        private void HandleOptionChange(Action optionChange)
        {
            optionChange.Invoke();
            SoundManager.PlaySound("DING");
            RegenerateElements();
        }

        private void ReturnToLevelSelect()
        {
            SetNewView(() => new LevelSelectView(GraphicsDevice, CheckBasePath));
        }

        private void SelectReplayPath()
        {
            var openResult = Dialog.FileOpen(PathConstants.EXT_REPLAY.Replace(".", ""), ReplayBasePath);
            if (openResult.IsOk)
            {
                ReplayBasePath = Path.GetDirectoryName(openResult.Path) ?? ReplayBasePath;
                RegenerateElements();
            }
        }
    }
}
