﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Loap.Views.Menu
{
    class MenuElement
    {
        public Texture2D SourceTexture;
        public Rectangle SourceRegion;
        public Rectangle ScreenRegion;

        public Color NormalColor = Color.White;
        public Color HoverColor = Color.White;

        public Action? Action;

        public bool AllowRapidFire;

        public MenuElement(Texture2D sourceTexture, Rectangle sourceRegion, Rectangle screenRegion, Action? action, Color normalColor, Color hoverColor, bool allowRapidFire)
        {
            SourceTexture = sourceTexture;
            SourceRegion = sourceRegion;
            ScreenRegion = screenRegion;
            Action = action;
            NormalColor = normalColor;
            HoverColor = hoverColor;
            AllowRapidFire = allowRapidFire;
        }
    }
}
