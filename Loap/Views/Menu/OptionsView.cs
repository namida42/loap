﻿using Loap.Audio;
using Loap.Constants;
using Loap.Misc;
using Loap.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Loap.Views.Menu
{
    class OptionsView : BaseMenuView
    {
        private readonly Texture2D _OptionGlyph;
        private readonly Rectangle _OptionBoxOn;
        private readonly Rectangle _OptionBoxOff;

        private static readonly Color PAGE_HEADER_COLOR = Color.Yellow;
        private static readonly Color SECTION_HEADER_COLOR = Color.YellowGreen;

        public OptionsView(GraphicsDevice graphics, bool saveSettingsOnOpen = true) : base(graphics)
        {
            _OptionGlyph = Texture2D.FromFile(GraphicsDevice, PathConstants.OptionBoxFilePath);
            _OptionBoxOn = new Rectangle(0, 0, _OptionGlyph.Width, _OptionGlyph.Height / 2);
            _OptionBoxOff = new Rectangle(0, _OptionGlyph.Height / 2, _OptionGlyph.Width, _OptionGlyph.Height / 2);

            if (saveSettingsOnOpen)
            {
                LoapSettings.Save();
                Hotkeys.Save();
            }
        }

        protected override void GenerateElements()
        {
            AddElement("Loap Options", new Point(24, 24), PAGE_HEADER_COLOR, -1, -1, null);

            AddElement("Display Options", new Point(48, 72), SECTION_HEADER_COLOR, -1, -1, null);
            AddElement("Scroll Background", new Point(80, 112), Color.White, -1, -1, null);
            AddElement(_OptionGlyph,
                LoapSettings.BackgroundScroll ? _OptionBoxOn : _OptionBoxOff, new Point(480, 133),
                () => HandleOptionChange(() => LoapSettings.BackgroundScroll = !LoapSettings.BackgroundScroll));
            AddElement("Limit Frame Rate", new Point(80, 152), Color.White, -1, -1, null);
            AddElement(_OptionGlyph,
                LoapSettings.LimitFrameRate ? _OptionBoxOn : _OptionBoxOff, new Point(480, 173),
                () => HandleOptionChange(() => LoapSettings.LimitFrameRate = !LoapSettings.LimitFrameRate));
            AddElement("Display Frame Rate", new Point(80, 192), Color.White, -1, -1, null);
            AddElement(_OptionGlyph,
                LoapSettings.DisplayFPS ? _OptionBoxOn : _OptionBoxOff, new Point(480, 213),
                () => HandleOptionChange(() => LoapSettings.DisplayFPS = !LoapSettings.DisplayFPS));

            AddElement("Misc Options", new Point(520, 72), SECTION_HEADER_COLOR, -1, -1, null);
            AddElement("Pause On Rewind", new Point(552, 112), Color.White, -1, -1, null);
            AddElement(_OptionGlyph,
                LoapSettings.PauseOnRewind ? _OptionBoxOn : _OptionBoxOff, new Point(940, 133),
                () => HandleOptionChange(() => LoapSettings.PauseOnRewind = !LoapSettings.PauseOnRewind));
            AddElement("Auto-Save Replays", new Point(552, 152), Color.White, -1, -1, null);
            AddElement(_OptionGlyph,
                LoapSettings.AutosaveReplays ? _OptionBoxOn : _OptionBoxOff, new Point(940, 173),
                () => HandleOptionChange(() => LoapSettings.AutosaveReplays = !LoapSettings.AutosaveReplays));

            AddElement("Audio Volume", new Point(48, 252), SECTION_HEADER_COLOR, -1, -1, null);
            AddElement("Music", new Point(80, 292), Color.White, -1, -1, null);
            AddElement("Sound", new Point(80, 332), Color.White, -1, -1, null);
            for (int i = 0; i < 11; i++)
            {
                int newVolume = i * 10;
                AddElement(_OptionGlyph,
                    LoapSettings.MusicVolume >= newVolume ? _OptionBoxOn : _OptionBoxOff,
                    new Point(240 + (24 * i), 313),
                    () => HandleMusicVolumeChange(() => newVolume));
                AddElement(_OptionGlyph,
                    LoapSettings.SoundVolume >= newVolume ? _OptionBoxOn : _OptionBoxOff,
                    new Point(240 + (24 * i), 353),
                    () => HandleOptionChange(() => LoapSettings.SoundVolume = newVolume));
            }

            AddElement("Minimap Options", new Point(520, 212), SECTION_HEADER_COLOR, -1, -1, null);
            AddElement("Show Minimap", new Point(552, 252), Color.White, -1, -1, null);
            AddElement(_OptionGlyph,
                LoapSettings.HideMinimap ? _OptionBoxOff : _OptionBoxOn, new Point(940, 273),
                () => HandleOptionChange(() => LoapSettings.HideMinimap = !LoapSettings.HideMinimap));
            AddElement("Width", new Point(552, 292), Color.White, -1, -1, null);
            AddElement("Height", new Point(552, 332), Color.White, -1, -1, null);
            int minimapSizeCap = Math.Min(LoapSettings.WindowWidth / 2, LoapSettings.WindowHeight / 2);
            for (int i = 0; i < 10; i++)
            {
                int newWidth = 90 + ((minimapSizeCap - 90) * i / 9);
                int newHeight = 90 + ((minimapSizeCap - 90) * i / 9);

                AddElement(_OptionGlyph,
                    LoapSettings.MinimapWidth >= newWidth ? _OptionBoxOn : _OptionBoxOff,
                    new Point(724 + (24 * i), 313),
                    () => HandleOptionChange(() => LoapSettings.MinimapWidth = newWidth));
                AddElement(_OptionGlyph,
                    LoapSettings.MinimapHeight >= newHeight ? _OptionBoxOn : _OptionBoxOff,
                    new Point(724 + (24 * i), 353),
                    () => HandleOptionChange(() => LoapSettings.MinimapHeight = newHeight));
            }
            AddElement("Resolution", new Point(552, 372), Color.White, -1, -1, null);
            int minimapResCap = (Math.Min(LoapSettings.MinimapWidth, LoapSettings.MinimapHeight) / 16) - 1;
            for (int i = 0; i < 8; i++)
            {
                int newRes = ((minimapResCap * i) / 7) + 1;
                AddElement(_OptionGlyph,
                    LoapSettings.MinimapBlockResolution >= newRes ? _OptionBoxOn : _OptionBoxOff,
                    new Point(772 + (24 * i), 393),
                    () => HandleOptionChange(() => LoapSettings.MinimapBlockResolution = newRes));
            }

            AddElement("Mouse Options", new Point(48, 392), SECTION_HEADER_COLOR, -1, -1, null);
            AddElement("Invert Panning", new Point(80, 432), Color.White, -1, -1, null);
            AddElement("Horz", new Point(424, 432), Color.White, -1, -1, null);
            AddElement("Vert", new Point(568, 432), Color.White, -1, -1, null);
            AddElement("Zoom", new Point(712, 432), Color.White, -1, -1, null);
            AddElement(_OptionGlyph,
                LoapSettings.InvertMousePanX ? _OptionBoxOn : _OptionBoxOff,
                new Point(536, 453),
                () => HandleOptionChange(() => LoapSettings.InvertMousePanX = !LoapSettings.InvertMousePanX));
            AddElement(_OptionGlyph,
                LoapSettings.InvertMousePanZ ? _OptionBoxOn : _OptionBoxOff,
                new Point(680, 453),
                () => HandleOptionChange(() => LoapSettings.InvertMousePanZ = !LoapSettings.InvertMousePanZ));
            AddElement(_OptionGlyph,
                LoapSettings.InvertMousePanY ? _OptionBoxOn : _OptionBoxOff,
                new Point(824, 453),
                () => HandleOptionChange(() => LoapSettings.InvertMousePanY = !LoapSettings.InvertMousePanY));
            AddElement("Invert Rotation", new Point(80, 472), Color.White, -1, -1, null);
            AddElement("Horz", new Point(424, 472), Color.White, -1, -1, null);
            AddElement("Vert", new Point(568, 472), Color.White, -1, -1, null);
            AddElement(_OptionGlyph,
                LoapSettings.InvertMouseRotate ? _OptionBoxOn : _OptionBoxOff,
                new Point(536, 493),
                () => HandleOptionChange(() => LoapSettings.InvertMouseRotate = !LoapSettings.InvertMouseRotate));
            AddElement(_OptionGlyph,
                LoapSettings.InvertMouseTilt ? _OptionBoxOn : _OptionBoxOff,
                new Point(680, 493),
                () => HandleOptionChange(() => LoapSettings.InvertMouseTilt = !LoapSettings.InvertMouseTilt));

            AddElement("Lock When Rotating", new Point(80, 512), Color.White, -1, -1, null);
            AddElement(_OptionGlyph,
                LoapSettings.DisableMouseLock ? _OptionBoxOff : _OptionBoxOn,
                new Point(488, 533),
                () => HandleOptionChange(() => LoapSettings.DisableMouseLock = !LoapSettings.DisableMouseLock));
            AddElement("Confine To Window", new Point(80, 552), Color.White, -1, -1, null);
            AddElement(_OptionGlyph,
                LoapSettings.ConfineCursor ? _OptionBoxOn : _OptionBoxOff,
                new Point(488, 573),
                () => HandleOptionChange(() => LoapSettings.ConfineCursor = !LoapSettings.ConfineCursor));

            AddElement("Edit Hotkeys", new Point((RenderConstants.DESIGN_WIDTH / 2), RenderConstants.DESIGN_HEIGHT - 24 - (Font.CharacterHeight * 3 / 2)),
                Color.Yellow, Color.White, 0, 1, OpenHotkeyMenu);

            AddElement("Confirm", new Point((RenderConstants.DESIGN_WIDTH / 2) - 96, RenderConstants.DESIGN_HEIGHT - 24), Color.Green, Color.White, 0, 1, SaveAndExit);
            AddElement("Cancel", new Point((RenderConstants.DESIGN_WIDTH / 2) + 96, RenderConstants.DESIGN_HEIGHT - 24), Color.Red, Color.White, 0, 1, RevertAndExit);
        }

        private void HandleMusicVolumeChange(Func<int> getNewVolume)
        {
            HandleOptionChange(() => LoapSettings.MusicVolume = getNewVolume.Invoke());
            MusicManager.ResetMusicVolume();
        }

        private void HandleOptionChange(Action action)
        {
            action.Invoke();
            SoundManager.PlaySound("DING");
            RegenerateElements();
        }

        private void SaveAndExit()
        {
            LoapSettings.Save();
            Hotkeys.Save();
            SetNewView(() => new TitleView(GraphicsDevice));
        }

        private void RevertAndExit()
        {
            LoapSettings.Load();
            Hotkeys.Load();
            SetNewView(() => new TitleView(GraphicsDevice));
        }

        private void OpenHotkeyMenu()
        {
            SetNewView(() => new HotkeyMenuView(GraphicsDevice));
        }
    }
}
