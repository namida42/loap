﻿using Loap.Constants;
using Loap.Misc;
using Loap.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Loap.Views.Menu
{
    abstract class BaseMenuView : BaseView
    {
        private const float BACKGROUND_SCROLL_SPEED = 0.5f;

        private readonly RenderTarget2D _DesignCanvas;

        private readonly List<MenuElement> _Elements = new List<MenuElement>();
        private bool _GeneratedElements;

        public float _BackgroundScroll;

        private readonly Texture2D _Background;
        private int _BackgroundRenderHeight;

        private Rectangle _CanvasOutputRect;

        private MenuElement? _MouseOverElement = null;

        protected bool SuspendInput;

        public BaseMenuView(GraphicsDevice graphics) : base(graphics)
        {
            _DesignCanvas = CreateRenderTarget(RenderConstants.DESIGN_WIDTH, RenderConstants.DESIGN_HEIGHT);
            CalculateOutputRect();
            _Background = LoadBackground();
        }

        protected abstract void GenerateElements();

        protected void RegenerateElements()
        {
            _GeneratedElements = false;
        }

        private void CalculateOutputRect()
        {
            float scale = Math.Min(Canvas.Width / (float)RenderConstants.DESIGN_WIDTH, Canvas.Height / (float)RenderConstants.DESIGN_HEIGHT);
            _CanvasOutputRect.Width = (int)Math.Round(RenderConstants.DESIGN_WIDTH * scale);
            _CanvasOutputRect.Height = (int)Math.Round(RenderConstants.DESIGN_HEIGHT * scale);
            _CanvasOutputRect.X = (Canvas.Width - _CanvasOutputRect.Width) / 2;
            _CanvasOutputRect.Y = (Canvas.Height - _CanvasOutputRect.Height) / 2;
        }

        private Texture2D LoadBackground()
        {
            var result = Texture2D.FromFile(GraphicsDevice, PathConstants.MenuBackgroundFilePath);
            _BackgroundRenderHeight = (int)Math.Round((float)Canvas.Width / result.Width * result.Height);
            return result;
        }

        protected void AddElement(Texture2D texture, Rectangle srcRegion, Point dstCenter, Action? action, Color? normalColor = null, Color? hoverColor = null, bool allowRapidFire = false)
        {
            Rectangle dstRect = new Rectangle();
            dstRect.X = dstCenter.X - (srcRegion.Width / 2);
            dstRect.Y = dstCenter.Y - (srcRegion.Height / 2);
            dstRect.Width = srcRegion.Width;
            dstRect.Height = srcRegion.Height;

            var internalNormalColor = normalColor.HasValue ? normalColor.Value : Color.White;
            var internalHoverColor = hoverColor.HasValue ? hoverColor.Value : Color.White;

            _Elements.Add(new MenuElement(texture, srcRegion, dstRect, action, internalNormalColor, internalHoverColor, allowRapidFire));
        }

        protected void AddElement(string text, Point dstPosition, Color color, int horzAlign, int vertAlign, Action? action)
        {
            AddElement(text, dstPosition, color, color, horzAlign, vertAlign, action);
        }

        protected void AddElement(string text, Point dstPosition, Color normalColor, Color hoverColor, int horzAlign, int vertAlign, Action? action, bool allowRapidFire = false)
        {
            var bounds = Font.CalculateBounds(text, new Point(0, 0));
            RenderTarget2D textCanvas = CreateRenderTarget(Math.Max(bounds.Width, 1), Math.Max(bounds.Height, 1));
            int xInCanvas = horzAlign < 0 ? 0 :
                horzAlign > 0 ? bounds.Width - 1 :
                bounds.Width / 2;

            GraphicsDevice.SetRenderTarget(textCanvas);
            GraphicsDevice.Clear(Color.Transparent);

            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp);
            Font.WriteText(SpriteBatch, text, new Point(xInCanvas, 0), Color.White, horzAlign, -1);
            SpriteBatch.End();

            switch (Math.Sign(horzAlign))
            {
                case -1:
                    dstPosition.X += bounds.Width / 2;
                    break;
                case 1:
                    dstPosition.X -= bounds.Width / 2;
                    break;
            }

            switch (Math.Sign(vertAlign))
            {
                case -1:
                    dstPosition.Y += bounds.Height / 2;
                    break;
                case 1:
                    dstPosition.Y -= bounds.Height / 2;
                    break;
            }

            AddElement(textCanvas, textCanvas.Bounds, dstPosition, action, normalColor, hoverColor, allowRapidFire);
        }

        protected override void DoUpdate(float elapsedTime)
        {
            if (!_GeneratedElements)
            {
                _Elements.Clear();
                GenerateElements();
                _GeneratedElements = true;
            }

            if (LoapSettings.BackgroundScroll)
                _BackgroundScroll = (_BackgroundScroll + (elapsedTime * BACKGROUND_SCROLL_SPEED * _BackgroundRenderHeight)) % _BackgroundRenderHeight;

            if (!SuspendInput)
            {
                Point mousePos = UserInput.MousePosition;
                Point internalMousePos = new Point(
                    (mousePos.X - _CanvasOutputRect.X) * RenderConstants.DESIGN_WIDTH / _CanvasOutputRect.Width,
                    (mousePos.Y - _CanvasOutputRect.Y) * RenderConstants.DESIGN_HEIGHT / _CanvasOutputRect.Height
                    );

                _MouseOverElement = null;
                foreach (var element in _Elements)
                    if (element.ScreenRegion.Contains(internalMousePos))
                        _MouseOverElement = element;

                if (UserInput.IsMouseButtonPressed(MouseButton.Left))
                {
                    if (_MouseOverElement != null)
                        _MouseOverElement.Action?.Invoke();
                }
                else if (UserInput.IsMouseButtonHeld(MouseButton.Left))
                    if (_MouseOverElement != null)
                        if (_MouseOverElement.AllowRapidFire)
                            _MouseOverElement.Action?.Invoke();
            }
        }

        protected sealed override void DoRender()
        {
            GraphicsDevice.SetRenderTarget(_DesignCanvas);
            GraphicsDevice.Clear(Color.Transparent);
            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            foreach (var element in _Elements)
                SpriteBatch.Draw(element.SourceTexture, element.ScreenRegion, element.SourceRegion, element == _MouseOverElement ? element.HoverColor : element.NormalColor);
            MenuCustomRenderToDesign();
            SpriteBatch.End();

            GraphicsDevice.SetRenderTarget(Canvas);
            GraphicsDevice.Clear(Color.Black);
            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            for (int y = (int)-_BackgroundScroll; y < Canvas.Height; y += _BackgroundRenderHeight)
                SpriteBatch.Draw(_Background, new Rectangle(0, y, Canvas.Width, _BackgroundRenderHeight), Color.White);
            SpriteBatch.Draw(_DesignCanvas, _CanvasOutputRect, Color.White);
            MenuCustomRenderToCanvas();
            Cursor.DrawCursor(SpriteBatch, UserInput.MousePosition, CursorGraphic.Standard);
            SpriteBatch.End();
        }

        protected virtual void MenuCustomRenderToDesign()
        {
            // Intentionally blank.
        }

        protected virtual void MenuCustomRenderToCanvas()
        {
            // Intentionally blank.
        }
    }
}
