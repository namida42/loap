﻿using Loap.Audio;
using Loap.Constants;
using Loap.Misc;
using Loap.Packs;
using Loap.Progress;
using Loap.Rendering;
using Loap.UI;
using Loap.Views.Menu;
using LoapCore.Constants;
using LoapCore.Entities;
using LoapCore.Files;
using LoapCore.Levels;
using LoapCore.Misc;
using LoapCore.Replays;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NativeFileDialogSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Loap.Views.Game
{
    class PreLevelView : BaseView
    {
        private readonly Renderer _Renderer;

        private readonly Level _Level;
        private readonly RenderTarget2D _InfoTarget;

        private readonly string _FullPreviewText;
        private bool _SkipToFullText;

        private readonly Rectangle _CanvasOutputRect;

        private readonly PreviewCameraInfo _PreviewCamera;
        private readonly Camera _Camera = new Camera();

        private readonly BlockCacheEntry[] _BlockCache;
        private readonly BlockExclusions _BlockExclusions;
        private readonly List<EntityInstance> _Entities = new List<EntityInstance>();

        private readonly List<HatchFlap> _HatchFlaps;

        private Replay? _Replay;

        private RenderTarget2D _LevelRenderTarget;

        public PreLevelView(GraphicsDevice graphics, string levelFile, Replay? replay = null) :
            this(graphics, LevelFileLoader.LoadLevel(levelFile), replay)
        {
        }

        public PreLevelView(GraphicsDevice graphics, Level level, Replay? replay = null) : base(graphics)
        {
            CalculateOutputRect(ref _CanvasOutputRect);

            _InfoTarget = CreateRenderTarget(RenderConstants.DESIGN_WIDTH, RenderConstants.DESIGN_HEIGHT);

            _Level = level;
            _PreviewCamera = _Level.LevelInfo.PreviewCamera;

            _Entities.AddRange(EntityInstance.CreateInstances(_Level.Entities, MetaEntityLoader.LoadLevelMetaEntities(_Level)));

            _Renderer = new Renderer(GraphicsDevice);
            _Renderer.Camera = _Camera;
            _Renderer.LoadTextures(_Level);
            _Renderer.GenerateBlockCache(_Level.BlockStructure, _Level.Overlays, out _BlockCache, out _BlockExclusions);
            if (_Renderer.ScreenTexture != null)
                ScreenTexture = CreateRenderTarget();

            _HatchFlaps = _Renderer.PrepareHatchFlaps(_Level.BlockStructure);

            _FullPreviewText = GeneratePreviewText();

            _Replay = replay;

            MusicManager.PlayMusic(Path.Combine(PathConstants.MusicPath, _Level.LevelInfo.Music + PathConstants.EXT_AUDIO));

            if (LoapSettings.RenderWidth > 0)
            {
                _LevelRenderTarget = CreateRenderTarget(
                    LoapSettings.RenderWidth,
                    LoapSettings.RenderWidth * GraphicsDevice.Viewport.Height / GraphicsDevice.Viewport.Width
                    );
            }
            else
                _LevelRenderTarget = CreateRenderTarget();
        }

        private void CalculateOutputRect(ref Rectangle target)
        {
            float scale = Math.Min(Canvas.Width / (float)RenderConstants.DESIGN_WIDTH, Canvas.Height / (float)RenderConstants.DESIGN_HEIGHT);
            target.Width = (int)Math.Round(RenderConstants.DESIGN_WIDTH * scale);
            target.Height = (int)Math.Round(RenderConstants.DESIGN_HEIGHT * scale);
            target.X = (Canvas.Width - target.Width) / 2;
            target.Y = (Canvas.Height - target.Height) / 2;
        }

        protected override void DoUpdate(float elapsedTime)
        {
            SetCameraPosition();

            if (UserInput.IsMouseButtonPressed(MouseButton.Left) || UserInput.IsRawKeyPressed(Keys.Enter))
                SetNewView(() => new GameView(GraphicsDevice, _Level, _Replay));

            if (UserInput.IsMouseButtonPressed(MouseButton.Right) || UserInput.IsRawKeyPressed(Keys.Escape))
                SetNewView(() => new LevelSelectView(GraphicsDevice));

            if (UserInput.IsMouseButtonPressed(MouseButton.Middle) || UserInput.IsRawKeyPressed(Keys.Space))
                _SkipToFullText = true;

            foreach (var entity in _Entities)
                if (entity.Effect == EntityEffect.None)
                    entity.Iteration = (int)(ViewRunTime * 15);

            if (UserInput.IsKeyNewlyReleased(HotkeyFunction.LoadReplay))
            {
                string replayPath;
                if (LoapProgress.CurrentLevel == null)
                    replayPath = Path.Combine(
                        PathConstants.ReplayPath
                        );
                else
                    replayPath = Path.Combine(
                        PathConstants.ReplayPath,
                        LoapProgress.CurrentLevel.Parent.FilePath
                        );

                Directory.CreateDirectory(replayPath);
                var openResult = Dialog.FileOpen(PathConstants.EXT_REPLAY.Replace(".", ""), replayPath);
                if (openResult.IsOk)
                {
                    _Replay = new Replay();
                    var dataItem = DataItemReader.LoadFromFile(openResult.Path);
                    ReplayFile.LoadReplayFromDataItem(dataItem, _Replay);
                }
            }
        }

        protected override void DoRender()
        {
            GraphicsDevice.SetRenderTarget(_InfoTarget);
            GraphicsDevice.Clear(Color.Transparent);

            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp);
            Font.WriteText(SpriteBatch, GetPreviewText(), new Point(12, 12), Color.White);

            if (_Replay != null)
                Font.WriteText(SpriteBatch, GameTexts.REPLAY_LOADED, new Point(12, RenderConstants.DESIGN_HEIGHT - 12 - 6 - Font.CharacterHeight), Color.White, -1, 1);

            Font.WriteText(SpriteBatch, GameTexts.LEFT_MOUSE_PLAY, new Point(12, RenderConstants.DESIGN_HEIGHT - 12), Color.White, -1, 1);
            Font.WriteText(SpriteBatch, GameTexts.RIGHT_MOUSE_EXIT, new Point(RenderConstants.DESIGN_WIDTH - 12, RenderConstants.DESIGN_HEIGHT - 12), Color.White, 1, 1);
            SpriteBatch.End();

            if (ScreenTexture != null)
                _Renderer.UpdateScreenTexture(ScreenTexture);

            RenderLevel();

            GraphicsDevice.SetRenderTarget(Canvas);
            GraphicsDevice.Clear(Color.DarkMagenta);

            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp);
            SpriteBatch.Draw(_LevelRenderTarget, GraphicsDevice.Viewport.Bounds, Color.White);
            SpriteBatch.End();

            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            SpriteBatch.Draw(_InfoTarget, _CanvasOutputRect, Color.White);
            Cursor.DrawCursor(SpriteBatch, UserInput.MousePosition, CursorGraphic.Standard);
            SpriteBatch.End();
        }

        private void RenderLevel()
        {
            _Renderer.PrepareSeaFrame((int)(ViewRunTime * 30));

            GraphicsDevice.SetRenderTarget(_LevelRenderTarget);

            GraphicsDevice.SamplerStates[0] = SamplerState.PointWrap;
            _Renderer.RenderSea(_Level.LevelInfo.Sea, (int)(ViewRunTime * 30));
            _Renderer.RenderSky();
            _Renderer.RenderLands(_Level.Lands);

            GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            List<RenderEntity> queue = new List<RenderEntity>();
            RenderEntity.ReleaseEntities();

            _Renderer.AddBlocksToEntityQueue(queue, _BlockCache, _BlockExclusions, (int)(ViewRunTime * 30), null, null);
            _Renderer.AddLevelEntitiesToEntityQueue(queue, _Entities, true);
            _Renderer.RenderEntityQueue(queue, true);

            queue.Clear();
            RenderEntity.ReleaseEntities();

            _Renderer.AddLevelEntitiesToEntityQueue(queue, _Entities, false);
            _Renderer.AddHatchFlapsToEntityQueue(queue, _HatchFlaps, RenderConstants.HATCH_OPEN_ITERATIONS);

            _Renderer.RenderEntityQueue(queue, false);
        }

        private string GeneratePreviewText()
        {
            // DEBUG: This really shouldn't be so hardcoded.

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(_Level.LevelInfo.Title);

            if (LoapProgress.CurrentLevel != null && LoapProgress.CurrentLevel.Parent != LoapPackCache.BasePack)
            {
                var parentPack = (LoapPackCachePackEntry)LoapProgress.CurrentLevel.Parent;
                builder.AppendLine(parentPack.Title + " " + (parentPack.Children.Where(c => c is LoapPackCacheLevelEntry).ToList().IndexOf(LoapProgress.CurrentLevel) + 1).ToString());
            }
            else
                builder.AppendLine();

            builder.AppendLine();
            builder.AppendLine("Lemmings  " + _Level.LevelInfo.LemmingCount.ToString());
            builder.AppendLine("To Save  " + _Level.LevelInfo.SaveRequirement.ToString());
            if (_Level.LevelInfo.TimeLimit > 0)
            {
                int limit = _Level.LevelInfo.TimeLimit;
                builder.AppendLine("Time Limit  " + (limit / 60).ToString() + ":" + (limit % 60).ToString("D2"));
            }
            else
                builder.AppendLine();

            if (LoapProgress.CurrentLevel != null)
            {
                var progress = LoapProgress.GetLevelProgress(LoapProgress.CurrentLevel);
                if (progress.Completion >= LevelCompletion.Attempted)
                {
                    builder.AppendLine();
                    builder.AppendLine("Records");
                    builder.AppendLine("  Lemmings Saved: " + progress.SaveRecord.ToString());

                    if (progress.Completion == LevelCompletion.Completed)
                    {
                        int frames = progress.TimeRecord % PhysicsConstants.ITERATIONS_PER_INGAME_SECOND;
                        int seconds = progress.TimeRecord / PhysicsConstants.ITERATIONS_PER_INGAME_SECOND;
                        int minutes = seconds / 60;
                        seconds %= 60;
                        string timeText = minutes.ToString("D2") + ":" +
                            seconds.ToString("D2") + "." +
                            ((int)Math.Round(frames * 100f / PhysicsConstants.ITERATIONS_PER_INGAME_SECOND)).ToString();

                        builder.AppendLine("  Fastest Time: " + timeText);
                        builder.AppendLine();
                        builder.AppendLine("  Fewest Skills:");

                        var skillTypes = (Skill[])Enum.GetValues(typeof(Skill));
                        string lineText = "";
                        for (int i = 0; i < skillTypes.Length; i++)
                        {
                            int skillRecord = progress.SkillRecord[skillTypes[i]];
                            lineText += skillRecord.ToString() + " " + skillTypes[i].ToString() + (skillRecord == 1 ? "" : "s");
                            if (i % 3 == 2 || i == skillTypes.Length - 1)
                            {
                                builder.AppendLine("    " + lineText);
                                lineText = "";
                            }
                            else
                                lineText += " -- ";
                        }

                        lineText = ""; // It always should be this anyway at this point, but just in case.

                        lineText += progress.TotalSkillsRecord.ToString() + " Total Skill" + (progress.TotalSkillsRecord == 1 ? "" : "s");
                        lineText += " -- ";
                        lineText += progress.SkillTypesRecord.ToString() + " Skill Type" + (progress.SkillTypesRecord == 1 ? "" : "s");

                        builder.AppendLine("    " + lineText);
                    }
                }
            }

            return builder.ToString();
        }

        private string GetPreviewText()
        {
            if (_SkipToFullText)
                return _FullPreviewText;

            int chars = (int)Math.Floor(ViewRunTime * 30);
            if (chars > _FullPreviewText.Length)
                return _FullPreviewText;
            else
                return _FullPreviewText[..chars];
        }

        private void SetCameraPosition()
        {
            switch (_PreviewCamera.Style)
            {
                case PreviewCameraStyle.Rotate:
                    _Camera.Position = new Vector3(_PreviewCamera.X, _PreviewCamera.Y, _PreviewCamera.Z);
                    _Camera.Rotation = _PreviewCamera.Rotation + (60f * ViewRunTime);
                    _Camera.Pitch = _PreviewCamera.Pitch;
                    break;
                case PreviewCameraStyle.Fixed:
                    _Camera.Position = new Vector3(_PreviewCamera.X, _PreviewCamera.Y, _PreviewCamera.Z);
                    _Camera.Rotation = _PreviewCamera.Rotation;
                    _Camera.Pitch = _PreviewCamera.Pitch;
                    break;
                default: // including PreviewCameraStyle.Pivot
                    _Camera.Rotation = _PreviewCamera.Rotation - (60f * ViewRunTime);
                    _Camera.Position = new Vector3(_PreviewCamera.X, _PreviewCamera.Y, _PreviewCamera.Z) -
                        Vector3.Transform(new Vector3(0, _PreviewCamera.Distance, 0), Matrix.CreateRotationZ(MathHelper.ToRadians(_Camera.Rotation)));
                    _Camera.Pitch = _PreviewCamera.Pitch;
                    break;
            }
        }
    }
}
