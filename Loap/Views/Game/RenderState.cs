﻿using Loap.Rendering;
using LoapCore.Misc;
using System.Collections.Generic;

namespace Loap.Views.Game
{
    class RenderState
    {
        public readonly List<LemmingParticle> Particles = new List<LemmingParticle>();
        public BlockExclusions BlockExclusions = new BlockExclusions(0, 0, 0);
        public readonly Dictionary<Point3D, int> DamageIterations = new Dictionary<Point3D, int>();

        public void CloneFrom(RenderState source)
        {
            Particles.Clear();
            foreach (var particle in source.Particles)
                Particles.Add(particle.Clone());

            BlockExclusions = source.BlockExclusions.Clone();

            DamageIterations.Clear();
            foreach (var pair in source.DamageIterations)
                DamageIterations.Add(pair.Key, pair.Value);
        }

        public RenderState Clone()
        {
            var result = new RenderState();
            result.CloneFrom(this);
            return result;
        }
    }
}
