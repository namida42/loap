﻿using Loap.Constants;
using LoapCore.Constants;
using LoapCore.Misc;
using Microsoft.Xna.Framework;
using System;

namespace Loap.Views.Game
{
    class LemmingParticle
    {
        private static Random ParticleRNG = new Random();

        public int Iterations;
        public Vector3 Position;
        public Vector3 Momentum;
        public float Scale;
        public int Direction;

        private int _DecayDelayFrames = UIConstants.PARTICLE_DECAY_DELAY_FRAMES;
        private float _InitialZ;
        private int _RotateDirection;

        public LemmingParticle(Vector3 position, float initialScale = UIConstants.PARTICLE_INITIAL_SCALE_DEFAULT)
        {
            Position = position;
            Scale = initialScale;
            _InitialZ = position.Z;

            Momentum = new Vector3(ParticleRNG.Next(1, 40), ParticleRNG.Next(1, 40), ParticleRNG.Next(30, 120));
            if (ParticleRNG.Next(2) == 0) Momentum.X = -Momentum.X;
            if (ParticleRNG.Next(2) == 0) Momentum.Y = -Momentum.Y;
            Momentum /= 10;
            Momentum /= PhysicsConstants.BLOCK_STEPS;

            Direction = ParticleRNG.Next(8);
            _RotateDirection = ParticleRNG.Next(2) == 0 ? 1 : -1;
        }

        private LemmingParticle()
        {
        }

        public static LemmingParticle[] CreateParticles(int particleCount, Vector3 position, float initialScale = UIConstants.PARTICLE_INITIAL_SCALE_DEFAULT)
        {
            var result = new LemmingParticle[particleCount];
            for (int i = 0; i < particleCount; i++)
            {
                result[i] = new LemmingParticle(position, initialScale);
                result[i].Update();
            }
            return result;
        }

        public LemmingParticle Clone()
        {
            var result = new LemmingParticle();

            result.Iterations = this.Iterations;
            result.Position = this.Position;
            result.Momentum = this.Momentum;
            result.Scale = this.Scale;
            result.Direction = this.Direction;

            result._DecayDelayFrames = this._DecayDelayFrames;
            result._InitialZ = this._InitialZ;
            result._RotateDirection = this._RotateDirection;

            return result;
        }

        public void Update()
        {
            if (Iterations > 0 && Iterations % UIConstants.PARTICLE_VERTICAL_MOMENTUM_PERIOD == 0)
                Momentum.Z -= 1f / PhysicsConstants.BLOCK_STEPS;

            Position += Momentum;

            if (Position.Z < _InitialZ)
            {
                if (_DecayDelayFrames > 0)
                    _DecayDelayFrames--;
                else
                    Scale = Math.Max(Scale - UIConstants.PARTICLE_SIZE_DECAY_RATE, 0);
            }

            Direction = Utils.ModuloNegativeAdjusted(Direction + _RotateDirection, 8);

            Iterations++;
        }
    }
}
