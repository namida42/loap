﻿using LoapCore.Game;

namespace Loap.Views.Game
{
    class SavedState
    {
        public readonly GameState GameState;
        public readonly RenderState RenderState;

        public int Iteration => GameState.Iteration; // For quick access

        /// <summary>
        /// Pass the actual, original instance of GameState / RenderState. The constructor takes care of creating clones instead of
        /// using the originals.
        /// </summary>
        public SavedState(GameState gameState, RenderState renderState)
        {
            GameState = gameState.Clone();
            RenderState = renderState.Clone();
        }

        public void Restore(GameState gameStateDst, RenderState renderStateDst)
        {
            gameStateDst.CloneFrom(GameState);
            renderStateDst.CloneFrom(RenderState);
        }
    }
}
