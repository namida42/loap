﻿using Loap.Constants;
using Loap.Misc;
using LoapCore.Constants;
using LoapCore.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Linq;

namespace Loap.Views.Game
{
    class GameViewMinimap : GameViewComponent
    {
        private const int BORDER_THICKNESS = 12;

        // These constants must all be negative!
        private const int MINIMAP_SEA = -1;
        private const int MINIMAP_UNSET = -2;
        private const int MINIMAP_ENTRANCE = -3;
        private const int MINIMAP_EXIT = -4;

        public readonly RenderTarget2D MinimapView;

        private Color _SeaColor = new Color(0, 0, 0, 255);
        private readonly int[,] _HeightData;
        private readonly RenderTarget2D _BaseMap; // Excludes lemmings!
        private readonly RenderTarget2D _MapWithLemmings; // This one includes them.
        private readonly RenderTarget2D _PlainPixel;
        private readonly int _BlockResolution;
        private readonly int _BlockFragmentSize;
        private readonly int _LemmingSize;
        private readonly SpriteBatch _SpriteBatch;

        private readonly Texture2D BorderTexture;
        private readonly Texture2D ToggleButtonTexture;

        public readonly Rectangle ToggleButtonRect;
        public readonly Rectangle MinimapRect;

        public bool Visible => !Level.LevelInfo.DisableMinimap && !LoapSettings.HideMinimap;

        private int _HighestZ = -1;

        public GameViewMinimap(GameView gameView, GraphicsDevice graphicsDevice) : base(gameView, graphicsDevice)
        {
            BorderTexture = Texture2D.FromFile(GraphicsDevice, PathConstants.MinimapBorderFilePath);
            ToggleButtonTexture = Texture2D.FromFile(GraphicsDevice, PathConstants.MinimapToggleFilePath);

            ToggleButtonRect = ToggleButtonTexture.Bounds;
            MinimapRect = new Rectangle(0, 0, LoapSettings.MinimapWidth, LoapSettings.MinimapHeight);

            MinimapView = GameView.MakeChildRenderTarget(LoapSettings.MinimapWidth, LoapSettings.MinimapHeight);

            _BlockResolution = LoapSettings.MinimapBlockResolution;
            _BlockFragmentSize = PhysicsConstants.BLOCK_STEPS / _BlockResolution;
            _LemmingSize = Math.Max((int)Math.Round(_BlockResolution / 4f), 1);

            _HeightData = new int[_BlockResolution * Blocks.Width, _BlockResolution * Blocks.Depth];

            _BaseMap = GameView.MakeChildRenderTarget(_BlockResolution * Blocks.Width, _BlockResolution * Blocks.Depth, true);
            _MapWithLemmings = GameView.MakeChildRenderTarget(_BaseMap.Width, _BaseMap.Height);
            _PlainPixel = GameView.MakeChildRenderTarget(1, 1);

            _SpriteBatch = new SpriteBatch(GraphicsDevice);

            for (int y = 0; y < _HeightData.GetLength(1); y++)
                for (int x = 0; x < _HeightData.GetLength(0); x++)
                    _HeightData[x, y] = MINIMAP_UNSET;

            InitializeRenderTargets();

            SetSeaColor();

            FindHighestZ();

            RefreshBaseMap();
        }

        private void FindHighestZ()
        {
            for (int z = Blocks.Height - 1; z > 1; z--)
                for (int y = 0; y < Blocks.Depth; y++)
                    for (int x = 0; x < Blocks.Width; x++)
                    {
                        if (Blocks[x, y, z] != null)
                        {
                            _HighestZ = z;
                            return;
                        }

                    }

            _HighestZ = 1;
        }

        private void InitializeRenderTargets()
        {
            GraphicsDevice.SetRenderTarget(_BaseMap);
            GraphicsDevice.Clear(Color.Transparent);

            GraphicsDevice.SetRenderTarget(_PlainPixel);
            GraphicsDevice.Clear(Color.White);

            GraphicsDevice.SetRenderTarget(null);
        }

        public void RefreshBaseMap()
        {
            RefreshBaseMap(0, 0, Blocks.Width, Blocks.Depth);
        }

        public void RefreshBaseMap(int x, int y)
        {
            RefreshBaseMap(x, y, 1, 1);
        }

        public void RefreshBaseMap(int xStart, int yStart, int width, int depth)
        {
            int topZ = Blocks.Height;

            GraphicsDevice.SetRenderTarget(_BaseMap);

            _SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp);
            for (int y = yStart; y < yStart + depth; y++)
                for (int x = xStart; x < xStart + width; x++)
                    for (int sY = 0; sY < _BlockResolution; sY++)
                        for (int sX = 0; sX < _BlockResolution; sX++)
                        {
                            int targetX = (x * _BlockResolution) + sX;
                            int targetY = _BaseMap.Height - ((y * _BlockResolution) + sY) - 1;

                            int oldHeightData = _HeightData[targetX, targetY];

                            if (oldHeightData != MINIMAP_ENTRANCE && oldHeightData != MINIMAP_EXIT)
                            {
                                int localX = (x * PhysicsConstants.BLOCK_STEPS) + (sX * _BlockFragmentSize) + (_BlockFragmentSize / 2);
                                int localY = (y * PhysicsConstants.BLOCK_STEPS) + (sY * _BlockFragmentSize) + (_BlockFragmentSize / 2);

                                var floorEffect = Execution.Engine.GetFloorEffectAt(localX, localY);
                                if (floorEffect == LevelFloorEffect.Solid || floorEffect == LevelFloorEffect.Slippery)
                                    _HeightData[targetX, targetY] = 0;
                                else
                                    _HeightData[targetX, targetY] = MINIMAP_SEA;

                                bool foundGround = false;

                                for (int z = topZ; z >= 0; z--)
                                {
                                    var thisBlock = Blocks[x, y, z];
                                    if (thisBlock != null && thisBlock.Physics == BlockPhysics.Entrance)
                                    {
                                        _HeightData[targetX, targetY] = MINIMAP_ENTRANCE;
                                        break;
                                    }
                                    else if (thisBlock != null && thisBlock.Faces.Values.Any(f => f.Physics == FacePhysics.Exit || f.Physics == FacePhysics.AnimateExit))
                                    {
                                        _HeightData[targetX, targetY] = MINIMAP_EXIT;
                                        break;
                                    }
                                    else if (!foundGround && Blocks.IsSolidAt(localX, localY, z * PhysicsConstants.SEGMENT_STEPS, false))
                                    {
                                        _HeightData[targetX, targetY] = z;
                                        foundGround = true;
                                    }
                                }
                            }

                            int newHeightData = _HeightData[targetX, targetY];

                            if (oldHeightData != newHeightData)
                                _SpriteBatch.Draw(_PlainPixel,
                                    new Rectangle(targetX, targetY, 1, 1),
                                    GetColorForHeight(newHeightData));

                        }
            _SpriteBatch.End();
        }

        private void SetSeaColor()
        {
            Texture2D srcTexture;
            using RenderTarget2D tempDstTexture = GameView.MakeChildRenderTarget(1, 1);
            if (Rendering.Renderer.FullHeightSkyGraphic)
                srcTexture = Rendering.Renderer.SkyTexture;
            else
                srcTexture = Rendering.Renderer.SeaTexture;

            GraphicsDevice.SetRenderTarget(tempDstTexture);
            _SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.LinearClamp);
            _SpriteBatch.Draw(srcTexture, new Rectangle(0, 0, 1, 1), Color.White);
            _SpriteBatch.End();

            var colors = new Color[1];
            tempDstTexture.GetData(colors);

            _SeaColor = colors[0];
        }

        public void RefreshMapView()
        {
            if (Visible)
            {
                GraphicsDevice.SetRenderTarget(_MapWithLemmings);
                GraphicsDevice.Clear(Color.Transparent);

                _SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
                _SpriteBatch.Draw(_BaseMap, _BaseMap.Bounds, Color.White);

                foreach (var lem in Execution.Engine.Lemmings)
                {
                    Point lemPosition = new Point(
                        (int)Math.Round(lem.X * PhysicsConstants.STEP_SIZE_FLOAT * _BlockResolution) - (_LemmingSize / 2),
                        _MapWithLemmings.Height - (int)Math.Round(lem.Y * PhysicsConstants.STEP_SIZE_FLOAT * _BlockResolution) - (_LemmingSize / 2)
                        );
                    _SpriteBatch.Draw(_PlainPixel, new Rectangle(lemPosition.X, lemPosition.Y, _LemmingSize, _LemmingSize), Color.White);
                }
                _SpriteBatch.End();
            }

            GraphicsDevice.SetRenderTarget(MinimapView);
            GraphicsDevice.Clear(Visible ? _SeaColor : Color.Transparent);

            if (Visible)
            {
                Vector2 origin = new Vector2(
                    Camera.Position.X * _BlockResolution,
                    _MapWithLemmings.Height - (Camera.Position.Y * _BlockResolution)
                    );

                _SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
                _SpriteBatch.Draw(_MapWithLemmings,
                    new Vector2(MinimapView.Width / 2f, MinimapView.Height / 2f),
                    null, Color.White,
                    MathHelper.ToRadians(Camera.Rotation), origin, 1, SpriteEffects.None, 1);

                _SpriteBatch.Draw(_PlainPixel, new Rectangle((MinimapView.Width - _LemmingSize) / 2, (MinimapView.Height - _LemmingSize) / 2, _LemmingSize, _LemmingSize), Color.Yellow);
                _SpriteBatch.End();
            }

            _SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            if (Visible)
            {
                for (int x = BORDER_THICKNESS; x < MinimapView.Width - BORDER_THICKNESS; x += (BorderTexture.Width - BORDER_THICKNESS * 2))
                {
                    _SpriteBatch.Draw(BorderTexture,
                        new Rectangle(x, 0, BorderTexture.Width - BORDER_THICKNESS * 2, BORDER_THICKNESS),
                        new Rectangle(BORDER_THICKNESS, 0, BorderTexture.Width - BORDER_THICKNESS * 2, BORDER_THICKNESS),
                        Color.White);
                    _SpriteBatch.Draw(BorderTexture,
                        new Rectangle(x, MinimapView.Height - BORDER_THICKNESS, BorderTexture.Width - BORDER_THICKNESS * 2, BORDER_THICKNESS),
                        new Rectangle(BORDER_THICKNESS, BorderTexture.Height - BORDER_THICKNESS, BorderTexture.Width - BORDER_THICKNESS * 2, BORDER_THICKNESS),
                        Color.White);
                }

                for (int y = BORDER_THICKNESS; y < MinimapView.Height - BORDER_THICKNESS; y += (BorderTexture.Height - BORDER_THICKNESS * 2))
                {
                    _SpriteBatch.Draw(BorderTexture,
                        new Rectangle(0, y, BORDER_THICKNESS, BorderTexture.Height - BORDER_THICKNESS * 2),
                        new Rectangle(0, BORDER_THICKNESS, BORDER_THICKNESS, BorderTexture.Height - BORDER_THICKNESS * 2),
                        Color.White);
                    _SpriteBatch.Draw(BorderTexture,
                        new Rectangle(MinimapView.Width - BORDER_THICKNESS, y, BORDER_THICKNESS, BorderTexture.Height - BORDER_THICKNESS * 2),
                        new Rectangle(BorderTexture.Width - BORDER_THICKNESS, BORDER_THICKNESS, BORDER_THICKNESS, BorderTexture.Height - BORDER_THICKNESS * 2),
                        Color.White);
                }

                _SpriteBatch.Draw(BorderTexture,
                    new Rectangle(0, 0, BORDER_THICKNESS, BORDER_THICKNESS),
                    new Rectangle(0, 0, BORDER_THICKNESS, BORDER_THICKNESS),
                    Color.White);
                _SpriteBatch.Draw(BorderTexture,
                        new Rectangle(MinimapView.Width - BORDER_THICKNESS, 0, BORDER_THICKNESS, BORDER_THICKNESS),
                        new Rectangle(BorderTexture.Width - BORDER_THICKNESS, 0, BORDER_THICKNESS, BORDER_THICKNESS),
                        Color.White);
                _SpriteBatch.Draw(BorderTexture,
                        new Rectangle(0, MinimapView.Height - BORDER_THICKNESS, BORDER_THICKNESS, BORDER_THICKNESS),
                        new Rectangle(0, BorderTexture.Height - BORDER_THICKNESS, BORDER_THICKNESS, BORDER_THICKNESS),
                        Color.White);
                _SpriteBatch.Draw(BorderTexture,
                        new Rectangle(MinimapView.Width - BORDER_THICKNESS, MinimapView.Height - BORDER_THICKNESS, BORDER_THICKNESS, BORDER_THICKNESS),
                        new Rectangle(BorderTexture.Width - BORDER_THICKNESS, BorderTexture.Height - BORDER_THICKNESS, BORDER_THICKNESS, BORDER_THICKNESS),
                        Color.White);
            }
            if (!Level.LevelInfo.DisableMinimap)
                _SpriteBatch.Draw(ToggleButtonTexture, new Rectangle(0, 0, ToggleButtonTexture.Width, ToggleButtonTexture.Height), Color.White);
            _SpriteBatch.End();
        }

        private const float HIGHEST_HUE = 120;
        private const float HIGHEST_SAT = 1f;
        private const float HIGHEST_VAL = 3f / 4f;
        private const float LOWEST_HUE = 40;
        private const float LOWEST_SAT = 1f;
        private const float LOWEST_VAL = 1f / 4f;

        private Color GetColorForHeight(int height)
        {
            if (height < 0)
            {
                return height switch
                {
                    MINIMAP_ENTRANCE => Color.Orange,
                    MINIMAP_EXIT => Color.Red,
                    MINIMAP_UNSET => Color.Black,
                    MINIMAP_SEA => _SeaColor,
                    _ => throw new ArgumentException("GameViewMinimap.GetColorForHeight passed an invalid negative value")
                };
            }
            else
            {
                float scale = Math.Clamp(1 - ((float)height / _HighestZ), 0, 1);
                float hue = HIGHEST_HUE + ((LOWEST_HUE - HIGHEST_HUE) * scale);
                float saturation = HIGHEST_SAT + ((LOWEST_SAT - HIGHEST_SAT) * scale);
                float value = HIGHEST_VAL + ((LOWEST_VAL - HIGHEST_VAL) * scale);

                int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
                double f = hue / 60 - Math.Floor(hue / 60);

                value *= 255;
                int v = Convert.ToInt32(value);
                int p = Convert.ToInt32(value * (1 - saturation));
                int q = Convert.ToInt32(value * (1 - f * saturation));
                int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

                if (hi == 0)
                    return new Color(v, t, p);
                else if (hi == 1)
                    return new Color(q, v, p);
                else if (hi == 2)
                    return new Color(p, v, t);
                else if (hi == 3)
                    return new Color(p, q, v);
                else if (hi == 4)
                    return new Color(t, p, v);
                else
                    return new Color(v, p, q);
            }
        }
    }
}
