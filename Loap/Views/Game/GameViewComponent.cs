﻿using Loap.Audio;
using Loap.Rendering;
using LoapCore.Blocks;
using LoapCore.Constants;
using LoapCore.Lemmings;
using LoapCore.Levels;
using LoapCore.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Loap.Views.Game
{
    abstract class GameViewComponent
    {
        protected readonly GameView GameView;
        protected readonly GraphicsDevice GraphicsDevice;

        protected GameViewRendering Rendering => GameView.Rendering;
        protected GameViewExecution Execution => GameView.Execution;
        protected GameViewUI UI => GameView.UI;
        protected GameViewMinimap Minimap => GameView.Minimap;

        protected List<Lemming> Lemmings => Execution.Engine.Lemmings;
        protected BlockStructure Blocks => Execution.Engine.Blocks;
        protected Level Level => Execution.Engine.Level;
        protected Camera Camera => Rendering.ActiveCamera;

        protected int Iteration => Execution.Engine.Iteration;

        public GameViewComponent(GameView gameView, GraphicsDevice graphicsDevice)
        {
            GraphicsDevice = graphicsDevice;
            GameView = gameView;

        }

        protected void PlaySound(string soundName)
        {
            SoundManager.PlaySound(soundName, 1);
        }

        protected void PlaySound(string soundName, Point3D position)
        {
            PlaySound(soundName, new Vector3(
                position.X * PhysicsConstants.STEP_SIZE_FLOAT,
                position.Y * PhysicsConstants.STEP_SIZE_FLOAT,
                position.Z * PhysicsConstants.STEP_SIZE_FLOAT
                ));
        }

        protected void PlaySound(string soundName, Vector3 position)
        {
            float volume = 1 - Math.Clamp((position - Camera.Position).Length() * 0.03125f, 0f, 0.75f);
            SoundManager.PlaySound(soundName, volume);
        }
    }
}
