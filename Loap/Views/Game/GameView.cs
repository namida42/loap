﻿using Loap.Constants;
using Loap.Misc;
using Loap.UI;
using Loap.Views.Menu;
using LoapCore.Levels;
using LoapCore.Misc;
using LoapCore.Replays;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Loap.Views.Game
{
    class GameView : BaseView
    {
        public readonly GameViewRendering Rendering;
        public readonly GameViewExecution Execution;
        public readonly GameViewUI UI;
        public readonly GameViewMinimap Minimap;

        private Level Level => Execution.Engine.Level;

        private Texture2D _TurnerArrows;

        public GameView(GraphicsDevice graphics, Level level, Replay? replay) : base(graphics)
        {
            // Take care if messing with the order of these!!
            Execution = new GameViewExecution(this, GraphicsDevice, level, replay);
            Rendering = new GameViewRendering(this, GraphicsDevice);
            UI = new GameViewUI(this, GraphicsDevice);
            Minimap = new GameViewMinimap(this, GraphicsDevice);
            // End order-sensitive.

            if (Rendering.Renderer.ScreenTexture != null)
                ScreenTexture = CreateRenderTarget();

            _TurnerArrows = Texture2D.FromFile(GraphicsDevice, PathConstants.TurnerArrowsFilePath);
        }

        protected override void DoUpdate(float elapsedTime)
        {
            UI.HandleInput(elapsedTime);
            Rendering.ActiveCamera.UpdateVirtualLemmingRotation(elapsedTime);
            Execution.HandleUpdate();
        }

        protected override void DoRender()
        {
            Rendering.Render();
            UI.RenderOverlays();

            Minimap.RefreshMapView();

            GraphicsDevice.SetRenderTarget(Canvas);
            GraphicsDevice.Clear(Color.Black);

            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp);
            SpriteBatch.Draw(Rendering.GameplayCanvas, Canvas.Bounds, Color.White);
            SpriteBatch.End();

            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            RenderMinimap();
            RenderTurnerArrow();
            SpriteBatch.Draw(UI.SidebarCanvas, UI.SidebarRect, Color.White);
            SpriteBatch.Draw(UI.SkillPanelCanvas, UI.SkillPanelRect, Color.White);

            if (UI.IsReplaying)
                Font.WriteText(SpriteBatch, UI.ReplayInsertMode ? GameTexts.REPLAY_INSERT_MODE : GameTexts.REPLAY_MODE,
                    new Point(UI.SkillPanelRect.X + 12, UI.SkillPanelRect.Y), Color.White, -1, 1);

            if (Execution.Engine.CheatMode)
                Font.WriteText(SpriteBatch, GameTexts.CHEAT_MODE, new Point(UI.SkillPanelRect.X + 12, UI.SkillPanelRect.Y - Font.CharacterHeight), Color.White, -1, 1);

            Cursor.DrawCursor(SpriteBatch, UserInput.MousePosition,
                (UI.SelectedLemming == null || UI.TurnerDirection != TurnerDirection.None) ? CursorGraphic.Standard : CursorGraphic.Open);
            SpriteBatch.End();
        }

        private void RenderMinimap()
        {
            SpriteBatch.Draw(Minimap.MinimapView, Minimap.MinimapView.Bounds, Color.White);
        }

        private void RenderTurnerArrow()
        {
            if (UI.TurnerDirection != TurnerDirection.None)
            {
                int arrowSize = _TurnerArrows.Width / 6;
                Rectangle srcRect = new Rectangle(
                    (UI.TurnerGraphicFrame % 6) * arrowSize,
                    (UI.TurnerGraphicFrame / 6) * arrowSize,
                    arrowSize, arrowSize
                    );

                float scale = Cursor.CursorScale;
                int size = (int)Math.Round(arrowSize * scale);
                Rectangle dstRect = new Rectangle(UI.TurnerGraphicPos.X - (size / 2), UI.TurnerGraphicPos.Y - (size / 2), size, size);

                SpriteBatch.Draw(_TurnerArrows, dstRect, srcRect, Color.White);
            }
        }

        private bool _ExitTriggered = false;
        public void Exit()
        {
            if (!_ExitTriggered)
            {
                _ExitTriggered = true;
                Execution.SaveProgress();
                SetNewView(() => new PostLevelView(GraphicsDevice, Execution.Engine));
            }
        }

        public RenderTarget2D MakeChildRenderTarget()
        {
            return CreateRenderTarget();
        }

        public RenderTarget2D MakeChildRenderTarget(int width, int height, bool preserveContents = false)
        {
            return CreateRenderTarget(width, height, preserveContents);
        }
    }
}
