﻿using Loap.Constants;
using Loap.Lemmings;
using Loap.Misc;
using Loap.Models;
using Loap.Rendering;
using LoapCore.Constants;
using LoapCore.Lemmings;
using LoapCore.Levels;
using LoapCore.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loap.Views.Game
{
    class GameViewRendering : GameViewComponent
    {
        public Camera ActiveCamera;
        public readonly List<Camera> Cameras = new List<Camera>();

        public readonly Renderer Renderer;

        public readonly RenderTarget2D GameplayCanvas;

        private readonly List<Texture2D> _LemmingTextures = new List<Texture2D>();

        private BlockCacheEntry[] _BlockCache;
        public readonly RenderState RenderState = new RenderState();

        private readonly LemmingSpriteInfo _ParasolInfo;
        private readonly LemmingSpriteInfo _WarpInfo;

        private readonly LoapModel _BrickTopFace;
        private readonly LoapModel _BrickRemainder;
        private readonly Texture2D _BrickTexture;

        private readonly Texture2D _Overheads;
        private readonly Texture2D _CameraGraphic;

        private readonly Texture2D _SpawnDirectionTexture;
        private readonly Texture2D _CrackTexture;

        private readonly List<HatchFlap> _HatchFlaps;

        public bool NeedRender = true;

        public GameViewRendering(GameView gameView, GraphicsDevice graphicsDevice) : base(gameView, graphicsDevice)
        {
            if (LoapSettings.RenderWidth > 0)
            {
                GameplayCanvas = GameView.MakeChildRenderTarget(
                    LoapSettings.RenderWidth,
                    LoapSettings.RenderWidth * GraphicsDevice.Viewport.Height / GraphicsDevice.Viewport.Width
                    );
            }
            else
                GameplayCanvas = GameView.MakeChildRenderTarget();

            PrepareCameras();
            ActiveCamera = Cameras[0];

            _ParasolInfo = LemmingSprites.GetSpriteInfo("FloaterUmbrella");
            _WarpInfo = LemmingSprites.GetSpriteInfo("Warper");

            Renderer = new Renderer(GraphicsDevice);
            Renderer.Camera = ActiveCamera;
            Renderer.LoadTextures(Level);
            Renderer.GenerateBlockCache(Level.BlockStructure, Level.Overlays, out _BlockCache, out RenderState.BlockExclusions);

            _HatchFlaps = Renderer.PrepareHatchFlaps(Level.BlockStructure);

            var brickModels = ObjFileReader.LoadModels(File.ReadAllLines(PathConstants.BrickModelFilePath));
            _BrickTopFace = brickModels["BrickTop"];
            _BrickRemainder = brickModels["BrickExceptTop"];
            _BrickTexture = Texture2D.FromFile(GraphicsDevice, PathConstants.BuilderBrickTextureFilePath);

            _Overheads = Texture2D.FromFile(GraphicsDevice, PathConstants.OverheadsTextureFilePath);
            _CameraGraphic = Texture2D.FromFile(GraphicsDevice, PathConstants.CameraGraphicFilePath);

            _SpawnDirectionTexture = Texture2D.FromFile(GraphicsDevice, PathConstants.SpawnDirectionGraphicFilePath);
            _CrackTexture = Texture2D.FromFile(GraphicsDevice, PathConstants.CrackGraphicFilePath);
            Renderer.CrackTexture = _CrackTexture;

            LoadLemmingSpriteTextures();
        }

        private void PrepareCameras()
        {
            if (Level.LevelInfo.Cameras.Count == 0)
            {
                var camera = CameraEntry.Default;
                Cameras.Add(new Camera(new Vector3(camera.X, camera.Y, camera.Z), camera.Rotation, camera.Pitch));
            }
            else
                foreach (var camera in Level.LevelInfo.Cameras)
                    Cameras.Add(new Camera(new Vector3(camera.X, camera.Y, camera.Z), camera.Rotation, camera.Pitch));
        }

        private void LoadLemmingSpriteTextures()
        {
            for (int i = 0; i < LemmingSprites.TextureSheetCount; i++)
                _LemmingTextures.Add(Texture2D.FromFile(GraphicsDevice,
                    Path.Combine(PathConstants.LemmingGraphicsPath, PathConstants.GRAPHICS_LEMMINGS_SPRITE_FILE_NAME_BASE + i.ToString() + PathConstants.EXT_IMAGE)));
        }

        public void UpdateParticles()
        {
            foreach (var particle in RenderState.Particles)
                particle.Update();

            RenderState.Particles.RemoveAll(p => p.Scale <= 0);

            foreach (var removeLem in Execution.Engine.RemovedOnLatestIterationLemmings)
                switch (removeLem.LemmingRemoveReason)
                {
                    case LemmingRemoveReason.Splat:
                        RenderState.Particles.AddRange(LemmingParticle.CreateParticles(
                            16,
                            new Vector3(removeLem.X * PhysicsConstants.STEP_SIZE_FLOAT, removeLem.Y * PhysicsConstants.STEP_SIZE_FLOAT, removeLem.Z * PhysicsConstants.STEP_SIZE_FLOAT)
                            ));
                        break;
                    case LemmingRemoveReason.Bomber:
                        RenderState.Particles.AddRange(LemmingParticle.CreateParticles(
                            16,
                            new Vector3(removeLem.X * PhysicsConstants.STEP_SIZE_FLOAT, removeLem.Y * PhysicsConstants.STEP_SIZE_FLOAT, removeLem.Z * PhysicsConstants.STEP_SIZE_FLOAT)
                            ));
                        break;
                }
        }

        public void Render()
        {
            Renderer.Camera = ActiveCamera;

            PrepareRender();

            GraphicsDevice.SamplerStates[0] = SamplerState.PointWrap;
            Renderer.RenderSea(Level.LevelInfo.Sea, Iteration);
            Renderer.RenderSky();
            Renderer.RenderLands(Level.Lands);

            GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            RenderEntities();
        }

        private void PrepareRender()
        {
            if (GameView.ScreenTexture != null)
                Renderer.UpdateScreenTexture(GameView.ScreenTexture);

            Renderer.PrepareSeaFrame(Iteration);
            GraphicsDevice.SetRenderTarget(GameplayCanvas);
            GraphicsDevice.Clear(Color.Black);
        }

        public void UpdateExclusions(List<Point3D> removedBlocks)
        {
            var refreshPoints = new List<Point3D>();
            refreshPoints.AddRange(removedBlocks);

            foreach (var point in removedBlocks)
            {
                refreshPoints.Add(new Point3D(point.X - 1, point.Y, point.Z));
                refreshPoints.Add(new Point3D(point.X + 1, point.Y, point.Z));
                refreshPoints.Add(new Point3D(point.X, point.Y - 1, point.Z));
                refreshPoints.Add(new Point3D(point.X, point.Y + 1, point.Z));
                refreshPoints.Add(new Point3D(point.X, point.Y, point.Z - 1));
                refreshPoints.Add(new Point3D(point.X, point.Y, point.Z + 1));
            }

            Renderer.UpdateBlockExclusions(RenderState.BlockExclusions!, Execution.Engine.Blocks, refreshPoints.Distinct());
        }

        public void UpdateDamage(List<Point3D> damagedBlocks)
        {
            foreach (var damagePoint in damagedBlocks)
                if (RenderState.DamageIterations.ContainsKey(damagePoint))
                    RenderState.DamageIterations[damagePoint] += 1;
                else
                    RenderState.DamageIterations.Add(damagePoint, 1);
        }

        public void RenderEntities()
        {
            List<RenderEntity> entities = new List<RenderEntity>();
            RenderEntity.ReleaseEntities();

            Renderer.AddBlocksToEntityQueue(entities, _BlockCache, RenderState.BlockExclusions, Iteration, RenderState.DamageIterations, Execution.Engine.GetExitFrameIndex);
            if (Execution.Engine.Iteration == 0)
                Renderer.AddHatchFlapsToEntityQueue(entities, _HatchFlaps, Execution.Engine.Iteration);
            Renderer.AddLevelEntitiesToEntityQueue(entities, Execution.Engine.EntityInstances, true);
            QueueBuilderBricks(entities, true);
            QueueSpawnDirectionIndicators(entities);
            Renderer.RenderEntityQueue(entities, true);
            entities.Clear();

            RenderEntity.ReleaseEntities();
            Renderer.AddLevelEntitiesToEntityQueue(entities, Execution.Engine.EntityInstances, false);
            if (Execution.Engine.Iteration > 0)
                Renderer.AddHatchFlapsToEntityQueue(entities, _HatchFlaps, Execution.Engine.Iteration);
            QueueLemmings(entities);
            QueueParticles(entities);
            QueueBuilderBricks(entities, false);
            QueueCameras(entities);

            Renderer.RenderEntityQueue(entities, false);
        }

        private void QueueSpawnDirectionIndicators(List<RenderEntity> queue)
        {
            foreach (var entrance in Execution.Engine.SpawnPoints)
            {
                int zOff = 1;
                while (entrance.BlockCoordinates.Z - zOff > 0)
                {
                    var block = Execution.Engine.Blocks[entrance.BlockCoordinates.X, entrance.BlockCoordinates.Y, entrance.BlockCoordinates.Z - zOff];
                    if (block != null && !PhysicsConstants.NonSolidBlockTypes.Contains(block.Physics))
                        break;

                    zOff++;
                    if (zOff >= RenderConstants.SPAWN_DIRECTION_RENDER_OFFSET_CAP)
                    {
                        zOff = RenderConstants.SPAWN_DIRECTION_RENDER_FALLBACK;
                        break;
                    }
                }

                Renderer.AddSpawnIndicatorToEntityQueue(queue,
                    new Vector3(entrance.BlockCoordinates.X, entrance.BlockCoordinates.Y, (entrance.BlockCoordinates.Z - zOff + 1) * PhysicsConstants.SEGMENT_SIZE_FLOAT),
                    entrance.SpawnDirection,
                    _SpawnDirectionTexture
                    );
            }
        }

        private void QueueBuilderBricks(List<RenderEntity> queue, bool tops)
        {
            foreach (var brick in Execution.Engine.BuilderBricks)
            {
                float brickRotation = Utils.DirectionToDegrees(brick.Direction);
                Vector3 pos = new Vector3(brick.Position.X + 0.5f, brick.Position.Y + 0.5f, (brick.Position.Z * PhysicsConstants.SEGMENT_SIZE_FLOAT) + PhysicsConstants.SEGMENT_SIZE_FLOAT) +
                    Vector3.Transform(new Vector3(0, 0.25f, 0), Matrix.CreateRotationZ(MathHelper.ToRadians(brickRotation)));

                if (tops)
                    Renderer.AddModelToEntityQueue(queue, _BrickTopFace, pos,
                        new Vector3(brick.Position.X + 0.5f, brick.Position.Y + 0.5f, (brick.Position.Z * PhysicsConstants.SEGMENT_SIZE_FLOAT) + PhysicsConstants.SEGMENT_SIZE_FLOAT),
                        brickRotation, 0, _BrickTexture, false, RenderConstants.RENDER_PRIORITY_BUILDER_BRICK_TOP);
                else
                    Renderer.AddModelToEntityQueue(queue, _BrickRemainder, pos, brickRotation, 0, _BrickTexture, false, RenderConstants.RENDER_PRIORITY_BUILDER_BRICK_REMAINDER);
            }
        }

        /// <summary>
        /// Takes cameraPos as a parameter for the sake of Virtual Lemming mode.
        /// </summary>
        public bool IsLemmingInFrontOfCamera(Vector3 cameraPos, Vector3 targetLemmingPos)
        {
            var modTarget = Vector3.Transform(targetLemmingPos - cameraPos, Matrix.CreateRotationZ(MathHelper.ToRadians(-Camera.Rotation)));
            return modTarget.Y > 0;
        }

        private void QueueCameras(List<RenderEntity> queue)
        {
            foreach (var camera in Cameras)
            {
                if (camera == ActiveCamera && camera.VirtualLemming == null)
                    continue;

                float idealRot = camera.Rotation - ActiveCamera.Rotation;
                FloatRectangle srcRect = new FloatRectangle(0, 0, 1 / 8f, 1);
                srcRect.X += Utils.ModuloNegativeAdjusted((int)Math.Round(idealRot / (360f / 8)), 8) / 8f;
                Vector2 size = new Vector2(RenderConstants.CAMERA_SPRITE_RENDER_SIZE, RenderConstants.CAMERA_SPRITE_RENDER_SIZE);

                if (size.X != 0 && size.Y != 0)
                    Renderer.AddBillboardToEntityQueue(
                        queue, camera.NonVirtualLemmingPosition, size,
                        srcRect, _CameraGraphic, RenderConstants.RENDER_PRIORITY_CAMERA,
                        Color.White
                        );
            }
        }

        private void QueueLemmings(List<RenderEntity> queue)
        {
            var animRefs = Lemmings.Select(l => GetLemmingAnimationData(l));

            var virtualLemmingPos = (Camera.VirtualLemming == null) ? default(Vector3) :
                new Vector3(
                    Camera.VirtualLemming.X * PhysicsConstants.STEP_SIZE_FLOAT,
                    Camera.VirtualLemming.Y * PhysicsConstants.STEP_SIZE_FLOAT,
                    Camera.VirtualLemming.Z * PhysicsConstants.STEP_SIZE_FLOAT
                    );

            foreach (var lemRef in animRefs)
            {
                Vector3 target = new Vector3(
                    lemRef.Lemming.X * PhysicsConstants.STEP_SIZE_FLOAT,
                    lemRef.Lemming.Y * PhysicsConstants.STEP_SIZE_FLOAT,
                    lemRef.Lemming.Z * PhysicsConstants.STEP_SIZE_FLOAT - (PhysicsConstants.STEP_SIZE_FLOAT * 2)
                    );

                if (Camera.VirtualLemming != null)
                {
                    if (!IsLemmingInFrontOfCamera(virtualLemmingPos, target))
                        continue;
                }
                else if (!IsLemmingInFrontOfCamera(Camera.Position, target))
                    continue;

                float idealRot = Utils.DirectionToDegrees(lemRef.Lemming.Direction) - Camera.Rotation;

                if (lemRef.Lemming.Action == LemmingAction.DeathSpin)
                    idealRot += (lemRef.Lemming.CurrentActionIterations * 22.5f);

                FloatRectangle srcRect = lemRef.SpriteInfo.GetFloatRect(idealRot, lemRef.SpriteInfo.GetKeyframeAdjustedFrameIndex(lemRef.FrameIndex, !lemRef.PostKeyFrame));
                Vector2 size = new Vector2(RenderConstants.LEMMING_SPRITE_RENDER_SIZE, RenderConstants.LEMMING_SPRITE_RENDER_SIZE);

                if (lemRef.Lemming.Action == LemmingAction.Climber)
                {
                    Vector2 baseOffset = Vector2.Transform(new Vector2(0, -RenderConstants.CLIMBER_MAX_OFFSET), Matrix.CreateRotationZ(MathHelper.ToRadians(idealRot)));
                    Vector2 shift = Vector2.Transform(new Vector2(baseOffset.X, baseOffset.Y), Matrix.CreateRotationZ(MathHelper.ToRadians(Camera.Rotation)));
                    target = new Vector3(target.X + shift.X, target.Y + shift.Y, target.Z);
                }

                if (lemRef.Lemming.Action == LemmingAction.Exiter || lemRef.Lemming.Action == LemmingAction.DeathSpin)
                {
                    float scale = 1 - (lemRef.Lemming.CurrentActionIterations / (float)(lemRef.Lemming.Action == LemmingAction.Exiter ? Lemming.EXITER_REMOVE_ITERATION : Lemming.DEATH_SPIN_REMOVE_ITERATION));
                    if (scale <= 0)
                        continue; // Shouldn't happen

                    float oldSizeY = size.Y;
                    size.X *= scale;
                    size.Y *= scale;
                    target.Z += (oldSizeY - size.Y) / 2;
                }

                if (lemRef.Lemming.Action == LemmingAction.Warper)
                {
                    int scaleBase = lemRef.Lemming.CurrentActionIterations <= Lemming.WARPING_DURATION / 2 ? lemRef.Lemming.CurrentActionIterations : lemRef.Lemming.CurrentActionIterations - 1;
                    float scale = Math.Abs(Lemming.WARPING_DURATION / 2 - scaleBase) / (float)(Lemming.WARPING_DURATION / 2);
                    if (scale <= 0) // This one actually can happen - and we have to do other stuff later still, so can't just "continue"
                    {
                        size.X = 0;
                        size.Y = 0;
                    }
                    else
                    {
                        size.X *= scale;
                        size.Y *= scale;
                        // We don't do the upwards shift on the Warper
                    }
                }

                if (lemRef.Lemming.Action == LemmingAction.Zipper)
                {
                    var srcPos = lemRef.Lemming.TransporterSource; // We only need the gradient, not the exact positions, so no need to convert to units
                    var dstPos = lemRef.Lemming.TransporterTarget;

                    float srcPosXY;
                    float dstPosXY;

                    if (srcPos.X == dstPos.X)
                    {
                        srcPosXY = srcPos.Y;
                        dstPosXY = dstPos.Y;
                    }
                    else if (srcPos.Y == dstPos.Y)
                    {
                        srcPosXY = srcPos.X;
                        dstPosXY = dstPos.X;
                    }
                    else
                    {
                        srcPosXY = MathF.Sqrt((srcPos.X * srcPos.X) + (srcPos.Y * srcPos.Y));
                        dstPosXY = MathF.Sqrt((dstPos.X * dstPos.X) + (dstPos.Y * dstPos.Y));
                    }

                    var srcPosZ = srcPos.Z / (float)PhysicsConstants.BLOCK_SEGMENTS;
                    var dstPosZ = dstPos.Z / (float)PhysicsConstants.BLOCK_SEGMENTS;

                    if (srcPosZ == dstPosZ)
                        target.Z += PhysicsConstants.SEGMENT_SIZE_FLOAT * 2.25f;
                    else
                    {
                        float slope = MathF.Abs((dstPosXY - srcPosXY) / (dstPosZ - srcPosZ));
                        float offset = slope - 1;

                        if (dstPosZ > srcPosZ)
                            offset += 0.5f;

                        target.Z += PhysicsConstants.SEGMENT_SIZE_FLOAT * offset;
                    }
                }

                if (size.X != 0 && size.Y != 0)
                    Renderer.AddBillboardToEntityQueue(
                        queue, target, size,
                        srcRect, _LemmingTextures[lemRef.TextureFile], RenderConstants.RENDER_PRIORITY_LEMMINGS,
                        Color.White
                        );

                if (lemRef.Lemming.Action == LemmingAction.Floater)
                {
                    FloatRectangle parasolSrcRect = _ParasolInfo.GetFloatRect(idealRot, 0);
                    Renderer.AddBillboardToEntityQueue(
                        queue, new Vector3(target.X, target.Y, target.Z + RenderConstants.LEMMING_SPRITE_RENDER_SIZE),
                        new Vector2(RenderConstants.LEMMING_SPRITE_RENDER_SIZE, RenderConstants.LEMMING_SPRITE_RENDER_SIZE),
                        parasolSrcRect, _LemmingTextures[_ParasolInfo.TextureFile], RenderConstants.RENDER_PRIORITY_LEMMINGS,
                        Color.White
                        );
                }

                if (lemRef.Lemming.Action == LemmingAction.Warper)
                {
                    FloatRectangle warperSrcRect = _WarpInfo.GetFloatRect(idealRot, (lemRef.Lemming.CurrentActionIterations / 2) % _WarpInfo.FrameCount);
                    Renderer.AddBillboardToEntityQueue(
                        queue, new Vector3(target.X, target.Y, target.Z),
                        new Vector2(RenderConstants.LEMMING_SPRITE_RENDER_SIZE, RenderConstants.LEMMING_SPRITE_RENDER_SIZE),
                        warperSrcRect, _LemmingTextures[_WarpInfo.TextureFile], RenderConstants.RENDER_PRIORITY_LEMMINGS,
                        Color.White
                        );
                }

                if (lemRef.Lemming == UI.HighlitLemming && Camera.VirtualLemming == null)
                {
                    FloatRectangle highlightIconSrcRect = new FloatRectangle(0.5f, 0, 0.5f, 1f);
                    Renderer.AddBillboardToEntityQueue(
                        queue, new Vector3(target.X, target.Y, target.Z + RenderConstants.LEMMING_SPRITE_RENDER_SIZE),
                        new Vector2(RenderConstants.OVERHEAD_ICON_RENDER_SIZE, RenderConstants.OVERHEAD_ICON_RENDER_SIZE),
                        highlightIconSrcRect, _Overheads, RenderConstants.RENDER_PRIORITY_LEMMINGS,
                        Color.White
                        );
                }
                else if (lemRef.Lemming.Action == LemmingAction.Shrugger)
                {
                    FloatRectangle shrugIconSrcRect = new FloatRectangle(0, 0, 0.5f, 1f);
                    Renderer.AddBillboardToEntityQueue(
                        queue, new Vector3(target.X, target.Y, target.Z + RenderConstants.LEMMING_SPRITE_RENDER_SIZE),
                        new Vector2(RenderConstants.OVERHEAD_ICON_RENDER_SIZE, RenderConstants.OVERHEAD_ICON_RENDER_SIZE),
                        shrugIconSrcRect, _Overheads, RenderConstants.RENDER_PRIORITY_LEMMINGS,
                        Color.White
                        );
                }
            }

            if (Camera.VirtualLemming != null && Camera.VirtualLemming.Action == LemmingAction.Shrugger)
            {
                Vector3 target = new Vector3(
                    Camera.VirtualLemming.X * PhysicsConstants.STEP_SIZE_FLOAT,
                    Camera.VirtualLemming.Y * PhysicsConstants.STEP_SIZE_FLOAT,
                    Camera.VirtualLemming.Z * PhysicsConstants.STEP_SIZE_FLOAT
                    );

                FloatRectangle shrugIconSrcRect = new FloatRectangle(0, 0, 0.5f, 1f);
                Renderer.AddBillboardToEntityQueue(
                    queue, new Vector3(target.X, target.Y, target.Z + PhysicsConstants.SEGMENT_SIZE_FLOAT),
                    new Vector2(RenderConstants.OVERHEAD_ICON_RENDER_SIZE / 2, RenderConstants.OVERHEAD_ICON_RENDER_SIZE / 2),
                    shrugIconSrcRect, _Overheads, RenderConstants.RENDER_PRIORITY_LEMMINGS,
                    Color.White
                    );
            }
        }

        private void QueueParticles(List<RenderEntity> queue)
        {
            if (RenderState.Particles.Any())
            {
                var spriteRef = LemmingSprites.GetSpriteInfo("Bomber");

                foreach (var particle in RenderState.Particles)
                {
                    float idealRot = (particle.Direction * 45) - Camera.Rotation;
                    FloatRectangle srcRect = spriteRef.GetFloatRect(idealRot, 4);

                    Renderer.AddBillboardToEntityQueue(
                            queue, particle.Position,
                            new Vector2(0.5f, 0.5f) * particle.Scale,
                            srcRect, _LemmingTextures[spriteRef.TextureFile], RenderConstants.RENDER_PRIORITY_PARTICLES,
                            Color.White
                        );
                }
            }
        }

        private static LemmingAnimationReference GetLemmingAnimationData(Lemming lemming)
        {
            return lemming.Action switch
            {
                LemmingAction.Walker => new LemmingAnimationReference(
                    (lemming.QueuedSkill == QueuedSkill.BuilderBegin || lemming.QueuedSkill == QueuedSkill.BuilderContinue || lemming.QueuedSkill == QueuedSkill.Shrugger) ?
                    "BuilderWalk" : "Walker", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Slider => new LemmingAnimationReference("Slider", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Builder => new LemmingAnimationReference("Builder", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Basher => new LemmingAnimationReference("Basher", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Miner => new LemmingAnimationReference("Miner", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Digger => new LemmingAnimationReference("Digger", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Exiter => new LemmingAnimationReference("Walker", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Faller => new LemmingAnimationReference("Faller", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Springer => new LemmingAnimationReference("Faller", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Bouncer => new LemmingAnimationReference("Faller", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Climber => new LemmingAnimationReference("Climber", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Floater => new LemmingAnimationReference("Floater", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Stunned => new LemmingAnimationReference("Stunned", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Shrugger => new LemmingAnimationReference("Shrugger", lemming.CurrentActionIterations / 3, false, lemming),
                LemmingAction.Blocker => new LemmingAnimationReference("Blocker", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Bomber => new LemmingAnimationReference("Bomber", lemming.CurrentActionIterations / 3, false, lemming),
                LemmingAction.DeathSpin => new LemmingAnimationReference("Walker", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Warper => new LemmingAnimationReference("Walker", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Zipper => new LemmingAnimationReference("Zipper", lemming.CurrentActionIterations / 2, false, lemming),
                LemmingAction.Turner => new LemmingAnimationReference(
                    lemming.TurnerDirection switch
                    {
                        TurnerDirection.Left => "TurnerLeft",
                        TurnerDirection.Right => "TurnerRight",
                        _ => throw new InvalidOperationException("Invalid turner direction."),
                    },
                    lemming.CurrentActionIterations / 2, false, lemming
                    ),
                LemmingAction.Drowner => new LemmingAnimationReference("Drowner",
                    (lemming.CurrentActionIterations >= Lemming.DROWNER_BEGIN_FINAL_FRAMES_ITERATION ?
                    (lemming.CurrentActionIterations - Lemming.DROWNER_BEGIN_FINAL_FRAMES_ITERATION) :
                    lemming.CurrentActionIterations) / 2,
                    lemming.CurrentActionIterations >= Lemming.DROWNER_BEGIN_FINAL_FRAMES_ITERATION,
                    lemming),
                LemmingAction.Zapper => lemming.CurrentActionIterations >= Lemming.ZAPPER_POOF_ITERATION ?
                    new LemmingAnimationReference("Poof", (lemming.CurrentActionIterations - Lemming.ZAPPER_POOF_ITERATION) / 2, false, lemming)
                    :
                    new LemmingAnimationReference("Zapper", 0,
                    lemming.CurrentActionIterations switch
                    {
                        0 => true,
                        1 => true,
                        4 => true,
                        5 => true,
                        14 => true,
                        15 => true,
                        18 => true,
                        19 => true,
                        _ => false
                    }
                    ,
                    lemming
                    ),
                _ => throw new NotImplementedException(),
            };
        }
    }
}
