﻿using Loap.Constants;
using LoapCore.Files;
using LoapCore.Misc;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace Loap.Views.Game
{
    static class SkillPanelGraphics
    {
        #region File Keywords
        private const string KEY_SPRITE = "SPRITE";
        private const string KEY_SKILL = "SKILL";
        private const string KEY_X = "X";
        private const string KEY_Y = "Y";
        private const string KEY_WIDTH = "WIDTH";
        private const string KEY_HEIGHT = "HEIGHT";
        private const string KEY_FRAMES = "FRAMES";
        private const string KEY_ANIMATION = "ANIMATION";
        #endregion

        public static int TotalSkillIconWidth;
        public static int MaxSkillIconHeight;

        public struct SkillPanelIconInfo
        {
            public Rectangle FirstFrame;
            public int FrameCount;
            public AnimationStyle AnimationStyle;
        }

        public static readonly Dictionary<Skill, SkillPanelIconInfo> PanelInfo = new Dictionary<Skill, SkillPanelIconInfo>();

        static SkillPanelGraphics()
        {
            LoadPanelIconInfo();
        }

        private static void LoadPanelIconInfo()
        {
            TotalSkillIconWidth = 0;
            MaxSkillIconHeight = 0;

            DataItem src = DataItemReader.LoadFromFile(PathConstants.SkillIconsMetainfoFilePath);
            foreach (var child in src.GetChildren(KEY_SPRITE))
            {
                var newInfo = new SkillPanelIconInfo();
                newInfo.FirstFrame = new Rectangle(
                    child.GetChildValueInt(KEY_X),
                    child.GetChildValueInt(KEY_Y),
                    child.GetChildValueInt(KEY_WIDTH),
                    child.GetChildValueInt(KEY_HEIGHT)
                    );
                newInfo.FrameCount = child.GetChildValueInt(KEY_FRAMES, 1);
                newInfo.AnimationStyle = child.GetChildValueEnum(KEY_ANIMATION, AnimationStyle.Normal);
                PanelInfo.Add(child.GetChildValueEnum<Skill>(KEY_SKILL), newInfo);

                TotalSkillIconWidth += newInfo.FirstFrame.Width;
                MaxSkillIconHeight = Math.Max(newInfo.FirstFrame.Height, MaxSkillIconHeight);
            }
        }
    }
}
