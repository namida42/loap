﻿using Loap.Audio;
using Loap.Constants;
using Loap.Misc;
using Loap.Progress;
using LoapCore.Constants;
using LoapCore.Game;
using LoapCore.Lemmings;
using LoapCore.Levels;
using LoapCore.Misc;
using LoapCore.Replays;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Loap.Views.Game
{
    class GameViewExecution : GameViewComponent
    {
        private const int SAVE_STATE_INTERVAL = PhysicsConstants.ITERATIONS_PER_INGAME_SECOND * 10;

        public readonly GameEngine Engine;

        private readonly UpdateTimer _Timer = new UpdateTimer(UIConstants.IDEAL_UPDATE_TIME_NORMAL);

        public bool ForceOneUpdate;

        private GameSpeed _GameSpeed;
        public GameSpeed GameSpeed
        {
            get
            {
                return _GameSpeed;
            }

            set
            {
                if (value != _GameSpeed)
                {
                    if (_GameSpeed == GameSpeed.Pause)
                        _Timer.Invalidate(GameView.ViewRunTime);

                    _GameSpeed = value;

                    _Timer.IdealDelay = (_GameSpeed == GameSpeed.FastForward) ? UIConstants.IDEAL_UPDATE_TIME_FASTFORWARD : UIConstants.IDEAL_UPDATE_TIME_NORMAL;
                    GameView.IdealFrameDelay = MathF.Min(_Timer.IdealDelay, UIConstants.IDEAL_FRAMERATE_TIME);
                }
            }
        }

        public int SkipTargetIteration;
        public Lemming[] SkipToShruggerBuilders = new Lemming[0];

        private LevelProgress? _Progress;

        public GameViewExecution(GameView gameView, GraphicsDevice graphicsDevice, Level level, Replay? replay) : base(gameView, graphicsDevice)
        {
            Engine = CreateGameEngine(level, replay);

            Engine.Replay.LevelTitle = level.LevelInfo.Title;

            if (LoapProgress.CurrentLevel != null)
            {
                _Progress = LoapProgress.GetLevelProgress(LoapProgress.CurrentLevel);
                if (_Progress.Completion == LevelCompletion.None)
                    _Progress.Completion = LevelCompletion.Attempted;

                Engine.Replay.LevelPath = LoapProgress.CurrentLevel.FilePath;
            }
            else
            {
                _Progress = null;
                Engine.Replay.LevelPath = "";
            }
        }

        private GameEngine CreateGameEngine(Level level, Replay? replay)
        {
            var entities = MetaEntityLoader.LoadLevelMetaEntities(level);

            foreach (var entity in entities)
                if (entity.Value.SoundEffect != "")
                    SoundManager.LoadSound(entity.Value.SoundEffect);

            var engine = new GameEngine(level, entities, replay);
            return engine;
        }

        public void HandleUpdate()
        {
            if (
                (SkipTargetIteration > Execution.Engine.Iteration || SkipToShruggerBuilders.Length > 0) ||
                (((GameSpeed != GameSpeed.Pause) || ForceOneUpdate) && _Timer.IsTimeForUpdate(GameView.ViewRunTime))
               )
            {
                ForceOneUpdate = false;

                if (SkipTargetIteration > Execution.Engine.Iteration)
                    PerformMultipleUpdates(delegate () { return Execution.Engine.Iteration >= SkipTargetIteration; });
                else if (SkipToShruggerBuilders.Length > 0)
                {
                    PerformMultipleUpdates(delegate ()
                    {
                        return !SkipToShruggerBuilders.All(l =>
                            (l.Action == LemmingAction.Builder || l.QueuedSkill == QueuedSkill.BuilderBegin || l.QueuedSkill == QueuedSkill.BuilderContinue) &&
                            !l.LemmingRemoved
                            );
                    });
                    SkipToShruggerBuilders = new Lemming[0];
                }
                else
                {
                    bool nukeWasActive = Execution.Engine.NukeActive;

                    UpdateAndCheckCompletion();
                    Rendering.UpdateParticles();

                    ApplyRemovals(Engine.RemovedOnLatestIterationBlocks);
                    ApplyDamage(Engine.DamagedOnLatestIterationBlocks);

                    if (Engine.Iteration % SAVE_STATE_INTERVAL == 0)
                        UI.CreateSavedState();

                    foreach (var sound in Engine.LatestIterationSounds.Distinct())
                        if (sound.Position.HasValue)
                            PlaySound(sound.SoundEffect, sound.Position.Value);
                        else
                            PlaySound(sound.SoundEffect);


                    if (Execution.Engine.NukeActive && !nukeWasActive)
                        PlaySound(SoundNames.NUKE_ACTIVATE);

                    UI.ValidateVirtualAndHighlitLemming();
                }

                if (Engine.Iteration >= Engine.LastIterationWithLemmingsAlive + UIConstants.OVERAGE_ITERATIONS)
                    GameView.Exit();
            }
        }

        public void PerformMultipleUpdates(Func<bool> determineCompleteFunc)
        {
            List<Point3D> removed = new List<Point3D>();

            while (!determineCompleteFunc.Invoke())
            {
                UpdateAndCheckCompletion();
                Rendering.UpdateParticles();
                removed.AddRange(Engine.RemovedOnLatestIterationBlocks);

                if (Engine.Iteration % SAVE_STATE_INTERVAL == 0)
                {
                    ApplyRemovals(removed);
                    removed.Clear();
                    UI.CreateSavedState();
                }
            }

            ApplyRemovals(removed);

            UI.ValidateVirtualAndHighlitLemming();

            _Timer.Invalidate(GameView.ViewRunTime);
        }

        private void ApplyRemovals(List<Point3D> removed)
        {
            if (removed.Count > 0)
            {
                Rendering.UpdateExclusions(removed);

                int minX = removed.Min(rp => rp.X);
                int minY = removed.Min(rp => rp.Y);
                int maxX = removed.Max(rp => rp.X);
                int maxY = removed.Max(rp => rp.Y);

                Minimap.RefreshBaseMap(minX, minY, maxX - minX + 1, maxY - minY + 1);
            }
        }

        private void ApplyDamage(List<Point3D> damaged)
        {
            Rendering.UpdateDamage(damaged);
        }

        private void UpdateAndCheckCompletion()
        {
            int prevSaved = Engine.LemmingsRescued;
            Engine.Update();

            if (prevSaved != Engine.LemmingsRescued && _Progress != null)
            {
                _Progress.SaveRecord = Math.Max(Engine.LemmingsRescued, _Progress.SaveRecord);

                if (Engine.LemmingsNeededToRescue <= 0)
                {
                    _Progress.Completion = LevelCompletion.Completed;
                    _Progress.TimeRecord = Utils.MinPositive(_Progress.TimeRecord, Engine.Iteration);

                    int totalSkills = 0;
                    int skillTypes = 0;

                    foreach (var skill in Utils.GetEnumValues<Skill>())
                    {
                        int skillUsed = Level.LevelInfo.Skillset[skill] - Engine.Skillset[skill];

                        _Progress.SkillRecord[skill] = Utils.MinPositive(_Progress.SkillRecord[skill], skillUsed);
                        totalSkills += skillUsed;
                        if (skillUsed > 0)
                            skillTypes += 1;
                    }

                    _Progress.TotalSkillsRecord = Utils.MinPositive(_Progress.TotalSkillsRecord, totalSkills);
                    _Progress.SkillTypesRecord = Utils.MinPositive(_Progress.SkillTypesRecord, skillTypes);
                }
            }
        }

        public void SaveProgress()
        {
            if (!Engine.CheatMode)
                if (LoapProgress.CurrentLevel != null && _Progress != null) // if the former's non-null, the latter should be too, but tidier this way
                    LoapProgress.SetLevelProgress(LoapProgress.CurrentLevel, _Progress);
        }
    }
}
