﻿using Loap.Constants;
using Loap.Misc;
using Loap.Progress;
using Loap.Rendering;
using Loap.UI;
using LoapCore.Constants;
using LoapCore.Files;
using LoapCore.Lemmings;
using LoapCore.Misc;
using LoapCore.Replays;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NativeFileDialogSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loap.Views.Game
{
    class GameViewUI : GameViewComponent
    {
        private const int SIDEBAR_DESIGN_WIDTH = 176;
        private const int SIDEBAR_DESIGN_HEIGHT = 500;

        private readonly SpriteBatch _SpriteBatch;

        public readonly RenderTarget2D SidebarCanvas;
        private readonly Texture2D _SidebarGraphics;
        public readonly Rectangle SidebarRect;

        public readonly RenderTarget2D SkillPanelCanvas;
        private readonly Texture2D _SkillPanelGraphics;
        public readonly Rectangle SkillPanelRect;

        public Lemming? SelectedLemming = null;
        public Skill SelectedSkill = Skill.Blocker;

        public TurnerDirection TurnerDirection;

        public Point TurnerGraphicPos;
        public int TurnerGraphicFrame;

        public bool ReplayInsertMode;

        private float _LastRRMinusClickTime = -1;
        private float _LastRRPlusClickTime = -1;
        private float _LastNukeClickTime = -1;

        public bool IsReplaying =>
            Execution.Engine.Replay.Actions.Any(a =>
            a.Iteration > Execution.Engine.Iteration ||
            (a.Iteration == Execution.Engine.Iteration && a.Action == ReplayAction.AssignSkill && Execution.GameSpeed == GameSpeed.Pause)
            );

        private int _CurrentCameraIndex;
        public int CurrentCameraIndex
        {
            get
            {
                return _CurrentCameraIndex;
            }

            set
            {
                _CurrentCameraIndex = Utils.ModuloNegativeAdjusted(value, Rendering.Cameras.Count);
                Rendering.ActiveCamera = Rendering.Cameras[_CurrentCameraIndex];
                ValidateVirtualAndHighlitLemming();
            }
        }

        public Lemming? HighlitLemming;
        private bool _PickingVirtualLemming;

        private Lemming? AutoAssignLemming
        {
            get
            {
                if (Camera.VirtualLemming != null)
                    return Camera.VirtualLemming;
                else if (HighlitLemming != null)
                    return HighlitLemming;
                else
                    return null;
            }
        }

        private readonly List<SavedState> _SavedStates = new List<SavedState>();
        private SavedState? _ManualSavedState = null;
        private DataItem _ManualSavedStateReplay = new DataItem();

        private Dictionary<Skill, float> _SkillAnimationTimers = new Dictionary<Skill, float>();

        public void ValidateVirtualAndHighlitLemming()
        {
            if (HighlitLemming != null)
            {
                if (HighlitLemming.LemmingRemoved)
                    HighlitLemming = null;
                else if (!Execution.Engine.Lemmings.Contains(HighlitLemming))
                    HighlitLemming = Execution.Engine.Lemmings.FirstOrDefault(lem => lem.Identifier == HighlitLemming.Identifier);
            }

            var camera = Rendering.ActiveCamera;

            if (camera.VirtualLemming != null)
            {
                if (camera.VirtualLemming.LemmingRemoved)
                    camera.VirtualLemming = null;
                else if (!Execution.Engine.Lemmings.Contains(camera.VirtualLemming))
                    camera.VirtualLemming = Execution.Engine.Lemmings.FirstOrDefault(lem => lem.Identifier == camera.VirtualLemming.Identifier);
            }
        }

        public GameViewUI(GameView gameView, GraphicsDevice graphicsDevice) : base(gameView, graphicsDevice)
        {
            _SpriteBatch = new SpriteBatch(GraphicsDevice);

            SidebarCanvas = GameView.MakeChildRenderTarget(SIDEBAR_DESIGN_WIDTH, SIDEBAR_DESIGN_HEIGHT);
            _SidebarGraphics = Texture2D.FromFile(GraphicsDevice, PathConstants.SidebarIconsFilePath);

            SkillPanelCanvas = GameView.MakeChildRenderTarget(SkillPanelGraphics.TotalSkillIconWidth, SkillPanelGraphics.MaxSkillIconHeight);
            _SkillPanelGraphics = Texture2D.FromFile(GraphicsDevice, PathConstants.SkillIconsFilePath);

            CalculateOverlayRects(ref SidebarRect, ref SkillPanelRect);

            CreateSavedState();

            foreach (var skill in Utils.GetEnumValues<Skill>())
                _SkillAnimationTimers.Add(skill, 0);
        }

        private void CalculateOverlayRects(ref Rectangle sidebar, ref Rectangle panel)
        {
            Rectangle panelBaseSize = new Rectangle(0, 0, SkillPanelGraphics.TotalSkillIconWidth, SkillPanelGraphics.MaxSkillIconHeight);
            float maxPanelWidth = GraphicsDevice.Viewport.Width;
            float maxPanelHeight = GraphicsDevice.Viewport.Height * UIConstants.MAXIMUM_SKILL_PANEL_HEIGHT;
            if (panelBaseSize.Width > maxPanelWidth || panelBaseSize.Height > maxPanelHeight)
            {
                int shrinkFactor = (int)Math.Ceiling(MathF.Max(panelBaseSize.Width / maxPanelWidth, panelBaseSize.Height / maxPanelHeight));

                panel = new Rectangle(
                    Math.Max(4 / shrinkFactor, 1),
                    GraphicsDevice.Viewport.Height - (panelBaseSize.Height / shrinkFactor) - Math.Max(4 / shrinkFactor, 1),
                    panelBaseSize.Width / shrinkFactor,
                    panelBaseSize.Height / shrinkFactor
                    );
            }
            else
            {
                int expandFactor = (int)Math.Floor(MathF.Min(maxPanelWidth / panelBaseSize.Width, maxPanelHeight / panelBaseSize.Height));

                panel = new Rectangle(
                    4 * expandFactor,
                    GraphicsDevice.Viewport.Height - (panelBaseSize.Height * expandFactor) - (4 * expandFactor),
                    panelBaseSize.Width * expandFactor,
                    panelBaseSize.Height * expandFactor
                    );
            }

            Rectangle sidebarBaseSize = new Rectangle(0, 0, SIDEBAR_DESIGN_WIDTH, SIDEBAR_DESIGN_HEIGHT);
            float maxSidebarWidth = GraphicsDevice.Viewport.Width * UIConstants.MAXIMUM_SIDEBAR_WIDTH;
            float maxSidebarHeight = GraphicsDevice.Viewport.Height - panel.Height;
            if (sidebarBaseSize.Width > maxSidebarWidth || sidebarBaseSize.Height > maxSidebarHeight)
            {
                int shrinkFactor = (int)Math.Ceiling(MathF.Max(sidebarBaseSize.Width / maxSidebarWidth, sidebarBaseSize.Height / maxSidebarHeight));

                sidebar = new Rectangle(
                    GraphicsDevice.Viewport.Width - (sidebarBaseSize.Width / shrinkFactor) - Math.Max(4 / shrinkFactor, 1),
                    Math.Max(4 / shrinkFactor, 1),
                    sidebarBaseSize.Width / shrinkFactor,
                    sidebarBaseSize.Height / shrinkFactor
                    );
            }
            else
            {
                int expandFactor = (int)Math.Floor(MathF.Min(maxSidebarWidth / sidebarBaseSize.Width, maxSidebarHeight / sidebarBaseSize.Height));

                sidebar = new Rectangle(
                    GraphicsDevice.Viewport.Width - (sidebarBaseSize.Width * expandFactor) - (4 * expandFactor),
                    4 * expandFactor,
                    sidebarBaseSize.Width * expandFactor,
                    sidebarBaseSize.Height * expandFactor
                    );
            }
        }

        private void TogglePause()
        {
            if (Execution.GameSpeed == GameSpeed.Pause)
                Execution.GameSpeed = GameSpeed.Normal;
            else
                Execution.GameSpeed = GameSpeed.Pause;

            PlaySound(SoundNames.DING);
        }

        private void ToggleFastForward()
        {
            if (Execution.GameSpeed == GameSpeed.FastForward)
                Execution.GameSpeed = GameSpeed.Normal;
            else
                Execution.GameSpeed = GameSpeed.FastForward;

            PlaySound(SoundNames.DING);
        }

        private void LoadReplay()
        {
            string replayPath;
            if (LoapProgress.CurrentLevel == null)
                replayPath = Path.Combine(
                    PathConstants.ReplayPath
                    );
            else
                replayPath = Path.Combine(
                    PathConstants.ReplayPath,
                    LoapProgress.CurrentLevel.Parent.FilePath
                    );

            Directory.CreateDirectory(replayPath);
            var openResult = Dialog.FileOpen(PathConstants.EXT_REPLAY.Replace(".", ""), replayPath);
            if (openResult.IsOk)
            {
                var dataItem = DataItemReader.LoadFromFile(openResult.Path);
                Execution.Engine.Replay.Clear();
                ReplayFile.LoadReplayFromDataItem(dataItem, Execution.Engine.Replay);
                LoadState(0);
            }
        }

        private void SaveReplay()
        {
            Execution.Engine.Replay.LevelTitle = Level.LevelInfo.Title;

            string replayPath;
            if (LoapProgress.CurrentLevel == null)
            {
                replayPath = Path.Combine(
                    PathConstants.ReplayPath
                    );
                Execution.Engine.Replay.LevelPath = "";
            }
            else
            {
                replayPath = Path.Combine(
                    PathConstants.ReplayPath,
                    LoapProgress.CurrentLevel.Parent.FilePath
                    );
                Execution.Engine.Replay.LevelPath = LoapProgress.CurrentLevel.FilePath;
            }

            Directory.CreateDirectory(replayPath);
            var saveResult = Dialog.FileSave(PathConstants.EXT_REPLAY.Replace(".", ""), replayPath);
            if (saveResult.IsOk)
            {
                string saveName = saveResult.Path;
                if (Path.GetExtension(saveName) == "")
                    saveName = saveName + PathConstants.EXT_REPLAY;

                DataItem replayItem = new DataItem();
                ReplayFile.SaveReplayToDataItem(Execution.Engine.Replay, replayItem);
                DataItemWriter.SaveToFile(saveName, replayItem);
                PlaySound(SoundNames.OKAY);
            }
        }

        private void AdjustSpawnRate(int change)
        {
            Execution.Engine.RecordSpawnRateChange(Execution.Engine.SpawnRate + change, !ReplayInsertMode);

            PlaySound(SoundNames.DING);

            if (change < 0)
                _LastRRMinusClickTime = GameView.ViewRunTime;
            if (change > 0)
                _LastRRPlusClickTime = GameView.ViewRunTime;
        }

        private void SelectSkill(Skill newSkill)
        {
            if (Execution.Engine.Skillset[newSkill] > 0 &&
                (newSkill != SelectedSkill || _PickingVirtualLemming || AutoAssignLemming != null))
            {
                TurnerDirection = TurnerDirection.None;
                _PickingVirtualLemming = false;
                SelectedSkill = newSkill;

                PlaySkillSelectSound(newSkill);

                if (AutoAssignLemming != null)
                {
                    if (AutoAssignLemming.MayBeAssignedSkill(SelectedSkill))
                    {
                        _SkillAnimationTimers[SelectedSkill] += float.Epsilon;

                        if (SelectedSkill == Skill.Turner)
                        {
                            PlaySound(SoundNames.OKAY, AutoAssignLemming.Position);
                            SelectedLemming = AutoAssignLemming;
                            UpdateTurnerDirection();
                        }
                        else
                        {
                            Execution.Engine.RecordAssignment(SelectedSkill, AutoAssignLemming, !ReplayInsertMode);
                            Execution.ForceOneUpdate = true;
                        }
                    }
                    else
                        PlaySound(SoundNames.UH_OH, AutoAssignLemming.Position);
                }
            }
        }

        private void ActivateNuke()
        {
            if (!Execution.Engine.NukeActive)
            {
                Execution.Engine.RecordNuke(!ReplayInsertMode);
                PlaySound(SoundNames.NUKE_ACTIVATE);
            }
        }

        public void HandleInput(float elapsedTime)
        {
            foreach (var skill in Utils.GetEnumValues<Skill>())
            {
                if (_SkillAnimationTimers[skill] > 0 || (SelectedSkill == skill && Execution.Engine.Skillset[skill] > 0 && AutoAssignLemming == null))
                    _SkillAnimationTimers[skill] += elapsedTime;

                var info = SkillPanelGraphics.PanelInfo[skill];
                var realFrameCount = Utils.GetAnimationCycleLength(info.FrameCount, info.AnimationStyle);

                if ((int)(_SkillAnimationTimers[skill] * 15) >= realFrameCount)
                {
                    if (SelectedSkill != skill || Execution.Engine.Skillset[skill] == 0 || AutoAssignLemming != null)
                        _SkillAnimationTimers[skill] = 0;
                    else
                        _SkillAnimationTimers[skill] -= realFrameCount / 15f;
                }
            }

            float moveMod = CalculateCameraMoveMod(elapsedTime);
            HandleKeyboardCameraMovement(moveMod);
            HandleMouseCameraMovement(moveMod);
            HandleKeyboardHotkeys();

            if (TurnerDirection == TurnerDirection.None)
                UpdateSelectedLemming();
            else
                UpdateTurnerDirection();

            if (UserInput.IsMouseButtonPressed(MouseButton.Left))
                HandleMouseClick(false);
            else if (UserInput.IsMouseButtonPressed(MouseButton.Right))
                HandleMouseClick(true);
        }

        private float CalculateCameraMoveMod(float elapsedTime)
        {
            float moveMod = elapsedTime * 3;

            if (UserInput.IsKeyHeld(HotkeyFunction.AdjustCameraSpeed))
            {
                var camSpeedParams = Hotkeys.GetParameters(HotkeyFunction.AdjustCameraSpeed);
                foreach (var param in camSpeedParams)
                    if (UserInput.IsKeyHeld(HotkeyFunction.AdjustCameraSpeed, param))
                    {
                        moveMod *= ((float)param / UIConstants.CAMERA_SPEED_FACTOR);
                        break;
                    }
            }

            return moveMod;
        }

        private static readonly Dictionary<Skill, string> SkillSelectSoundMapping = new Dictionary<Skill, string>
        {
            { Skill.Basher, SoundNames.SKILL_BASHER },
            { Skill.Blocker, SoundNames.SKILL_BLOCKER },
            { Skill.Bomber, SoundNames.SKILL_BOMBER },
            { Skill.Builder, SoundNames.SKILL_BUILDER },
            { Skill.Climber, SoundNames.SKILL_CLIMBER },
            { Skill.Digger, SoundNames.SKILL_DIGGER },
            { Skill.Floater, SoundNames.SKILL_FLOATER },
            { Skill.Miner, SoundNames.SKILL_MINER },
            { Skill.Turner, SoundNames.SKILL_TURNER }
        };

        private void PlaySkillSelectSound(Skill selectedSkill)
        {
            var soundName = SkillSelectSoundMapping.ContainsKey(selectedSkill)
                ? SkillSelectSoundMapping[selectedSkill]
                : SoundNames.DING;

            PlaySound(soundName);
        }

        private void HandleKeyboardHotkeys()
        {
            if (UserInput.IsKeyPressed(HotkeyFunction.ChangeCamera))
            {
                var cameraParams = Hotkeys.GetParameters(HotkeyFunction.ChangeCamera);
                foreach (var param in cameraParams)
                    if (param <= Rendering.Cameras.Count && UserInput.IsKeyPressed(HotkeyFunction.ChangeCamera, param))
                    {
                        CurrentCameraIndex = param - 1;
                        PlaySound(SoundNames.DING);
                        break;
                    }
            }
            else if (UserInput.IsKeyPressed(HotkeyFunction.CloneCamera))
            {
                var cameraParams = Hotkeys.GetParameters(HotkeyFunction.ChangeCamera);
                foreach (var param in cameraParams)
                    if (param <= Rendering.Cameras.Count + 1 && UserInput.IsKeyPressed(HotkeyFunction.CloneCamera, param))
                    {
                        if (param == Rendering.Cameras.Count + 1)
                            Rendering.Cameras.Add(new Camera(Camera));
                        else
                            Rendering.Cameras[param - 1] = new Camera(Camera);

                        CurrentCameraIndex = param - 1;
                        PlaySound(SoundNames.DING);
                        break;
                    }
            }
            else if (UserInput.IsKeyPressed(HotkeyFunction.ResetCamera))
                ResetCurrentCamera();

            if (UserInput.IsKeyPressed(HotkeyFunction.Pause))
                TogglePause();

            if (UserInput.IsKeyPressed(HotkeyFunction.FastForward))
                ToggleFastForward();

            if (UserInput.IsKeyPressed(HotkeyFunction.FrameSkip))
            {
                var skipParams = Hotkeys.GetParameters(HotkeyFunction.FrameSkip);
                foreach (var param in skipParams)
                    if (UserInput.IsKeyPressed(HotkeyFunction.FrameSkip, param))
                    {
                        if (param < 0)
                            LoadState(Execution.Engine.Iteration + param);
                        else if (param == 1)
                            Execution.ForceOneUpdate = true;
                        else if (param > 1)
                            Execution.SkipTargetIteration = Execution.Engine.Iteration + param;

                        break;
                    }
            }

            if (UserInput.IsKeyPressed(HotkeyFunction.CutNextReplayAction) || UserInput.IsKeyPressed(HotkeyFunction.CutLastReplayAction))
            {
                ReplayEntry? toCut;

                if (UserInput.IsKeyPressed(HotkeyFunction.CutNextReplayAction))
                    toCut = Execution.Engine.Replay.Actions.Where(a => a.Iteration >= Execution.Engine.Iteration)
                        .OrderBy(a => a.Iteration)
                        .FirstOrDefault();
                else
                    toCut = Execution.Engine.Replay.Actions.Where(a => a.Iteration <= Execution.Engine.Iteration)
                        .OrderByDescending(a => a.Iteration)
                        .FirstOrDefault();

                if (toCut != null)
                {
                    PlaySound(SoundNames.DING);

                    Execution.Engine.Replay.Actions.Remove(toCut);
                    if (toCut.Iteration <= Execution.Engine.Iteration)
                    {
                        _SavedStates.RemoveAll(s => s.Iteration > toCut.Iteration);
                        LoadState(Execution.Engine.Iteration);
                    }
                }
            }

            if (UserInput.IsKeyPressed(HotkeyFunction.SkipToLastAssignment))
            {
                int targetFrame;
                var actions = Execution.Engine.Replay.Actions.Where(a => (a.Action == ReplayAction.AssignSkill || a.Action == ReplayAction.Nuke) && a.Iteration < Execution.Engine.Iteration);
                if (actions.Count() == 0)
                    targetFrame = 0;
                else
                    targetFrame = actions.OrderByDescending(a => a.Iteration).First().Iteration;

                if (targetFrame < Execution.Engine.Iteration) // Just in case
                    LoadState(targetFrame);
            }
            else if (UserInput.IsKeyPressed(HotkeyFunction.SkipToNextShrugger))
            {
                Execution.SkipToShruggerBuilders = Execution.Engine.Lemmings
                    .Where(l => !l.LemmingRemoved && (l.Action == LemmingAction.Builder || l.QueuedSkill == QueuedSkill.BuilderBegin || l.QueuedSkill == QueuedSkill.BuilderContinue))
                    .ToArray();
            }

            if (UserInput.IsKeyPressed(HotkeyFunction.SelectSkill))
            {
                var skillParams = Hotkeys.GetParameters(HotkeyFunction.SelectSkill);
                foreach (var param in skillParams)
                    if (UserInput.IsKeyPressed(HotkeyFunction.SelectSkill, param))
                        SelectSkill((Skill)param);
            }

            if (UserInput.IsKeyPressed(HotkeyFunction.Nuke))
                ActivateNuke();

            if (UserInput.IsKeyPressed(HotkeyFunction.ChangeSpawnRate))
            {
                var srParams = Hotkeys.GetParameters(HotkeyFunction.ChangeSpawnRate);
                foreach (var param in srParams)
                    if (param != 0 && UserInput.IsKeyPressed(HotkeyFunction.ChangeSpawnRate, param))
                    {
                        AdjustSpawnRate(param);
                        break;
                    }
            }

            if (UserInput.IsKeyNewlyReleased(HotkeyFunction.LoadReplay))
                LoadReplay();
            if (UserInput.IsKeyNewlyReleased(HotkeyFunction.SaveReplay))
                SaveReplay();

            if (UserInput.IsKeyPressed(HotkeyFunction.Quit))
                GameView.Exit();

            if (UserInput.IsKeyPressed(HotkeyFunction.Restart))
                LoadState(0);

            if (UserInput.IsKeyPressed(HotkeyFunction.ToggleReplayInsert))
            {
                PlaySound(SoundNames.DING);
                ReplayInsertMode = !ReplayInsertMode;
            }

            if (UserInput.IsKeyPressed(HotkeyFunction.SaveState))
                CreateManualSaveState();

            if (UserInput.IsKeyPressed(HotkeyFunction.LoadState))
                LoadManualSaveState();

            if (!Level.LevelInfo.DisableMinimap && UserInput.IsKeyPressed(HotkeyFunction.ToggleMinimap))
            {
                LoapSettings.HideMinimap = !LoapSettings.HideMinimap;
                PlaySound(SoundNames.DING);
                LoapSettings.Save();
            }

            if (UserInput.IsKeyPressed(HotkeyFunction.CheatMode) && !Execution.Engine.CheatMode)
            {
                Execution.Engine.CheatMode = true;
                PlaySound(SoundNames.DING);
            }
        }

        public void CreateSavedState()
        {
            _SavedStates.RemoveAll(state => state.Iteration >= Execution.Engine.Iteration);
            _SavedStates.Add(new SavedState(Execution.Engine.GameState, Rendering.RenderState));
        }

        private void LoadState(int targetFrame, bool alsoSetTargetIteration = true)
        {
            var state = _SavedStates.LastOrDefault(s => s.Iteration <= targetFrame);

            if (state == null)
            {
                state = _SavedStates[0]; // If there is not at least one element, the bug is elsewhere, as one should be generated when GameViewUI is created.
                targetFrame = 0;
            }

            _SavedStates.RemoveAll(s => s.Iteration > state.Iteration);

            if (alsoSetTargetIteration)
            {
                if (targetFrame < Execution.Engine.Iteration)
                    Execution.GameSpeed = GameSpeed.Pause;
                Execution.SkipTargetIteration = targetFrame;
            }
            else if (state.Iteration < Execution.Engine.Iteration)
                Execution.GameSpeed = GameSpeed.Pause;

            SelectedLemming = null;
            TurnerDirection = TurnerDirection.None;
            state.Restore(Execution.Engine.GameState, Rendering.RenderState);
            ValidateVirtualAndHighlitLemming();
        }

        private void CreateManualSaveState()
        {
            _ManualSavedState = new SavedState(Execution.Engine.GameState, Rendering.RenderState);
            _ManualSavedStateReplay.Children.Clear();
            ReplayFile.SaveReplayToDataItem(Execution.Engine.Replay, _ManualSavedStateReplay);
        }

        private void LoadManualSaveState()
        {
            if (_ManualSavedState != null)
            {
                _SavedStates.RemoveAll(s => s.Iteration != 0);
                SelectedLemming = null;
                TurnerDirection = TurnerDirection.None;
                _ManualSavedState.Restore(Execution.Engine.GameState, Rendering.RenderState);
                Execution.Engine.Replay.Clear();
                ReplayFile.LoadReplayFromDataItem(_ManualSavedStateReplay, Execution.Engine.Replay);
                ValidateVirtualAndHighlitLemming();
            }
        }

        private void HandleKeyboardCameraMovement(float moveMod)
        {
            Vector3 cameraMove = new Vector3(0, 0, 0);

            if (UserInput.IsKeyHeld(HotkeyFunction.MoveCamera, Hotkeys.CAMERA_AXIS_Y))
                cameraMove += new Vector3(0, 1, 0);
            if (UserInput.IsKeyHeld(HotkeyFunction.MoveCamera, -Hotkeys.CAMERA_AXIS_Y))
                cameraMove += new Vector3(0, -1, 0);
            if (UserInput.IsKeyHeld(HotkeyFunction.MoveCamera, Hotkeys.CAMERA_AXIS_X))
                cameraMove += new Vector3(1, 0, 0);
            if (UserInput.IsKeyHeld(HotkeyFunction.MoveCamera, -Hotkeys.CAMERA_AXIS_X))
                cameraMove += new Vector3(-1, 0, 0);

            if (cameraMove.X != 0 || cameraMove.Y != 0)
            {
                cameraMove.Normalize();
                cameraMove = Vector3.Transform(cameraMove * moveMod, Matrix.CreateRotationZ(MathHelper.ToRadians(Camera.Rotation)));
            }

            if (UserInput.IsKeyHeld(HotkeyFunction.MoveCamera, Hotkeys.CAMERA_AXIS_Z))
                cameraMove += new Vector3(0, 0, moveMod);
            if (UserInput.IsKeyHeld(HotkeyFunction.MoveCamera, -Hotkeys.CAMERA_AXIS_Z))
                cameraMove += new Vector3(0, 0, -moveMod);

            if (cameraMove.X != 0 || cameraMove.Y != 0 || cameraMove.Z != 0)
                MoveCamera(cameraMove);

            if (UserInput.IsKeyHeld(HotkeyFunction.MoveCamera, Hotkeys.CAMERA_PITCH))
                Camera.Pitch += moveMod * UIConstants.CAMERA_PITCH_MOD;
            if (UserInput.IsKeyHeld(HotkeyFunction.MoveCamera, -Hotkeys.CAMERA_PITCH))
                Camera.Pitch -= moveMod * UIConstants.CAMERA_PITCH_MOD;
            if (UserInput.IsKeyHeld(HotkeyFunction.MoveCamera, Hotkeys.CAMERA_ROTATION))
                Camera.Rotation -= moveMod * UIConstants.CAMERA_ROTATION_MOD;
            if (UserInput.IsKeyHeld(HotkeyFunction.MoveCamera, -Hotkeys.CAMERA_ROTATION))
                Camera.Rotation += moveMod * UIConstants.CAMERA_ROTATION_MOD;
        }

        private void HandleMouseCameraMovement(float moveMod)
        {
            float mouseMoveX = UserInput.MouseDelta.X;
            float mouseMoveY = UserInput.MouseDelta.Y;

            Vector3 cameraMove = new Vector3(
                mouseMoveX * (LoapSettings.InvertMousePanX ? -1 : 1),
                UserInput.MouseScrollDelta * (LoapSettings.InvertMousePanY ? -1 : 1),
                -mouseMoveY * (LoapSettings.InvertMousePanZ ? -1 : 1));

            if (!UserInput.IsMouseButtonHeld(MouseButton.Middle))
            {
                cameraMove.X = 0;
                cameraMove.Z = 0;
                // but *not* Y
            }

            if (cameraMove.X != 0 || cameraMove.Y != 0 || cameraMove.Z != 0)
            {
                cameraMove = new Vector3(
                    cameraMove.X * UIConstants.CAMERA_MOUSE_MOVEMENT_XZ_MOD * LoapSettings.MouseCameraSensitivityXZPan,
                    cameraMove.Y * UIConstants.CAMERA_MOUSE_MOVEMENT_Y_MOD * LoapSettings.MouseCameraSensitivityYPan,
                    cameraMove.Z * UIConstants.CAMERA_MOUSE_MOVEMENT_XZ_MOD * LoapSettings.MouseCameraSensitivityXZPan
                    );
                cameraMove = Vector3.Transform(cameraMove * moveMod, Matrix.CreateRotationZ(MathHelper.ToRadians(Camera.Rotation)));

                MoveCamera(cameraMove);
            }

            if (UserInput.IsMouseButtonHeld(MouseButton.Right))
            {
                Camera.Rotation += -mouseMoveX * moveMod * UIConstants.CAMERA_ROTATION_MOD * UIConstants.CAMERA_MOUSE_ROTATION_MOD * LoapSettings.MouseCameraSensitivityRotate * (LoapSettings.InvertMouseRotate ? -1 : 1);
                Camera.Pitch += -mouseMoveY * moveMod * UIConstants.CAMERA_PITCH_MOD * UIConstants.CAMERA_MOUSE_ROTATION_MOD * LoapSettings.MouseCameraSensitivityRotate * (LoapSettings.InvertMouseTilt ? -1 : 1);
            }

            if ((UserInput.IsMouseButtonHeld(MouseButton.Middle) && Camera.VirtualLemming == null) || UserInput.IsMouseButtonHeld(MouseButton.Right))
                UserInput.LockMouse();
        }

        private void ResetCurrentCamera()
        {
            if (CurrentCameraIndex > Level.LevelInfo.Cameras.Count)
                PlaySound(SoundNames.UH_OH);
            else
            {
                PlaySound(SoundNames.DING);
                var sourceCamera = Level.LevelInfo.Cameras[CurrentCameraIndex];
                Camera.VirtualLemming = null;
                Camera.Position = new Vector3(sourceCamera.X, sourceCamera.Y, sourceCamera.Z);
                Camera.Rotation = sourceCamera.Rotation;
                Camera.Pitch = sourceCamera.Pitch;
            }
        }

        private void MoveCamera(Vector3 delta)
        {
            if (Camera.VirtualLemming != null)
                return;

            int stepCount = (int)(delta.Length() / PhysicsConstants.STEP_SIZE_FLOAT);
            Vector3 step = delta / stepCount;

            for (int i = 0; i < stepCount; i++)
            {
                bool mayMoveX = MayMoveCamera(new Vector3(step.X, 0, 0));
                bool mayMoveY = MayMoveCamera(new Vector3(0, step.Y, 0));
                bool mayMoveBoth = MayMoveCamera(new Vector3(step.X, step.Y, 0));

                if (!mayMoveX && (Math.Abs(step.X) > Math.Abs(step.Y * 3 / 4)))
                    break;
                if (!mayMoveY && (Math.Abs(step.Y) > Math.Abs(step.X * 3 / 4)))
                    break;

                if (mayMoveX && mayMoveY && mayMoveBoth)
                    Camera.Position += new Vector3(step.X, step.Y, 0);
                else if (mayMoveX && !mayMoveY)
                    Camera.Position += new Vector3(step.X, 0, 0);
                else if (mayMoveY && !mayMoveX)
                    Camera.Position += new Vector3(0, step.Y, 0);
                else if (mayMoveX && mayMoveY)
                {
                    float segmentAngle = Utils.ModuloNegativeAdjusted(Camera.Rotation, 180);
                    if (segmentAngle < 45 || segmentAngle >= 135)
                        Camera.Position += new Vector3(0, step.Y, 0);
                    else
                        Camera.Position += new Vector3(step.X, 0, 0);
                }

                if (MayMoveCamera(new Vector3(0, 0, step.Z)))
                    Camera.Position += new Vector3(0, 0, step.Z);
            }
        }

        private bool MayMoveCamera(Vector3 offset)
        {
            var newPos = Camera.Position + offset;
            if (newPos.X < -UIConstants.CAMERA_MAX_DISTANCE_XY ||
                newPos.Y < -UIConstants.CAMERA_MAX_DISTANCE_XY ||
                newPos.Z < 0)
                return false;

            if (newPos.X > Level.BlockStructure.Width + UIConstants.CAMERA_MAX_DISTANCE_XY ||
                newPos.Y > Level.BlockStructure.Depth + UIConstants.CAMERA_MAX_DISTANCE_XY ||
                newPos.Z > (Level.BlockStructure.Height / 4) + UIConstants.CAMERA_MAX_DISTANCE_Z)
                return false;

            if (UserInput.IsKeyHeld(HotkeyFunction.PhaseCamera))
                return true;

            for (int zOff = (offset.Z <= 0 ? -1 : 0); zOff <= (offset.Z >= 0 ? 1 : 0); zOff++)
                for (int yOff = (offset.Y <= 0 ? -1 : 0); yOff <= (offset.Y >= 0 ? 1 : 0); yOff++)
                    for (int xOff = (offset.X <= 0 ? -1 : 0); xOff <= (offset.X >= 0 ? 1 : 0); xOff++)
                    {
                        if (xOff == 0 && yOff == 0 && zOff == 0)
                            continue;

                        var newPosBlocks = FloatPosToUnitPos(
                            Camera.Position.X + offset.X + (UIConstants.CAMERA_SOLIDITY_CHECK_OFFSET * xOff),
                            Camera.Position.Y + offset.Y + (UIConstants.CAMERA_SOLIDITY_CHECK_OFFSET * yOff),
                            Camera.Position.Z + offset.Z + (UIConstants.CAMERA_SOLIDITY_CHECK_OFFSET * zOff)
                            );

                        if (Blocks.IsSolidAt(newPosBlocks.X, newPosBlocks.Y, newPosBlocks.Z, true))
                            return false;
                    }

            return true;
        }

        private Point3D FloatPosToUnitPos(float x, float y, float z)
        {
            return new Point3D(
                    (int)Math.Round(x / PhysicsConstants.STEP_SIZE_FLOAT),
                    (int)Math.Round(y / PhysicsConstants.STEP_SIZE_FLOAT),
                    (int)Math.Round(z / PhysicsConstants.STEP_SIZE_FLOAT)
                );
        }

        public void RenderOverlays()
        {
            RenderSidebar();
            RenderPanel();
        }

        public void RenderSidebar()
        {
            GraphicsDevice.SetRenderTarget(SidebarCanvas);
            GraphicsDevice.Clear(Color.Transparent);
            _SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);

            int sidebarY = 0;

            WriteSidebarText(GameTexts.LEMMINGS_IN, Color.White, new Point(0, sidebarY), -1);
            WriteSidebarText(Execution.Engine.LemmingsToSpawn.ToString(), Color.White, new Point(SIDEBAR_DESIGN_WIDTH - 1, sidebarY), 1);
            sidebarY += Font.CharacterHeight + 5;

            Color lemmingCountColor = (Execution.Engine.LemmingsAlive + Execution.Engine.LemmingsRescued < Level.LevelInfo.SaveRequirement) ?
                UIConstants.NOT_ENOUGH_LEMMINGS_COLOR : Color.White;
            WriteSidebarText(GameTexts.LEMMINGS_LIVE, Color.White, new Point(0, sidebarY), -1);
            WriteSidebarText((Execution.Engine.LemmingsAlive).ToString(), lemmingCountColor, new Point(SIDEBAR_DESIGN_WIDTH - 1, sidebarY), 1);
            sidebarY += Font.CharacterHeight + 5;

            Color saveAmountColor = (Execution.Engine.LemmingsNeededToRescue <= 0) ?
                UIConstants.SAVE_REQUIREMENT_MET_COLOR : Color.White;
            WriteSidebarText(GameTexts.LEMMINGS_GOAL, Color.White, new Point(0, sidebarY), -1);
            WriteSidebarText((-Execution.Engine.LemmingsNeededToRescue).ToString(), saveAmountColor, new Point(SIDEBAR_DESIGN_WIDTH - 1, sidebarY), 1);
            sidebarY += Font.CharacterHeight + 5;

            if (Level.LevelInfo.TimeLimit == 0)
            {
                _SpriteBatch.Draw(_SidebarGraphics, new Rectangle(0, sidebarY, 48, 48), new Rectangle(0, 768, 48, 48), Color.White);
                int timeValue = Execution.Engine.Iteration / PhysicsConstants.ITERATIONS_PER_INGAME_SECOND;

                WriteSidebarText((timeValue / 60).ToString() + ":" + (timeValue % 60).ToString("D2"),
                     Color.White, new Point(SIDEBAR_DESIGN_WIDTH - 1, sidebarY + 6), 1);
            }
            else
            {
                _SpriteBatch.Draw(_SidebarGraphics, new Rectangle(0, sidebarY, 48, 48), new Rectangle(0, 720, 48, 48), Color.White);
                int timeValue = Level.LevelInfo.TimeLimit - (Execution.Engine.Iteration / PhysicsConstants.ITERATIONS_PER_INGAME_SECOND);

                Color timeDrawColor = timeValue > 30 ? Color.Yellow :
                    timeValue >= 0 ? Color.DarkRed :
                    Color.Purple;

                if (timeValue > 0)
                    WriteSidebarText((timeValue / 60).ToString() + ":" + (timeValue % 60).ToString("D2"),
                        timeDrawColor, new Point(SIDEBAR_DESIGN_WIDTH - 1, sidebarY + 6), 1);
                else if (timeValue == 0)
                    WriteSidebarText("0:00", timeDrawColor, new Point(SIDEBAR_DESIGN_WIDTH - 1, sidebarY + 6), 1);
                else
                {
                    timeValue = Math.Abs(timeValue);
                    WriteSidebarText("-" + (timeValue / 60).ToString() + ":" + (timeValue % 60).ToString("D2"),
                        timeDrawColor, new Point(SIDEBAR_DESIGN_WIDTH - 1, sidebarY + 6), 1);
                }
            }

            sidebarY += 48 + 5;

            int iconsLeft = SIDEBAR_DESIGN_WIDTH - 144;

            // VL, Highlight

            int vlOffset = (Camera.VirtualLemming != null || _PickingVirtualLemming) ? 72 : 0;

            _SpriteBatch.Draw(_SidebarGraphics, new Rectangle(iconsLeft, sidebarY, 72, 72), new Rectangle(0, 72 + vlOffset, 72, 72), Color.White);

            int highlightOffset = (Camera.VirtualLemming != null) ? 72 :
                (HighlitLemming == null) ? 0 :
                Utils.ModuloNegativeAdjusted((int)(GameView.ViewRunTime * PhysicsConstants.ITERATIONS_PER_INGAME_SECOND), 6) * 72 + 72;

            _SpriteBatch.Draw(_SidebarGraphics, new Rectangle(iconsLeft + 72, sidebarY, 72, 72), new Rectangle(0, 216 + highlightOffset, 72, 72), Color.White);

            sidebarY += 72;

            // Nuke, FF

            int nukeOffset = Execution.Engine.NukeActive ?
                Utils.ModuloNegativeAdjusted((int)(GameView.ViewRunTime * PhysicsConstants.ITERATIONS_PER_INGAME_SECOND), 11) * 72 + 72 :
                0;

            _SpriteBatch.Draw(_SidebarGraphics, new Rectangle(iconsLeft, sidebarY, 72, 72), new Rectangle(144, nukeOffset, 72, 72), Color.White);
            _SpriteBatch.Draw(_SidebarGraphics, new Rectangle(iconsLeft + 72, sidebarY, 72, 72), new Rectangle(0, 816 + (Execution.GameSpeed == GameSpeed.FastForward ? 72 : 0), 72, 72), Color.White);

            sidebarY += 72;

            // Camera, Pause

            int pauseOffset = Execution.GameSpeed == GameSpeed.Pause ?
                Utils.ModuloNegativeAdjusted((int)(GameView.ViewRunTime * PhysicsConstants.ITERATIONS_PER_INGAME_SECOND), 14) * 72 :
                0;

            _SpriteBatch.Draw(_SidebarGraphics, new Rectangle(iconsLeft, sidebarY, 72, 72), new Rectangle(0, 0, 72, 72), Color.White);
            _SpriteBatch.Draw(_SidebarGraphics, new Rectangle(iconsLeft + 72, sidebarY, 72, 72), new Rectangle(72, pauseOffset, 72, 72), Color.White);

            WriteSidebarText((CurrentCameraIndex + 1).ToString(), Color.White, new Point(iconsLeft + 68, sidebarY + 32), 1);

            sidebarY += 72;

            // RR buttons

            _SpriteBatch.Draw(_SidebarGraphics, new Rectangle(iconsLeft, sidebarY, 72, 72), new Rectangle(0, 960 + (_LastRRMinusClickTime > GameView.ViewRunTime - UIConstants.RR_BUTTON_VISUAL_EFFECT_TIME ? 72 : 0), 72, 72), Color.White);
            _SpriteBatch.Draw(_SidebarGraphics, new Rectangle(iconsLeft + 72, sidebarY, 72, 72), new Rectangle(144, 864 + (_LastRRPlusClickTime > GameView.ViewRunTime - UIConstants.RR_BUTTON_VISUAL_EFFECT_TIME ? 72 : 0), 72, 72), Color.White);

            sidebarY += 72;

            // RR text

            WriteSidebarText(Level.LevelInfo.SpawnRate.ToString(), Color.White, new Point(iconsLeft + 36, sidebarY), 0);
            WriteSidebarText(Execution.Engine.SpawnRate.ToString(), Color.White, new Point(iconsLeft + 72 + 36, sidebarY), 0);


            _SpriteBatch.End();
        }

        public void RenderPanel()
        {
            GraphicsDevice.SetRenderTarget(SkillPanelCanvas);
            GraphicsDevice.Clear(Color.Transparent);

            _SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);

            if (TurnerDirection == TurnerDirection.None && SelectedLemming != null)
            {
                var localAction = SelectedLemming.Action;
                if (localAction == LemmingAction.Walker)
                    localAction = SelectedLemming.QueuedSkill switch
                    {
                        QueuedSkill.None => LemmingAction.Walker,
                        QueuedSkill.Blocker => LemmingAction.Blocker,
                        QueuedSkill.Turner => LemmingAction.Turner,
                        QueuedSkill.BuilderBegin => LemmingAction.Builder,
                        QueuedSkill.BuilderContinue => LemmingAction.Builder,
                        QueuedSkill.Shrugger => LemmingAction.Shrugger,
                        QueuedSkill.BasherBegin => LemmingAction.Basher,
                        QueuedSkill.BasherOverflow => LemmingAction.Basher,
                        QueuedSkill.BasherContinue => LemmingAction.Basher,
                        QueuedSkill.MinerBegin => LemmingAction.Miner,
                        QueuedSkill.MinerContinue => LemmingAction.Miner,
                        QueuedSkill.Digger => LemmingAction.Digger,
                        _ => throw new NotImplementedException()
                    };

                Font.WriteText(_SpriteBatch,
                    localAction.ToString().ToUpperInvariant() + (
                        SelectedLemming.IsClimber && SelectedLemming.IsFloater ? " cf" :
                        SelectedLemming.IsClimber ? " c" :
                        SelectedLemming.IsFloater ? " f" :
                        ""
                        ),
                    new Point(32, 0), Color.White
                    );
            }

            int x = 0;
            foreach (Skill skill in Utils.GetEnumValues<Skill>())
            {
                var info = SkillPanelGraphics.PanelInfo[skill];
                Rectangle srcRect = info.FirstFrame;

                int skillCount = Execution.Engine.Skillset[skill];

                int frame = Utils.CalculateAnimationFrameIndex((int)(_SkillAnimationTimers[skill] * 15), info.FrameCount, info.AnimationStyle);
                srcRect.Y += frame * srcRect.Height;

                Rectangle dstRect = new Rectangle(
                    x,
                    SkillPanelCanvas.Height - srcRect.Height,
                    srcRect.Width,
                    srcRect.Height
                    );

                _SpriteBatch.Draw(_SkillPanelGraphics, dstRect, srcRect, Color.White);

                if (skillCount > 0)
                    Font.WriteText(_SpriteBatch, skillCount.ToString(), new Point(dstRect.Right - 12, dstRect.Bottom - 12), Color.White, 1, 1);

                x += info.FirstFrame.Width;
            }

            _SpriteBatch.End();
        }

        private void WriteSidebarText(string text, Color color, Point pos, int alignment)
        {
            Font.WriteText(_SpriteBatch, text, pos, color, alignment);
        }

        private void HandleSidebarClick(bool isRightButton)
        {
            Point localMouse = new Point(
                (UserInput.MousePosition.X - SidebarRect.Left) * SIDEBAR_DESIGN_WIDTH / SidebarRect.Width,
                (UserInput.MousePosition.Y - SidebarRect.Top) * SIDEBAR_DESIGN_HEIGHT / SidebarRect.Height
                );

            // Button rects start at (SIDEBAR_DESIGN_WIDTH - 144, 171), not accounting for downsizing

            int buttonsLeft = SIDEBAR_DESIGN_WIDTH - 144;

            if (new Rectangle(buttonsLeft + 6, 171 + 6, 60, 60).Contains(localMouse) && !isRightButton)
            {
                // Virtual Lemming
                if (Camera.VirtualLemming != null)
                    Camera.VirtualLemming = null;
                else if (_PickingVirtualLemming)
                    _PickingVirtualLemming = false;
                else if (HighlitLemming != null)
                    Camera.VirtualLemming = HighlitLemming;
                else
                    _PickingVirtualLemming = true;

                PlaySound(SoundNames.DING);
            }

            if (new Rectangle(buttonsLeft + 72 + 6, 171 + 6, 60, 60).Contains(localMouse) && !isRightButton)
            {
                // Highlight
                if (Camera.VirtualLemming != null)
                {
                    int lemmingIndex = (Execution.Engine.Lemmings.IndexOf(Camera.VirtualLemming) + 1) % Execution.Engine.Lemmings.Count;
                    Camera.VirtualLemming = Execution.Engine.Lemmings[lemmingIndex];
                }
                else if (HighlitLemming != null)
                    HighlitLemming = null;
                else
                {
                    foreach (var action in Execution.Engine.Replay.Actions.Where(a => a.Action == ReplayAction.AssignSkill).Reverse())
                    {
                        HighlitLemming = Execution.Engine.Lemmings.FirstOrDefault(lem => lem.Identifier == action.LemmingIdentifier);
                        if (HighlitLemming != null)
                            break;
                    }

                    if (HighlitLemming == null && Execution.Engine.Lemmings.Count > 0)
                        HighlitLemming = Execution.Engine.Lemmings[0];
                }

                PlaySound(SoundNames.DING);
            }

            if (new Rectangle(buttonsLeft + 6, 243 + 6, 60, 60).Contains(localMouse) && !isRightButton)
            {
                // Nuke
                if (GameView.ViewRunTime - _LastNukeClickTime < UIConstants.NUKE_DOUBLE_CLICK_TIME)
                    ActivateNuke();
                else
                    _LastNukeClickTime = GameView.ViewRunTime;
            }

            if (new Rectangle(buttonsLeft + 72 + 6, 243 + 6, 60, 60).Contains(localMouse) && !isRightButton)
                ToggleFastForward();

            if (new Rectangle(buttonsLeft + 6, 315 + 6, 60, 60).Contains(localMouse))
            {
                // Camera
                if (isRightButton)
                    ResetCurrentCamera();
                else
                {
                    CurrentCameraIndex++;
                    PlaySound(SoundNames.DING);
                }
            }

            if (new Rectangle(buttonsLeft + 72 + 6, 315 + 6, 60, 60).Contains(localMouse) && !isRightButton)
                TogglePause();

            if (new Rectangle(buttonsLeft + 6, 387 + 6, 60, 60).Contains(localMouse))
            {
                // RR Minus
                if (isRightButton)
                    AdjustSpawnRate(-10);
                else
                    AdjustSpawnRate(-1);
            }

            if (new Rectangle(buttonsLeft + 72 + 6, 387 + 6, 60, 60).Contains(localMouse))
            {
                // RR Plus
                if (isRightButton)
                    AdjustSpawnRate(10);
                else
                    AdjustSpawnRate(1);
            }
        }

        private void HandleMouseClick(bool isRightButton)
        {
            bool handledByOverlay = false;

            if (!Level.LevelInfo.DisableMinimap && Minimap.ToggleButtonRect.Contains(UserInput.MousePosition) && !isRightButton)
            {
                handledByOverlay = true;
                LoapSettings.HideMinimap = !LoapSettings.HideMinimap;
                PlaySound(SoundNames.DING);
                LoapSettings.Save();
            }
            else if (SidebarRect.Contains(UserInput.MousePosition))
            {
                HandleSidebarClick(isRightButton);
                handledByOverlay = true;
                TurnerDirection = TurnerDirection.None;
            }
            else if (SkillPanelRect.Contains(UserInput.MousePosition) && !isRightButton)
            {
                var newSkill = DetermineSkillIconAtMousePosition();
                if (newSkill.HasValue)
                {
                    TurnerDirection = TurnerDirection.None;
                    SelectSkill(newSkill.Value);
                    handledByOverlay = true;
                }
            }

            if (!handledByOverlay && !isRightButton)
            {
                if (SelectedLemming != null)
                {
                    if (_PickingVirtualLemming && TurnerDirection == TurnerDirection.None)
                    {
                        Camera.VirtualLemming = SelectedLemming;
                        _PickingVirtualLemming = false;
                    }
                    if (Camera.VirtualLemming != null && TurnerDirection == TurnerDirection.None)
                        Camera.VirtualLemming = SelectedLemming;
                    else if (HighlitLemming != null && TurnerDirection == TurnerDirection.None)
                        HighlitLemming = SelectedLemming;
                    else if (SelectedLemming.MayBeAssignedSkill(SelectedSkill))
                    {
                        if (TurnerDirection != TurnerDirection.None)
                        {
                            Execution.Engine.RecordAssignment(Skill.Turner, SelectedLemming, !ReplayInsertMode, TurnerDirection);
                            Execution.ForceOneUpdate = true;
                            TurnerDirection = TurnerDirection.None;
                        }
                        else if (SelectedSkill == Skill.Turner)
                        {
                            PlaySound(SoundNames.OKAY, SelectedLemming.Position);
                            UpdateTurnerDirection();
                        }
                        else
                        {
                            Execution.Engine.RecordAssignment(SelectedSkill, SelectedLemming, !ReplayInsertMode);
                            Execution.ForceOneUpdate = true;
                        }
                    }
                    else
                        PlaySound(SoundNames.UH_OH, SelectedLemming.Position);
                }
                else if (!ReplayInsertMode)
                    Execution.Engine.Replay.Actions.RemoveAll(a => a.Iteration > Execution.Engine.Iteration || (a.Iteration == Execution.Engine.Iteration && a.Action == ReplayAction.AssignSkill));
            }
        }

        private Skill? DetermineSkillIconAtMousePosition()
        {
            int modX = (int)MathF.Round((UserInput.MousePosition.X - SkillPanelRect.X) * SkillPanelGraphics.TotalSkillIconWidth / (float)SkillPanelRect.Width);
            int modY = (int)MathF.Round((UserInput.MousePosition.Y - SkillPanelRect.Y) * SkillPanelGraphics.MaxSkillIconHeight / (float)SkillPanelRect.Height);

            foreach (Skill skill in Utils.GetEnumValues<Skill>())
            {
                var info = SkillPanelGraphics.PanelInfo[skill];
                Rectangle modRect = info.FirstFrame;
                modRect.Y = SkillPanelGraphics.MaxSkillIconHeight - modRect.Height;
                if (modRect.Contains(modX, modY))
                    return skill;
            }

            return null;
        }

        private void UpdateTurnerDirection()
        {
            // We also calculate the position / frame of the graphic here, as it saves doubling the effort.

            if (SelectedLemming == null || SelectedLemming.LemmingRemoved || !SelectedLemming.MayBeAssignedSkill(Skill.Turner))
            {
                UpdateSelectedLemming();
                TurnerDirection = TurnerDirection.None;
            }
            else
            {
                if (TurnerDirection == TurnerDirection.None)
                    TurnerDirection = TurnerDirection.Left; // Fallback

                Vector3 lemmingPos = new Vector3(
                    SelectedLemming.X * PhysicsConstants.STEP_SIZE_FLOAT,
                    SelectedLemming.Y * PhysicsConstants.STEP_SIZE_FLOAT,
                    SelectedLemming.Z * PhysicsConstants.STEP_SIZE_FLOAT + PhysicsConstants.SEGMENT_SIZE_FLOAT
                    );

                Vector3 lemmingPosProjected = (Camera.VirtualLemming != null) ?
                    new Vector3(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2, GraphicsDevice.Viewport.MaxDepth) :
                    GraphicsDevice.Viewport.Project(lemmingPos, Rendering.Renderer.ProjectionMatrix, Camera.ViewMatrix, Matrix.Identity);

                if (GraphicsDevice.Viewport.Bounds.Contains(new Vector2(lemmingPosProjected.X, lemmingPosProjected.Y)))
                {
                    float dividerRotation = Utils.ModuloNegativeAdjusted(-(Camera.Rotation - Utils.DirectionToDegrees(SelectedLemming.Direction)), 360);

                    Vector2 center = new Vector2(lemmingPosProjected.X, lemmingPosProjected.Y);
                    Vector2 slope = Vector2.Transform(new Vector2(0, -96), Matrix.CreateRotationZ(MathHelper.ToRadians(-dividerRotation)));

                    slope = new Vector2(MathF.Round(slope.X * 32) / 32, MathF.Round(slope.Y * 32) / 32);

                    if (slope.Y == 0)
                    {
                        if (dividerRotation < 180)
                        {
                            if (UserInput.MousePosition.Y < center.Y)
                                TurnerDirection = TurnerDirection.Right;
                            else
                                TurnerDirection = TurnerDirection.Left;
                        }
                        else
                        {
                            if (UserInput.MousePosition.Y < center.Y)
                                TurnerDirection = TurnerDirection.Left;
                            else
                                TurnerDirection = TurnerDirection.Right;
                        }
                    }
                    else if (slope.X == 0)
                    {
                        if (dividerRotation < 90 || dividerRotation > 270)
                        {
                            if (UserInput.MousePosition.X < center.X)
                                TurnerDirection = TurnerDirection.Left;
                            else
                                TurnerDirection = TurnerDirection.Right;
                        }
                        else
                        {
                            if (UserInput.MousePosition.X < center.X)
                                TurnerDirection = TurnerDirection.Right;
                            else
                                TurnerDirection = TurnerDirection.Left;

                        }
                    }
                    else
                    {
                        float slopePerX = slope.Y / slope.X;
                        float lineYAtMouse = (slopePerX * (UserInput.MousePosition.X - center.X)) + center.Y;
                        if (dividerRotation < 180)
                        {
                            if (UserInput.MousePosition.Y < lineYAtMouse)
                                TurnerDirection = TurnerDirection.Right;
                            else
                                TurnerDirection = TurnerDirection.Left;
                        }
                        else
                        {
                            if (UserInput.MousePosition.Y < lineYAtMouse)
                                TurnerDirection = TurnerDirection.Left;
                            else
                                TurnerDirection = TurnerDirection.Right;
                        }
                    }

                    float turnerGraphicAngle = -dividerRotation + (TurnerDirection == TurnerDirection.Left ? -90 : 90);
                    float turnerArrowDist = Math.Clamp(96 - MathF.Pow((lemmingPos - Camera.Position).Length() / 2, 2), 32, 96);
                    TurnerGraphicPos = (center + Vector2.Transform(new Vector2(0, -turnerArrowDist), Matrix.CreateRotationZ(MathHelper.ToRadians(turnerGraphicAngle)))).ToPoint();
                    TurnerGraphicFrame = (int)Math.Round(Utils.ModuloNegativeAdjusted(-turnerGraphicAngle + 5.625f, 360) / 11.25f) % 32;
                }
                else
                {
                    UpdateSelectedLemming();
                    TurnerDirection = TurnerDirection.None;
                }
            }
        }

        private enum ForceWalkerOption { Neither, ForceWalker, ForceNonWalker }

        private bool CheckForceWalkerMatch(Lemming lemming, ForceWalkerOption option)
        {
            if (option == ForceWalkerOption.ForceWalker)
            {
                switch (lemming.Action)
                {
                    case LemmingAction.Walker:
                        return (lemming.QueuedSkill == QueuedSkill.None || lemming.QueuedSkill == QueuedSkill.Shrugger);
                    case LemmingAction.Shrugger:
                    case LemmingAction.Slider:
                        return true;
                    case LemmingAction.Faller:
                    case LemmingAction.Stunned:
                    case LemmingAction.Blocker:
                    case LemmingAction.Turner:
                    case LemmingAction.Builder:
                    case LemmingAction.Basher:
                    case LemmingAction.Miner:
                    case LemmingAction.Digger:
                    case LemmingAction.Climber:
                    case LemmingAction.Floater:
                    case LemmingAction.Drowner:
                    case LemmingAction.Zapper:
                    case LemmingAction.DeathSpin:
                    case LemmingAction.Bomber:
                    case LemmingAction.Exiter:
                    case LemmingAction.Warper:
                    case LemmingAction.Zipper:
                    case LemmingAction.Springer:
                    case LemmingAction.Bouncer:
                        return false;
                    default:
                        throw new NotImplementedException();
                }
            }

            if (option == ForceWalkerOption.ForceNonWalker)
            {
                switch (lemming.Action)
                {
                    case LemmingAction.Walker:
                        return lemming.QueuedSkill != QueuedSkill.None;
                    case LemmingAction.Faller:
                    case LemmingAction.Blocker:
                    case LemmingAction.Turner:
                    case LemmingAction.Builder:
                    case LemmingAction.Shrugger:
                    case LemmingAction.Basher:
                    case LemmingAction.Miner:
                    case LemmingAction.Digger:
                    case LemmingAction.Climber:
                    case LemmingAction.Floater:
                    case LemmingAction.Warper:
                    case LemmingAction.Zipper:
                    case LemmingAction.Springer:
                    case LemmingAction.Bouncer:
                        return true;
                    case LemmingAction.Slider:
                    case LemmingAction.Stunned:
                    case LemmingAction.Drowner:
                    case LemmingAction.Zapper:
                    case LemmingAction.DeathSpin:
                    case LemmingAction.Bomber:
                    case LemmingAction.Exiter:
                        return false;
                    default:
                        throw new NotImplementedException();
                }
            }

            return true;
        }

        private void UpdateSelectedLemming()
        {
            var viewport = GraphicsDevice.Viewport;
            var cameraMatrix = Camera.ViewMatrix;
            var projectionMatrix = Rendering.Renderer.ProjectionMatrix;

            Vector3 mouseVector = new Vector3(UserInput.MousePosition.X, UserInput.MousePosition.Y, viewport.MinDepth);

            if (Minimap.Visible && Minimap.MinimapRect.Contains(new Vector2(mouseVector.X, mouseVector.Y)))
            {
                SelectedLemming = null;
                return;
            }

            if (!Level.LevelInfo.DisableMinimap && Minimap.ToggleButtonRect.Contains(new Vector2(mouseVector.X, mouseVector.Y)))
            {
                SelectedLemming = null;
                return;
            }

            Matrix matchCameraRotation = Matrix.CreateRotationZ(MathHelper.ToRadians(Camera.Rotation));
            var offsetVert = new Vector3(0f, 0f, 0.25f);
            var offsetHorz = Vector3.Transform(new Vector3(-0.1875f, 0f, 0f), matchCameraRotation);

            Lemming? bestLemming = null;
            float maxDepthDiff = viewport.MaxDepth;
            float minDepthXYDiff = float.PositiveInfinity;

            Vector3 cameraReferencePos = (Camera.VirtualLemming == null) ?
                Camera.Position : new Vector3(
                    Camera.VirtualLemming.X * PhysicsConstants.STEP_SIZE_FLOAT,
                    Camera.VirtualLemming.Y * PhysicsConstants.STEP_SIZE_FLOAT,
                    Camera.VirtualLemming.Z * PhysicsConstants.STEP_SIZE_FLOAT
                    );

            ForceWalkerOption forceWalkerOption = UserInput.IsKeyHeld(HotkeyFunction.ForceSelectWalker) ? ForceWalkerOption.ForceWalker :
                UserInput.IsKeyHeld(HotkeyFunction.ForceSelectNonWalker) ? ForceWalkerOption.ForceNonWalker :
                ForceWalkerOption.Neither;

            foreach (var lemming in Execution.Engine.Lemmings)
            {
                var lemPos = new Vector3(
                        lemming.Position.X * PhysicsConstants.STEP_SIZE_FLOAT,
                        lemming.Position.Y * PhysicsConstants.STEP_SIZE_FLOAT,
                        lemming.Position.Z * PhysicsConstants.STEP_SIZE_FLOAT
                        );

                if (!CheckForceWalkerMatch(lemming, forceWalkerOption))
                    continue;

                if (lemming.Action == LemmingAction.Zipper)
                {
                    var srcPos = lemming.TransporterSource; // We only need the gradient, not the exact positions, so no need to convert to units
                    var dstPos = lemming.TransporterTarget;

                    float srcPosXY;
                    float dstPosXY;

                    if (srcPos.X == dstPos.X)
                    {
                        srcPosXY = srcPos.Y;
                        dstPosXY = dstPos.Y;
                    }
                    else if (srcPos.Y == dstPos.Y)
                    {
                        srcPosXY = srcPos.X;
                        dstPosXY = dstPos.X;
                    }
                    else
                    {
                        srcPosXY = MathF.Sqrt((srcPos.X * srcPos.X) + (srcPos.Y * srcPos.Y));
                        dstPosXY = MathF.Sqrt((dstPos.X * dstPos.X) + (dstPos.Y * dstPos.Y));
                    }

                    var srcPosZ = srcPos.Z / (float)PhysicsConstants.BLOCK_SEGMENTS;
                    var dstPosZ = dstPos.Z / (float)PhysicsConstants.BLOCK_SEGMENTS;

                    if (srcPosZ == dstPosZ)
                        lemPos.Z += PhysicsConstants.SEGMENT_SIZE_FLOAT * 2.25f;
                    else
                    {
                        float slope = MathF.Abs((dstPosXY - srcPosXY) / (dstPosZ - srcPosZ));
                        float offset = slope - 1;

                        if (dstPosZ > srcPosZ)
                            offset += 0.5f;

                        lemPos.Z += PhysicsConstants.SEGMENT_SIZE_FLOAT * offset;
                    }
                }

                if (!Rendering.IsLemmingInFrontOfCamera(cameraReferencePos, lemPos))
                    continue;

                var thisDepthVector = viewport.Project(offsetVert + lemPos, projectionMatrix, cameraMatrix, Matrix.Identity);

                if (!viewport.Bounds.Contains(new Vector2(thisDepthVector.X, thisDepthVector.Y)))
                    continue;

                var footDepthVector = viewport.Project(lemPos, projectionMatrix, cameraMatrix, Matrix.Identity);
                var sideDepthVector = viewport.Project(offsetHorz + lemPos, projectionMatrix, cameraMatrix, Matrix.Identity);

                float sideDist = Math.Abs(sideDepthVector.X - thisDepthVector.X);
                float vertDist = Math.Abs(footDepthVector.Y - thisDepthVector.Y);

                thisDepthVector -= mouseVector;

                if (Math.Abs(thisDepthVector.X) <= sideDist && Math.Abs(thisDepthVector.Y) <= vertDist)
                {
                    if (thisDepthVector.Z < maxDepthDiff)
                    {
                        bestLemming = lemming;
                        maxDepthDiff = thisDepthVector.Z;
                        minDepthXYDiff = new Vector2(thisDepthVector.X, thisDepthVector.Y).Length();
                    }
                    else if (thisDepthVector.Z == maxDepthDiff)
                    {
                        float diagonal = new Vector2(thisDepthVector.X, thisDepthVector.Y).Length();
                        if (diagonal < minDepthXYDiff)
                        {
                            bestLemming = lemming;
                            maxDepthDiff = thisDepthVector.Z;
                            minDepthXYDiff = diagonal;
                        }
                    }
                }
            }

            SelectedLemming = bestLemming;
        }
    }
}
