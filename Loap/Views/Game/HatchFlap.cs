﻿using LoapCore.Blocks;
using LoapCore.Misc;
using Microsoft.Xna.Framework;

namespace Loap.Views.Game
{
    class HatchFlap
    {
        public Vector3 Origin;
        public Direction Side;
        public BlockTextureRef OutsideTexture;
        public BlockTextureRef InsideTexture;

        public HatchFlap(Vector3 origin, Direction side, BlockTextureRef outsideTexture, BlockTextureRef insideTexture)
        {
            Origin = origin;
            Side = side;
            OutsideTexture = outsideTexture;
            InsideTexture = insideTexture;
        }
    }
}
