﻿using Loap.Constants;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loap.Audio
{
    static class SoundManager
    {
        private static readonly Dictionary<string, Sound?> _LoadedSounds = new Dictionary<string, Sound?>();
        private static readonly List<string> _CoreSounds = new List<string>();

        static SoundManager()
        {
            LoadCoreSounds();
        }

        private static void LoadCoreSounds()
        {
            var files = Directory.GetFiles(PathConstants.SoundPath, "*" + PathConstants.EXT_AUDIO);
            foreach (var file in files)
            {
                var name = Path.GetFileNameWithoutExtension(file);
                _CoreSounds.Add(name);
                LoadSound(name);
            }
        }

        public static void LoadSound(string name)
        {
            if (!_LoadedSounds.ContainsKey(name))
            {
                if (File.Exists(Path.Combine(PathConstants.SoundPath, name + PathConstants.EXT_AUDIO)))
                    _LoadedSounds.Add(name, new Sound(Path.Combine(PathConstants.SoundPath, name + PathConstants.EXT_AUDIO)));
                else
                    _LoadedSounds.Add(name, null);
            }
        }

        public static void ClearSoundCache()
        {
            foreach (var key in _LoadedSounds.Keys.ToArray())
            {
                if (!_CoreSounds.Contains(key))
                    _LoadedSounds.Remove(key);
            }
        }

        public static void PlaySound(string name, float volume = 1f)
        {
            LoadSound(name);

            var sound = _LoadedSounds[name];
            if (sound != null)
                sound.Play(volume);
        }
    }
}
