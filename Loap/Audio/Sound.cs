﻿using Loap.Misc;
using Microsoft.Xna.Framework.Audio;
using NVorbis;
using System;
using System.Linq;

namespace Loap.Audio
{
    class Sound
    {
        private SoundEffect _SoundEffect;
        public readonly string Filename;

        public Sound(string filename)
        {
            Filename = filename;

            VorbisReader reader = new VorbisReader(filename);
            var sampleRate = reader.SampleRate;
            var channels = reader.Channels;

            int samples = (int)reader.TotalSamples * reader.Channels;
            var tempBuffer = new float[samples];
            reader.ReadSamples(tempBuffer, 0, samples);
            var buffer = FloatBufferToByteBuffer(tempBuffer);

            _SoundEffect = new SoundEffect(buffer, sampleRate, channels == 1 ? AudioChannels.Mono : AudioChannels.Stereo);
        }

        public void Play(float volume)
        {
            _SoundEffect.Play(volume * LoapSettings.SoundVolume / 100f, 0, 0);
        }

        private static byte[] FloatBufferToByteBuffer(float[] floatBuffer)
        {
            var shortBuffer = floatBuffer.Select(f => (short)Math.Clamp(Math.Round(f * 32767), -32767, 32767));

            byte[] result = new byte[floatBuffer.Length * 2];
            int i = 0;

            foreach (var sample in shortBuffer)
            {
                result[i] = (byte)(sample & 0x00FF);
                result[i + 1] = (byte)((sample & 0xFF00) >> 8);
                i += 2;
            }

            return result;
        }
    }
}
