﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loap.Audio
{
    static class MusicManager
    {
        private static readonly List<Music> _LoadedMusic = new List<Music>();
        private static Music? _PlayingMusic;

        private const int MUSIC_CACHE_CAP = 3;

        public static void PlayMusic(string filename)
        {
            if (_PlayingMusic == null || filename != _PlayingMusic.Filename)
            {
                StopMusic();

                Music? newMusic = GetMusic(filename);

                _PlayingMusic = newMusic;

                if (newMusic != null)
                    newMusic.Play();
            }
        }

        public static void ResetMusicVolume()
        {
            if (_PlayingMusic != null)
                _PlayingMusic.ResetVolume();
        }

        public static void StopMusic()
        {
            if (_PlayingMusic != null)
            {
                _PlayingMusic.Stop();
                _PlayingMusic = null;
            }
        }

        private static Music? GetMusic(string filename)
        {
            Music? result = _LoadedMusic.FirstOrDefault(p => p.Filename == filename);

            if (result == null)
            {
                if (File.Exists(filename))
                {
                    result = new Music(filename);
                    _LoadedMusic.Add(result);
                    while (_LoadedMusic.Count > MUSIC_CACHE_CAP)
                        _LoadedMusic.RemoveAt(0);
                }
                else
                    return null;
            }
            else
            {
                if (_LoadedMusic.IndexOf(result) != 0)
                {
                    _LoadedMusic.Remove(result);
                    _LoadedMusic.Add(result); // bump it to the end of the list
                }
            }

            return result;
        }
    }
}
