﻿using Loap.Misc;
using Microsoft.Xna.Framework.Audio;
using NVorbis;
using System;
using System.Linq;

namespace Loap.Audio
{
    class Music
    {
        private DynamicSoundEffectInstance _SoundInstance;

        private byte[]? _IntroBuffer;
        private byte[] _LoopBuffer;

        public readonly string Filename;

        public Music(string filename)
        {
            Filename = filename;

            VorbisReader reader = new VorbisReader(filename);

            _SoundInstance = new DynamicSoundEffectInstance(reader.SampleRate, reader.Channels == 1 ? AudioChannels.Mono : AudioChannels.Stereo);

            int totalSamples = (int)reader.TotalSamples * reader.Channels;
            int loopStart = 0;
            int loopLength = -1;

            if (int.TryParse(reader.Tags.GetTagSingle("LOOPSTART"), out loopStart) ||
                int.TryParse(reader.Tags.GetTagSingle("LOOPLENGTH"), out loopLength))
            {
                loopStart *= reader.Channels;
                loopLength *= reader.Channels;
            }
            else
            {
                loopStart = 0;
                loopLength = -1;
            }

            if (loopLength < 0)
                loopLength = totalSamples - loopStart;

            GC.TryStartNoGCRegion(128 * 1024 * 1024);

            if (loopStart > 0)
            {
                var introTempBuffer = new float[loopStart];
                reader.ReadSamples(introTempBuffer, 0, loopStart);
                _IntroBuffer = FloatBufferToByteBuffer(introTempBuffer);
            }

            var loopTempBuffer = new float[loopLength];
            reader.ReadSamples(loopTempBuffer, 0, loopLength);

            _LoopBuffer = FloatBufferToByteBuffer(loopTempBuffer);

            _SoundInstance.BufferNeeded += SoundInstance_BufferNeeded;

            GC.EndNoGCRegion();
            GC.Collect(2);
        }

        public void Play()
        {
            _SoundInstance.Volume = LoapSettings.MusicVolume / 100f;
            if (_IntroBuffer != null)
                _SoundInstance.SubmitBuffer(_IntroBuffer);
            else
                _SoundInstance.SubmitBuffer(_LoopBuffer);
            _SoundInstance.SubmitBuffer(_LoopBuffer);
            _SoundInstance.Play();
        }

        public void Stop()
        {
            _SoundInstance.Stop();
        }

        public void ResetVolume()
        {
            _SoundInstance.Volume = LoapSettings.MusicVolume / 100f;
        }

        private void SoundInstance_BufferNeeded(object? sender, EventArgs e)
        {
            _SoundInstance.SubmitBuffer(_LoopBuffer);
        }

        private static byte[] FloatBufferToByteBuffer(float[] floatBuffer)
        {
            var shortBuffer = floatBuffer.Select(f => (short)Math.Clamp(Math.Round(f * 32767), -32767, 32767));

            byte[] result = new byte[floatBuffer.Length * 2];
            int i = 0;

            foreach (var sample in shortBuffer)
            {
                result[i] = (byte)(sample & 0x00FF);
                result[i + 1] = (byte)((sample & 0xFF00) >> 8);
                i += 2;
            }

            return result;
        }
    }
}
