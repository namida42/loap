﻿using LoapCore.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Loap.Rendering
{
    class BlockCacheEntry
    {
        public struct FrameEntry
        {
            public Texture2D Texture;
            public Vector2[] TextureCoordinates;
            public Color Shading;
        }

        public Vector3[] VertexPositions = null!; // These will always have values assigned during creation, it just isn't obvious to the compiler.
        public FrameEntry[] FrameEntries = null!;
        public bool Transparency;
        public bool Inside;
        public Point3DAndDirection StructurePosition;
        public bool AnimatedExitFace;
        public bool Overlay;
        public bool IgnoreCull;

        public Vector3 Midpoint;

        public VertexPositionColorTexture[][] FrameVertices = null!;

        public void GenerateVertices()
        {
            FrameVertices = new VertexPositionColorTexture[FrameEntries.Length][];
            for (int i = 0; i < FrameEntries.Length; i++)
                FrameVertices[i] = GetVerticesForFrame(i);
        }

        private VertexPositionColorTexture[] GetVerticesForFrame(int frameIndex)
        {
            var result = new VertexPositionColorTexture[VertexPositions.Length];
            var frameEntry = FrameEntries[frameIndex];

            for (int i = 0; i < VertexPositions.Length; i++)
                result[i] = new VertexPositionColorTexture(
                    VertexPositions[i],
                    frameEntry.Shading,
                    frameEntry.TextureCoordinates[i]
                    );

            return result;
        }
    }
}
