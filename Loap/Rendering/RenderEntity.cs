﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Loap.Rendering
{
    class RenderEntity
    {
        private const int INITIAL_POOL_SIZE = 4096;
        private static List<RenderEntity> _EntityPool = new List<RenderEntity>();
        private static int _PoolIndex = 0;

        public Texture2D Texture = null!; // These will always have values assigned during creation, it just isn't obvious to the compiler.
        public bool Transparency;
        public VertexPositionColorTexture[] Vertices = null!;
        public Vector3 Midpoint;
        public int Priority;
        public float Bias;

        static RenderEntity()
        {
            for (int i = 0; i < INITIAL_POOL_SIZE; i++)
                _EntityPool.Add(new RenderEntity());
        }

        private RenderEntity()
        {
        }

        private void SetValues(Texture2D texture, bool transparency, Vector3 midpoint, VertexPositionColorTexture[] vertices, int priority, float bias)
        {
            Texture = texture;
            Transparency = transparency;
            Priority = priority;

            Vertices = vertices;
            Midpoint = midpoint;
            Bias = bias;
        }

        public static RenderEntity Create(Texture2D texture, bool transparency, Vector3 midpoint, VertexPositionColorTexture[] vertices, int priority, float bias)
        {
            RenderEntity result;
            if (_PoolIndex >= _EntityPool.Count)
            {
                for (int i = _PoolIndex; i < _PoolIndex * 2; i++)
                    _EntityPool.Add(new RenderEntity());
            }

            result = _EntityPool[_PoolIndex];

            _PoolIndex++;
            result.SetValues(texture, transparency, midpoint, vertices, priority, bias);

            return result;
        }

        public static void ReleaseEntities()
        {
            _PoolIndex = 0;
        }
    }
}
