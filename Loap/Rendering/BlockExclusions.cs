﻿using LoapCore.Misc;

namespace Loap.Rendering
{
    class BlockExclusions
    {
        private readonly bool[,,,] _Exclusions = new bool[0, 0, 0, 0];

        public BlockExclusions(int width, int depth, int height)
        {
            _Exclusions = new bool[width, depth, height, 6];
        }

        private BlockExclusions(bool[,,,] source)
        {
            _Exclusions = (bool[,,,])source.Clone();
        }

        public BlockExclusions Clone()
        {
            return new BlockExclusions(_Exclusions);
        }

        public bool this[int x, int y, int z, Direction face]
        {
            get
            {
                return _Exclusions[x, y, z, (int)face];
            }

            set
            {
                _Exclusions[x, y, z, (int)face] = value;
            }
        }
    }
}
