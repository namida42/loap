﻿using Loap.Blocks;
using Loap.Constants;
using Loap.Misc;
using Loap.Models;
using Loap.Views.Game;
using LoapCore.Blocks;
using LoapCore.Constants;
using LoapCore.Entities;
using LoapCore.Levels;
using LoapCore.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loap.Rendering
{
    class Renderer
    {
        private readonly GraphicsDevice GraphicsDevice;
        private readonly AlphaTestEffect Effect;
        private readonly SpriteBatch SpriteBatch;
        private VertexBuffer Vertices;

        private readonly List<VertexPositionColorTexture> VertexQueue = new List<VertexPositionColorTexture>();

        public Camera Camera;
        public Texture2D Texture;

        private readonly DepthStencilState _StencilStateZNone;
        private readonly DepthStencilState _StencilStateZWriteOnly;
        private readonly DepthStencilState _StencilStateZApply;
        public readonly Matrix ProjectionMatrix;

        public Texture2D SkyTexture;
        public Texture2D SeaTexture;
        public RenderTarget2D? TempSeaTexture;
        public int SeaFrames { get; private set; }
        public readonly Dictionary<string, Texture2D> LandTextures = new Dictionary<string, Texture2D>();
        public readonly Dictionary<string, Texture2D> BlockTextures = new Dictionary<string, Texture2D>();
        public readonly Dictionary<string, Texture2D> EntityTextures = new Dictionary<string, Texture2D>();
        public Texture2D? CrackTexture;

        public RenderTarget2D? ScreenTexture;
        private RenderTarget2D? _ScreenTextureTemp;

        public bool FullHeightSkyGraphic;

        public Renderer(GraphicsDevice device)
        {
            Camera = new Camera();

            GraphicsDevice = device;

            Texture = RenderConstants.BlankTexture;

            ProjectionMatrix = CalculateProjectionMatrix(GraphicsDevice);

            _StencilStateZNone = new DepthStencilState();
            _StencilStateZNone.DepthBufferEnable = false;
            _StencilStateZNone.DepthBufferWriteEnable = false;
            _StencilStateZNone.DepthBufferFunction = CompareFunction.Always;

            _StencilStateZWriteOnly = new DepthStencilState();
            _StencilStateZWriteOnly.DepthBufferEnable = true;
            _StencilStateZWriteOnly.DepthBufferWriteEnable = true;
            _StencilStateZWriteOnly.DepthBufferFunction = CompareFunction.Always;

            _StencilStateZApply = new DepthStencilState();
            _StencilStateZApply.DepthBufferEnable = true;
            _StencilStateZApply.DepthBufferWriteEnable = true;
            _StencilStateZApply.DepthBufferFunction = CompareFunction.LessEqual;

            Effect = new AlphaTestEffect(GraphicsDevice);
            Effect.Texture = Texture;
            Effect.VertexColorEnabled = true;
            Effect.Projection = ProjectionMatrix;

            SpriteBatch = new SpriteBatch(GraphicsDevice);

            Vertices = new VertexBuffer(GraphicsDevice, typeof(VertexPositionColorTexture), RenderConstants.INITIAL_VERTEX_CAP, BufferUsage.WriteOnly);

            SkyTexture = RenderConstants.BlankTexture;
            SeaTexture = RenderConstants.BlankTexture;
        }

        // Texture Management

        public void LoadTextures(Level level)
        {
            if (level.LevelInfo.Sky.Texture != "")
            {
                SkyTexture = Texture2D.FromFile(GraphicsDevice, Path.Combine(PathConstants.SkyTexturesPath, level.LevelInfo.Sky.Texture + PathConstants.EXT_IMAGE));
                FullHeightSkyGraphic = level.LevelInfo.Sky.FullHeight;
            }

            if (level.LevelInfo.Sea.Texture != "" && !level.LevelInfo.Sky.FullHeight)
            {
                SeaTexture = Texture2D.FromFile(GraphicsDevice, Path.Combine(PathConstants.SeaTexturesPath, level.LevelInfo.Sea.Texture + PathConstants.EXT_IMAGE));
                SeaFrames = SeaTexture.Height / SeaTexture.Width;
                if (SeaFrames > 1)
                    TempSeaTexture = new RenderTarget2D(GraphicsDevice, SeaTexture.Width, SeaTexture.Width); // not a typo, it's intended to use SeaTexture.Width for the height too
            }

            LoadLandTextures(level.Lands);
            LoadBlockTextures(level.BlockStructure, level.Overlays);
            LoadEntityTextures(level.Entities);
        }

        public void LoadLandTextures(List<LandRegion> lands)
        {
            foreach (var land in lands)
                if ((land.Texture != "") && !LandTextures.ContainsKey(land.Texture))
                {
                    var newTexture = Texture2D.FromFile(GraphicsDevice,
                        Path.Combine(PathConstants.LandTexturesPath, land.Texture + PathConstants.EXT_IMAGE));
                    LandTextures.Add(land.Texture, newTexture);
                }
        }

        public void LoadBlockTextures(BlockStructure structure, List<LevelOverlay> overlays)
        {
            bool needScreenTexture = false;

            for (int z = 0; z < structure.Height; z++)
                for (int y = 0; y < structure.Depth; y++)
                    for (int x = 0; x < structure.Width; x++)
                    {
                        var thisBlock = structure[x, y, z];
                        if (thisBlock == null)
                            continue;

                        foreach (var face in thisBlock.Faces.Values)
                        {
                            foreach (var texture in face.TextureOuter)
                                if ((texture.File != "") && (texture.File[0] != '*') && !BlockTextures.ContainsKey(texture.File))
                                {
                                    var newTexture = Texture2D.FromFile(GraphicsDevice,
                                        Path.Combine(PathConstants.BlockTexturesPath, texture.File + PathConstants.EXT_IMAGE));
                                    BlockTextures.Add(texture.File, newTexture);
                                }
                                else if (texture.File == RenderConstants.SCREEN_TEXTURE)
                                    needScreenTexture = true;

                            foreach (var texture in face.TextureInner)
                                if ((texture.File != "") && (texture.File[0] != '*') && !BlockTextures.ContainsKey(texture.File))
                                {
                                    var newTexture = Texture2D.FromFile(GraphicsDevice,
                                        Path.Combine(PathConstants.BlockTexturesPath, texture.File + PathConstants.EXT_IMAGE));
                                    BlockTextures.Add(texture.File, newTexture);
                                }
                                else if (texture.File == RenderConstants.SCREEN_TEXTURE)
                                    needScreenTexture = true;
                        }
                    }

            foreach (var overlay in overlays)
            {
                if (overlay.FrontTexture.File != "" && overlay.FrontTexture.File[0] != '*' && !BlockTextures.ContainsKey(overlay.FrontTexture.File))
                {
                    var newTexture = Texture2D.FromFile(GraphicsDevice,
                        Path.Combine(PathConstants.BlockTexturesPath, overlay.FrontTexture.File + PathConstants.EXT_IMAGE));
                    BlockTextures.Add(overlay.FrontTexture.File, newTexture);
                }
                else if (overlay.FrontTexture.File == RenderConstants.SCREEN_TEXTURE)
                    needScreenTexture = true;

                if (overlay.BackTexture.File != "" && overlay.BackTexture.File[0] != '*' && !BlockTextures.ContainsKey(overlay.BackTexture.File))
                {
                    var newTexture = Texture2D.FromFile(GraphicsDevice,
                        Path.Combine(PathConstants.BlockTexturesPath, overlay.BackTexture.File + PathConstants.EXT_IMAGE));
                    BlockTextures.Add(overlay.BackTexture.File, newTexture);
                }
                else if (overlay.BackTexture.File == RenderConstants.SCREEN_TEXTURE)
                    needScreenTexture = true;
            }

            if (needScreenTexture)
            {
                ScreenTexture = new RenderTarget2D(GraphicsDevice, RenderConstants.SCREEN_TEXTURE_SIZE, RenderConstants.SCREEN_TEXTURE_SIZE);
                _ScreenTextureTemp = new RenderTarget2D(GraphicsDevice,
                    RenderConstants.SCREEN_TEXTURE_SIZE * RenderConstants.SCREEN_TEXTURE_HORIZONTAL_CELLS / 4,
                    RenderConstants.SCREEN_TEXTURE_SIZE * RenderConstants.SCREEN_TEXTURE_VERTICAL_CELLS / 4
                    );
                BlockTextures.Add(RenderConstants.SCREEN_TEXTURE, ScreenTexture);
            }

            BlockTextures.Add("", RenderConstants.BlankTexture);
        }

        public void LoadEntityTextures(List<LevelEntity> entities)
        {
            foreach (var entity in entities)
            {
                if (!EntityTextures.ContainsKey(entity.Identifier))
                {
                    var newTexture = Texture2D.FromFile(GraphicsDevice,
                        Path.Combine(PathConstants.EntitiesPath, entity.Identifier + PathConstants.EXT_IMAGE));
                    EntityTextures.Add(entity.Identifier, newTexture);
                }
            }
        }

        public void UpdateScreenTexture(Texture2D rawScreenTexture)
        {
            if (_ScreenTextureTemp == null || ScreenTexture == null)
                throw new InvalidOperationException(); // Should never happen, as this is only called if those are not null.

            GraphicsDevice.SetRenderTarget(_ScreenTextureTemp);
            GraphicsDevice.Clear(Color.Transparent);
            Rectangle tempCanvasDstRect = new Rectangle(
                RenderConstants.SCREEN_TEXTURE_BOUNDARY_LEFT, RenderConstants.SCREEN_TEXTURE_BOUNDARY_TOP,
                _ScreenTextureTemp.Width - RenderConstants.SCREEN_TEXTURE_BOUNDARY_LEFT - RenderConstants.SCREEN_TEXTURE_BOUNDARY_RIGHT,
                _ScreenTextureTemp.Height - RenderConstants.SCREEN_TEXTURE_BOUNDARY_TOP - RenderConstants.SCREEN_TEXTURE_BOUNDARY_BOTTOM
                );
            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp);
            SpriteBatch.Draw(rawScreenTexture, tempCanvasDstRect, Color.White);
            SpriteBatch.End();

            Rectangle srcRect = new Rectangle(0, 0, RenderConstants.SCREEN_TEXTURE_SIZE / 4, RenderConstants.SCREEN_TEXTURE_SIZE / 4);
            Rectangle dstRect = srcRect;

            GraphicsDevice.SetRenderTarget(ScreenTexture);
            GraphicsDevice.Clear(Color.Transparent);

            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp);

            for (int y = 0; y < 4; y++)
            {
                for (int x = 0; x < 4; x++)
                {
                    SpriteBatch.Draw(_ScreenTextureTemp, dstRect, srcRect, Color.White);

                    srcRect.X += srcRect.Width;
                    dstRect.X += dstRect.Width;

                    if (srcRect.X >= _ScreenTextureTemp.Width)
                    {
                        srcRect.X = 0;
                        srcRect.Y += srcRect.Height;
                    }

                    if (dstRect.X >= ScreenTexture.Width)
                    {
                        dstRect.X = 0;
                        dstRect.Y += dstRect.Height;
                    }

                    if (srcRect.Y >= _ScreenTextureTemp.Height)
                        break;
                }

                if (srcRect.Y >= _ScreenTextureTemp.Height)
                    break;
            }

            SpriteBatch.End();
        }

        // Block Cache and related preparation

        public void GenerateBlockCache(BlockStructure structure, List<LevelOverlay> overlays, out BlockCacheEntry[] blockCache, out BlockExclusions exclusions)
        {
            exclusions = CalculateExclusions(structure);

            List<BlockCacheEntry> result = new List<BlockCacheEntry>();

            for (int z = 0; z < structure.Height; z++)
                for (int y = 0; y < structure.Depth; y++)
                    for (int x = 0; x < structure.Width; x++)
                    {
                        var thisBlock = structure[x, y, z];
                        if (thisBlock != null)
                            foreach (Direction side in Utils.GetEnumValues<Direction>())
                            {
                                if ((thisBlock.Faces[side].TextureOuter.Count > 0) &&
                                    (thisBlock.Visibility == BlockVisibility.Normal || thisBlock.Visibility == BlockVisibility.Transparent))
                                {
                                    var newEntries = GenerateBlockCacheEntriesForFace(thisBlock, thisBlock.Faces[side], new Point3D(x, y, z), false);
                                    foreach (var newEntry in newEntries)
                                        if (newEntry.VertexPositions.Length > 0)
                                            result.Add(newEntry);
                                }

                                if ((thisBlock.Faces[side].TextureInner.Count > 0) &&
                                    (thisBlock.Visibility == BlockVisibility.Normal || thisBlock.Visibility == BlockVisibility.Transparent))
                                {
                                    var newEntries = GenerateBlockCacheEntriesForFace(thisBlock, thisBlock.Faces[side], new Point3D(x, y, z), true);
                                    foreach (var newEntry in newEntries)
                                        if (newEntry.VertexPositions.Length > 0)
                                            result.Add(newEntry);
                                }
                            }
                    }

            foreach (var overlay in overlays)
            {
                if (overlay.FrontTexture.File != "")
                {
                    var newEntry = GenerateOverlayEntry(overlay, false);
                    if (newEntry.VertexPositions.Length > 0)
                        result.Add(newEntry);
                }

                if (overlay.BackTexture.File != "")
                {
                    var newEntry = GenerateOverlayEntry(overlay, true);
                    if (newEntry.VertexPositions.Length > 0)
                        result.Add(newEntry);
                }
            }

            blockCache = result.ToArray();
        }

        private BlockCacheEntry GenerateOverlayEntry(LevelOverlay overlay, bool inside)
        {
            BlockCacheEntry result = new BlockCacheEntry();

            var model = BlockModels.GetModel(BlockShape.Solid, 0);
            Vector3 renderLocation = new Vector3(overlay.Position.X, overlay.Position.Y, overlay.Position.Z * PhysicsConstants.SEGMENT_SIZE_FLOAT);

            result.VertexPositions = model.Faces[overlay.Side].Vertices
                .Select(v => v.Position + renderLocation).ToArray();
            result.Transparency = true;
            result.FrameEntries = new[] { MakeFrameEntry(inside ? overlay.BackTexture : overlay.FrontTexture, model.Faces[overlay.Side].Vertices, overlay.Side, null, inside) };
            result.Inside = inside;

            result.StructurePosition = new Point3DAndDirection(overlay.Position, overlay.Side);

            var localSide = overlay.Side;

            result.Midpoint = new Vector3(overlay.Position.X + 0.5f, overlay.Position.Y + 0.5f, (overlay.Position.Z + 0.5f) * PhysicsConstants.SEGMENT_SIZE_FLOAT) +
                localSide switch
                {
                    Direction.XMinus => new Vector3(-0.5f, 0, 0),
                    Direction.XPlus => new Vector3(0.5f, 0, 0),
                    Direction.YMinus => new Vector3(0, -0.5f, 0),
                    Direction.YPlus => new Vector3(0, 0.5f, 0),
                    Direction.ZMinus => new Vector3(0, 0, -0.5f * PhysicsConstants.SEGMENT_SIZE_FLOAT),
                    Direction.ZPlus => new Vector3(0, 0, 0.5f * PhysicsConstants.SEGMENT_SIZE_FLOAT),
                    _ => throw new NotImplementedException(),
                };

            if (inside)
            {
                result.VertexPositions = result.VertexPositions.Reverse().ToArray();
                for (int i = 0; i < result.FrameEntries.Length; i++)
                    result.FrameEntries[i].TextureCoordinates = result.FrameEntries[i].TextureCoordinates.Reverse().ToArray();
            }

            result.Overlay = true;

            result.GenerateVertices();

            return result;

        }

        private BlockCacheEntry[] GenerateBlockCacheEntriesForFace(BlockInfo block, FaceInfo face, Point3D location, bool inside)
        {
            var model = BlockModels.GetModel(block.Shape, block.Segment);
            var srcVertices = (model.Faces[face.Side].SlopeVertices.Count > 0) && (block.Visibility == BlockVisibility.Transparent) ?
                model.Faces[face.Side].SlopeVertices :
                model.Faces[face.Side].Vertices;

            var distinctNormals = srcVertices.Select(v => v.Normal).Distinct().ToArray();

            var result = new BlockCacheEntry[distinctNormals.Length];

            for (int i = 0; i < distinctNormals.Length; i++)
            {
                var vertexSubset = srcVertices.Where(v => v.Normal == distinctNormals[i]).ToList();
                result[i] = GenerateBlockCacheEntryForFace(vertexSubset, block, face, location, inside);
            }

            return result;
        }

        private BlockCacheEntry GenerateBlockCacheEntryForFace(List<VertexPositionNormalTexture> srcVertices, BlockInfo block, FaceInfo face, Point3D location, bool inside)
        {
            BlockCacheEntry result = new BlockCacheEntry();

            Vector3 renderLocation = new Vector3(location.X, location.Y, location.Z * PhysicsConstants.SEGMENT_SIZE_FLOAT);

            result.VertexPositions = srcVertices
                .Select(v => ApplyBlockRotations(v.Position, block.Rotations) + renderLocation).ToArray();
            result.Transparency = block.Visibility == BlockVisibility.Transparent;
            result.FrameEntries = MakeFrameEntries(inside ? face.TextureInner : face.TextureOuter, srcVertices, face.Side,
                (face.Side != Direction.ZMinus && face.Side != Direction.ZPlus) ? Utils.RotateDirection(face.Side, block.Rotations) : face.Side,
                inside).ToArray();
            result.Inside = inside;

            result.StructurePosition = new Point3DAndDirection(location, face.Side);

            var localSide = face.Side;
            if (localSide != Direction.ZMinus && localSide != Direction.ZPlus)
                localSide = Utils.RotateDirection(localSide, block.Rotations);

            result.Midpoint = new Vector3(location.X + 0.5f, location.Y + 0.5f, (location.Z + 0.5f) * PhysicsConstants.SEGMENT_SIZE_FLOAT) +
                localSide switch
                {
                    Direction.XMinus => new Vector3(-0.5f + PhysicsConstants.STEP_SIZE_FLOAT, 0, 0),
                    Direction.XPlus => new Vector3(0.5f - PhysicsConstants.STEP_SIZE_FLOAT, 0, 0),
                    Direction.YMinus => new Vector3(0, -0.5f + PhysicsConstants.STEP_SIZE_FLOAT, 0),
                    Direction.YPlus => new Vector3(0, 0.5f - PhysicsConstants.STEP_SIZE_FLOAT, 0),
                    Direction.ZMinus => new Vector3(0, 0, -0.5f * PhysicsConstants.SEGMENT_SIZE_FLOAT + PhysicsConstants.STEP_SIZE_FLOAT),
                    Direction.ZPlus => new Vector3(0, 0, 0.5f * PhysicsConstants.SEGMENT_SIZE_FLOAT - PhysicsConstants.STEP_SIZE_FLOAT),
                    _ => throw new NotImplementedException(),
                };

            if (inside)
            {
                result.VertexPositions = result.VertexPositions.Reverse().ToArray();
                for (int i = 0; i < result.FrameEntries.Length; i++)
                    result.FrameEntries[i].TextureCoordinates = result.FrameEntries[i].TextureCoordinates.Reverse().ToArray();
            }

            if (face.Physics == FacePhysics.AnimateExit && !inside)
                result.AnimatedExitFace = true;

            if (srcVertices.Count > 0)
            {
                var normal = srcVertices[0].Normal;
                result.IgnoreCull = !(
                    ((normal.X == 0) && (normal.Y == 0)) ||
                    ((normal.X == 0) && (normal.Z == 0)) ||
                    ((normal.Y == 0) && (normal.Z == 0))
                    );
            }

            result.GenerateVertices();

            return result;
        }

        private List<BlockCacheEntry.FrameEntry> MakeFrameEntries(List<BlockTextureRef> refs, List<VertexPositionNormalTexture> srcVertices, Direction side, Direction viewSide, bool inside)
        {
            List<BlockCacheEntry.FrameEntry> result = new List<BlockCacheEntry.FrameEntry>();

            for (int i = 0; i < refs.Count; i++)
            {
                BlockCacheEntry.FrameEntry newEntry = MakeFrameEntry(refs[i], srcVertices, side, viewSide, inside);
                result.Add(newEntry);
            }

            return result;
        }

        private BlockCacheEntry.FrameEntry MakeFrameEntry(BlockTextureRef texRef, List<VertexPositionNormalTexture> srcVertices, Direction side, Direction? viewSide, bool inside)
        {
            var result = new BlockCacheEntry.FrameEntry();

            result.Texture = BlockTextures.ContainsKey(texRef.File) ? BlockTextures[texRef.File] : RenderConstants.BlankTexture;
            result.Shading = CreateShadingColor(texRef.Shading, viewSide);

            bool isTopOrBottom = (side == Direction.ZMinus || side == Direction.ZPlus);
            int textureIndex = texRef.Index;
            int xSlot = (textureIndex % 16) / 4;
            int ySlot = textureIndex / 16;
            int ySubSlot = isTopOrBottom ? 0 : textureIndex % 4;

            result.TextureCoordinates = srcVertices.Select(v =>
                AdjustBlockTextureCoordinate(
                    inside ? new Vector2(1 - v.TextureCoordinate.X, v.TextureCoordinate.Y) : v.TextureCoordinate,
                    texRef, xSlot, ySlot, ySubSlot, isTopOrBottom)
                ).ToArray();

            return result;
        }

        private BlockExclusions CalculateExclusions(BlockStructure structure)
        {
            int width = structure.Width;
            int depth = structure.Depth;
            int height = structure.Height;

            var result = new BlockExclusions(width, depth, height);

            for (int z = 0; z < height; z++)
                for (int y = 0; y < depth; y++)
                    for (int x = 0; x < width; x++)
                        UpdateSingleBlockExclusions(result, structure, x, y, z);

            return result;
        }

        public void UpdateBlockExclusions(BlockExclusions exclusions, BlockStructure structure, IEnumerable<Point3D> updatePoints)
        {
            foreach (var point in updatePoints)
                UpdateSingleBlockExclusions(exclusions, structure, point.X, point.Y, point.Z);
        }

        private void UpdateSingleBlockExclusions(BlockExclusions exclusions, BlockStructure structure, int x, int y, int z)
        {
            var thisBlock = structure[x, y, z];
            if (thisBlock == null)
            {
                exclusions[x, y, z, Direction.XMinus] = true;
                exclusions[x, y, z, Direction.XPlus] = true;
                exclusions[x, y, z, Direction.YMinus] = true;
                exclusions[x, y, z, Direction.YPlus] = true;
                exclusions[x, y, z, Direction.ZMinus] = true;
                exclusions[x, y, z, Direction.ZPlus] = true;
            }
            else
            {
                var model = BlockModels.GetModel(thisBlock.Shape, thisBlock.Segment);

                foreach (Direction sideInModel in Utils.GetEnumValues<Direction>())
                {
                    if ((model.Faces[sideInModel].Vertices.Count == 0) ||
                        (z < RenderConstants.MINIMUM_FACE_RENDER_HEIGHT_BLOCKS - 1) ||
                        (z == RenderConstants.MINIMUM_FACE_RENDER_HEIGHT_BLOCKS - 1 && sideInModel != Direction.ZPlus))
                    {
                        exclusions[x, y, z, sideInModel] = true;
                        continue;
                    }

                    exclusions[x, y, z, sideInModel] = false;

                    bool isVertical = !PhysicsConstants.NonVerticalDirections.Contains(sideInModel);

                    if (isVertical)
                    {
                        if (!IsNonContactSide(model.Faces[sideInModel], sideInModel))
                        {
                            int moveZ = sideInModel == Direction.ZPlus ? 1 : -1;

                            var adjBlock = structure[x, y, z + moveZ];
                            if (adjBlock != null && adjBlock.Visibility != BlockVisibility.Invisible && adjBlock.Visibility != BlockVisibility.Transparent)
                            {
                                var adjSide = Utils.InvertDirection(sideInModel);
                                var adjModel = BlockModels.GetModel(adjBlock.Shape, adjBlock.Segment);

                                if (!IsNonContactSide(adjModel.Faces[adjSide], adjSide))
                                {
                                    int thisRotations = thisBlock.Rotations;
                                    int adjRotations = adjBlock.Rotations;

                                    var thisTextureCoords = model.Faces[sideInModel].Vertices.Select(v =>
                                    {
                                        var result = v.TextureCoordinate;
                                        Utils.RotateWithinZeroToOne(ref result.X, ref result.Y, -thisRotations);
                                        return result;
                                    }
                                        );
                                    var adjTextureCoords = adjModel.Faces[adjSide].Vertices.Select(v =>
                                    {
                                        var result = v.TextureCoordinate;
                                        Utils.RotateWithinZeroToOne(ref result.X, ref result.Y, adjRotations);
                                        result = new Vector2(result.X, 1 - result.Y);
                                        return result;
                                    }
                                        );

                                    exclusions[x, y, z, sideInModel] = true;
                                    foreach (var coord in thisTextureCoords)
                                        if (!Utils.IsPointInsidePolygon(coord.X, coord.Y, adjTextureCoords.Select(c => c.X), adjTextureCoords.Select(c => c.Y), true))
                                        {
                                            exclusions[x, y, z, sideInModel] = false;
                                            break;
                                        }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!IsNonContactSide(model.Faces[sideInModel], sideInModel))
                        {
                            var sideInStructure = Utils.RotateDirection(sideInModel, thisBlock.Rotations);

                            int moveX = 0;
                            int moveY = 0;

                            moveY = 1;
                            Utils.RotateAroundOrigin(ref moveX, ref moveY, Utils.DirectionToRotation(sideInStructure));

                            var adjBlock = structure[x + moveX, y + moveY, z];
                            if (adjBlock != null && adjBlock.Visibility != BlockVisibility.Invisible && adjBlock.Visibility != BlockVisibility.Transparent)
                            {
                                var adjSideInStructure = Utils.InvertDirection(sideInStructure);
                                var adjSideInModel = Utils.RotateDirection(adjSideInStructure, -adjBlock.Rotations);
                                var adjModel = BlockModels.GetModel(adjBlock.Shape, adjBlock.Segment);

                                if (!IsNonContactSide(adjModel.Faces[adjSideInModel], adjSideInModel))
                                {
                                    var thisTextureCoords = model.Faces[sideInModel].Vertices.Select(v => v.TextureCoordinate);
                                    var adjTextureCoords = adjModel.Faces[adjSideInModel].Vertices
                                        .Select(v => new Vector2(1 - v.TextureCoordinate.X, v.TextureCoordinate.Y));

                                    exclusions[x, y, z, sideInModel] = true;
                                    foreach (var coord in thisTextureCoords)
                                        if (!Utils.IsPointInsidePolygon(coord.X, coord.Y, adjTextureCoords.Select(c => c.X), adjTextureCoords.Select(c => c.Y), true))
                                        {
                                            exclusions[x, y, z, sideInModel] = false;
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static bool IsNonContactSide(FaceModel model, Direction side)
        {
            // Special case for mixed contact / non-contact sides

            var vertices = model.Vertices;
            return side switch
            {
                Direction.XMinus => vertices.Any(v => v.Position.X != 0),
                Direction.XPlus => vertices.Any(v => v.Position.X != 1),
                Direction.YMinus => vertices.Any(v => v.Position.Y != 0),
                Direction.YPlus => vertices.Any(v => v.Position.Y != 1),
                Direction.ZMinus => vertices.Any(v => v.Position.Z != 0),
                Direction.ZPlus => vertices.Any(v => v.Position.Z != 0.25f),
                _ => throw new ArgumentException("Invalid direction", nameof(side)),
            };
        }

        private static Vector3 ApplyBlockRotations(Vector3 input, int rotations)
        {
            return Utils.ModuloNegativeAdjusted(rotations, 4) switch
            {
                0 => input,
                1 => new Vector3(1 - input.Y, input.X, input.Z),
                2 => new Vector3(1 - input.X, 1 - input.Y, input.Z),
                3 => new Vector3(input.Y, 1 - input.X, input.Z),
                _ => input,
            };
        }

        private static Vector2 AdjustBlockTextureCoordinate(Vector2 input, BlockTextureRef textureRef, int xSlot, int ySlot, int ySubSlot, bool isTopOrBottom)
        {
            if (!isTopOrBottom)
                input.Y = (input.Y * 0.25f) + (0.25f * (3 - ySubSlot));

            if (textureRef.Flip)
                input.X = 1 - input.X;

            for (int i = 0; i < textureRef.Rotations; i++)
                input = new Vector2(input.Y, 1 - input.X);

            float texOriginX = xSlot * 0.25f;
            float texOriginY = ySlot * 0.25f;

            return new Vector2(texOriginX + (input.X / 4), texOriginY + (input.Y / 4));
        }

        public List<HatchFlap> PrepareHatchFlaps(BlockStructure blocks)
        {
            var result = new List<HatchFlap>();

            var entranceBlocks = blocks.ToDictionary().Where(p => p.Value.Physics == BlockPhysics.Entrance);
            foreach (var pair in entranceBlocks)
            {
                var pos = pair.Key;
                var info = pair.Value;

                var vectorPosCenter = new Vector3(pos.X + 0.5f, pos.Y + 0.5f, pos.Z * PhysicsConstants.SEGMENT_SIZE_FLOAT);

                AddHatchFlap(result, vectorPosCenter - new Vector3(-0.5f, 0, 0), Direction.XMinus, info);
                AddHatchFlap(result, vectorPosCenter - new Vector3(0.5f, 0, 0), Direction.XPlus, info);
                AddHatchFlap(result, vectorPosCenter - new Vector3(0, -0.5f, 0), Direction.YMinus, info);
                AddHatchFlap(result, vectorPosCenter - new Vector3(0, 0.5f, 0), Direction.YPlus, info);
            }

            return result;
        }

        private void AddHatchFlap(List<HatchFlap> dst, Vector3 pos, Direction side, BlockInfo blockInfo)
        {
            var actualFace = blockInfo.Faces[Utils.RotateDirection(side, -blockInfo.Rotations)];

            var outerTexture = actualFace.TextureOuter.Count == 0 ?
                new BlockTextureRef("", -1, 0, true, 0) :
                actualFace.TextureOuter[0];
            var innerTexture = actualFace.TextureInner.Count == 0 ?
                new BlockTextureRef(outerTexture.File, outerTexture.Index, outerTexture.Rotations, !outerTexture.Flip, outerTexture.Shading) :
                actualFace.TextureInner[0];

            if (outerTexture.Index >= 0 || innerTexture.Index >= 0)
                dst.Add(new HatchFlap(pos, side, outerTexture, innerTexture));
        }

        // Non-Entity Level Elements Rendering

        public void RenderQueue(bool transparency, DepthStencilState depthStencilState)
        {
            if (VertexQueue.Count > 0)
            {
                Effect.Texture = Texture;
                Effect.View = Camera.ViewMatrix;
                GraphicsDevice.BlendState = transparency ? BlendState.AlphaBlend : BlendState.Opaque;


                EnsureSufficientBufferSize(VertexQueue.Count);
                Vertices.SetData(VertexQueue.ToArray());

                GraphicsDevice.SetVertexBuffer(Vertices);

                GraphicsDevice.DepthStencilState = depthStencilState;
                GraphicsDevice.RasterizerState = RenderConstants.RasterizerState;

                foreach (EffectPass pass in Effect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, VertexQueue.Count / 3);
                }

                VertexQueue.Clear();
            }
        }

        private void EnsureSufficientBufferSize(int requirement)
        {
            if (requirement > Vertices.VertexCount)
            {
                int newCount = Vertices.VertexCount;
                while (newCount < requirement)
                    newCount *= 2;

                Vertices = new VertexBuffer(GraphicsDevice, typeof(VertexPositionColorTexture), newCount, BufferUsage.WriteOnly);
            }
        }

        public List<VertexPositionColorTexture> SaveQueue(bool clearQueue)
        {
            var result = VertexQueue.ToList();
            if (clearQueue)
                VertexQueue.Clear();
            return result;
        }

        public void RestoreQueue(List<VertexPositionColorTexture> queue)
        {
            VertexQueue.AddRange(queue);
        }

        // Non-Entity Level Elements Preparation

        public void QueueQuad(VertexPositionColorTexture topLeft, VertexPositionColorTexture topRight, VertexPositionColorTexture bottomRight, VertexPositionColorTexture bottomLeft)
        {
            VertexQueue.Add(topLeft);
            VertexQueue.Add(topRight);
            VertexQueue.Add(bottomLeft);
            VertexQueue.Add(topRight);
            VertexQueue.Add(bottomRight);
            VertexQueue.Add(bottomLeft);
        }

        public void PrepareSeaFrame(int iteration)
        {
            if (TempSeaTexture != null)
            {
                GraphicsDevice.SetRenderTarget(TempSeaTexture);
                SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp);

                int frameHeight = SeaTexture.Height / SeaFrames;
                Rectangle src = new Rectangle(0, ((iteration / 2) % SeaFrames) * frameHeight, SeaTexture.Width, frameHeight);
                SpriteBatch.Draw(SeaTexture, TempSeaTexture.Bounds, src, Color.White);

                SpriteBatch.End();
            }
        }

        public void RenderSea(SeaInfo seaInfo, int iteration)
        {
            if (!FullHeightSkyGraphic)
            {
                float offsetX = Utils.ModuloNegativeAdjusted(seaInfo.MotionXFloat * iteration, 1);
                float offsetY = Utils.ModuloNegativeAdjusted(seaInfo.MotionYFloat * iteration, 1);

                QueueSea(offsetX, offsetY);
                Texture = TempSeaTexture ?? SeaTexture;
                RenderQueue(false, _StencilStateZNone);
            }
        }

        private void QueueSea(float texOffsetX, float texOffsetY)
        {
            FloatRectangle region = new FloatRectangle(Camera.Position.X - RenderConstants.SKY_DISTANCE, Camera.Position.Y - RenderConstants.SKY_DISTANCE,
                RenderConstants.SKY_DISTANCE * 2, RenderConstants.SKY_DISTANCE * 2);

            float zPos = PhysicsConstants.LAND_HEIGHT * PhysicsConstants.STEP_SIZE_FLOAT;

            var topLeft = new VertexPositionColorTexture(
                new Vector3(region.X, region.Y, zPos),
                Color.White,
                new Vector2(-(region.X - texOffsetX), region.Y - texOffsetY)
                );

            var topRight = new VertexPositionColorTexture(
                new Vector3(region.X + region.Width, region.Y, zPos),
                Color.White,
                new Vector2(-(region.X + region.Width - texOffsetX), region.Y - texOffsetY)
                );

            var bottomLeft = new VertexPositionColorTexture(
                new Vector3(region.X, region.Y + region.Height, zPos),
                Color.White,
                new Vector2(-(region.X - texOffsetX), region.Y + region.Height - texOffsetY)
                );

            var bottomRight = new VertexPositionColorTexture(
                new Vector3(region.X + region.Width, region.Y + region.Height, zPos),
                Color.White,
                new Vector2(-(region.X + region.Width - texOffsetX), region.Y + region.Height - texOffsetY)
                );

            QueueQuad(topLeft, topRight, bottomRight, bottomLeft);
        }

        public void RenderSky()
        {
            if (FullHeightSkyGraphic)
                DrawFullHeightSky();
            else
            {
                QueueWrapSky();
                Texture = SkyTexture;
                RenderQueue(false, _StencilStateZNone);
            }
        }

        private void DrawFullHeightSky()
        {
            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointWrap);

            float scale = GraphicsDevice.Viewport.Height / (float)SkyTexture.Height;
            Rectangle dstRect = new Rectangle(0, 0, (int)Math.Floor(SkyTexture.Width * scale), (int)Math.Floor(SkyTexture.Height * scale));

            int startX = (int)(-Utils.ModuloNegativeAdjusted(-Camera.Rotation, 360) / 360 * dstRect.Width);

            for (int y = 0; y < GraphicsDevice.Viewport.Height; y += dstRect.Height)
                for (int x = startX; x < GraphicsDevice.Viewport.Width; x += dstRect.Width)
                    SpriteBatch.Draw(SkyTexture, new Rectangle(x, y, dstRect.Width, dstRect.Height), Color.White);

            SpriteBatch.End();
        }

        private void QueueWrapSky()
        {
            float maxZ = CalculateSkyMaxZ();

            float step = MathHelper.ToRadians(360f / RenderConstants.SKY_SEGMENTS);
            float texWidth = 1f / RenderConstants.SKY_SEGMENTS;
            float halfTexWidth = texWidth / 2f;

            for (int i = 0; i < RenderConstants.SKY_SEGMENTS; i++)
            {
                float startAngle = (i * step) - (step / 2);
                float endAngle = startAngle + step;

                Vector2 start = Vector2.Transform(new Vector2(0, RenderConstants.SKY_DISTANCE), Matrix.CreateRotationZ(startAngle)) + new Vector2(Camera.Position.X, Camera.Position.Y);
                Vector2 end = Vector2.Transform(new Vector2(0, RenderConstants.SKY_DISTANCE), Matrix.CreateRotationZ(endAngle)) + new Vector2(Camera.Position.X, Camera.Position.Y);

                float texMiddle = texWidth * i;

                var topLeft = new VertexPositionColorTexture(
                    new Vector3(start, maxZ),
                    Color.White,
                    new Vector2(texMiddle - halfTexWidth, 0)
                    );

                var topRight = new VertexPositionColorTexture(
                    new Vector3(end, maxZ),
                    Color.White,
                    new Vector2(texMiddle + halfTexWidth, 0)
                    );

                var bottomRight = new VertexPositionColorTexture(
                    new Vector3(end, PhysicsConstants.LAND_HEIGHT * PhysicsConstants.STEP_SIZE_FLOAT),
                    Color.White,
                    new Vector2(texMiddle + halfTexWidth, 1)
                    );

                var bottomLeft = new VertexPositionColorTexture(
                    new Vector3(start, PhysicsConstants.LAND_HEIGHT * PhysicsConstants.STEP_SIZE_FLOAT),
                    Color.White,
                    new Vector2(texMiddle - halfTexWidth, 1)
                    );

                QueueQuad(topLeft, topRight, bottomRight, bottomLeft);
            }
        }

        private float CalculateSkyMaxZ()
        {
            var viewport = GraphicsDevice.Viewport;

            Vector3 skyCheckPos = Vector3.Transform(new Vector3(0, RenderConstants.SKY_DISTANCE, 0), Matrix.CreateRotationZ(MathHelper.ToRadians(Camera.Rotation)))
                + Camera.Position;

            var camMatrix = Camera.ViewMatrix;

            var groundVector = viewport.Project(new Vector3(skyCheckPos.X, skyCheckPos.Y, PhysicsConstants.LAND_HEIGHT * PhysicsConstants.STEP_SIZE_FLOAT),
                ProjectionMatrix, camMatrix, Matrix.Identity);

            if (groundVector.Y > 0)
            {
                float maxZ = RenderConstants.SKY_MINIMUM_HEIGHT;
                var minHeightVector = viewport.Project(new Vector3(skyCheckPos.X, skyCheckPos.Y, RenderConstants.SKY_MINIMUM_HEIGHT),
                    ProjectionMatrix, camMatrix, Matrix.Identity);

                if (minHeightVector.Y > 0)
                {
                    float lowerBound = RenderConstants.SKY_MINIMUM_HEIGHT;
                    float upperBound = float.PositiveInfinity;
                    maxZ = RenderConstants.SKY_MINIMUM_HEIGHT;

                    while (Math.Ceiling(minHeightVector.Y) != 0)
                    {
                        if (minHeightVector.Y < 0)
                        {
                            upperBound = maxZ;
                            maxZ = (lowerBound + upperBound) / 2;
                        }
                        else
                        {
                            lowerBound = maxZ;
                            maxZ = (upperBound == float.PositiveInfinity) ? maxZ * 2 : (lowerBound + upperBound) / 2;
                        }

                        minHeightVector = viewport.Project(new Vector3(skyCheckPos.X, skyCheckPos.Y, maxZ),
                            ProjectionMatrix, camMatrix, Matrix.Identity);
                    }
                }

                return maxZ;
            }
            else
                return -1;
        }

        public void RenderLands(List<LandRegion> lands)
        {
            int landIndex = 0;
            while (landIndex < lands.Count)
            {
                string texture = lands[landIndex].Texture;
                if (texture == "")
                {
                    landIndex++;
                    continue;
                }

                for (int i = landIndex; i < lands.Count; i++)
                    if (lands[landIndex].Texture != texture)
                        break;
                    else
                    {
                        QueueLand(lands[landIndex]);
                        landIndex++;
                    }

                Texture = LandTextures[texture];
                RenderQueue(false, _StencilStateZNone);
            }
        }

        public void QueueLand(LandRegion land)
        {
            var shade = CreateShadingColor(land.Shading, null);
            VertexQueue.AddRange(land.TriangulatedVertices.Select(lv =>
            {
                Vector3 pos = new Vector3(lv.X * PhysicsConstants.STEP_SIZE_FLOAT, lv.Y * PhysicsConstants.STEP_SIZE_FLOAT, PhysicsConstants.LAND_HEIGHT * PhysicsConstants.STEP_SIZE_FLOAT);
                Vector2 texturePos = new Vector2(-pos.X, pos.Y);

                return new VertexPositionColorTexture(pos, shade, texturePos);
            }
                ));
        }

        // Entity Queue Rendering

        public void RenderEntityQueue(List<RenderEntity> queue, bool isBlocks)
        {
            var newQueue = isBlocks ? SortEntitiesByDistances(queue) : queue;
            RenderSortedEntityQueue(newQueue.ToArray(), isBlocks);
            queue.Clear();
        }

        private void RenderSortedEntityQueue(RenderEntity[] queue, bool isBlocks)
        {
            int completed = 0;
            while (completed < queue.Length)
            {
                var first = queue[completed];
                while (completed < queue.Length && queue[completed].Texture == first.Texture && queue[completed].Transparency == first.Transparency)
                {
                    VertexQueue.AddRange(queue[completed].Vertices);
                    completed++;
                }

                Texture = first.Texture;
                RenderQueue(first.Transparency, isBlocks ? _StencilStateZWriteOnly : _StencilStateZApply);
            }
        }

        private Dictionary<Vector3, float> __ResultDistCache = new Dictionary<Vector3, float>();

        private IEnumerable<RenderEntity> SortEntitiesByDistances(List<RenderEntity> entities)
        {
            var viewport = GraphicsDevice.Viewport;
            var viewMatrix = Camera.ViewMatrix;
            var identity = Matrix.Identity;

            __ResultDistCache.Clear();

            return entities
                .OrderByDescending(e =>
                {
                    float result;
                    if (__ResultDistCache.ContainsKey(e.Midpoint))
                        result = __ResultDistCache[e.Midpoint];
                    else
                    {
                        var transformedMidpoint = Camera.Position - e.Midpoint;

                        result = transformedMidpoint.Length();
                        __ResultDistCache.Add(e.Midpoint, result);
                    }

                    return result + e.Bias;
                }
                )
                .ThenBy(re => re.Priority);
        }

        // Entity Queue Preparation

        public void AddModelToEntityQueue(List<RenderEntity> queue, LoapModel model, Vector3 position, float rotation, float pitch,
            Texture2D texture, bool textureTransparency, int priority)
        {
            AddModelToEntityQueue(queue, model, position, position, rotation, pitch, texture, textureTransparency, priority);
        }

        public void AddModelToEntityQueue(List<RenderEntity> queue, LoapModel model, Vector3 position, Vector3 midpoint, float rotation, float pitch,
            Texture2D texture, bool textureTransparency, int priority)
        {
            Matrix transform = Matrix.CreateRotationX(MathHelper.ToRadians(pitch)) * Matrix.CreateRotationZ(MathHelper.ToRadians(rotation)) *
                Matrix.CreateTranslation(position);

            queue.Add(RenderEntity.Create(
                texture, textureTransparency, position,
                model.Vertices.Select(v =>
                    new VertexPositionColorTexture(
                        Vector3.Transform(v.Position, transform),
                        Color.White,
                        v.TextureCoordinate
                        )
                    ).ToArray(),
                priority, 0f
                ));
        }

        public void AddBlocksToEntityQueue(List<RenderEntity> queue, BlockCacheEntry[] blockCache, BlockExclusions exclusions,
            int iteration, Dictionary<Point3D, int>? damageIterations, Func<Point3D, Direction, int>? getExitFrameFunc)
        {
            queue.AddRange(blockCache.Where(cacheEntry => cacheEntry.Overlay || !CheckIfInExclusions(cacheEntry, exclusions)).Select(cacheEntry =>
                {
                    int frameIndex;

                    if (cacheEntry.AnimatedExitFace)
                    {
                        if (getExitFrameFunc != null)
                            frameIndex = getExitFrameFunc(cacheEntry.StructurePosition.Position, cacheEntry.StructurePosition.Direction);
                        else
                            frameIndex = 0;
                    }
                    else
                        frameIndex = Utils.ModuloNegativeAdjusted(iteration / 2, cacheEntry.FrameEntries.Length);

                    var frameEntry = cacheEntry.FrameEntries[frameIndex];

                    return RenderEntity.Create(
                        frameEntry.Texture ?? RenderConstants.BlankTexture,
                        cacheEntry.Transparency,
                        cacheEntry.Midpoint,
                        cacheEntry.FrameVertices[frameIndex],
                        cacheEntry.Inside ? RenderConstants.RENDER_PRIORITY_BLOCKS_INNER : RenderConstants.RENDER_PRIORITY_BLOCKS_OUTER,
                        0f
                        );
                }
                ));

            if (damageIterations != null && CrackTexture != null)
                queue.AddRange(blockCache.Where(cacheEntry => !cacheEntry.Overlay &&
                    !CheckIfInExclusions(cacheEntry, exclusions) &&
                    damageIterations.ContainsKey(cacheEntry.StructurePosition.Position) &&
                    damageIterations[cacheEntry.StructurePosition.Position] >= 3 &&
                    cacheEntry.FrameVertices.Length > 0)
                    .Select(cacheEntry =>
                    {
                        var moddedVertices = new VertexPositionColorTexture[cacheEntry.FrameVertices[0].Length];

                        float highX = cacheEntry.FrameVertices[0].Select(v => v.TextureCoordinate.X).Max();
                        float highY = cacheEntry.FrameVertices[0].Select(v => v.TextureCoordinate.Y).Max();

                        for (int i = 0; i < moddedVertices.Length; i++)
                        {
                            var vertex = cacheEntry.FrameVertices[0][i];
                            if (vertex.TextureCoordinate.X == highX && (highX % 0.25f == 0))
                                vertex.TextureCoordinate.X = 0.25f;
                            else
                                vertex.TextureCoordinate.X %= 0.25f;
                            if (vertex.TextureCoordinate.Y == highY && (highY % 0.25f == 0))
                                vertex.TextureCoordinate.Y = 0.25f;
                            else
                                vertex.TextureCoordinate.Y %= 0.25f;
                            vertex.TextureCoordinate.X *= 4;
                            vertex.TextureCoordinate.Y *= 4;
                            vertex.TextureCoordinate.Y /= 16f;
                            vertex.TextureCoordinate.Y += Math.Clamp(damageIterations[cacheEntry.StructurePosition.Position] / 3 - 3, 0, 15) / 16f;
                            moddedVertices[i] = vertex;
                        }

                        return RenderEntity.Create(
                            CrackTexture, true, cacheEntry.Midpoint,
                            moddedVertices,
                            cacheEntry.Inside ? RenderConstants.RENDER_PRIORITY_CRACKS_INNER : RenderConstants.RENDER_PRIORITY_CRACKS_OUTER,
                            0f
                            );
                    }
                    ));
        }

        private bool CheckIfInExclusions(BlockCacheEntry entry, BlockExclusions exclusions)
        {
            return exclusions[
                entry.StructurePosition.Position.X,
                entry.StructurePosition.Position.Y,
                entry.StructurePosition.Position.Z,
                entry.StructurePosition.Direction
                ];
        }

        private static VertexPositionColorTexture[] _HatchFlapVertices = new[]
        {
            new VertexPositionColorTexture(new Vector3(0.5f, 0, 0), Color.White, new Vector2(0, 0)),
            new VertexPositionColorTexture(new Vector3(-0.5f, 0, 0), Color.White, new Vector2(1, 0)),
            new VertexPositionColorTexture(new Vector3(-0.5f, -0.5f, 0), Color.White, new Vector2(1, 1)),
            new VertexPositionColorTexture(new Vector3(0.5f, -0.5f, 0), Color.White, new Vector2(0, 1))
        };

        public void AddHatchFlapsToEntityQueue(List<RenderEntity> queue, List<HatchFlap> flaps, int iteration)
        {
            float rotateDegrees = Math.Clamp(180f - (iteration * 135f / RenderConstants.HATCH_OPEN_ITERATIONS), 45f, 180f);

            foreach (var flap in flaps)
            {
                var localVertices = new VertexPositionColorTexture[4];

                Vector3 topLeft = flap.Side switch
                {
                    Direction.XMinus => new Vector3(0, 0.5f, 0),
                    Direction.YMinus => new Vector3(-0.5f, 0, 0),
                    Direction.XPlus => new Vector3(0, -0.5f, 0),
                    Direction.YPlus => new Vector3(0.5f, 0, 0),
                    _ => throw new ArgumentException()
                };

                Vector3 topRight = flap.Side switch
                {
                    Direction.XMinus => new Vector3(0, -0.5f, 0),
                    Direction.YMinus => new Vector3(0.5f, 0, 0),
                    Direction.XPlus => new Vector3(0, 0.5f, 0),
                    Direction.YPlus => new Vector3(-0.5f, 0, 0),
                    _ => throw new ArgumentException()
                };

                Vector3 bottomLeft = flap.Side switch
                {
                    Direction.XMinus => new Vector3(0.5f, 0.5f, 0),
                    Direction.YMinus => new Vector3(-0.5f, 0.5f, 0),
                    Direction.XPlus => new Vector3(-0.5f, -0.5f, 0),
                    Direction.YPlus => new Vector3(0.5f, -0.5f, 0),
                    _ => throw new ArgumentException()
                };

                Vector3 bottomRight = flap.Side switch
                {
                    Direction.XMinus => new Vector3(0.5f, -0.5f, 0),
                    Direction.YMinus => new Vector3(0.5f, 0.5f, 0),
                    Direction.XPlus => new Vector3(-0.5f, 0.5f, 0),
                    Direction.YPlus => new Vector3(-0.5f, -0.5f, 0),
                    _ => throw new ArgumentException()
                };

                var rotateMatrix = flap.Side switch
                {
                    Direction.XMinus => Matrix.CreateRotationY(MathHelper.ToRadians(rotateDegrees)),
                    Direction.XPlus => Matrix.CreateRotationY(MathHelper.ToRadians(-rotateDegrees)),
                    Direction.YMinus => Matrix.CreateRotationX(MathHelper.ToRadians(-rotateDegrees)),
                    Direction.YPlus => Matrix.CreateRotationX(MathHelper.ToRadians(rotateDegrees)),
                    _ => throw new ArgumentException()
                };

                if (flap.OutsideTexture.File != "" && flap.OutsideTexture.Index >= 0)
                {
                    int textureIndex = flap.OutsideTexture.Index;
                    int xSlot = (textureIndex % 16) / 4;
                    int ySlot = textureIndex / 16;
                    int ySubSlot = textureIndex % 4;

                    var topLeftVertex = new VertexPositionColorTexture(
                        Vector3.Transform(topLeft, rotateMatrix) + flap.Origin,
                        CreateShadingColor(flap.OutsideTexture.Shading, flap.Side),
                        AdjustBlockTextureCoordinate(new Vector2(0, 1), flap.OutsideTexture, xSlot, ySlot, ySubSlot, false)
                        );

                    var topRightVertex = new VertexPositionColorTexture(
                        Vector3.Transform(topRight, rotateMatrix) + flap.Origin,
                        CreateShadingColor(flap.OutsideTexture.Shading, flap.Side),
                        AdjustBlockTextureCoordinate(new Vector2(1, 1), flap.OutsideTexture, xSlot, ySlot, ySubSlot, false)
                        );

                    var bottomLeftVertex = new VertexPositionColorTexture(
                        Vector3.Transform(bottomLeft, rotateMatrix) + flap.Origin,
                        CreateShadingColor(flap.OutsideTexture.Shading, flap.Side),
                        AdjustBlockTextureCoordinate(new Vector2(0, 3), flap.OutsideTexture, xSlot, ySlot, ySubSlot, false)
                        );

                    var bottomRightVertex = new VertexPositionColorTexture(
                        Vector3.Transform(bottomRight, rotateMatrix) + flap.Origin,
                        CreateShadingColor(flap.OutsideTexture.Shading, flap.Side),
                        AdjustBlockTextureCoordinate(new Vector2(1, 3), flap.OutsideTexture, xSlot, ySlot, ySubSlot, false)
                        );

                    Vector3 midpoint;
                    if (iteration == 0)
                        midpoint = flap.Origin - flap.Side switch
                        {
                            Direction.XMinus => new Vector3(0.5f, 0, 0),
                            Direction.XPlus => new Vector3(-0.5f, 0, 0),
                            Direction.YMinus => new Vector3(0, 0.5f, 0),
                            Direction.YPlus => new Vector3(0, -0.5f, 0),
                            _ => throw new ArgumentException()
                        };
                    else
                        midpoint = (topLeftVertex.Position + topRightVertex.Position + bottomLeftVertex.Position + bottomRightVertex.Position) / 4;

                    queue.Add(RenderEntity.Create(
                        BlockTextures[flap.OutsideTexture.File], true, midpoint,
                        new[] { topLeftVertex, topRightVertex, bottomRightVertex, topLeftVertex, bottomRightVertex, bottomLeftVertex },
                        RenderConstants.RENDER_PRIORITY_HATCH_FLAPS, 0
                        ));
                }

                if (flap.InsideTexture.File != "" && flap.InsideTexture.Index >= 0)
                {
                    int textureIndex = flap.InsideTexture.Index;
                    int xSlot = (textureIndex % 16) / 4;
                    int ySlot = textureIndex / 16;
                    int ySubSlot = textureIndex % 4;

                    var topLeftVertex = new VertexPositionColorTexture(
                        Vector3.Transform(topRight, rotateMatrix) + flap.Origin,
                        CreateShadingColor(flap.OutsideTexture.Shading, flap.Side),
                        AdjustBlockTextureCoordinate(new Vector2(0, 1), flap.OutsideTexture, xSlot, ySlot, ySubSlot, false)
                        );

                    var topRightVertex = new VertexPositionColorTexture(
                        Vector3.Transform(topLeft, rotateMatrix) + flap.Origin,
                        CreateShadingColor(flap.OutsideTexture.Shading, flap.Side),
                        AdjustBlockTextureCoordinate(new Vector2(1, 1), flap.OutsideTexture, xSlot, ySlot, ySubSlot, false)
                        );

                    var bottomLeftVertex = new VertexPositionColorTexture(
                        Vector3.Transform(bottomRight, rotateMatrix) + flap.Origin,
                        CreateShadingColor(flap.OutsideTexture.Shading, flap.Side),
                        AdjustBlockTextureCoordinate(new Vector2(0, 3), flap.OutsideTexture, xSlot, ySlot, ySubSlot, false)
                        );

                    var bottomRightVertex = new VertexPositionColorTexture(
                        Vector3.Transform(bottomLeft, rotateMatrix) + flap.Origin,
                        CreateShadingColor(flap.OutsideTexture.Shading, flap.Side),
                        AdjustBlockTextureCoordinate(new Vector2(1, 3), flap.OutsideTexture, xSlot, ySlot, ySubSlot, false)
                        );

                    Vector3 midpoint;
                    if (iteration == 0)
                        midpoint = flap.Origin - flap.Side switch
                        {
                            Direction.XMinus => new Vector3(0.5f, 0, 0),
                            Direction.XPlus => new Vector3(-0.5f, 0, 0),
                            Direction.YMinus => new Vector3(0, 0.5f, 0),
                            Direction.YPlus => new Vector3(0, -0.5f, 0),
                            _ => throw new ArgumentException()
                        };
                    else
                        midpoint = (topLeftVertex.Position + topRightVertex.Position + bottomLeftVertex.Position + bottomRightVertex.Position) / 4;

                    queue.Add(RenderEntity.Create(
                        BlockTextures[flap.InsideTexture.File], true, midpoint,
                        new[] { topLeftVertex, topRightVertex, bottomRightVertex, topLeftVertex, bottomRightVertex, bottomLeftVertex },
                        RenderConstants.RENDER_PRIORITY_HATCH_FLAPS, 0
                        ));
                }
            }
        }

        /// <summary>
        /// The placement of the billboard will be the middle on the X/Y axes, and the bottom on the Z axis
        /// </summary>
        public void AddBillboardToEntityQueue(List<RenderEntity> queue, Vector3 position, Vector2 size, FloatRectangle sourceRect, Texture2D texture, int priority, Color color)
        {
            Matrix transform = Matrix.CreateRotationZ(MathHelper.ToRadians(Camera.Rotation)) *
                Matrix.CreateTranslation(position);

            var topLeft = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(-size.X / 2f, 0, 0), transform),
                color,
                new Vector2(sourceRect.X, sourceRect.Y + sourceRect.Height)
                );

            var topRight = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(size.X / 2f, 0, 0), transform),
                color,
                new Vector2(sourceRect.X + sourceRect.Width, sourceRect.Y + sourceRect.Height)
                );

            var bottomRight = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(size.X / 2f, 0, size.Y), transform),
                color,
                new Vector2(sourceRect.X + sourceRect.Width, sourceRect.Y)
                );

            var bottomLeft = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(-size.X / 2f, 0, size.Y), transform),
                color,
                new Vector2(sourceRect.X, sourceRect.Y)
                );

            queue.Add(RenderEntity.Create(
                texture, true,
                new Vector3(position.X, position.Y, position.Z + (size.Y / 2)),
                new[] { topLeft, topRight, bottomLeft, topRight, bottomRight, bottomLeft },
                priority,
                -RenderConstants.BILLBOARD_BIAS
                ));
        }

        public void AddSpawnIndicatorToEntityQueue(List<RenderEntity> queue, Vector3 position, Direction direction, Texture2D texture)
        {
            var texturePoints = new[]
            {
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(1, 1),
                new Vector2(0, 1)
            };

            for (int i = 0; i < Utils.DirectionToRotation(direction); i++)
                for (int vi = 0; vi < 4; vi++)
                    texturePoints[vi] = new Vector2(1 - texturePoints[vi].Y, texturePoints[vi].X);

            var topLeft = new VertexPositionColorTexture(
                new Vector3(0, 1, 0) + position,
                Color.White,
                texturePoints[0]
                );

            var topRight = new VertexPositionColorTexture(
                new Vector3(1, 1, 0) + position,
                Color.White,
                texturePoints[1]
                );

            var bottomLeft = new VertexPositionColorTexture(
                new Vector3(0, 0, 0) + position,
                Color.White,
                texturePoints[3]
                );

            var bottomRight = new VertexPositionColorTexture(
                new Vector3(1, 0, 0) + position,
                Color.White,
                texturePoints[2]
                );

            queue.Add(RenderEntity.Create(
                texture, true,
                position,
                new[] { topLeft, topRight, bottomLeft, topRight, bottomRight, bottomLeft },
                RenderConstants.RENDER_PRIORITY_SPAWN_DIRECTION,
                -RenderConstants.BILLBOARD_BIAS
                ));

            queue.Add(RenderEntity.Create(
                texture, true,
                position,
                new[] { topLeft, bottomLeft, topRight, topRight, bottomLeft, bottomRight },
                RenderConstants.RENDER_PRIORITY_SPAWN_DIRECTION,
                -RenderConstants.BILLBOARD_BIAS
                ));
        }

        private void AddFlatGraphicToEntityQueue(List<RenderEntity> queue, Vector3 position, Vector2 size, float rotation, FloatRectangle sourceRect, Texture2D texture,
            int priority, Color color, bool doubleSided)
        {
            Matrix transform = Matrix.CreateRotationZ(MathHelper.ToRadians(rotation)) *
                Matrix.CreateTranslation(position);

            var topLeft = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(0, -size.X / 2f, 0), transform),
                color,
                new Vector2(sourceRect.X + sourceRect.Width, sourceRect.Y + sourceRect.Height)
                );

            var topRight = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(0, size.X / 2f, 0), transform),
                color,
                new Vector2(sourceRect.X, sourceRect.Y + sourceRect.Height)
                );

            var bottomRight = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(0, size.X / 2f, size.Y), transform),
                color,
                new Vector2(sourceRect.X, sourceRect.Y)
                );

            var bottomLeft = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(0, -size.X / 2f, size.Y), transform),
                color,
                new Vector2(sourceRect.X + sourceRect.Width, sourceRect.Y)
                );

            queue.Add(RenderEntity.Create(
                texture, true,
                new Vector3(position.X, position.Y, position.Z + (size.Y / 2)),
                new[] { topLeft, topRight, bottomLeft, topRight, bottomRight, bottomLeft },
                priority,
                -RenderConstants.BILLBOARD_BIAS
                ));

            if (doubleSided)
                queue.Add(RenderEntity.Create(
                    texture, true,
                    new Vector3(position.X, position.Y, position.Z + (size.Y / 2)),
                    new[] { bottomLeft, topRight, topLeft, bottomLeft, bottomRight, topRight },
                    priority,
                    -RenderConstants.BILLBOARD_BIAS
                    ));
        }

        private void AddFloorGraphicToEntityQueue(List<RenderEntity> queue, Vector3 position, float rotation, FloatRectangle sourceRect, Texture2D texture,
            int priority, Color color, bool doubleSided)
        {
            Matrix transform = Matrix.CreateRotationZ(MathHelper.ToRadians(rotation)) *
                Matrix.CreateTranslation(position);

            var topLeft = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(-0.5f, -0.5f, 0), transform),
                color,
                new Vector2(sourceRect.X, sourceRect.Y + sourceRect.Height)
                );

            var topRight = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(0.5f, -0.5f, 0), transform),
                color,
                new Vector2(sourceRect.X + sourceRect.Width, sourceRect.Y + sourceRect.Height)
                );

            var bottomRight = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(0.5f, 0.5f, 0), transform),
                color,
                new Vector2(sourceRect.X + sourceRect.Width, sourceRect.Y)
                );

            var bottomLeft = new VertexPositionColorTexture(
                Vector3.Transform(new Vector3(-0.5f, 0.5f, 0), transform),
                color,
                new Vector2(sourceRect.X, sourceRect.Y)
                );

            queue.Add(RenderEntity.Create(
                    texture, true,
                    new Vector3(position.X, position.Y, position.Z),
                    new[] { bottomLeft, topRight, topLeft, bottomLeft, bottomRight, topRight },
                    priority,
                    -RenderConstants.BILLBOARD_BIAS
                    ));

            if (doubleSided)
                queue.Add(RenderEntity.Create(
                texture, true,
                new Vector3(position.X, position.Y, position.Z),
                new[] { topLeft, topRight, bottomLeft, topRight, bottomRight, bottomLeft },
                priority,
                -RenderConstants.BILLBOARD_BIAS
                ));
        }

        public void AddLevelEntitiesToEntityQueue(List<RenderEntity> queue, List<EntityInstance> instances, bool faceEntities)
        {
            foreach (var instance in instances.Where(i => faceEntities ? i.Height == 0 : i.Height > 0))
                AddLevelEntityToEntityQueue(queue, instance);
        }

        public void AddLevelEntityToEntityQueue(List<RenderEntity> queue, EntityInstance instance)
        {
            if (instance.Effect == EntityEffect.RopeSlideStart || instance.Effect == EntityEffect.RopeSlideEnd)
                return;

            var texture = EntityTextures[instance.Identifier];

            var position = new Vector3(instance.Position.X, instance.Position.Y, instance.Position.Z * PhysicsConstants.SEGMENT_SIZE_FLOAT);
            position += new Vector3(PhysicsConstants.HALF_BLOCK_STEPS * PhysicsConstants.STEP_SIZE_FLOAT, PhysicsConstants.HALF_BLOCK_STEPS * PhysicsConstants.STEP_SIZE_FLOAT, 0);
            position += new Vector3(instance.Offset.X * PhysicsConstants.STEP_SIZE_FLOAT, instance.Offset.Y * PhysicsConstants.STEP_SIZE_FLOAT, instance.Offset.Z * PhysicsConstants.STEP_SIZE_FLOAT);

            FloatRectangle srcRect;
            if (instance.Frames < 2)
                srcRect = new FloatRectangle(0, 0, 1, 1);
            else
            {
                int frame = Utils.CalculateAnimationFrameIndex(instance.Iteration / 2, instance.Frames, instance.AnimationStyle);
                srcRect = new FloatRectangle(0, frame * (1f / instance.Frames), 1, 1f / instance.Frames);
            }

            if (instance.Height > 0)
            {
                Vector2 size = new Vector2(0, instance.Height * PhysicsConstants.SEGMENT_SIZE_FLOAT);
                size.X = (texture.Width / (texture.Height / (float)instance.Frames)) * size.Y;

                if (instance.Directionality == EntityDirectionality.None)
                    AddBillboardToEntityQueue(queue, position, size, srcRect, texture, RenderConstants.RENDER_PRIORITY_ENTITIES, Color.White);
                else
                    AddFlatGraphicToEntityQueue(queue, position, size, instance.Rotations * 90, srcRect, texture, RenderConstants.RENDER_PRIORITY_ENTITIES, Color.White, true);
            }
            else
                AddFloorGraphicToEntityQueue(queue, position, instance.Rotations * 90, srcRect, texture, RenderConstants.RENDER_PRIORITY_ENTITIES, Color.White, true);
        }

        // Misc

        private static Color CreateShadingColor(int shadeValue, Direction? side)
        {
            if (side.HasValue)
                switch (side.Value)
                {
                    case Direction.XMinus:
                    case Direction.XPlus:
                        shadeValue += 1;
                        break;
                    case Direction.YMinus:
                    case Direction.YPlus:
                        shadeValue += 2;
                        break;
                    case Direction.ZMinus:
                        shadeValue += 3;
                        break;
                }

            return RenderConstants.Shades[Math.Clamp(shadeValue, 0, 8)];
        }

        public static Matrix CalculateProjectionMatrix(GraphicsDevice graphicsDevice)
        {
            return Matrix.CreatePerspectiveFieldOfView(
                RenderConstants.FIELD_OF_VIEW,
                graphicsDevice.Viewport.Width / (float)graphicsDevice.Viewport.Height,
                RenderConstants.NEAR_PLANE_DISTANCE,
                RenderConstants.FAR_PLANE_DISTANCE
                );
        }
    }
}
