﻿using Loap.Constants;
using LoapCore.Constants;
using LoapCore.Lemmings;
using LoapCore.Misc;
using Microsoft.Xna.Framework;
using System;

namespace Loap.Rendering
{
    class Camera
    {
        public Camera() : this(new Vector3(0, 0, 0), 0, 0)
        { }

        public Camera(Vector3 position, float rotation, float pitch)
        {
            Position = position;
            Rotation = rotation;
            Pitch = pitch;
        }

        public Camera(Camera source)
        {
            _Position = source._Position;
            _Rotation = source._Rotation;
            _VirtualLemmingRotation = source._VirtualLemmingRotation;
            _Pitch = source._Pitch;
            _VirtualLemmingPitch = source._VirtualLemmingPitch;
            VirtualLemming = source.VirtualLemming;
        }

        private Vector3 _Position;
        public Vector3 Position
        {
            get
            {
                return (VirtualLemming == null) ?
                    _Position :
                    new Vector3(
                        (VirtualLemming.X * PhysicsConstants.STEP_SIZE_FLOAT),
                        (VirtualLemming.Y * PhysicsConstants.STEP_SIZE_FLOAT),
                        Math.Max((VirtualLemming.Z * PhysicsConstants.STEP_SIZE_FLOAT) + PhysicsConstants.SEGMENT_SIZE_FLOAT, PhysicsConstants.CAMERA_MIN_Z)
                        ) +
                    Vector3.Transform(new Vector3(0, RenderConstants.VIRTUAL_LEMMING_OFFSET, 0), Matrix.CreateRotationZ(MathHelper.ToRadians(Rotation)));
            }

            set
            {
                if (VirtualLemming == null)
                    _Position = new Vector3(value.X, value.Y, Math.Max(value.Z, PhysicsConstants.CAMERA_MIN_Z + UIConstants.CAMERA_SOLIDITY_CHECK_OFFSET));
            }
        }

        public Vector3 NonVirtualLemmingPosition => _Position;

        private float _Rotation;
        private float _VirtualLemmingRotation;
        private float _VirtualLemmingRotationMod;
        public float Rotation
        {
            get
            {
                if (VirtualLemming == null)
                    return _Rotation;
                else
                {
                    return _VirtualLemmingRotation + _VirtualLemmingRotationMod;
                }
            }

            set
            {
                if (VirtualLemming == null)
                    _Rotation = Utils.ModuloNegativeAdjusted(value, 360);
                else
                {
                    int skillMod = VirtualLemming.Action == LemmingAction.Blocker ? 90 :
                        VirtualLemming.Action == LemmingAction.Turner ? 180 :
                        0;

                    _VirtualLemmingRotationMod = Math.Clamp(value - _VirtualLemmingRotation, -89, 89);
                }
            }
        }

        private float BaseVirtualLemmingTargetRotation
        {
            get
            {
                if (_VirtualLemming == null)
                    return 0;

                int skillMod = _VirtualLemming.Action == LemmingAction.Blocker ? 90 :
                        _VirtualLemming.Action == LemmingAction.Turner ? 180 :
                        0;

                return Utils.ModuloNegativeAdjusted(Utils.DirectionToDegrees(_VirtualLemming.Direction) + skillMod, 360);
            }
        }

        public void UpdateVirtualLemmingRotation(float elapsedTime)
        {
            float target = BaseVirtualLemmingTargetRotation;
            _VirtualLemmingRotation = Utils.ModuloNegativeAdjusted(_VirtualLemmingRotation, 360);


            if (_VirtualLemmingRotation - target != 0)
            {
                float moveAmount = UIConstants.CAMERA_VIRTUAL_LEMMING_ROTATE_MOD * elapsedTime;

                float diffUpwards = Utils.ModuloNegativeAdjusted(target - _VirtualLemmingRotation, 360);
                float diffDownwards = Utils.ModuloNegativeAdjusted(_VirtualLemmingRotation - target, 360);

                if (diffUpwards <= moveAmount || diffDownwards <= moveAmount)
                    _VirtualLemmingRotation = target;
                else if (diffDownwards < diffUpwards)
                    _VirtualLemmingRotation -= moveAmount;
                else
                    _VirtualLemmingRotation += moveAmount;
            }
        }

        private float _Pitch;
        private float _VirtualLemmingPitch;
        public float Pitch
        {
            get
            {
                return (VirtualLemming == null) ?
                    _Pitch :
                    _VirtualLemmingPitch;
            }
            set
            {
                if (VirtualLemming == null)
                    _Pitch = Math.Clamp(value, -PhysicsConstants.CAMERA_PITCH_CAP, PhysicsConstants.CAMERA_PITCH_CAP);
                else
                    _VirtualLemmingPitch = Math.Clamp(value, -PhysicsConstants.CAMERA_PITCH_CAP, PhysicsConstants.CAMERA_PITCH_CAP);
            }
        }

        private Lemming? _VirtualLemming;
        public Lemming? VirtualLemming
        {
            get => _VirtualLemming;
            set
            {
                bool isNew = false;
                if (value != null)
                    if (_VirtualLemming == null || value.Identifier != _VirtualLemming.Identifier)
                        isNew = true;

                _VirtualLemming = value;

                if (isNew)
                {
                    _VirtualLemmingRotation = BaseVirtualLemmingTargetRotation;
                    _VirtualLemmingRotationMod = 0;
                    _VirtualLemmingPitch = 0;
                }
            }
        }

        public Vector3 ForwardVector
        {
            get
            {
                Vector3 forward = new Vector3(0, 1, 0);
                forward = Vector3.Transform(forward, Matrix.CreateRotationX(MathHelper.ToRadians(Pitch)));
                forward = Vector3.Transform(forward, Matrix.CreateRotationZ(MathHelper.ToRadians(Rotation)));

                return forward;
            }
        }

        public Matrix ViewMatrix
        {
            get
            {
                Vector3 target = Vector3.Transform(ForwardVector, Matrix.CreateTranslation(Position));

                return Matrix.CreateLookAt(Position, target, new Vector3(0, 0, 1));
            }
        }
    }
}
