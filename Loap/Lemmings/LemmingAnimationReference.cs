﻿using LoapCore.Lemmings;

namespace Loap.Lemmings
{
    struct LemmingAnimationReference
    {
        public string SpriteName;
        public int FrameIndex;
        public Lemming Lemming;
        public LemmingSpriteInfo SpriteInfo;
        public int TextureFile;
        public bool PostKeyFrame;

        public LemmingAnimationReference(string spriteName, int frameIndex, bool postKeyFrame, Lemming lemming)
        {
            SpriteName = spriteName;
            FrameIndex = frameIndex;
            PostKeyFrame = postKeyFrame;
            Lemming = lemming;
            SpriteInfo = LemmingSprites.GetSpriteInfo(spriteName);
            TextureFile = SpriteInfo.TextureFile;
        }
    }
}
