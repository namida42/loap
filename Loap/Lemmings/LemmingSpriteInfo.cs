﻿using Loap.Constants;
using Loap.Misc;
using LoapCore.Misc;
using System;

namespace Loap.Lemmings
{
    class LemmingSpriteInfo
    {
        public string Name = "";
        public int FrameCount;
        public int Directions;
        public int TextureFile;
        public int TextureStartPosition;
        public AnimationStyle AnimationStyle;
        public int KeyFrame;

        public LemmingSpriteInfo(string name, int frameCount, int directions, int textureFile, int textureStartPosition, AnimationStyle animStyle, int keyFrame)
        {
            Name = name;
            FrameCount = frameCount;
            Directions = directions;
            TextureFile = textureFile;
            TextureStartPosition = textureStartPosition;
            AnimationStyle = animStyle;
            KeyFrame = keyFrame;
        }

        public FloatRectangle GetFloatRect(float idealRotation, int frameIndex)
        {
            FloatRectangle result = new FloatRectangle(0, 0, 1f / RenderConstants.LEMMING_SPRITES_PER_SHEET_AXIS, 1f / RenderConstants.LEMMING_SPRITES_PER_SHEET_AXIS);
            result.X += TextureStartPosition % RenderConstants.LEMMING_SPRITES_PER_SHEET_AXIS;
            result.Y += TextureStartPosition / RenderConstants.LEMMING_SPRITES_PER_SHEET_AXIS; // This is an integer division intentionally.

            if (Directions > 1)
                result.X += Utils.ModuloNegativeAdjusted((int)Math.Round(idealRotation / (360f / Directions)), Directions);

            result.Y += frameIndex % FrameCount;

            result.X /= RenderConstants.LEMMING_SPRITES_PER_SHEET_AXIS;
            result.Y /= RenderConstants.LEMMING_SPRITES_PER_SHEET_AXIS;

            return result;
        }

        public int GetKeyframeAdjustedFrameIndex(int frame, bool beforeKeyframe)
        {
            if (FrameCount == 1)
                return 0;

            if (KeyFrame <= 0)
                beforeKeyframe = false;
            if (KeyFrame >= FrameCount)
                beforeKeyframe = true;

            int frameCount = beforeKeyframe ? KeyFrame : FrameCount - KeyFrame;
            int frameIndex = Utils.CalculateAnimationFrameIndex(frame, frameCount, AnimationStyle);

            if (beforeKeyframe)
                return frameIndex;
            else
                return frameIndex + KeyFrame;
        }
    }
}
