﻿using Loap.Constants;
using LoapCore.Files;
using LoapCore.Misc;
using System;
using System.Collections.Generic;

namespace Loap.Lemmings
{
    static class LemmingSprites
    {
        public static int TextureSheetCount { get; private set; }

        private static readonly Dictionary<string, LemmingSpriteInfo> _SpriteInfos = new Dictionary<string, LemmingSpriteInfo>();

        public static LemmingSpriteInfo GetSpriteInfo(string spriteName)
        {
            return _SpriteInfos[spriteName];
        }

        static LemmingSprites()
        {
            LoadSpriteMetainfo();
        }

        private static void LoadSpriteMetainfo()
        {
            var spriteInfoBaseItem = DataItemReader.LoadFromFile(PathConstants.LemmingGraphicsMetainfoFilePath);

            TextureSheetCount = 0;

            foreach (var spriteItem in spriteInfoBaseItem.GetChildren(LemmingSpriteKeys.SECTION))
            {
                var newInfo = new LemmingSpriteInfo(
                    spriteItem.GetChildValue(LemmingSpriteKeys.SPRITE_NAME),
                    spriteItem.GetChildValueInt(LemmingSpriteKeys.SPRITE_FRAMES),
                    spriteItem.GetChildValueInt(LemmingSpriteKeys.SPRITE_DIRECTIONS),
                    spriteItem.GetChildValueInt(LemmingSpriteKeys.SPRITE_TEXTURE_FILE),
                    spriteItem.GetChildValueInt(LemmingSpriteKeys.SPRITE_POSITION),
                    spriteItem.GetChildValueEnum(LemmingSpriteKeys.SPRITE_ANIMATION_STYLE, AnimationStyle.Normal),
                    spriteItem.GetChildValueInt(LemmingSpriteKeys.SPRITE_KEY_FRAME)
                    );

                TextureSheetCount = Math.Max(TextureSheetCount, newInfo.TextureFile + 1);

                _SpriteInfos.Add(newInfo.Name, newInfo);
            }
        }
    }
}
