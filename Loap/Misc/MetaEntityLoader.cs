﻿using Loap.Constants;
using LoapCore.Entities;
using LoapCore.Files;
using LoapCore.Levels;
using System.Collections.Generic;
using System.IO;

namespace Loap.Misc
{
    static class MetaEntityLoader
    {
        public static Dictionary<string, MetaEntity> LoadLevelMetaEntities(Level level)
        {
            Dictionary<string, MetaEntity> result = new Dictionary<string, MetaEntity>();

            foreach (var entity in level.Entities)
                if (!result.ContainsKey(entity.Identifier))
                {
                    var localIdentifier = entity.Identifier.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                    var item = DataItemReader.LoadFromFile(Path.Combine(PathConstants.EntitiesPath, localIdentifier + PathConstants.EXT_METAENTITY));
                    var newMetaEntity = new MetaEntity(entity.Identifier);
                    newMetaEntity.LoadFromDataItem(item);
                    result.Add(entity.Identifier, newMetaEntity);
                }

            return result;
        }
    }
}
