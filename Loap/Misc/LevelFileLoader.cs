﻿using Loap.Constants;
using LoapCore.Files;
using LoapCore.L3DImport;
using LoapCore.Levels;
using System.IO;

namespace Loap.Misc
{
    class LevelFileLoader
    {
        private static string _LastL3DConvertPath = "";

        public static Level LoadLevel(string levelFile)
        {
            if (Path.GetExtension(levelFile).ToLowerInvariant() != PathConstants.EXT_LEVEL)
            {
                // Attempt to detect and convert L3D level files

                string path = Path.GetDirectoryName(levelFile) ?? Path.GetPathRoot(levelFile) ?? "";
                if (!Path.EndsInDirectorySeparator(path))
                    path += Path.DirectorySeparatorChar;
                string name = Path.GetFileNameWithoutExtension(levelFile);
                string ext = Path.GetExtension(levelFile);
                if (File.Exists(path + "LEVEL" + ext) && File.Exists(path + "BLK" + ext))
                {
                    LoadTranslationData(path);
                    return L3DConverter.LoadL3DLevel(path + "LEVEL" + ext, path + "BLK" + ext);
                }
                else if (File.Exists(path + name + ".LEVEL") && File.Exists(path + name + ".BLK"))
                {
                    LoadTranslationData(path);
                    return L3DConverter.LoadL3DLevel(path + name + ".LEVEL", path + name + ".BLK");
                }
            }

            DataItem item = DataItemReader.LoadFromFile(levelFile);
            return LevelLoader.LoadLevelFromDataItem(item);
        }

        private static void LoadTranslationData(string path)
        {
            var pathInLevels = Path.GetRelativePath(PathConstants.LevelsPath, path);
            if (!pathInLevels.StartsWith('.') && !Path.IsPathRooted(pathInLevels))
            {
                if (_LastL3DConvertPath != pathInLevels)
                {
                    L3DTranslation.ResetToDefaultTranslationData();

                    var pathElements = pathInLevels.Split(new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
                    string pathSoFar = PathConstants.LevelsPath;
                    foreach (var folder in pathElements)
                    {
                        if (!string.IsNullOrEmpty(folder))
                        {
                            pathSoFar = Path.Combine(pathSoFar, folder);
                            if (File.Exists(Path.Combine(pathSoFar, PathConstants.TRANSLATION_FILE_NAME)))
                            {
                                DataItem addTranslationData = DataItemReader.LoadFromFile(Path.Combine(pathSoFar, PathConstants.TRANSLATION_FILE_NAME));
                                L3DTranslation.LoadTranslationData(addTranslationData);
                            }
                        }
                    }

                    _LastL3DConvertPath = pathInLevels;
                }
            }
            else
            {
                if (_LastL3DConvertPath != "")
                    L3DTranslation.ResetToDefaultTranslationData();
                _LastL3DConvertPath = "";
            }
        }
    }
}
