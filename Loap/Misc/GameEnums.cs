﻿namespace Loap.Misc
{
    enum CursorGraphic { Standard, Open }
    enum MouseButton { Left, Right, Middle }
    enum GameSpeed { Normal, FastForward, Pause }
    enum LevelCompletion { None, Attempted, Completed } // Order-sensitive: anything that counts as some form of completed *must* come after Attempted
    enum ReplayResult { NoMatch, Failed, Undetermined, Passed }
}
