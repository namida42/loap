﻿using Loap.Constants;
using LoapCore.Files;
using System;
using System.Collections.Generic;
using System.IO;

namespace Loap.Misc
{
    static class LoapSettings
    {
        private const string KEY_FULLSCREEN = "FULLSCREEN";
        private const string KEY_WINDOW_WIDTH = "WINDOW_WIDTH";
        private const string KEY_WINDOW_HEIGHT = "WINDOW_HEIGHT";
        private const string KEY_RENDER_WIDTH = "RENDER_WIDTH";
        private const string KEY_VSYNC = "VSYNC";
        private const string KEY_LIMIT_FRAME_RATE = "LIMIT_FRAME_RATE";
        private const string KEY_DISPLAY_FPS = "DISPLAY_FPS";
        private const string KEY_BACKGROUND_SCROLL = "BACKGROUND_SCROLL";
        private const string KEY_MUSIC_VOLUME = "MUSIC_VOLUME";
        private const string KEY_SOUND_VOLUME = "SOUND_VOLUME";
        private const string KEY_PAUSE_ON_REWIND = "PAUSE_ON_REWIND";
        private const string KEY_CONFINE_CURSOR = "CONFINE_CURSOR";
        private const string KEY_DISABLE_MOUSE_LOCK = "DISABLE_MOUSE_LOCK";
        private const string KEY_MOUSE_CAMERA_SENSITIVITY_XZ_PAN = "CAMERA_SENSITIVITY_XZ_PAN";
        private const string KEY_MOUSE_CAMERA_SENSITIVITY_Y_PAN = "CAMERA_SENSITIVITY_Y_PAN";
        private const string KEY_MOUSE_CAMERA_SENSITIVITY_ROTATE = "CAMERA_SENSITIVITY_ROTATE";
        private const string KEY_INVERT_MOUSE_PAN_X = "INVERT_MOUSE_PAN_X";
        private const string KEY_INVERT_MOUSE_PAN_Y = "INVERT_MOUSE_PAN_Y";
        private const string KEY_INVERT_MOUSE_PAN_Z = "INVERT_MOUSE_PAN_Z";
        private const string KEY_INVERT_MOUSE_ROTATE = "INVERT_MOUSE_ROTATE";
        private const string KEY_INVERT_MOUSE_TILT = "INVERT_MOUSE_TILT";
        private const string KEY_AUTOSAVE_REPLAY = "AUTOSAVE_REPLAYS";
        private const string KEY_HIDE_MINIMAP = "HIDE_MINIMAP";
        private const string KEY_MINIMAP_WIDTH = "MINIMAP_WIDTH";
        private const string KEY_MINIMAP_HEIGHT = "MINIMAP_HEIGHT";
        private const string KEY_MINIMAP_BLOCK_RESOLUTION = "MINIMAP_BLOCK_RESOLUTION";

        public static bool FullScreen;
        public static int WindowWidth;
        public static int WindowHeight;
        public static int RenderWidth;
        public static bool VSync;
        public static bool LimitFrameRate;
        public static bool DisplayFPS;
        public static bool BackgroundScroll;
        public static int MusicVolume;
        public static int SoundVolume;
        public static bool PauseOnRewind;
        public static bool ConfineCursor;
        public static bool DisableMouseLock;
        public static float MouseCameraSensitivityXZPan;
        public static float MouseCameraSensitivityYPan;
        public static float MouseCameraSensitivityRotate;
        public static bool InvertMousePanX;
        public static bool InvertMousePanY;
        public static bool InvertMousePanZ;
        public static bool InvertMouseRotate;
        public static bool InvertMouseTilt;
        public static bool AutosaveReplays;
        public static bool HideMinimap;
        public static int MinimapWidth;
        public static int MinimapHeight;
        public static int MinimapBlockResolution;

        public static bool LoadedSettings;

        public static void SetDefault()
        {
            FullScreen = false;
            WindowWidth = 1280;
            WindowHeight = 720;
            RenderWidth = 0;
            VSync = false;
            LimitFrameRate = true;
            DisplayFPS = false;
            BackgroundScroll = true;
            MusicVolume = 100;
            SoundVolume = 100;
            PauseOnRewind = true;
            ConfineCursor = false;
            DisableMouseLock = false;
            MouseCameraSensitivityXZPan = 0.33f;
            MouseCameraSensitivityYPan = 0.33f;
            MouseCameraSensitivityRotate = 0.33f;
            InvertMousePanX = false;
            InvertMousePanY = false;
            InvertMousePanZ = false;
            InvertMouseRotate = false;
            InvertMouseTilt = false;
            AutosaveReplays = true;
            HideMinimap = false;
            MinimapWidth = 240;
            MinimapHeight = 240;
            MinimapBlockResolution = 8;
        }

        private static string GetStringValue(Dictionary<string, string> iniDict, string label)
        {
            if (iniDict.ContainsKey(label))
                return iniDict[label];
            else
                return "";
        }

        public static void Sanitize()
        {
            WindowWidth = Math.Max(WindowWidth, 480);
            WindowHeight = Math.Max(WindowHeight, 270);
            if (RenderWidth != 0) RenderWidth = Math.Clamp(RenderWidth, 240, WindowWidth * 2);
            MusicVolume = Math.Clamp(MusicVolume, 0, 100);
            SoundVolume = Math.Clamp(SoundVolume, 0, 100);
            MouseCameraSensitivityXZPan = Math.Clamp(MouseCameraSensitivityXZPan, 0.025f, 1f);
            MouseCameraSensitivityYPan = Math.Clamp(MouseCameraSensitivityYPan, 0.025f, 1f);
            MouseCameraSensitivityRotate = Math.Clamp(MouseCameraSensitivityRotate, 0.025f, 1f);

            int minimapSizeCap = Math.Min(WindowWidth / 2, WindowHeight / 2);

            MinimapWidth = Math.Clamp(MinimapWidth, 90, minimapSizeCap);
            MinimapHeight = Math.Clamp(MinimapHeight, 90, minimapSizeCap);
            MinimapBlockResolution = Math.Clamp(MinimapBlockResolution, 1, Math.Min(MinimapWidth, MinimapHeight) / 16);
        }

        public static void Load()
        {
            SetDefault();

            if (File.Exists(PathConstants.SettingsFile))
            {
                LoadedSettings = true;

                var settingsData = IniFile.ReadIniFile(PathConstants.SettingsFile);

                if (bool.TryParse(GetStringValue(settingsData, KEY_FULLSCREEN), out bool newFullScreen))
                    FullScreen = newFullScreen;
                if (int.TryParse(GetStringValue(settingsData, KEY_WINDOW_WIDTH), out int newWindowWidth))
                    WindowWidth = newWindowWidth;
                if (int.TryParse(GetStringValue(settingsData, KEY_WINDOW_HEIGHT), out int newWindowHeight))
                    WindowHeight = newWindowHeight;
                if (int.TryParse(GetStringValue(settingsData, KEY_RENDER_WIDTH), out int newRenderWidth))
                    RenderWidth = newRenderWidth;
                if (bool.TryParse(GetStringValue(settingsData, KEY_VSYNC), out bool newVSync))
                    VSync = newVSync;
                if (bool.TryParse(GetStringValue(settingsData, KEY_LIMIT_FRAME_RATE), out bool newLimitFrameRate))
                    LimitFrameRate = newLimitFrameRate;
                if (bool.TryParse(GetStringValue(settingsData, KEY_DISPLAY_FPS), out bool newDisplayFPS))
                    DisplayFPS = newDisplayFPS;
                if (bool.TryParse(GetStringValue(settingsData, KEY_BACKGROUND_SCROLL), out bool newBackgroundScroll))
                    BackgroundScroll = newBackgroundScroll;
                if (int.TryParse(GetStringValue(settingsData, KEY_MUSIC_VOLUME), out int newMusicVolume))
                    MusicVolume = newMusicVolume;
                if (int.TryParse(GetStringValue(settingsData, KEY_SOUND_VOLUME), out int newSoundVolume))
                    SoundVolume = newSoundVolume;
                if (bool.TryParse(GetStringValue(settingsData, KEY_PAUSE_ON_REWIND), out bool newPauseOnRewind))
                    PauseOnRewind = newPauseOnRewind;
                if (bool.TryParse(GetStringValue(settingsData, KEY_CONFINE_CURSOR), out bool newConfineCursor))
                    ConfineCursor = newConfineCursor;
                if (bool.TryParse(GetStringValue(settingsData, KEY_DISABLE_MOUSE_LOCK), out bool newDisableMouseLock))
                    DisableMouseLock = newDisableMouseLock;
                if (float.TryParse(GetStringValue(settingsData, KEY_MOUSE_CAMERA_SENSITIVITY_XZ_PAN), out float newMouseCameraSensitivityXZPan))
                    MouseCameraSensitivityXZPan = newMouseCameraSensitivityXZPan;
                if (float.TryParse(GetStringValue(settingsData, KEY_MOUSE_CAMERA_SENSITIVITY_Y_PAN), out float newMouseCameraSensitivityYPan))
                    MouseCameraSensitivityYPan = newMouseCameraSensitivityYPan;
                if (float.TryParse(GetStringValue(settingsData, KEY_MOUSE_CAMERA_SENSITIVITY_ROTATE), out float newMouseCameraSensitivityRotate))
                    MouseCameraSensitivityRotate = newMouseCameraSensitivityRotate;
                if (bool.TryParse(GetStringValue(settingsData, KEY_INVERT_MOUSE_PAN_X), out bool newInvertMousePanX))
                    InvertMousePanX = newInvertMousePanX;
                if (bool.TryParse(GetStringValue(settingsData, KEY_INVERT_MOUSE_PAN_Y), out bool newInvertMousePanY))
                    InvertMousePanY = newInvertMousePanY;
                if (bool.TryParse(GetStringValue(settingsData, KEY_INVERT_MOUSE_PAN_Z), out bool newInvertMousePanZ))
                    InvertMousePanZ = newInvertMousePanZ;
                if (bool.TryParse(GetStringValue(settingsData, KEY_INVERT_MOUSE_ROTATE), out bool newInvertMouseRotate))
                    InvertMouseRotate = newInvertMouseRotate;
                if (bool.TryParse(GetStringValue(settingsData, KEY_INVERT_MOUSE_TILT), out bool newInvertMouseTilt))
                    InvertMouseTilt = newInvertMouseTilt;
                if (bool.TryParse(GetStringValue(settingsData, KEY_AUTOSAVE_REPLAY), out bool newAutosaveReplays))
                    AutosaveReplays = newAutosaveReplays;
                if (bool.TryParse(GetStringValue(settingsData, KEY_HIDE_MINIMAP), out bool newHideMinimap))
                    HideMinimap = newHideMinimap;
                if (int.TryParse(GetStringValue(settingsData, KEY_MINIMAP_WIDTH), out int newMinimapWidth))
                    MinimapWidth = newMinimapWidth;
                if (int.TryParse(GetStringValue(settingsData, KEY_MINIMAP_HEIGHT), out int newMinimapHeight))
                    MinimapHeight = newMinimapHeight;
                if (int.TryParse(GetStringValue(settingsData, KEY_MINIMAP_BLOCK_RESOLUTION), out int newMinimapBlockResolution))
                    MinimapBlockResolution = newMinimapBlockResolution;
            }
            else
                LoadedSettings = false;
        }

        public static void Save()
        {
            using var fs = new FileStream(PathConstants.SettingsFile, FileMode.Create);
            using var writer = new StreamWriter(fs);

            writer.WriteLine("# Display options");
            writer.WriteLine(KEY_BACKGROUND_SCROLL + "=" + BackgroundScroll.ToString());
            writer.WriteLine(KEY_FULLSCREEN + "=" + FullScreen.ToString());
            writer.WriteLine(KEY_WINDOW_WIDTH + "=" + WindowWidth.ToString());
            writer.WriteLine(KEY_WINDOW_HEIGHT + "=" + WindowHeight.ToString());
            writer.WriteLine(KEY_RENDER_WIDTH + "=" + RenderWidth.ToString());
            writer.WriteLine(KEY_VSYNC + "=" + VSync.ToString());
            writer.WriteLine(KEY_LIMIT_FRAME_RATE + "=" + LimitFrameRate.ToString());
            writer.WriteLine(KEY_DISPLAY_FPS + "=" + DisplayFPS.ToString());
            writer.WriteLine();

            writer.WriteLine("# Minimap options");
            writer.WriteLine(KEY_HIDE_MINIMAP + "=" + HideMinimap.ToString());
            writer.WriteLine(KEY_MINIMAP_WIDTH + "=" + MinimapWidth.ToString());
            writer.WriteLine(KEY_MINIMAP_HEIGHT + "=" + MinimapHeight.ToString());
            writer.WriteLine(KEY_MINIMAP_BLOCK_RESOLUTION + "=" + MinimapBlockResolution.ToString());
            writer.WriteLine();

            writer.WriteLine("# Sound options");
            writer.WriteLine(KEY_MUSIC_VOLUME + "=" + MusicVolume.ToString());
            writer.WriteLine(KEY_SOUND_VOLUME + "=" + SoundVolume.ToString());
            writer.WriteLine();

            writer.WriteLine("# Input options");
            writer.WriteLine(KEY_CONFINE_CURSOR + "=" + ConfineCursor.ToString());
            writer.WriteLine(KEY_MOUSE_CAMERA_SENSITIVITY_XZ_PAN + "=" + MouseCameraSensitivityXZPan.ToString());
            writer.WriteLine(KEY_MOUSE_CAMERA_SENSITIVITY_Y_PAN + "=" + MouseCameraSensitivityYPan.ToString());
            writer.WriteLine(KEY_MOUSE_CAMERA_SENSITIVITY_ROTATE + "=" + MouseCameraSensitivityRotate.ToString());
            writer.WriteLine(KEY_INVERT_MOUSE_PAN_X + "=" + InvertMousePanX.ToString());
            writer.WriteLine(KEY_INVERT_MOUSE_PAN_Y + "=" + InvertMousePanY.ToString());
            writer.WriteLine(KEY_INVERT_MOUSE_PAN_Z + "=" + InvertMousePanZ.ToString());
            writer.WriteLine(KEY_INVERT_MOUSE_ROTATE + "=" + InvertMouseRotate.ToString());
            writer.WriteLine(KEY_INVERT_MOUSE_TILT + "=" + InvertMouseTilt.ToString());
            writer.WriteLine(KEY_DISABLE_MOUSE_LOCK + "=" + DisableMouseLock.ToString());
            writer.WriteLine();

            writer.WriteLine("# Misc options");
            writer.WriteLine(KEY_PAUSE_ON_REWIND + "=" + PauseOnRewind.ToString());
            writer.WriteLine(KEY_AUTOSAVE_REPLAY + "=" + AutosaveReplays.ToString());
            writer.WriteLine();
        }
    }
}
