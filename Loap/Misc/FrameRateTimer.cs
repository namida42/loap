﻿namespace Loap.Misc
{
    class FrameRateTimer
    {
        public double SegmentStart { get; private set; }
        public double OverallStart { get; private set; }

        public int OverallUpdateCalls { get; private set; }
        public int SegmentUpdateCalls { get; private set; }
        public int OverallRenderCalls { get; private set; }
        public int SegmentRenderCalls { get; private set; }

        public void ResetAll(double time)
        {
            OverallStart = time;
            OverallUpdateCalls = 0;
            OverallRenderCalls = 0;

            ResetSegment(time);
        }

        public void ResetSegment(double time)
        {
            SegmentStart = time;
            SegmentUpdateCalls = 0;
            SegmentRenderCalls = 0;
        }

        public void RegisterUpdate() { OverallUpdateCalls++; SegmentUpdateCalls++; }
        public void RegisterRender() { OverallRenderCalls++; SegmentRenderCalls++; }

        public double GetOverallUpdateRate(double now) { return CalculateOverDuration(now, OverallStart, OverallUpdateCalls); }
        public double GetOverallRenderRate(double now) { return CalculateOverDuration(now, OverallStart, OverallRenderCalls); }
        public double GetOverallDuration(double now) { return now - OverallStart; }
        public double GetSegmentUpdateRate(double now) { return CalculateOverDuration(now, SegmentStart, SegmentUpdateCalls); }
        public double GetSegmentRenderRate(double now) { return CalculateOverDuration(now, SegmentStart, SegmentRenderCalls); }
        public double GetSegmentDuration(double now) { return now - SegmentStart; }

        private double CalculateOverDuration(double now, double start, int updates)
        {
            return updates / (now - start);
        }
    }
}
