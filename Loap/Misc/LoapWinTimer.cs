﻿using System.Runtime.InteropServices;

namespace Loap.Misc
{
    class LoapWinTimer
    {
#if OS_WINDOWS
        [DllImport("ntdll.dll", EntryPoint = "NtSetTimerResolution")]
        public static extern void NtSetTimerResolution(uint DesiredResolution, bool SetResolution, ref uint CurrentResolution);
#endif

        public static void Set1MsResolution()
        {
#if OS_WINDOWS
            uint CurrentResolution = 0;
            NtSetTimerResolution(1000, true, ref CurrentResolution);
#endif
            // Do nothing on non-Windows OSes.
        }
    }
}
