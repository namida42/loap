﻿using Loap.Constants;
using System;

namespace Loap.Misc
{
    class UpdateTimer
    {
        public float IdealDelay;
        public readonly float MaxCatchupFrames;

        private double _LastUpdateTime;
        private double _RealLastUpdateTime;

        private float MaxBackTimer => IdealDelay * MaxCatchupFrames;

        public UpdateTimer(float idealDelay, float maxCatchupFrames = UIConstants.DEFAULT_MAX_CATCHUP_FRAMES)
        {
            IdealDelay = idealDelay;
            MaxCatchupFrames = maxCatchupFrames;
        }

        public void Invalidate(float currentTime)
        {
            _LastUpdateTime = currentTime;
            _RealLastUpdateTime = currentTime;
        }

        public bool IsTimeForUpdate(float currentTime)
        {
            if (currentTime - _RealLastUpdateTime < IdealDelay / UIConstants.CATCHUP_IDEAL_DELAY_DIVISOR)
                return false;
            else if (currentTime - _LastUpdateTime < IdealDelay)
                return false;
            else
            {
                _LastUpdateTime = Math.Max(_LastUpdateTime + IdealDelay, currentTime - MaxBackTimer);
                _RealLastUpdateTime = currentTime;
                return true;
            }
        }
    }
}
