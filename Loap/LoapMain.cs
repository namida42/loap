﻿using Loap.Constants;
using Loap.Misc;
using Loap.UI;
using Loap.Views;
using Loap.Views.Menu;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Runtime;

#nullable disable

namespace Loap
{
    class LoapMain : Game
    {
        private const float FRAME_RATE_INTERVAL = 2.5f;
        private const float STABILIZE_TIME_CAP = 5f;
        private const int MINIMUM_STABILIZE_UPDATES = 30;
        private const float STABILIZE_TOLERANCE = 0.025f;

        private bool _FrameRateLimited;
        private float _FPSLimitSpeed;
        private float _LastFrameRateReading = 0;

        private readonly GraphicsDeviceManager _Graphics;

        private SpriteBatch _SpriteBatch;
        private BaseView _View;

        private readonly FrameRateTimer _FrameRateTimer = new FrameRateTimer();

        private float _StabilizeTotalTime = 0f;
        private float _StabilizeLastTime = 0f;
        private int _StabilizeUpdates = 0;

        public LoapMain() : base()
        {
            _Graphics = new GraphicsDeviceManager(this);
        }

        private void SetFPSLimit(bool enableLimit, float timeStep = 0)
        {
            if (enableLimit)
            {
                if (timeStep <= 0)
                    timeStep = UIConstants.IDEAL_FRAMERATE_TIME;

                _FrameRateLimited = true;
                _FPSLimitSpeed = timeStep;
                IsFixedTimeStep = true;
                TargetElapsedTime = TimeSpan.FromSeconds(timeStep);
            }
            else
            {
                _FrameRateLimited = false;
                IsFixedTimeStep = false;
            }
        }

        protected override void Initialize()
        {
            base.Initialize();

            GCSettings.LatencyMode = GCLatencyMode.SustainedLowLatency;

            UserInput.Initialize();

            Mouse.WindowHandle = Window.Handle;

            LoapSettings.Load();
            Hotkeys.Load();

            if (!LoapSettings.LoadedSettings)
            {
                int maxWidth = Math.Min(GraphicsDevice.Adapter.CurrentDisplayMode.Width * 3 / 4, RenderConstants.DEFAULT_INTERNAL_CANVAS_WIDTH);
                int maxHeight = Math.Min(GraphicsDevice.Adapter.CurrentDisplayMode.Width * 3 / 4, RenderConstants.DEFAULT_INTERNAL_CANVAS_HEIGHT);

                if (maxHeight < maxWidth * 9 / 16)
                {
                    LoapSettings.WindowWidth = maxHeight * 16 / 9;
                    LoapSettings.WindowHeight = maxHeight;
                }
                else
                {
                    LoapSettings.WindowWidth = maxWidth;
                    LoapSettings.WindowHeight = maxWidth * 9 / 16;
                }
            }

            if (LoapSettings.FullScreen)
            {
                LoapSettings.WindowWidth = GraphicsDevice.Adapter.CurrentDisplayMode.Width;
                LoapSettings.WindowHeight = GraphicsDevice.Adapter.CurrentDisplayMode.Height;
            }
            else
            {
                LoapSettings.WindowWidth = Math.Min(GraphicsDevice.Adapter.CurrentDisplayMode.Width * 9 / 10, LoapSettings.WindowWidth);
                LoapSettings.WindowHeight = Math.Min(GraphicsDevice.Adapter.CurrentDisplayMode.Height * 9 / 10, LoapSettings.WindowHeight);
            }

            LoapSettings.Sanitize();
            LoapSettings.Save();

            _Graphics.SynchronizeWithVerticalRetrace = LoapSettings.VSync;
            if (LoapSettings.FullScreen)
            {
                _Graphics.IsFullScreen = true;
                _Graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
                _Graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;
            }
            else
            {
                _Graphics.IsFullScreen = false;
                _Graphics.PreferredBackBufferWidth = LoapSettings.WindowWidth;
                _Graphics.PreferredBackBufferHeight = LoapSettings.WindowHeight;
            }
            _Graphics.ApplyChanges();

            SetFPSLimit(LoapSettings.LimitFrameRate);

            RenderConstants.PrepareBlankTexture(GraphicsDevice);
            Font.LoadFont(GraphicsDevice);
            Cursor.LoadCursors(GraphicsDevice);

            _SpriteBatch = new SpriteBatch(GraphicsDevice);

            _View = new TitleView(GraphicsDevice);
            _View.ForceOneUpdate(float.Epsilon);

            _FrameRateTimer.ResetAll(0);

            LoapWinTimer.Set1MsResolution();
        }

        protected override void Update(GameTime gameTime)
        {
            if (_View.NewViewFunction != null && _View.FadeTime == 0)
            {
                var newView = _View.NewViewFunction();

                if (newView == null)
                {
                    Exit();
                    return;
                }

                _View = newView;
                _View.ForceOneUpdate(float.Epsilon);
                GC.Collect(2);

                _StabilizeTotalTime = 0;
                _StabilizeUpdates = 0;
            }
            else
            {
                if (!_View.Stabilized)
                {
                    float thisTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

                    _StabilizeTotalTime += thisTime;
                    _StabilizeUpdates++;

                    float averageTime = _StabilizeTotalTime / _StabilizeUpdates;

                    if (_StabilizeTotalTime >= STABILIZE_TIME_CAP)
                        _View.Stabilized = true;
                    if (_StabilizeUpdates >= MINIMUM_STABILIZE_UPDATES &&
                        Math.Abs(averageTime - thisTime) <= Math.Min(averageTime * STABILIZE_TOLERANCE, thisTime * STABILIZE_TOLERANCE) &&
                        Math.Abs(_StabilizeLastTime - thisTime) <= Math.Min(_StabilizeLastTime * STABILIZE_TOLERANCE, thisTime * STABILIZE_TOLERANCE))
                        _View.Stabilized = true;

                    _StabilizeLastTime = thisTime;
                }
                else
                {
                    UserInput.Update(Window.Position);
                    _View.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
                    _FrameRateTimer.RegisterUpdate();
                }
            }

            if (_FrameRateTimer.GetSegmentDuration(gameTime.TotalGameTime.TotalSeconds) > FRAME_RATE_INTERVAL)
            {
                _LastFrameRateReading = (float)_FrameRateTimer.GetSegmentRenderRate(gameTime.TotalGameTime.TotalSeconds);
                _FrameRateTimer.ResetSegment(gameTime.TotalGameTime.TotalSeconds);
            }

            bool needSaveSettings = false;

            if (UserInput.IsKeyPressed(HotkeyFunction.RemoveFrameLimiter))
            {
                LoapSettings.LimitFrameRate = !LoapSettings.LimitFrameRate;
                needSaveSettings = true;
            }

            if (UserInput.IsKeyPressed(HotkeyFunction.ShowFPS))
            {
                LoapSettings.DisplayFPS = !LoapSettings.DisplayFPS;
                needSaveSettings = true;
            }

            if (UserInput.IsKeyPressed(HotkeyFunction.ConfineCursor))
            {
                LoapSettings.ConfineCursor = !LoapSettings.ConfineCursor;
                needSaveSettings = true;
            }

            if (needSaveSettings && !(_View is OptionsView))
                LoapSettings.Save();


            if (_FrameRateLimited != LoapSettings.LimitFrameRate)
                SetFPSLimit(LoapSettings.LimitFrameRate, _FPSLimitSpeed);
            else if (LoapSettings.LimitFrameRate && (_View.IdealFrameDelay != _FPSLimitSpeed))
                SetFPSLimit(true, _View.IdealFrameDelay);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            _FrameRateTimer.RegisterRender();

            _View.Render();

            if (_View.ScreenTexture != null)
                DrawToRenderTarget(_View.ScreenTexture);

            DrawToRenderTarget(null);

            if (LoapSettings.DisplayFPS)
            {
                _SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
                Font.WriteText(_SpriteBatch, _LastFrameRateReading.ToString("N2") + " FPS", new Point(GraphicsDevice.Viewport.Width - 4, GraphicsDevice.Viewport.Height - 4), Color.Magenta, 1, 1);
                _SpriteBatch.End();
            }
        }

        private void DrawToRenderTarget(RenderTarget2D renderTarget)
        {
            GraphicsDevice.SetRenderTarget(renderTarget);

            var boundsRect = (renderTarget == null) ?
                new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height) :
                renderTarget.Bounds;

            float fadeIntensity = Math.Clamp(
                (_View.NewViewFunction == null) ?
                (UIConstants.FADE_TIME - _View.FadeTime) / UIConstants.FADE_TIME :
                _View.FadeTime / UIConstants.FADE_TIME,
                0, 1);
            fadeIntensity = MathF.Sqrt(fadeIntensity);

            if (fadeIntensity == 0)
                GraphicsDevice.Clear(Color.Black);
            else
            {
                _SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp);
                _SpriteBatch.Draw(_View.Canvas, boundsRect, new Color(fadeIntensity, fadeIntensity, fadeIntensity, 1));
                _SpriteBatch.End();
            }
        }

        protected override void OnExiting(object sender, ExitingEventArgs args)
        {
            UserInput.Deinitialize();
            base.OnExiting(sender, args);
        }
    }
}