﻿using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Loap.Models
{
    class LoapModel // Named this instead of just "Model" to avoid conflict with Monogame's Model class
    {
        public readonly List<VertexPositionNormalTexture> Vertices = new List<VertexPositionNormalTexture>();
    }
}
