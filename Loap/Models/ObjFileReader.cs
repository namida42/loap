﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Loap.Models
{
    static class ObjFileReader
    {
        public static LoapModel LoadModel(IEnumerable<string> input, string? modelName = null)
        {
            var models = LoadModels(input);
            LoapModel? result = null;

            if (modelName == null)
                result = models.Values.FirstOrDefault();
            else if (models.ContainsKey(modelName))
                result = models[modelName];

            if (result == null)
                throw new ArgumentException("Model " + (modelName ?? "<null>") + " not found in file", nameof(modelName));
            else
                return result;
        }

        public static Dictionary<string, LoapModel> LoadModels(IEnumerable<string> input)
        {
            Dictionary<string, LoapModel> result = new Dictionary<string, LoapModel>();
            List<string> inputList = input.ToList();

            List<Vector3> Vertices = new List<Vector3>();
            List<Vector2> TextureVertices = new List<Vector2>();
            List<Vector3> Normals = new List<Vector3>();

            int curLine = 0;
            while (curLine < inputList.Count)
                ProcessModel(result, inputList, ref curLine, Vertices, TextureVertices, Normals);

            return result;
        }

        private static string[] SplitLine(string line)
        {
            return line.Split(' ');
        }

        private static void ProcessModel(Dictionary<string, LoapModel> result, List<string> input, ref int curLine,
            List<Vector3> vertices, List<Vector2> textureVertices, List<Vector3> normals)
        {
            string? newName = null;
            int origCurLine = curLine;

            while (curLine < input.Count)
            {
                var thisLine = SplitLine(input[curLine]);
                curLine++;

                if ((thisLine.Length > 0) && (thisLine[0] == "o"))
                {
                    if (thisLine.Length > 1)
                        newName = thisLine[1];
                    break;
                }
            }

            if (newName == null)
            {
                newName = "";
                curLine = origCurLine;
            }

            List<string> FacesRaw = new List<string>();

            while (curLine < input.Count)
            {
                var thisLine = SplitLine(input[curLine]);

                if ((thisLine.Length > 0) && (thisLine[0] == "o"))
                    break;

                curLine++;

                if (thisLine.Length > 0)
                    switch (thisLine[0])
                    {
                        case "v":
                            {
                                Vector3 newVertex = new Vector3(
                                    float.Parse(thisLine[1], CultureInfo.InvariantCulture),
                                    float.Parse(thisLine[2], CultureInfo.InvariantCulture),
                                    float.Parse(thisLine[3], CultureInfo.InvariantCulture)
                                    );
                                vertices.Add(newVertex);
                                break;
                            }
                        case "vt":
                            {
                                Vector2 newVertex = new Vector2(
                                    float.Parse(thisLine[1], CultureInfo.InvariantCulture),
                                    1 - float.Parse(thisLine[2], CultureInfo.InvariantCulture)
                                    );
                                textureVertices.Add(newVertex);
                                break;
                            }
                        case "vn":
                            {
                                Vector3 newVertex = new Vector3(
                                    float.Parse(thisLine[1], CultureInfo.InvariantCulture),
                                    float.Parse(thisLine[2], CultureInfo.InvariantCulture),
                                    float.Parse(thisLine[3], CultureInfo.InvariantCulture)
                                    );
                                normals.Add(newVertex);
                                break;
                            }
                        case "f":
                            {
                                FacesRaw.Add(string.Join(' ', thisLine[1..4]));
                                break;
                            }
                    }
            }

            LoapModel newModel = new LoapModel();

            foreach (var triRaw in FacesRaw.Select(f => SplitLine(f)))
                for (int face = 0; face < 3; face++)
                {
                    int[] indices = triRaw[face].Split('/').Select(i => int.Parse(i, CultureInfo.InvariantCulture)).ToArray();
                    newModel.Vertices.Add(new VertexPositionNormalTexture(
                        vertices[indices[0] - 1],
                        normals[indices[2] - 1],
                        textureVertices[indices[1] - 1]
                        ));
                }

            result.Add(newName, newModel);
        }
    }
}
