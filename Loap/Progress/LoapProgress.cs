﻿using Loap.Constants;
using Loap.Packs;
using LoapCore.Files;
using System.IO;

namespace Loap.Progress
{
    static class LoapProgress
    {
        private static DataItem _ProgressData;

        public static LoapPackCacheLevelEntry? CurrentLevel = null; // KLUDGE: A much better way is needed of passing the level's path around.

        static LoapProgress()
        {
            if (File.Exists(PathConstants.ProgressFile))
                _ProgressData = DataItemReader.LoadFromFile(PathConstants.ProgressFile);
            else
                _ProgressData = new DataItem();
        }

        private static DataItem GetDataItemForPath(string pathFromLevelsDir, bool createIfNotExists)
        {
            var pathElements = pathFromLevelsDir.Split(Path.DirectorySeparatorChar);
            DataItem item = _ProgressData;
            foreach (var element in pathElements)
            {
                string modElement = element.Replace(' ', '_');

                if (createIfNotExists || item.GetChildExists(modElement))
                    item = item.GetOrAddChild(modElement);
                else
                {
                    item = new DataItem(); // Return a dummy.
                    break;
                }
            }

            return item;
        }

        public static LevelProgress GetLevelProgress(LoapPackCacheLevelEntry levelEntry)
        {
            var item = GetDataItemForPath(levelEntry.FilePath, false);

            var result = new LevelProgress();
            result.LoadFromDataItem(item);
            return result;
        }

        public static void SetLevelProgress(LoapPackCacheLevelEntry levelEntry, LevelProgress progress, bool saveProgress = true)
        {
            var item = GetDataItemForPath(levelEntry.FilePath, true);
            progress.WriteToDataItem(item);

            if (saveProgress)
                SaveProgressFile();
        }

        public static void SaveProgressFile()
        {
            DataItemWriter.SaveToFile(PathConstants.ProgressFile, _ProgressData);
        }
    }
}
