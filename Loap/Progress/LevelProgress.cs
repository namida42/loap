﻿using Loap.Misc;
using LoapCore.Files;
using LoapCore.Misc;
using System.Collections.Generic;

namespace Loap.Progress
{
    class LevelProgress
    {
        #region File Keys
        private const string KEY_COMPLETION = "COMPLETION";
        private const string KEY_SAVE_RECORD = "SAVE_RECORD";
        private const string KEY_TIME_RECORD = "TIME_RECORD";
        private const string KEY_TOTAL_SKILLS_RECORD = "TOTAL_SKILLS_RECORD";
        private const string KEY_SKILL_TYPES_RECORD = "SKILL_TYPES_RECORD";
        private const string KEY_SKILL_RECORD_SUFFIX = "S_RECORD";
        #endregion

        public LevelCompletion Completion;
        public int SaveRecord;
        public int TimeRecord = -1;

        public int TotalSkillsRecord = -1;
        public int SkillTypesRecord = -1;
        public Dictionary<Skill, int> SkillRecord;

        public LevelProgress()
        {
            SkillRecord = new Dictionary<Skill, int>();
            foreach (var skill in Utils.GetEnumValues<Skill>())
                SkillRecord.Add(skill, -1);
        }

        public void LoadFromDataItem(DataItem item)
        {
            Completion = item.GetChildValueEnum(KEY_COMPLETION, LevelCompletion.None);
            SaveRecord = item.GetChildValueInt(KEY_SAVE_RECORD, 0);
            TimeRecord = item.GetChildValueInt(KEY_TIME_RECORD, -1);
            TotalSkillsRecord = item.GetChildValueInt(KEY_TOTAL_SKILLS_RECORD, -1);
            SkillTypesRecord = item.GetChildValueInt(KEY_SKILL_TYPES_RECORD, -1);

            foreach (var skill in Utils.GetEnumValues<Skill>())
                SkillRecord[skill] = item.GetChildValueInt(skill.ToString() + KEY_SKILL_RECORD_SUFFIX, -1);
        }

        public void WriteToDataItem(DataItem item)
        {
            item.Children.Clear();
            item.SetChildValueEnum(KEY_COMPLETION, Completion);

            if (Completion > LevelCompletion.Attempted)
            {
                item.SetChildValueInt(KEY_SAVE_RECORD, SaveRecord);
                item.SetChildValueInt(KEY_TIME_RECORD, TimeRecord);
                item.SetChildValueInt(KEY_TOTAL_SKILLS_RECORD, TotalSkillsRecord);
                item.SetChildValueInt(KEY_SKILL_TYPES_RECORD, SkillTypesRecord);

                foreach (var skill in Utils.GetEnumValues<Skill>())
                    item.SetChildValueInt(skill.ToString() + KEY_SKILL_RECORD_SUFFIX, SkillRecord[skill]);
            }
        }
    }
}
