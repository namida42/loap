﻿namespace Loap
{
    class Program
    {
        private static readonly LoapMain _Loap = new LoapMain();

        static void Main()
        {
            _Loap.Run();
        }
    }
}
