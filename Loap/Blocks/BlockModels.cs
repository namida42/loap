﻿using Loap.Constants;
using Loap.Models;
using LoapCore.Constants;
using LoapCore.Misc;
using System.Collections.Generic;
using System.IO;

namespace Loap.Blocks
{
    static class BlockModels
    {
        private static readonly Dictionary<string, BlockModel> _SegmentModels = new Dictionary<string, BlockModel>();

        public static BlockModel GetModel(BlockShape shape, int segment)
        {
            return _SegmentModels[shape.ToString() + segment.ToString()];
        }

        static BlockModels()
        {
            LoadBlockModelData();
        }

        private static void LoadBlockModelData()
        {
            var objDataRaw = File.ReadAllLines(PathConstants.BlocksModelFilePath);
            var objData = ObjFileReader.LoadModels(objDataRaw);

            foreach (BlockShape shape in Utils.GetEnumValues<BlockShape>())
                for (int i = 0; i < PhysicsConstants.BLOCK_SEGMENTS; i++)
                {
                    if (!objData.ContainsKey(shape.ToString() + ".Layer" + i.ToString()))
                        break;

                    BlockModel newModel = new BlockModel();
                    newModel.LoadFromModels(objData, shape, i);
                    _SegmentModels.Add(newModel.Key, newModel);
                }
        }
    }
}
