﻿using Loap.Models;
using LoapCore.Constants;
using LoapCore.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Loap.Blocks
{
    class BlockModel
    {
        private const float FILE_POSITION_MULTIPLIER = 8f;

        public BlockShape Shape { get; private set; }
        public int Segment { get; private set; }
        public readonly Dictionary<Direction, FaceModel> Faces = new Dictionary<Direction, FaceModel>();
        private bool _Loaded;

        public string Key => Shape.ToString() + Segment.ToString();

        public BlockModel()
        {
            foreach (Direction dir in Utils.GetEnumValues<Direction>())
                Faces.Add(dir, new FaceModel(dir));
        }

        public void LoadFromModels(Dictionary<string, LoapModel> sourceModels, BlockShape shape, int segment)
        {
            if (_Loaded)
                throw new InvalidOperationException("SegmentModel is already loaded.");

            foreach (var list in Faces.Values)
                list.Vertices.Clear();

            Shape = shape;
            Segment = segment;

            LoapModel src = sourceModels[shape.ToString() + ".Layer" + segment.ToString()];
            foreach (var vertex in src.Vertices)
            {
                Direction side = DetermineSide(vertex.Normal);
                VertexPositionNormalTexture newVertex = vertex;

                newVertex = new VertexPositionNormalTexture(
                    newVertex.Position - new Vector3(0, 0, Segment * PhysicsConstants.SEGMENT_SIZE_FLOAT),
                    newVertex.Normal,
                    newVertex.TextureCoordinate
                    );

                if ((side != Direction.ZMinus) && (side != Direction.ZPlus))
                {
                    newVertex = new VertexPositionNormalTexture(
                        newVertex.Position,
                        newVertex.Normal,
                        newVertex.TextureCoordinate + new Vector2(0, Segment * PhysicsConstants.SEGMENT_SIZE_FLOAT)
                        );

                    newVertex.TextureCoordinate.Y = (newVertex.TextureCoordinate.Y - 0.75f) * 4;
                }

                Faces[side].Vertices.Add(newVertex);
            }

            SetSlopeVertices(Faces[Direction.ZMinus], 0f);
            SetSlopeVertices(Faces[Direction.ZPlus], PhysicsConstants.SEGMENT_SIZE_FLOAT);

            _Loaded = true;
        }

        private void SetSlopeVertices(FaceModel face, float faceHeight)
        {
            for (int i = 0; i < face.Vertices.Count; i += 3)
            {
                if (face.Vertices[i].Position.Z != faceHeight ||
                    face.Vertices[i + 1].Position.Z != faceHeight ||
                    face.Vertices[i + 2].Position.Z != faceHeight)
                {
                    face.SlopeVertices.Add(face.Vertices[i]);
                    face.SlopeVertices.Add(face.Vertices[i + 1]);
                    face.SlopeVertices.Add(face.Vertices[i + 2]);
                }
            }
        }

        private Direction DetermineSide(Vector3 normal)
        {
            if (normal.X == -1 && normal.Y == 0 && normal.Z == 0)
                return Direction.XMinus;
            else if (normal.X == 1 && normal.Y == 0 && normal.Z == 0)
                return Direction.XPlus;
            else if (normal.X == 0 && normal.Y == -1 && normal.Z == 0)
                return Direction.YMinus;
            else if (normal.X == 0 && normal.Y == 1 && normal.Z == 0)
                return Direction.YPlus;
            else if (normal.X == 0 && normal.Y == 0 && normal.Z == -1)
                return Direction.ZMinus;
            else if (normal.X == 0 && normal.Y == 0 && normal.Z == 1)
                return Direction.ZPlus;
            else switch (Shape)
                {
                    case BlockShape.Deflector:
                        return Direction.XMinus;
                    case BlockShape.Ramp45Above:
                    case BlockShape.Ramp45Below:
                        return Direction.YMinus;
                    case BlockShape.Ramp22Above:
                    case BlockShape.CornerInsideAbove:
                        return Direction.ZPlus;
                    case BlockShape.Ramp22Below:
                    case BlockShape.CornerInsideBelow:
                        return Direction.ZMinus;
                    case BlockShape.Corner45Above:
                    case BlockShape.Corner45Below:
                        return Direction.XMinus;
                    case BlockShape.Corner90Above:
                    case BlockShape.Corner90Below:
                    case BlockShape.LargePyramidAbove:
                    case BlockShape.LargePyramidBelow:
                    case BlockShape.SmallPyramidAbove:
                    case BlockShape.SmallPyramidBelow:
                        if (normal.X < 0 && normal.Y == 0)
                            return Direction.XMinus;
                        else if (normal.X > 0 && normal.Y == 0)
                            return Direction.XPlus;
                        else if (normal.X == 0 && normal.Y < 0)
                            return Direction.YMinus;
                        else if (normal.X == 0 && normal.Y > 0)
                            return Direction.YPlus;
                        else
                            throw new ArgumentException("Pyramid or Corner90 block has a nonstandard normal that could not be processed.", nameof(Shape));
                    default:
                        throw new ArgumentException("Nonstandard normal on a shape where this should not occur.", nameof(Shape));
                }
        }
    }
}
