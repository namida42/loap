﻿using System;

namespace Loap.Views.Game
{
    struct BlockTextureKey : IEquatable<BlockTextureKey>
    {
        public string TextureFile;
        public bool HasTransparency;
        public bool Animated;

        public BlockTextureKey(string textureFile, bool hasTransparency, bool animated)
        {
            TextureFile = textureFile;
            HasTransparency = hasTransparency;
            Animated = animated;
        }

        public override bool Equals(object? obj)
        {
            return obj is BlockTextureKey key && Equals(key);
        }

        public bool Equals(BlockTextureKey other)
        {
            return TextureFile == other.TextureFile &&
                   HasTransparency == other.HasTransparency &&
                   Animated == other.Animated;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TextureFile, HasTransparency, Animated);
        }

        public static bool operator ==(BlockTextureKey left, BlockTextureKey right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(BlockTextureKey left, BlockTextureKey right)
        {
            return !(left == right);
        }
    }
}
