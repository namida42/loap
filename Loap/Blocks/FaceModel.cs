﻿using LoapCore.Misc;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Loap.Blocks
{
    class FaceModel
    {
        public FaceModel(Direction side)
        {
            Side = side;
        }

        public readonly Direction Side;
        public readonly List<VertexPositionNormalTexture> Vertices = new List<VertexPositionNormalTexture>();
        public readonly List<VertexPositionNormalTexture> SlopeVertices = new List<VertexPositionNormalTexture>();
    }
}
