﻿using Loap.Constants;
using LoapCore.Files;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Loap.UI
{
    static class Font
    {
        private static Texture2D? _FontTexture;
        private static readonly Dictionary<char, Rectangle> _CharRects = new Dictionary<char, Rectangle>();

        public static int CharacterHeight { get; private set; }
        private static int _SpaceWidth;
        private static char _FallbackChar;

        public static void LoadFont(GraphicsDevice graphicsDevice)
        {
            _FontTexture = Texture2D.FromFile(graphicsDevice, PathConstants.FontFilePath);
            var fontItem = DataItemReader.LoadFromFile(PathConstants.FontMetainfoFilePath);

            int defaultWidth = fontItem.GetChildValueInt(UIMetainfoKeys.FONT_DEFAULT_WIDTH);
            int defaultLowercaseWidth = fontItem.GetChildValueInt(UIMetainfoKeys.FONT_DEFAULT_LOWERCASE_WIDTH);
            _SpaceWidth = fontItem.GetChildValueInt(UIMetainfoKeys.FONT_SPACE_WIDTH);

            int cellWidth = fontItem.GetChildValueInt(UIMetainfoKeys.GENERAL_CELL_WIDTH);
            int cellHeight = fontItem.GetChildValueInt(UIMetainfoKeys.GENERAL_CELL_HEIGHT);

            char firstChar = fontItem.GetChildValue(UIMetainfoKeys.FONT_FIRST)[0];
            char lastChar = fontItem.GetChildValue(UIMetainfoKeys.FONT_LAST)[0];
            string missing = fontItem.GetChildValue(UIMetainfoKeys.FONT_MISSING);

            Rectangle baseRect = new Rectangle(0, 0, cellWidth, cellHeight);
            for (char thisChar = firstChar; thisChar <= lastChar; thisChar++)
            {
                if (!missing.Contains(thisChar))
                {
                    Rectangle thisCharRect = baseRect;

                    var sizeSection = fontItem.GetChildren(UIMetainfoKeys.FONT_SIZE_SECTION).FirstOrDefault(sec => sec.GetChildValue(UIMetainfoKeys.FONT_CHARACTER)[0] == thisChar);
                    if (sizeSection != null)
                        thisCharRect.Width = sizeSection.GetChildValueInt(UIMetainfoKeys.FONT_WIDTH);
                    else
                        thisCharRect.Width = (thisChar >= 'a' && thisChar <= 'z') ? defaultLowercaseWidth : defaultWidth;

                    _CharRects.Add(thisChar, thisCharRect);
                }

                baseRect.X += cellWidth;
                if (baseRect.X >= _FontTexture.Width)
                {
                    baseRect.X = 0;
                    baseRect.Y += cellHeight;
                }
            }

            _FallbackChar = _CharRects.ContainsKey('?') ? '?' : ' ';
            CharacterHeight = cellHeight;
        }

        private static string Sanitize(string input)
        {
            if (input == null)
                return "";

            StringBuilder sanitizedString = new StringBuilder(input.Length);
            foreach (var c in input)
                if (c == '\r')
                    continue;
                else if (c == ' ' || c == '\n' || _CharRects.ContainsKey(c))
                    sanitizedString.Append(c);
                else
                    sanitizedString.Append(_FallbackChar);

            return sanitizedString.ToString();
        }

        public static void WriteText(SpriteBatch spriteBatch, string text, Point position, Color color, int horzAlign = -1, int vertAlign = -1)
        {
            text = Sanitize(text);
            Point lineStartPos = position;
            Rectangle bounds = InternalCalculateBounds(text, position);

            switch (Math.Sign(horzAlign))
            {
                case -1:
                    break; // Do nothing, that's the default
                case 0:
                    lineStartPos.X -= (bounds.Width / 2);
                    break;
                case 1:
                    lineStartPos.X = lineStartPos.X - bounds.Width + 1;
                    break;
            }
            switch (Math.Sign(vertAlign))
            {
                case -1:
                    break; // Do nothing, that's the default
                case 0:
                    lineStartPos.Y -= (bounds.Height / 2);
                    break;
                case 1:
                    lineStartPos.Y = lineStartPos.Y - bounds.Height + 1;
                    break;
            }

            Point currentPos = lineStartPos;
            foreach (var c in text)
                if (c == ' ')
                    currentPos.X += _SpaceWidth;
                else if (c == '\n')
                {
                    lineStartPos.Y += CharacterHeight + 1;
                    currentPos = lineStartPos;
                }
                else
                {
                    spriteBatch.Draw(_FontTexture, new Vector2(currentPos.X, currentPos.Y), _CharRects[c], color);
                    currentPos.X += _CharRects[c].Width;
                }
        }

        public static Rectangle CalculateBounds(string text, Point position)
        {
            text = Sanitize(text);
            return InternalCalculateBounds(text, position);
        }

        private static Rectangle InternalCalculateBounds(string text, Point position)
        {
            Point lineStartPos = new Point(0, 0);
            Point currentPos = lineStartPos;
            Rectangle result = new Rectangle(0, 0, 0, 0);
            foreach (var c in text)
            {
                if (c == ' ')
                    currentPos.X += _SpaceWidth;
                else if (c == '\n')
                {
                    lineStartPos.Y += CharacterHeight + 1;
                    currentPos = lineStartPos;
                }
                else
                    currentPos.X += _CharRects[c].Width;

                result.Width = Math.Max(result.Width, currentPos.X);
                result.Height = Math.Max(result.Height, currentPos.Y + CharacterHeight + 1);
            }

            result.Offset(position);
            return result;
        }
    }
}
