﻿using Loap.Constants;
using Loap.Misc;
using LoapCore.Files;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Loap.UI
{
    static class Cursor
    {
        private static Texture2D? _CursorTexture;
        private static readonly Dictionary<CursorGraphic, Rectangle> _CursorRects = new Dictionary<CursorGraphic, Rectangle>();
        public static float CursorScale { get; private set; }

        public static void LoadCursors(GraphicsDevice graphicsDevice)
        {
            _CursorTexture = Texture2D.FromFile(graphicsDevice, PathConstants.CursorFilePath);
            var cursorItem = DataItemReader.LoadFromFile(PathConstants.CursorMetainfoFilePath);

            int cellWidth = cursorItem.GetChildValueInt(UIMetainfoKeys.GENERAL_CELL_WIDTH);
            int cellHeight = cursorItem.GetChildValueInt(UIMetainfoKeys.GENERAL_CELL_HEIGHT);
            int cellsX = _CursorTexture.Width / cellWidth;

            int widthBasis = cursorItem.GetChildValueInt(UIMetainfoKeys.CURSOR_WIDTH_BASIS);
            int heightBasis = cursorItem.GetChildValueInt(UIMetainfoKeys.CURSOR_HEIGHT_BASIS);
            CursorScale = Math.Min(graphicsDevice.Viewport.Width / (float)widthBasis, graphicsDevice.Viewport.Height / (float)heightBasis);

            foreach (var cursorSec in cursorItem.GetChildren(UIMetainfoKeys.CURSOR))
            {
                CursorGraphic graphic = cursorSec.GetChildValueEnum<CursorGraphic>(UIMetainfoKeys.CURSOR_GRAPHIC);
                int slotIndex = cursorSec.GetChildValueInt(UIMetainfoKeys.CURSOR_INDEX);

                Rectangle slotRect = new Rectangle(cellWidth * (slotIndex % cellsX), cellHeight * (slotIndex / cellsX), cellWidth, cellHeight);

                _CursorRects.Add(graphic, slotRect);
            }
        }

        public static void DrawCursor(SpriteBatch spriteBatch, Point center, CursorGraphic graphic)
        {
            Rectangle srcRect = _CursorRects[graphic];
            Rectangle dstRect = new Rectangle(-srcRect.Width / 2, -srcRect.Height / 2, srcRect.Width, srcRect.Height);
            dstRect.X = center.X + (int)Math.Round(dstRect.X * CursorScale);
            dstRect.Y = center.Y + (int)Math.Round(dstRect.Y * CursorScale);
            dstRect.Width = (int)Math.Round(dstRect.Width * CursorScale);
            dstRect.Height = (int)Math.Round(dstRect.Height * CursorScale);

            spriteBatch.Draw(_CursorTexture, dstRect, srcRect, Color.White);
        }
    }
}
