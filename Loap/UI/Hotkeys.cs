﻿using Loap.Constants;
using LoapCore.Constants;
using LoapCore.Files;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loap.UI
{
    static class Hotkeys
    {
        private const string KEY_ENTRY = "HOTKEY";

        public const int CAMERA_AXIS_X = 1;
        public const int CAMERA_AXIS_Y = 2;
        public const int CAMERA_AXIS_Z = 3;
        public const int CAMERA_ROTATION = 4;
        public const int CAMERA_PITCH = 5;

        public static readonly List<HotkeyEntry> Entries = new List<HotkeyEntry>();

        private const int SHIFT_MODIFIER_STEPS = 0;
        private const int CTRL_MODIFIER_STEPS = 1;
        private const int ALT_MODIFIER_STEPS = 2;

        private const int IGNORE_MODIFIER_MASK = -1;
        private const int NO_MODIFIER_MASK = 0;
        private const int SHIFT_MODIFIER_MASK = 1 << SHIFT_MODIFIER_STEPS;
        private const int CTRL_MODIFIER_MASK = 1 << CTRL_MODIFIER_STEPS;
        private const int ALT_MODIFIER_MASK = 1 << ALT_MODIFIER_STEPS;

        public static void SetDefault()
        {
            Entries.Clear();

            Entries.Add(new HotkeyEntry(HotkeyFunction.Modifier, Keys.LeftShift, -1, SHIFT_MODIFIER_STEPS));
            Entries.Add(new HotkeyEntry(HotkeyFunction.Modifier, Keys.RightShift, -1, SHIFT_MODIFIER_STEPS));
            Entries.Add(new HotkeyEntry(HotkeyFunction.Modifier, Keys.LeftControl, -1, CTRL_MODIFIER_STEPS));
            Entries.Add(new HotkeyEntry(HotkeyFunction.Modifier, Keys.RightControl, -1, CTRL_MODIFIER_STEPS));
            Entries.Add(new HotkeyEntry(HotkeyFunction.Modifier, Keys.LeftAlt, -1, ALT_MODIFIER_STEPS));
            Entries.Add(new HotkeyEntry(HotkeyFunction.Modifier, Keys.RightAlt, -1, ALT_MODIFIER_STEPS));

            Entries.Add(new HotkeyEntry(HotkeyFunction.Pause, Keys.P));
            Entries.Add(new HotkeyEntry(HotkeyFunction.FastForward, Keys.Enter));

            Entries.Add(new HotkeyEntry(HotkeyFunction.FrameSkip, Keys.B, IGNORE_MODIFIER_MASK, -1));
            Entries.Add(new HotkeyEntry(HotkeyFunction.FrameSkip, Keys.N, IGNORE_MODIFIER_MASK, 1));
            Entries.Add(new HotkeyEntry(HotkeyFunction.FrameSkip, Keys.OemComma, NO_MODIFIER_MASK, -PhysicsConstants.ITERATIONS_PER_INGAME_SECOND));
            Entries.Add(new HotkeyEntry(HotkeyFunction.FrameSkip, Keys.OemPeriod, NO_MODIFIER_MASK, PhysicsConstants.ITERATIONS_PER_INGAME_SECOND));
            Entries.Add(new HotkeyEntry(HotkeyFunction.FrameSkip, Keys.Space, IGNORE_MODIFIER_MASK, PhysicsConstants.ITERATIONS_PER_INGAME_SECOND * 5));

            Entries.Add(new HotkeyEntry(HotkeyFunction.CutLastReplayAction, Keys.OemComma, CTRL_MODIFIER_MASK));
            Entries.Add(new HotkeyEntry(HotkeyFunction.CutNextReplayAction, Keys.OemPeriod, CTRL_MODIFIER_MASK));

            Entries.Add(new HotkeyEntry(HotkeyFunction.SkipToLastAssignment, Keys.OemOpenBrackets, IGNORE_MODIFIER_MASK));
            Entries.Add(new HotkeyEntry(HotkeyFunction.SkipToNextShrugger, Keys.OemCloseBrackets, IGNORE_MODIFIER_MASK));

            Entries.Add(new HotkeyEntry(HotkeyFunction.Nuke, Keys.Q, ALT_MODIFIER_MASK));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ChangeSpawnRate, Keys.OemPlus, NO_MODIFIER_MASK, 1));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ChangeSpawnRate, Keys.OemMinus, NO_MODIFIER_MASK, -1));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ChangeSpawnRate, Keys.OemPlus, CTRL_MODIFIER_MASK, 9));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ChangeSpawnRate, Keys.OemMinus, CTRL_MODIFIER_MASK, -9));

            Entries.Add(new HotkeyEntry(HotkeyFunction.SaveReplay, Keys.U, NO_MODIFIER_MASK));
            Entries.Add(new HotkeyEntry(HotkeyFunction.LoadReplay, Keys.U, SHIFT_MODIFIER_MASK));

            Entries.Add(new HotkeyEntry(HotkeyFunction.Restart, Keys.Escape, SHIFT_MODIFIER_MASK));
            Entries.Add(new HotkeyEntry(HotkeyFunction.Quit, Keys.Escape, NO_MODIFIER_MASK));

            Entries.Add(new HotkeyEntry(HotkeyFunction.ToggleMinimap, Keys.M));

            Entries.Add(new HotkeyEntry(HotkeyFunction.PhaseCamera, Keys.Tab));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ResetCamera, Keys.Tab, CTRL_MODIFIER_MASK));
            Entries.Add(new HotkeyEntry(HotkeyFunction.AdjustCameraSpeed, Keys.LeftShift, IGNORE_MODIFIER_MASK, UIConstants.CAMERA_SPEED_FACTOR * 7 / 3));

            Entries.Add(new HotkeyEntry(HotkeyFunction.MoveCamera, Keys.W, IGNORE_MODIFIER_MASK, CAMERA_AXIS_Y));
            Entries.Add(new HotkeyEntry(HotkeyFunction.MoveCamera, Keys.A, IGNORE_MODIFIER_MASK, -CAMERA_AXIS_X));
            Entries.Add(new HotkeyEntry(HotkeyFunction.MoveCamera, Keys.S, IGNORE_MODIFIER_MASK, -CAMERA_AXIS_Y));
            Entries.Add(new HotkeyEntry(HotkeyFunction.MoveCamera, Keys.D, IGNORE_MODIFIER_MASK, CAMERA_AXIS_X));
            Entries.Add(new HotkeyEntry(HotkeyFunction.MoveCamera, Keys.R, IGNORE_MODIFIER_MASK, CAMERA_AXIS_Z));
            Entries.Add(new HotkeyEntry(HotkeyFunction.MoveCamera, Keys.F, IGNORE_MODIFIER_MASK, -CAMERA_AXIS_Z));
            Entries.Add(new HotkeyEntry(HotkeyFunction.MoveCamera, Keys.I, IGNORE_MODIFIER_MASK, CAMERA_PITCH));
            Entries.Add(new HotkeyEntry(HotkeyFunction.MoveCamera, Keys.J, IGNORE_MODIFIER_MASK, -CAMERA_ROTATION));
            Entries.Add(new HotkeyEntry(HotkeyFunction.MoveCamera, Keys.K, IGNORE_MODIFIER_MASK, -CAMERA_PITCH));
            Entries.Add(new HotkeyEntry(HotkeyFunction.MoveCamera, Keys.L, IGNORE_MODIFIER_MASK, CAMERA_ROTATION));

            for (int i = 0; i < 9; i++)
            {
                Entries.Add(new HotkeyEntry(HotkeyFunction.ChangeCamera, (Keys)((int)Keys.D1 + i), NO_MODIFIER_MASK, i + 1));
                Entries.Add(new HotkeyEntry(HotkeyFunction.CloneCamera, (Keys)((int)Keys.D1 + i), SHIFT_MODIFIER_MASK, i + 1));
            }

            Entries.Add(new HotkeyEntry(HotkeyFunction.ForceSelectWalker, Keys.LeftControl));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ForceSelectWalker, Keys.RightControl));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ForceSelectNonWalker, Keys.LeftAlt));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ForceSelectNonWalker, Keys.RightAlt));

            Entries.Add(new HotkeyEntry(HotkeyFunction.RemoveFrameLimiter, Keys.Back));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ShowFPS, Keys.Delete));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ConfineCursor, Keys.F10));

            Entries.Add(new HotkeyEntry(HotkeyFunction.SaveState, Keys.F1));
            Entries.Add(new HotkeyEntry(HotkeyFunction.LoadState, Keys.F2));
            Entries.Add(new HotkeyEntry(HotkeyFunction.ToggleReplayInsert, Keys.F3));

            Entries.Add(new HotkeyEntry(HotkeyFunction.CheatMode, Keys.C, ALT_MODIFIER_MASK));
        }

        public static int[] GetParameters(HotkeyFunction key)
        {
            return Entries.Where(e => e.Function == key).Select(e => e.Parameter).ToArray();
        }

        public static void Sanitize()
        {
            foreach (var hotkey in Entries)
            {
                switch (hotkey.Function)
                {
                    case HotkeyFunction.Modifier:
                        hotkey.KeyModifier = -1;
                        break;
                    case HotkeyFunction.ChangeSpawnRate:
                        hotkey.Parameter = Math.Clamp(hotkey.Parameter, -9, 9);
                        break;
                }

                if (hotkey.KeyModifier < -1)
                    hotkey.KeyModifier = -1;
            }

            Entries.RemoveAll(hk => hk.Function == HotkeyFunction.Modifier && (hk.Parameter < 0 || hk.Parameter > 31));
            Entries.RemoveAll(hk => hk.Function == HotkeyFunction.FrameSkip && hk.Parameter == 0);
            Entries.RemoveAll(hk => hk.Function == HotkeyFunction.SelectSkill && (hk.Parameter < 0 || hk.Parameter > 8));
            Entries.RemoveAll(hk => hk.Function == HotkeyFunction.ChangeSpawnRate && hk.Parameter == 0);
            Entries.RemoveAll(hk => hk.Function == HotkeyFunction.ChangeCamera && (hk.Parameter < 1 || hk.Parameter > 9));
            Entries.RemoveAll(hk => hk.Function == HotkeyFunction.CloneCamera && (hk.Parameter < 1 || hk.Parameter > 9));
            Entries.RemoveAll(hk => hk.Function == HotkeyFunction.MoveCamera && (hk.Parameter < -5 || hk.Parameter > 5 || hk.Parameter == 0));
            Entries.RemoveAll(hk => hk.Function == HotkeyFunction.AdjustCameraSpeed && hk.Parameter < 1);
        }

        public static void Load()
        {
            Entries.Clear();

            if (File.Exists(PathConstants.HotkeysFile))
            {
                var item = DataItemReader.LoadFromFile(PathConstants.HotkeysFile);
                foreach (var src in item.GetChildren(KEY_ENTRY))
                    Entries.Add(HotkeyEntry.FromDataItem(src));
            }
            else
                SetDefault();

            Sanitize();
            Save();
        }

        public static void Save()
        {
            var sortedEntries = Entries.OrderBy(e => e.Function).ThenBy(e => Math.Abs(e.Parameter)).ThenBy(e => -e.Parameter).ThenBy(e => e.Key).ThenBy(e => e.KeyModifier);
            var result = new DataItem();

            foreach (var entry in sortedEntries)
            {
                var dst = result.AddChildSection(KEY_ENTRY);
                entry.SaveToDataItem(dst);
            }

            DataItemWriter.SaveToFile(PathConstants.HotkeysFile, result);
        }
    }
}
