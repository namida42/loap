﻿using Loap.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Linq;
using SharpHook;

namespace Loap.UI
{
    static class UserInput
    {
        private static KeyboardState _LastKeyboard;
        private static int _LastShift;
        private static MouseState _LastMouse;
        private static KeyboardState _CurrentKeyboard;
        private static int _CurrentShift;
        private static MouseState _CurrentMouse;
        private static Point _LastMousePoint;
        private static Point _CurrentMousePoint;
        private static Point _CurrentMouseDelta;

        private static Point _WindowPosition;
        private static Point _LastHookMousePosition;
        private static Point _LockedMouseHookDelta;

        private static bool _LockMouseNextUpdate;
        private static bool _MouseLocked;

        private static SimpleGlobalHook _GlobalHook = new SimpleGlobalHook();
        private static bool _CanHook;

        static UserInput()
        {
            _CanHook = true;

            if (_CanHook)
            {
                _GlobalHook.MouseMoved += MouseMoveHook;
                _GlobalHook.MouseDragged += MouseMoveHook;
            }
        }

        public static void Initialize()
        {
            if (_CanHook)
                _GlobalHook.RunAsync();
        }

        public static void Deinitialize()
        {
            if (_CanHook)
                _GlobalHook.Dispose();
        }

        public static void Update(Point windowPosition)
        {
            _WindowPosition = windowPosition;

            _LastKeyboard = _CurrentKeyboard;
            _LastMouse = _CurrentMouse;
            _LastMousePoint = _CurrentMousePoint;
            _LastShift = _CurrentShift;

            _CurrentKeyboard = Keyboard.GetState();
            _CurrentShift = GetShiftState(_CurrentKeyboard);
            _CurrentMouse = Mouse.GetState();

            if (_MouseLocked)
            {
                if (!_CanHook)
                {
                    _CurrentMouseDelta = _CurrentMousePoint - _LastMousePoint;
                    _CurrentMousePoint = _LastMousePoint;
                    Mouse.SetPosition(_CurrentMousePoint.X, _CurrentMousePoint.Y);
                }
                else
                {
                    _CurrentMouseDelta = _LockedMouseHookDelta;
                    _CurrentMousePoint = _LastMousePoint;
                }
            }
            else
                _CurrentMousePoint = _CurrentMouse.Position;

            _MouseLocked = _LockMouseNextUpdate;
            _LockMouseNextUpdate = false;
            _LockedMouseHookDelta = new Point(0, 0);

            if (LoapSettings.ConfineCursor && !LoapSettings.FullScreen && !_CanHook)
            {
                bool needResetCursor = false;
                Point newMousePos = new Point(_CurrentMouse.X, _CurrentMouse.Y);

                if (_CurrentMouse.X < 0)
                {
                    needResetCursor = true;
                    newMousePos.X = 0;
                }

                if (_CurrentMouse.X >= LoapSettings.WindowWidth)
                {
                    needResetCursor = true;
                    newMousePos.X = LoapSettings.WindowWidth - 1;
                }

                if (_CurrentMouse.Y < 0)
                {
                    needResetCursor = true;
                    newMousePos.Y = 0;
                }

                if (_CurrentMouse.Y >= LoapSettings.WindowHeight)
                {
                    needResetCursor = true;
                    newMousePos.Y = LoapSettings.WindowHeight - 1;
                }

                if (needResetCursor)
                    Mouse.SetPosition(newMousePos.X, newMousePos.Y);
            }
        }

        public static Point MousePosition => _CurrentMousePoint;
        public static Point MouseDelta => _CurrentMouseDelta;
        public static int MouseScrollDelta => _CurrentMouse.ScrollWheelValue - _LastMouse.ScrollWheelValue;

        private static int GetShiftState(KeyboardState state)
        {
            int result = 0;
            foreach (var modKey in Hotkeys.Entries.Where(e => e.Function == HotkeyFunction.Modifier))
                if (state.IsKeyDown(modKey.Key))
                    result |= (1 << modKey.Parameter);

            return result;
        }

        private static bool GetFunctionKeyState(HotkeyFunction key, int? parameter, KeyboardState state, int shift)
        {
            foreach (var entry in Hotkeys.Entries.Where(e => e.Function == key && (parameter == null || e.Parameter == parameter.Value)))
                if (entry.KeyModifier < 0 || entry.KeyModifier == shift)
                    if (state.IsKeyDown(entry.Key))
                        return true;

            return false;
        }

        public static bool IsKeyPressed(HotkeyFunction key, int? parameter = null)
        {
            return GetFunctionKeyState(key, parameter, _CurrentKeyboard, _CurrentShift) && !GetFunctionKeyState(key, parameter, _LastKeyboard, _LastShift);
        }

        public static bool IsKeyHeld(HotkeyFunction key, int? parameter = null)
        {
            return GetFunctionKeyState(key, parameter, _CurrentKeyboard, _CurrentShift);
        }

        public static bool IsKeyNewlyReleased(HotkeyFunction key, int? parameter = null)
        {
            return !GetFunctionKeyState(key, parameter, _CurrentKeyboard, _CurrentShift) && GetFunctionKeyState(key, parameter, _LastKeyboard, _LastShift);
        }

        public static bool IsKeyReleased(HotkeyFunction key, int? parameter = null)
        {
            return !GetFunctionKeyState(key, parameter, _CurrentKeyboard, _CurrentShift);
        }

        public static bool IsRawKeyPressed(Keys key)
        {
            return _CurrentKeyboard.IsKeyDown(key) && !_LastKeyboard.IsKeyDown(key);
        }

        public static bool IsRawKeyHeld(Keys key)
        {
            return _CurrentKeyboard.IsKeyDown(key);
        }

        public static bool IsRawKeyNewlyReleased(Keys key)
        {
            return !_CurrentKeyboard.IsKeyDown(key) && _LastKeyboard.IsKeyDown(key);
        }

        public static bool IsRawKeyReleased(Keys key)
        {
            return !_CurrentKeyboard.IsKeyDown(key);
        }

        private static void GetMouseButtonState(MouseButton button, out ButtonState last, out ButtonState current)
        {
            switch (button)
            {
                case MouseButton.Left:
                    last = _LastMouse.LeftButton;
                    current = _CurrentMouse.LeftButton;
                    break;
                case MouseButton.Right:
                    last = _LastMouse.RightButton;
                    current = _CurrentMouse.RightButton;
                    break;
                case MouseButton.Middle:
                    last = _LastMouse.MiddleButton;
                    current = _CurrentMouse.MiddleButton;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public static bool IsMouseButtonPressed(MouseButton button)
        {
            GetMouseButtonState(button, out ButtonState last, out ButtonState current);
            return (current == ButtonState.Pressed) && (last == ButtonState.Released);
        }

        public static bool IsMouseButtonHeld(MouseButton button)
        {
            GetMouseButtonState(button, out _, out ButtonState current);
            return current == ButtonState.Pressed;
        }

        public static bool IsMouseButtonNewlyReleased(MouseButton button)
        {
            GetMouseButtonState(button, out ButtonState last, out ButtonState current);
            return (current == ButtonState.Released) && (last == ButtonState.Pressed);
        }

        public static bool IsMouseButtonReleased(MouseButton button)
        {
            GetMouseButtonState(button, out _, out ButtonState current);
            return current == ButtonState.Released;
        }

        public static void LockMouse()
        {
            if (!LoapSettings.DisableMouseLock)
            {
                _LockMouseNextUpdate = true;
            }
        }

        private static void MouseMoveHook(object? sender, MouseHookEventArgs args)
        {
            Point clientAreaPoint = new Point(args.Data.X, args.Data.Y) - _WindowPosition;

            if (_MouseLocked)
            {
                _LockedMouseHookDelta += clientAreaPoint - _LastHookMousePosition;
                args.SuppressEvent = true;
            }
            else if (LoapSettings.ConfineCursor)
            {
                Point target = clientAreaPoint;
                if (target.X < 0) target.X = 0;
                if (target.X > LoapSettings.WindowWidth - 1) target.X = LoapSettings.WindowWidth - 1;
                if (target.Y < 0) target.Y = 0;
                if (target.Y > LoapSettings.WindowHeight - 1) target.Y = LoapSettings.WindowHeight - 1;

                if (target != clientAreaPoint)
                {
                    args.SuppressEvent = true;

                    SharpHook.Native.UioHookEvent newEvent = new SharpHook.Native.UioHookEvent();
                    newEvent.Type = SharpHook.Native.EventType.MouseMoved;
                    newEvent.Mouse.X = (short)(_WindowPosition.X + target.X);
                    newEvent.Mouse.Y = (short)(_WindowPosition.Y + target.Y);
                    SharpHook.Native.UioHook.PostEvent(ref newEvent);
                }
                else
                    _LastHookMousePosition = clientAreaPoint;
            }
            else
                _LastHookMousePosition = clientAreaPoint;
        }
    }
}
