﻿using LoapCore.Files;
using Microsoft.Xna.Framework.Input;
using System;
using System.Linq;

namespace Loap.UI
{
    enum HotkeyFunction
    {
        Modifier,
        ForceSelectWalker, ForceSelectNonWalker,
        RemoveFrameLimiter, ShowFPS, ConfineCursor,
        Pause, FastForward, FrameSkip, SkipToLastAssignment, SkipToNextShrugger,
        SelectSkill, Nuke, ChangeSpawnRate,
        SaveReplay, LoadReplay, ToggleReplayInsert, CutLastReplayAction, CutNextReplayAction,
        Restart, Quit,
        SaveState, LoadState,
        ChangeCamera, CloneCamera, MoveCamera, PhaseCamera, AdjustCameraSpeed, ResetCamera,
        ToggleMinimap, CheatMode
    }

    class HotkeyEntry
    {
        public static readonly HotkeyFunction[] SaveParameterFunctions = new[]
        {
            HotkeyFunction.Modifier, HotkeyFunction.FrameSkip, HotkeyFunction.SelectSkill, HotkeyFunction.ChangeSpawnRate,
            HotkeyFunction.ChangeCamera, HotkeyFunction.CloneCamera, HotkeyFunction.MoveCamera, HotkeyFunction.AdjustCameraSpeed
        };

        private const string KEY_FUNCTION = "FUNCTION";
        private const string KEY_KEY = "KEY";
        private const string KEY_MODIFIER = "MODIFIER";
        private const string KEY_PARAMETER = "PARAMETER";

        public HotkeyFunction Function;
        public Keys Key;
        public int KeyModifier = -1;
        public int Parameter;

        public HotkeyEntry()
        {
        }

        public HotkeyEntry(HotkeyFunction function, Keys key, int modifier = -1, int parameter = 0)
        {
            Function = function;
            Key = key;
            KeyModifier = modifier;
            Parameter = parameter;
        }

        public static HotkeyEntry FromDataItem(DataItem item)
        {
            var result = new HotkeyEntry();

            result.Function = item.GetChildValueEnum<HotkeyFunction>(KEY_FUNCTION);
            result.Key = item.GetChildValueEnum<Keys>(KEY_KEY);
            result.KeyModifier = item.GetChildValueInt(KEY_MODIFIER, -1);
            result.Parameter = item.GetChildValueInt(KEY_PARAMETER);

            if (result.Function == HotkeyFunction.Modifier)
                result.KeyModifier = -1;

            return result;
        }

        public void SaveToDataItem(DataItem dst)
        {
            dst.SetChildValueEnum(KEY_FUNCTION, Function);
            dst.SetChildValueEnum(KEY_KEY, Key);

            if (KeyModifier >= 0 && Function != HotkeyFunction.Modifier)
                dst.SetChildValueInt(KEY_MODIFIER, KeyModifier);

            if (SaveParameterFunctions.Contains(Function))
                dst.SetChildValueInt(KEY_PARAMETER, Parameter);
        }
    }
}
