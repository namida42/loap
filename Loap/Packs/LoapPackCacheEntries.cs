﻿using Loap.Constants;
using Loap.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Loap.Packs
{
    class LoapPackCacheBaseEntry
    {
        public readonly LoapPackCacheBaseEntry Parent;

        public LoapPackCacheBaseEntry(LoapPackCacheBaseEntry parent)
        {
            Parent = parent;
        }

        public string FilePath = "";
        public string Title = "";
        public string Author = "";
        public DateTime ModifiedTime;

        protected bool _Initialized;

        public string FullPath => Path.Combine(PathConstants.LevelsPath, FilePath);

        public virtual void GetDataFromFile(bool includeLevels)
        {
            _Initialized = true;
        }
    }

    class LoapPackCachePackEntry : LoapPackCacheBaseEntry
    {
        public readonly List<LoapPackCacheBaseEntry> Children = new List<LoapPackCacheBaseEntry>();

        public LoapPackCachePackEntry(LoapPackCacheBaseEntry parent) : base(parent)
        {
        }

        private void OrderList()
        {
            Children.Sort(CompareChildren);
        }

        private static int CompareChildren(LoapPackCacheBaseEntry a, LoapPackCacheBaseEntry b)
        {
            bool aIsPack = a is LoapPackCachePackEntry;
            bool bIsPack = b is LoapPackCachePackEntry;

            if (aIsPack && !bIsPack)
                return -1;
            else if (!aIsPack && bIsPack)
                return 1;

            return string.Compare(a.FilePath, b.FilePath);
        }

        public override void GetDataFromFile(bool includeLevels)
        {
            if (!_Initialized || (Directory.GetLastWriteTime(FullPath) != ModifiedTime))
            {
                Title = Path.GetFileName(FilePath) ?? "";
                int spaceIndex = Title.IndexOf(' ');
                if (spaceIndex > 0)
                {
                    string firstSubString = Title.Substring(0, spaceIndex);
                    if (int.TryParse(firstSubString, out _))
                        Title = Title.Substring(spaceIndex + 1);
                }
            }

            RebuildSubdirs();
            RebuildLevelFiles();

            foreach (var child in Children)
                child.GetDataFromFile(child is LoapPackCachePackEntry ? false : includeLevels);

            OrderList();

            ModifiedTime = Directory.GetLastWriteTime(FullPath);
            base.GetDataFromFile(includeLevels);
        }

        private void RebuildSubdirs()
        {
            var subdirChildren = Children.Where(c => c is LoapPackCachePackEntry).Cast<LoapPackCachePackEntry>();

            foreach (var child in subdirChildren)
            {
                if (!Directory.Exists(child.FullPath))
                    Children.Remove(child);
            }

            var subfolders = Directory.GetDirectories(FullPath);
            foreach (var subfolder in subfolders)
            {
                string newPath = Path.GetRelativePath(PathConstants.LevelsPath, subfolder);

                if (!subdirChildren.Any(c => c.FilePath == newPath))
                {
                    var newChild = new LoapPackCachePackEntry(this);
                    newChild.FilePath = newPath;
                    Children.Add(newChild);
                }
            }
        }

        private void RebuildLevelFiles()
        {
            var levelChildren = Children.Where(c => c is LoapPackCacheLevelEntry).Cast<LoapPackCacheLevelEntry>();

            foreach (var child in levelChildren)
            {
                if (!File.Exists(child.FullPath))
                    Children.Remove(child);
            }

            var files = Directory.GetFiles(FullPath);
            foreach (var filePath in files)
            {
                string filename = Path.GetFileName(filePath);

                if (Path.GetExtension(filename).ToUpperInvariant() != PathConstants.EXT_LEVEL)
                    if (Path.GetFileNameWithoutExtension(filename).ToUpperInvariant() != "LEVEL")
                        continue;

                string newPath = Path.Combine(FilePath, filename);

                if (!levelChildren.Any(c => c.FilePath == newPath))
                {
                    var newChild = new LoapPackCacheLevelEntry(this);
                    newChild.FilePath = newPath;
                    Children.Add(newChild);
                }
            }
        }

        public LoapPackCacheLevelEntry FindNextLevel(LoapPackCacheLevelEntry currentLevel)
        {
            if (!Children.Contains(currentLevel))
                return currentLevel;

            var levelChildren = Children.Where(c => c is LoapPackCacheLevelEntry).Cast<LoapPackCacheLevelEntry>().ToList();
            int childIndex = levelChildren.IndexOf(currentLevel);
            if (childIndex < levelChildren.Count - 1)
                return levelChildren[childIndex + 1];

            var nextPack = FindNextPack(this, true);
            return (LoapPackCacheLevelEntry)(nextPack.Children.Where(c => c is LoapPackCacheLevelEntry).First());
        }

        public static LoapPackCachePackEntry FindNextPack(LoapPackCachePackEntry origin, bool mustContainLevels)
        {
            if (origin == LoapPackCache.BasePack)
                return origin;

            var currentPack = origin;
            if (currentPack.Parent == LoapPackCache.BasePack)
                currentPack = GoToDeepestFirstPack(currentPack);
            else
            {
                var parent = (LoapPackCachePackEntry)currentPack.Parent;
                var parentPacks = parent.Children.Where(c => c is LoapPackCachePackEntry).Cast<LoapPackCachePackEntry>().ToList();
                var parentIndex = parentPacks.IndexOf(currentPack);
                if (parentIndex < parentPacks.Count - 1)
                    currentPack = GoToDeepestFirstPack(parentPacks[parentIndex + 1]);
                else
                    currentPack = parent;
            }

            if (mustContainLevels)
                while (currentPack.Children.Count(c => c is LoapPackCacheLevelEntry) == 0)
                    currentPack = FindNextPack(currentPack, true);

            return currentPack;
        }

        private static LoapPackCachePackEntry GoToDeepestFirstPack(LoapPackCachePackEntry origin)
        {
            var currentPack = origin;
            while (currentPack.Children.Count(c => c is LoapPackCachePackEntry) > 0)
                currentPack = (LoapPackCachePackEntry)currentPack.Children.Where(c => c is LoapPackCachePackEntry).First();

            return currentPack;
        }
    }

    class LoapPackCacheLevelEntry : LoapPackCacheBaseEntry
    {
        public LoapPackCacheLevelEntry(LoapPackCacheBaseEntry parent) : base(parent)
        {
        }

        public override void GetDataFromFile(bool includeLevels)
        {
            if (!includeLevels)
                return;

            if (_Initialized && (File.GetLastWriteTime(FullPath) == ModifiedTime))
                return;

            var tempLevel = LevelFileLoader.LoadLevel(FullPath);

            Title = tempLevel.LevelInfo.Title;
            Author = ""; // TODO: Author support

            ModifiedTime = File.GetLastWriteTime(FullPath);
            base.GetDataFromFile(includeLevels);
        }
    }
}
