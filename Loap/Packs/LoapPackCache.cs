﻿using Loap.Constants;
using System.IO;
using System.Linq;

namespace Loap.Packs
{
    static class LoapPackCache
    {
        public static readonly LoapPackCachePackEntry BasePack;

        static LoapPackCache()
        {
            BasePack = new LoapPackCachePackEntry(null!);
            BasePack.Title = "<BASE PACK>";
            BasePack.FilePath = "";

            Refresh(false);
        }

        public static void Refresh(bool includeLevels)
        {
            BasePack.GetDataFromFile(includeLevels);
        }

        public static LoapPackCachePackEntry? GetPackByDirectory(string path)
        {
            string localPath = Path.IsPathRooted(path) ?
                Path.GetRelativePath(PathConstants.LevelsPath, path) : path;

            return FindRecursive(BasePack, localPath);
        }

        private static LoapPackCachePackEntry? FindRecursive(LoapPackCachePackEntry origin, string path)
        {
            if (origin.FilePath == path)
                return origin;
            else
            {
                foreach (var child in origin.Children.Where(c => c is LoapPackCachePackEntry).Cast<LoapPackCachePackEntry>())
                {
                    var childResult = FindRecursive(child, path);
                    if (childResult != null)
                        return childResult;
                }
            }

            return null;
        }
    }
}
