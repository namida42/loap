﻿namespace Loap.Constants
{
    static class LemmingSpriteKeys
    {
        public const string SECTION = "SPRITE";
        public const string SPRITE_NAME = "NAME";
        public const string SPRITE_FRAMES = "FRAMES";
        public const string SPRITE_DIRECTIONS = "DIRECTIONS";
        public const string SPRITE_TEXTURE_FILE = "TEXTURE_FILE";
        public const string SPRITE_POSITION = "POSITION";
        public const string SPRITE_REVERSE_ANIMATION = "REVERSE_ANIMATION";
        public const string SPRITE_ANIMATION_STYLE = "ANIMATION";
        public const string SPRITE_KEY_FRAME = "KEY_FRAME";
    }
}
