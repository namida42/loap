﻿namespace Loap.Constants
{
    static class GameTexts
    {
        public const string CREDIT_TEXT = "Visit the Lemmings Forums for support";

        public const string LEFT_MOUSE_PLAY = "Left Click to play";
        public const string RIGHT_MOUSE_EXIT = "Right Click to exit";

        public const string LEMMINGS_IN = "WAIT";
        public const string LEMMINGS_LIVE = "LIVE";
        public const string LEMMINGS_GOAL = "SAVE";

        public const string REPLAY_LOADED = "Replay loaded";
        public const string REPLAY_MODE = "Replay mode";
        public const string REPLAY_INSERT_MODE = "Replay mode (Insert)";
        public const string CHEAT_MODE = "Cheat mode";

        public const string POSTVIEW_HEADER = "ALL LEMMINGS ACCOUNTED FOR";
        public const string POSTVIEW_HEADER_CHEAT_MODE = "CHEAT MODE WAS ACTIVATED";
        public const string POSTVIEW_YOU_SAVED = "You saved";
        public const string POSTVIEW_YOU_NEEDED = "You needed";
        public const string POSTVIEW_YOUR_TIME = "Time";
        public const string LEFT_MOUSE_RETRY = "Left Click to retry";
        public const string LEFT_MOUSE_NEXT = "Left Click for next level";
    }
}
