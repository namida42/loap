﻿using LoapCore.Constants;
using Microsoft.Xna.Framework;

namespace Loap.Constants
{
    static class UIConstants
    {
        public const float FADE_TIME = 0.5f;

        public const float MAXIMUM_SKILL_PANEL_HEIGHT = 0.225f;
        public const float MAXIMUM_SIDEBAR_WIDTH = 0.1375f;

        public const float IDEAL_FRAMERATE_TIME = 1 / 60f;
        public const float IDEAL_UPDATE_TIME_NORMAL = 1 / 30f;
        public const float IDEAL_UPDATE_TIME_FASTFORWARD = 1 / 120f;
        public const int DEFAULT_MAX_CATCHUP_FRAMES = 4;
        public const int CATCHUP_IDEAL_DELAY_DIVISOR = 3;

        public const float PARTICLE_INITIAL_SCALE_DEFAULT = 0.4f;
        public const float PARTICLE_SIZE_DECAY_RATE = PARTICLE_INITIAL_SCALE_DEFAULT / 10;
        public const int PARTICLE_DECAY_DELAY_FRAMES = 5;
        public const int PARTICLE_VERTICAL_MOMENTUM_PERIOD = 3;

        public const int CAMERA_SPEED_FACTOR = 100;
        public const float CAMERA_SPEED_STANDARD = 1.5f;
        public const float CAMERA_SPEED_FAST = 3.5f;
        public const float CAMERA_ROTATION_MOD = 11.25f;
        public const float CAMERA_PITCH_MOD = 5.625f;
        public const float CAMERA_MOUSE_MOVEMENT_XZ_MOD = 0.75f;
        public const float CAMERA_MOUSE_MOVEMENT_Y_MOD = 0.375f;
        public const float CAMERA_MOUSE_ROTATION_MOD = 1.5f;
        public const float CAMERA_VIRTUAL_LEMMING_ROTATE_MOD = 270f;
        public const float CAMERA_SOLIDITY_CHECK_OFFSET = 0.125f;

        public const float CAMERA_MAX_DISTANCE_XY = 24f;
        public const float CAMERA_MAX_DISTANCE_Z = 16f;

        public static Color NOT_ENOUGH_LEMMINGS_COLOR => new Color(0.75f, 0, 0);
        public static Color SAVE_REQUIREMENT_MET_COLOR => new Color(0, 0.75f, 0);

        public const float MAX_LEMMING_SELECTION_DISTANCE = 48;

        public const float RR_BUTTON_VISUAL_EFFECT_TIME = 0.125f;
        public const float NUKE_DOUBLE_CLICK_TIME = 0.375f;

        public const int OVERAGE_ITERATIONS = PhysicsConstants.ITERATIONS_PER_INGAME_SECOND * 5 / 2;

        public static Color PACK_COLOR => new Color(0.75f, 0.75f, 0);
        public static Color LEVEL_NOT_ATTEMPTED_COLOR => new Color(0.75f, 0, 0);
        public static Color LEVEL_ATTEMPTED_COLOR => new Color(0.75f, 0.375f, 0);
        public static Color LEVEL_COMPLETED_COLOR => new Color(0, 0.75f, 0);
        public static Color LEVEL_SELECT_DISABLED_COLOR => new Color(0.5f, 0.5f, 0.5f);

        public static Color POST_LEMMING_MISSING_COLOR => new Color(0.125f, 0f, 0f);
    }
}
