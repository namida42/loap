﻿namespace Loap.Constants
{
    static class UIMetainfoKeys
    {
        public const string GENERAL_CELL_WIDTH = "CELL_WIDTH";
        public const string GENERAL_CELL_HEIGHT = "CELL_HEIGHT";

        public const string FONT_DEFAULT_WIDTH = "DEFAULT";
        public const string FONT_DEFAULT_LOWERCASE_WIDTH = "LOWERCASE";
        public const string FONT_SPACE_WIDTH = "SPACE";
        public const string FONT_SIZE_SECTION = "SIZE";
        public const string FONT_CHARACTER = "CHARACTER";
        public const string FONT_WIDTH = "WIDTH";
        public const string FONT_FIRST = "FIRST";
        public const string FONT_LAST = "LAST";
        public const string FONT_MISSING = "MISSING";

        public const string CURSOR = "CURSOR";
        public const string CURSOR_INDEX = "INDEX";
        public const string CURSOR_GRAPHIC = "GRAPHIC";
        public const string CURSOR_WIDTH_BASIS = "WIDTH_BASIS";
        public const string CURSOR_HEIGHT_BASIS = "HEIGHT_BASIS";
    }
}
