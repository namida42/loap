﻿using LoapCore.Constants;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Loap.Constants
{
    static class RenderConstants
    {
        public const int DESIGN_WIDTH = 960;
        public const int DESIGN_HEIGHT = 720;

        public const int DEFAULT_INTERNAL_CANVAS_WIDTH = 1280;
        public const int DEFAULT_INTERNAL_CANVAS_HEIGHT = 720;

        // Render priorities for 3D scenes. Higher number = will overwrite lower number.
        public const int RENDER_PRIORITY_BUILDER_BRICK_REMAINDER = 1;
        public const int RENDER_PRIORITY_BLOCKS_OUTER = 2;
        public const int RENDER_PRIORITY_CRACKS_OUTER = 3;
        public const int RENDER_PRIORITY_OVERLAY_OUTER = 4;
        public const int RENDER_PRIORITY_BUILDER_BRICK_TOP = 5;
        public const int RENDER_PRIORITY_OVERLAY_INNER = 6;
        public const int RENDER_PRIORITY_CRACKS_INNER = 7;
        public const int RENDER_PRIORITY_BLOCKS_INNER = 8;
        public const int RENDER_PRIORITY_SPAWN_DIRECTION = 9;
        public const int RENDER_PRIORITY_HATCH_FLAPS = 10;
        public const int RENDER_PRIORITY_PARTICLES = 11;
        public const int RENDER_PRIORITY_CAMERA = 12;
        public const int RENDER_PRIORITY_ENTITIES = 13;
        public const int RENDER_PRIORITY_LEMMINGS = 14;

        public const int INITIAL_VERTEX_CAP = 256;

        public const int SPAWN_DIRECTION_RENDER_OFFSET_CAP = 16;
        public const int SPAWN_DIRECTION_RENDER_FALLBACK = 4;

        public const float FIELD_OF_VIEW = MathF.PI / 4; // 45 degrees
        public const float NEAR_PLANE_DISTANCE = 1f / FAR_PLANE_DISTANCE;
        public const float FAR_PLANE_DISTANCE = 384;

        public const int SKY_SEGMENTS = 64;
        public const float SKY_DISTANCE = 128;
        public const float SKY_MINIMUM_HEIGHT = 128;

        public const int LEMMING_SPRITES_PER_SHEET_AXIS = 16;
        public const float CLIMBER_MAX_OFFSET = 0.125f;
        public const float LEMMING_SPRITE_RENDER_SIZE = 0.5f;
        public const float OVERHEAD_ICON_RENDER_SIZE = 0.25f;
        public const float CAMERA_SPRITE_RENDER_SIZE = 0.5f;
        public const float VIRTUAL_LEMMING_OFFSET = -0.25f;

        public const int HATCH_OPEN_ITERATIONS = 54;

        public const float BILLBOARD_BIAS = 0.5f;

        private const int BLANK_TEXTURE_SIZE = 32;

        public const float SHADING_INCREMENT = 0.0375f;
        public const float X_AXIS_FACE_SHADING = SHADING_INCREMENT * 1;
        public const float Y_AXIS_FACE_SHADING = SHADING_INCREMENT * 2;
        public const float BOTTOM_FACE_SHADING = SHADING_INCREMENT * 3;

        public const int MINIMUM_FACE_RENDER_HEIGHT_BLOCKS = 1;
        public const float MINIMUM_FACE_RENDER_HEIGHT = PhysicsConstants.SEGMENT_SIZE_FLOAT * MINIMUM_FACE_RENDER_HEIGHT_BLOCKS;
        public const float INSIDE_FACE_OFFSET = PhysicsConstants.STEP_SIZE_FLOAT / 16f;

        public const string SCREEN_TEXTURE = "*screen";
        public const int SCREEN_TEXTURE_SIZE = 512;
        public const int SCREEN_TEXTURE_HORIZONTAL_CELLS = 5;
        public const int SCREEN_TEXTURE_VERTICAL_CELLS = 3;
        public const int SCREEN_TEXTURE_BOUNDARY_TOP = 54;
        public const int SCREEN_TEXTURE_BOUNDARY_LEFT = 64;
        public const int SCREEN_TEXTURE_BOUNDARY_RIGHT = 64;
        public const int SCREEN_TEXTURE_BOUNDARY_BOTTOM = 0;

        private static RasterizerState? _RasterizerState;
        public static RasterizerState RasterizerState
        {
            get
            {
                if (_RasterizerState == null)
                {
                    _RasterizerState = new RasterizerState();
                    _RasterizerState.CullMode = CullMode.CullClockwiseFace;
                    _RasterizerState.MultiSampleAntiAlias = false;
                }

                return _RasterizerState;
            }
        }

        private static Texture2D? _BlankTexture;
        public static Texture2D BlankTexture
        {
            get
            {
                return _BlankTexture ?? throw new InvalidOperationException("Blank texture not initialized.");
            }
        }

        public static void PrepareBlankTexture(GraphicsDevice device)
        {
            _BlankTexture = new Texture2D(device, RenderConstants.BLANK_TEXTURE_SIZE, RenderConstants.BLANK_TEXTURE_SIZE);
            var blankTexData = new Color[RenderConstants.BLANK_TEXTURE_SIZE * RenderConstants.BLANK_TEXTURE_SIZE];
            for (int i = 0; i < RenderConstants.BLANK_TEXTURE_SIZE * RenderConstants.BLANK_TEXTURE_SIZE; i++)
                blankTexData[i] = Color.Black;
            BlankTexture.SetData(blankTexData);
        }

        public static readonly Color[] Shades = new Color[]
        {
            new Color(1f, 1f, 1f),
            new Color(0.92931247f, 0.9192879f, 0.9166285f),
            new Color(0.8777827f, 0.8551368f, 0.84491575f),
            new Color(0.82104707f, 0.78436863f, 0.7750721f),
            new Color(0.7717539f, 0.7283613f, 0.7287369f),
            new Color(0.7242903f, 0.6830836f, 0.6786098f),
            new Color(0.6927124f, 0.6377905f, 0.6417888f),
            new Color(0.65146124f, 0.5912619f, 0.58841527f),
            new Color(0.6080401f, 0.54690063f, 0.54970765f)
        };
    }
}
