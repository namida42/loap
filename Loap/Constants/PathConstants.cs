﻿using System;
using System.IO;
using System.Reflection;

namespace Loap.Constants
{
    class PathConstants
    {
        public const string EXT_MODEL = ".obj";
        public const string EXT_IMAGE = ".png";
        public const string EXT_AUDIO = ".ogg";
        public const string EXT_LEVEL = ".lplv";
        public const string EXT_METAENTITY = ".lpme";
        public const string EXT_REPLAY = ".lprp";
        public const string EXT_GENERAL = ".lpmi";

        public static string AppPath => AppDomain.CurrentDomain.BaseDirectory!;

        private static string SETTINGS_FILE = Path.ChangeExtension(Path.GetFileName(Assembly.GetEntryAssembly()!.Location), ".ini");
        private static string HOTKEYS_FILE = "hotkeys" + EXT_GENERAL;
        private static string PROGRESS_FILE = "progress" + EXT_GENERAL;
        public static string SettingsFile => Path.Combine(AppPath, SETTINGS_FILE);
        public static string HotkeysFile => Path.Combine(AppPath, HOTKEYS_FILE);
        public static string ProgressFile => Path.Combine(AppPath, PROGRESS_FILE);

        private const string DATA_FOLDER = "data";
        public static string DataPath => Path.Combine(AppPath, DATA_FOLDER);

        private const string REPLAY_FOLDER = "replay";
        public const string AUTO_REPLAY_FOLDER = "Auto";
        public static string ReplayPath => Path.Combine(AppPath, REPLAY_FOLDER);

        private const string ENTITIES_FOLDER = "entities";
        private const string GRAPHICS_FOLDER = "gfx";
        private const string LEVELS_FOLDER = "levels";
        private const string MODELS_FOLDER = "models";
        private const string MUSIC_FOLDER = "music";
        private const string SOUND_FOLDER = "sfx";
        private const string TEXTURES_FOLDER = "textures";
        public static string EntitiesPath => Path.Combine(AppPath, DATA_FOLDER, ENTITIES_FOLDER);
        public static string GraphicsPath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER);
        public static string LevelsPath => Path.Combine(AppPath, DATA_FOLDER, LEVELS_FOLDER);
        public static string ModelsPath => Path.Combine(AppPath, DATA_FOLDER, MODELS_FOLDER);
        public static string MusicPath => Path.Combine(AppPath, DATA_FOLDER, MUSIC_FOLDER);
        public static string SoundPath => Path.Combine(AppPath, DATA_FOLDER, SOUND_FOLDER);

        private const string TEXTURES_BLOCKS_FOLDER = "blocks";
        private const string TEXTURES_LAND_FOLDER = "land";
        private const string TEXTURES_SEA_FOLDER = "sea";
        private const string TEXTURES_SKY_FOLDER = "sky";
        public static string BlockTexturesPath => Path.Combine(AppPath, DATA_FOLDER, TEXTURES_FOLDER, TEXTURES_BLOCKS_FOLDER);
        public static string LandTexturesPath => Path.Combine(AppPath, DATA_FOLDER, TEXTURES_FOLDER, TEXTURES_LAND_FOLDER);
        public static string SeaTexturesPath => Path.Combine(AppPath, DATA_FOLDER, TEXTURES_FOLDER, TEXTURES_SEA_FOLDER);
        public static string SkyTexturesPath => Path.Combine(AppPath, DATA_FOLDER, TEXTURES_FOLDER, TEXTURES_SKY_FOLDER);

        private const string GRAPHICS_LEMMINGS_FOLDER = "lemmings";
        public const string GRAPHICS_LEMMINGS_SPRITE_FILE_NAME_BASE = "lemmings";
        private const string GRAPHICS_LEMMINGS_METAINFO_FILE_NAME = "sprites" + EXT_GENERAL;
        public static string LemmingGraphicsPath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_LEMMINGS_FOLDER);
        public static string LemmingGraphicsMetainfoFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_LEMMINGS_FOLDER, GRAPHICS_LEMMINGS_METAINFO_FILE_NAME);

        private const string GRAPHICS_UI_FOLDER = "ui";
        private const string CURSOR_FILENAME_BASE = "cursors";
        private const string FONT_FILENAME_BASE = "font";
        private const string MENU_BACKGROUND_FILENAME_BASE = "background";
        private const string POSTVIEW_LEMMINGS_FILENAME_BASE = "postview_lemming";
        private const string SIDEBAR_ICONS_FILENAME_BASE = "sidebar_icons";
        private const string SKILL_ICONS_FILENAME_BASE = "skill_icons";
        private const string TURNER_ARROWS_FILENAME_BASE = "turner_arrows";
        private const string MINIMAP_BORDER_FILENAME_BASE = "minimap_border";
        private const string MINIMAP_TOGGLE_FILENAME_BASE = "minimap_toggle";
        private const string PANELS_FILENAME = "panels";
        private const string TITLE_FILENAME = "title";
        private const string OPTION_BOX_FILENAME = "option_box";
        public static string MenuBackgroundFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, MENU_BACKGROUND_FILENAME_BASE + EXT_IMAGE);
        public static string FontFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, FONT_FILENAME_BASE + EXT_IMAGE);
        public static string FontMetainfoFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, FONT_FILENAME_BASE + EXT_GENERAL);
        public static string CursorFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, CURSOR_FILENAME_BASE + EXT_IMAGE);
        public static string CursorMetainfoFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, CURSOR_FILENAME_BASE + EXT_GENERAL);
        public static string PostviewLemmingsFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, POSTVIEW_LEMMINGS_FILENAME_BASE + EXT_IMAGE);
        public static string SidebarIconsFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, SIDEBAR_ICONS_FILENAME_BASE + EXT_IMAGE);
        public static string SkillIconsFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, SKILL_ICONS_FILENAME_BASE + EXT_IMAGE);
        public static string SkillIconsMetainfoFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, SKILL_ICONS_FILENAME_BASE + EXT_GENERAL);
        public static string TurnerArrowsFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, TURNER_ARROWS_FILENAME_BASE + EXT_IMAGE);
        public static string MinimapBorderFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, MINIMAP_BORDER_FILENAME_BASE + EXT_IMAGE);
        public static string MinimapToggleFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, MINIMAP_TOGGLE_FILENAME_BASE + EXT_IMAGE);
        public static string PanelsFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, PANELS_FILENAME + EXT_IMAGE);
        public static string TitleFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, TITLE_FILENAME + EXT_IMAGE);
        public static string OptionBoxFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_UI_FOLDER, OPTION_BOX_FILENAME + EXT_IMAGE);

        private const string GRAPHICS_GAME_FOLDER = "game";
        private const string BUILDER_BRICK_FILENAME_BASE = "builder_brick";
        private const string OVERHEAD_GRAPHICS_FILENAME_BASE = "overhead";
        private const string CAMERA_GRAPHIC_FILENAME_BASE = "camera";
        private const string SPAWN_DIRECTION_GRAPHIC_FILENAME_BASE = "spawn_direction";
        private const string CRACK_GRAPHIC_FILENAME_BASE = "destruction";
        public static string BuilderBrickTextureFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_GAME_FOLDER, BUILDER_BRICK_FILENAME_BASE + EXT_IMAGE);
        public static string OverheadsTextureFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_GAME_FOLDER, OVERHEAD_GRAPHICS_FILENAME_BASE + EXT_IMAGE);
        public static string CameraGraphicFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_GAME_FOLDER, CAMERA_GRAPHIC_FILENAME_BASE + EXT_IMAGE);
        public static string SpawnDirectionGraphicFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_GAME_FOLDER, SPAWN_DIRECTION_GRAPHIC_FILENAME_BASE + EXT_IMAGE);
        public static string CrackGraphicFilePath => Path.Combine(AppPath, DATA_FOLDER, GRAPHICS_FOLDER, GRAPHICS_GAME_FOLDER, CRACK_GRAPHIC_FILENAME_BASE + EXT_IMAGE);

        private const string BLOCKS_MODEL_FILE_NAME = "blocks" + EXT_MODEL;
        private const string BRICK_MODEL_FILE_NAME = "brick" + EXT_MODEL;
        public static string BlocksModelFilePath => Path.Combine(AppPath, DATA_FOLDER, MODELS_FOLDER, BLOCKS_MODEL_FILE_NAME);
        public static string BrickModelFilePath => Path.Combine(AppPath, DATA_FOLDER, MODELS_FOLDER, BRICK_MODEL_FILE_NAME);

        public const string TRANSLATION_FILE_NAME = "translation" + EXT_GENERAL;
    }
}
